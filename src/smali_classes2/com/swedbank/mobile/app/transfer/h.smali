.class public final Lcom/swedbank/mobile/app/transfer/h;
.super Lcom/swedbank/mobile/architect/a/d;
.source "TransferPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/o;",
        "Lcom/swedbank/mobile/app/transfer/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;

.field private final b:Lcom/swedbank/mobile/core/ui/ad;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/app/Application;

.field private final e:Lcom/swedbank/mobile/business/transfer/d;


# direct methods
.method public constructor <init>(Ljava/util/Map;Landroid/app/Application;Lcom/swedbank/mobile/business/transfer/d;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_plugins"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/transfer/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/business/transfer/d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataMappers"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/h;->c:Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/h;->d:Landroid/app/Application;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    .line 27
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/h;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 28
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/h;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/business/transfer/d;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/h;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/h;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 12

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/h;->d:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/h;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 34
    invoke-interface {v1}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v1

    .line 35
    sget-object v2, Lcom/swedbank/mobile/app/transfer/h$l;->a:Lcom/swedbank/mobile/app/transfer/h$l;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/app/transfer/k;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/k;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 37
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    .line 38
    invoke-interface {v2}, Lcom/swedbank/mobile/business/transfer/d;->b()Lio/reactivex/o;

    move-result-object v2

    .line 39
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/h;->c:Ljava/util/Map;

    .line 181
    new-instance v4, Lcom/swedbank/mobile/app/transfer/h$a;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/transfer/h$a;-><init>(Ljava/util/Map;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 182
    new-instance v3, Lcom/swedbank/mobile/app/transfer/h$b;

    invoke-direct {v3}, Lcom/swedbank/mobile/app/transfer/h$b;-><init>()V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, "map { it.toMappedData(da\u2026t, hasMoreToLoad) }\n    }"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 49
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v4}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v4

    check-cast v4, Lio/reactivex/s;

    .line 50
    sget-object v5, Lcom/swedbank/mobile/app/transfer/h$h;->a:Lcom/swedbank/mobile/app/transfer/h$h;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/transfer/h;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    sget-object v6, Lcom/swedbank/mobile/app/transfer/h$i;->a:Lcom/swedbank/mobile/app/transfer/h$i;

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v5

    check-cast v5, Lio/reactivex/s;

    .line 51
    iget-object v6, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    invoke-interface {v6}, Lcom/swedbank/mobile/business/transfer/d;->f()Lio/reactivex/o;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/app/transfer/h$j;->a:Lcom/swedbank/mobile/app/transfer/h$j;

    check-cast v7, Lio/reactivex/c/h;

    invoke-virtual {v6, v7}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v6

    check-cast v6, Lio/reactivex/s;

    .line 48
    invoke-static {v4, v5, v6}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v4

    .line 52
    new-instance v5, Lcom/swedbank/mobile/app/transfer/h$k;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/transfer/h$k;-><init>(Lcom/swedbank/mobile/app/transfer/h;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    .line 72
    sget-object v5, Lcom/swedbank/mobile/app/transfer/h$f;->a:Lcom/swedbank/mobile/app/transfer/h$f;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/transfer/h;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 73
    new-instance v6, Lcom/swedbank/mobile/app/transfer/h$g;

    iget-object v7, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/transfer/h$g;-><init>(Lcom/swedbank/mobile/business/transfer/d;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/transfer/j;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/transfer/j;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 74
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 75
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 77
    sget-object v6, Lcom/swedbank/mobile/app/transfer/h$p;->a:Lcom/swedbank/mobile/app/transfer/h$p;

    check-cast v6, Lkotlin/e/a/b;

    invoke-virtual {p0, v6}, Lcom/swedbank/mobile/app/transfer/h;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v6

    .line 78
    new-instance v7, Lcom/swedbank/mobile/app/transfer/h$q;

    invoke-direct {v7, p0}, Lcom/swedbank/mobile/app/transfer/h$q;-><init>(Lcom/swedbank/mobile/app/transfer/h;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v6, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v6

    .line 79
    invoke-virtual {v6}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v6

    .line 80
    invoke-virtual {v6}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v6

    .line 82
    sget-object v7, Lcom/swedbank/mobile/app/transfer/h$m;->a:Lcom/swedbank/mobile/app/transfer/h$m;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {p0, v7}, Lcom/swedbank/mobile/app/transfer/h;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    .line 83
    new-instance v8, Lcom/swedbank/mobile/app/transfer/h$n;

    invoke-direct {v8, p0}, Lcom/swedbank/mobile/app/transfer/h$n;-><init>(Lcom/swedbank/mobile/app/transfer/h;)V

    check-cast v8, Lio/reactivex/c/g;

    invoke-virtual {v7, v8}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v7

    .line 84
    invoke-virtual {v7}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v7

    .line 85
    invoke-virtual {v7}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v7

    .line 87
    iget-object v8, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    .line 88
    invoke-interface {v8}, Lcom/swedbank/mobile/business/transfer/d;->e()Lio/reactivex/o;

    move-result-object v8

    .line 89
    invoke-virtual {v8}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v8

    .line 90
    sget-object v9, Lcom/swedbank/mobile/app/transfer/h$e;->a:Lcom/swedbank/mobile/app/transfer/h$e;

    check-cast v9, Lio/reactivex/c/h;

    invoke-virtual {v8, v9}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v8

    .line 92
    iget-object v9, p0, Lcom/swedbank/mobile/app/transfer/h;->e:Lcom/swedbank/mobile/business/transfer/d;

    .line 93
    invoke-interface {v9}, Lcom/swedbank/mobile/business/transfer/d;->g()Lio/reactivex/o;

    move-result-object v9

    .line 94
    new-instance v10, Lcom/swedbank/mobile/app/transfer/h$o;

    invoke-direct {v10, p0, v0}, Lcom/swedbank/mobile/app/transfer/h$o;-><init>(Lcom/swedbank/mobile/app/transfer/h;Landroid/content/res/Resources;)V

    check-cast v10, Lio/reactivex/c/h;

    invoke-virtual {v9, v10}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const/16 v9, 0x8

    .line 103
    new-array v9, v9, [Lio/reactivex/s;

    .line 104
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v9, v3

    const/4 v1, 0x1

    .line 105
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v9, v1

    const/4 v1, 0x2

    .line 106
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v9, v1

    const/4 v1, 0x3

    .line 107
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v9, v1

    const/4 v1, 0x4

    .line 108
    check-cast v6, Lio/reactivex/s;

    aput-object v6, v9, v1

    const/4 v1, 0x5

    .line 109
    check-cast v7, Lio/reactivex/s;

    aput-object v7, v9, v1

    const/4 v1, 0x6

    .line 110
    check-cast v8, Lio/reactivex/s;

    aput-object v8, v9, v1

    const/4 v1, 0x7

    .line 111
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v9, v1

    .line 103
    invoke-static {v9}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026lPaymentExecutionsStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v1, Lcom/swedbank/mobile/app/transfer/s;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x7f

    const/4 v11, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v11}, Lcom/swedbank/mobile/app/transfer/s;-><init>(ZZLjava/lang/String;Lcom/swedbank/mobile/app/plugins/list/b;ZLcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/transfer/h$c;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/transfer/h;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/transfer/h$c;-><init>(Lcom/swedbank/mobile/app/transfer/h;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 184
    new-instance v3, Lcom/swedbank/mobile/app/transfer/i;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/i;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 185
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    sget-object v1, Lcom/swedbank/mobile/app/transfer/h$d;->a:Lcom/swedbank/mobile/app/transfer/h$d;

    check-cast v1, Lio/reactivex/c/d;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026, curr -> prev === curr }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
