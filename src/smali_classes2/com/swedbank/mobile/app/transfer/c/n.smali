.class public final Lcom/swedbank/mobile/app/transfer/c/n;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PaymentRequestViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/c/m;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentContainer"

    const-string v4, "getContentContainer()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "selectedAccountView"

    const-string v4, "getSelectedAccountView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "accountSpinner"

    const-string v4, "getAccountSpinner()Landroid/widget/Spinner;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "amountInput"

    const-string v4, "getAmountInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "descriptionInput"

    const-string v4, "getDescriptionInput()Landroid/widget/EditText;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "successView"

    const-string v4, "getSuccessView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "requestBtn"

    const-string v4, "getRequestBtn()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/n;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "closeBtn"

    const-string v4, "getCloseBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->m:Lio/reactivex/o;

    .line 37
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_view_root:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->c:Lkotlin/f/c;

    .line 38
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->d:Lkotlin/f/c;

    .line 39
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_account:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->e:Lkotlin/f/c;

    .line 40
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_account_spinner:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->f:Lkotlin/f/c;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_amount:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->g:Lkotlin/f/c;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_description:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->h:Lkotlin/f/c;

    .line 43
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_success_image:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->i:Lkotlin/f/c;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_action_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->j:Lkotlin/f/c;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_close_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->k:Lkotlin/f/c;

    .line 48
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<AccountId>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->l:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/EditText;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->k()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/EditText;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->p()Landroid/widget/EditText;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/c/n;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->r()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Button;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->s()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/TextView;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->i()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Spinner;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->j()Landroid/widget/Spinner;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/content/Context;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final h()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/transfer/c/n;)Lcom/b/c/c;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->l:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/view/View;
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->q()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final i()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final j()Landroid/widget/Spinner;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    return-object v0
.end method

.method private final k()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final p()Landroid/widget/EditText;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method private final q()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final r()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    return-object v0
.end method

.method private final s()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/n;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->l:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/c/q;)V
    .locals 16
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->a()Lcom/swedbank/mobile/app/transfer/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/transfer/b;->f()Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->e(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/TextView;

    move-result-object v2

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->b()Ljava/util/List;

    move-result-object v0

    .line 190
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-le v2, v6, :cond_3

    .line 191
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->f(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 192
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->f(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    instance-of v7, v2, Lcom/swedbank/mobile/app/transfer/c/a;

    if-nez v7, :cond_1

    move-object v2, v4

    :cond_1
    check-cast v2, Lcom/swedbank/mobile/app/transfer/c/a;

    if-nez v2, :cond_2

    .line 194
    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/a;

    .line 199
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->g(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/content/Context;

    move-result-object v7

    .line 201
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->h(Lcom/swedbank/mobile/app/transfer/c/n;)Lcom/b/c/c;

    move-result-object v8

    check-cast v8, Lio/reactivex/c/g;

    .line 194
    invoke-direct {v2, v7, v0, v8}, Lcom/swedbank/mobile/app/transfer/c/a;-><init>(Landroid/content/Context;Ljava/util/List;Lio/reactivex/c/g;)V

    .line 198
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->f(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/swedbank/mobile/app/transfer/c/a;->a(Landroid/widget/Spinner;)V

    goto :goto_0

    .line 204
    :cond_2
    invoke-virtual {v2, v0}, Lcom/swedbank/mobile/app/transfer/c/a;->a(Ljava/util/List;)V

    goto :goto_0

    .line 208
    :cond_3
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->f(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 89
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->c()Z

    move-result v0

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->d()Z

    move-result v2

    .line 91
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->e()Z

    move-result v7

    .line 210
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->c(Lcom/swedbank/mobile/app/transfer/c/n;)Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v8

    .line 211
    invoke-virtual {v8, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setEnabled(Z)V

    .line 212
    invoke-virtual {v8, v7}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setShimmering(Z)V

    if-eqz v2, :cond_4

    .line 214
    sget v0, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_ask_money_again:I

    goto :goto_1

    .line 215
    :cond_4
    sget v0, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_ask_money:I

    .line 213
    :goto_1
    invoke-virtual {v8, v0}, Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;->setText(I)V

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->d()Z

    move-result v0

    .line 220
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->d(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Button;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    xor-int/2addr v0, v6

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    .line 221
    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 94
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->e()Z

    move-result v0

    xor-int/2addr v0, v6

    .line 224
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->e(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->g(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/content/Context;

    move-result-object v7

    if-eqz v0, :cond_6

    .line 225
    sget v8, Lcom/swedbank/mobile/app/transfer/a$b;->brand_brown_text_icon:I

    goto :goto_3

    .line 226
    :cond_6
    sget v8, Lcom/swedbank/mobile/app/transfer/a$b;->brand_brown_text_icon_26:I

    .line 224
    :goto_3
    invoke-static {v7, v8}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 228
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->f(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/Spinner;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 229
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->a(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 230
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->b(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 95
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->d()Z

    move-result v0

    .line 232
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->i(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/view/View;

    move-result-object v2

    .line 235
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_7

    const/4 v7, 0x1

    goto :goto_4

    :cond_7
    const/4 v7, 0x0

    :goto_4
    if-eqz v7, :cond_8

    if-nez v0, :cond_9

    .line 234
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    :cond_8
    if-eqz v0, :cond_9

    const/4 v0, 0x0

    .line 237
    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    .line 238
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 239
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 244
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    .line 243
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 96
    :cond_9
    :goto_5
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/q;->f()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 252
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->g(Lcom/swedbank/mobile/app/transfer/c/n;)Landroid/content/Context;

    move-result-object v1

    .line 253
    move-object v2, v4

    check-cast v2, Ljava/lang/Integer;

    .line 256
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_b

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 258
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v6

    if-eqz v2, :cond_a

    move-object v7, v0

    check-cast v7, Ljava/lang/Iterable;

    const-string v0, "\n"

    move-object v8, v0

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3e

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 259
    :cond_a
    sget v0, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    .line 261
    :cond_b
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_c

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 262
    invoke-static {v0}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    const-string v1, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 263
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    .line 265
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-direct {v1, v4, v6, v4}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v1, Lcom/swedbank/mobile/core/ui/widget/s$c;

    move-object/from16 v2, p0

    .line 251
    invoke-virtual {v2, v0, v1}, Lcom/swedbank/mobile/app/transfer/c/n;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_7

    :cond_c
    move-object/from16 v2, p0

    .line 262
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_d
    move-object/from16 v2, p0

    .line 266
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/n;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :goto_7
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/n;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 34
    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/c/n;->a(Lcom/swedbank/mobile/app/transfer/c/q;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/c/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 62
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 63
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->k()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v0

    check-cast v0, Lio/reactivex/o;

    .line 64
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->p()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/b/b/e/a;->a(Landroid/widget/TextView;)Lcom/b/b/a;

    move-result-object v1

    check-cast v1, Lio/reactivex/o;

    .line 182
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    .line 183
    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/n$a;

    invoke-direct {v2}, Lcom/swedbank/mobile/app/transfer/c/n$a;-><init>()V

    check-cast v2, Lio/reactivex/c/c;

    .line 182
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observables.combineLates\u2026ring()\n        )\n      })"

    .line 183
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/c/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 72
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->r()Lcom/swedbank/mobile/core/ui/widget/ShimmeringButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 184
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    .line 74
    new-instance v0, Lcom/swedbank/mobile/app/transfer/c/n$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/c/n$b;-><init>(Lcom/swedbank/mobile/app/transfer/c/n;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {v1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "requestBtn\n      .clicks\u2026tring()\n        )\n      }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 9

    .line 173
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 174
    new-instance v1, Lcom/swedbank/mobile/app/transfer/c/n$c;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/c/n$c;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/o;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/c/o;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 175
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/c/n;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->o()Landroid/view/View;

    move-result-object v0

    .line 176
    new-instance v1, Lcom/swedbank/mobile/app/transfer/c/n$d;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/c/n$d;-><init>(Landroid/view/View;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/o;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/c/o;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(rootView::hideKeyboard)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->g()Landroid/view/View;

    move-result-object v1

    .line 179
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    move-object v3, v2

    check-cast v3, Lio/reactivex/o;

    .line 56
    new-instance v1, Lcom/swedbank/mobile/app/transfer/c/n$e;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/c/n$e;-><init>(Landroid/view/View;)V

    move-object v6, v1

    check-cast v6, Lkotlin/e/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 180
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void
.end method

.method public f()Lio/reactivex/o;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/n;->m:Lio/reactivex/o;

    .line 82
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->h()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 83
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/n;->s()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 185
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 83
    invoke-virtual {v0, v2}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeW\u2026geWith(closeBtn.clicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
