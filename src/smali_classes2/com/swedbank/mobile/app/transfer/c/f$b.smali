.class final synthetic Lcom/swedbank/mobile/app/transfer/c/f$b;
.super Lkotlin/e/b/i;
.source "PaymentRequestPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/c/f;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/transfer/c/q;",
        "Lcom/swedbank/mobile/app/transfer/c/q$a;",
        "Lcom/swedbank/mobile/app/transfer/c/q;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/c/f;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/c/q$a;)Lcom/swedbank/mobile/app/transfer/c/q;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/c/q$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/f$b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/transfer/c/f;

    .line 118
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/c/q$a$a;

    if-eqz v0, :cond_0

    .line 119
    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/q$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/q$a$a;->a()Lcom/swedbank/mobile/app/transfer/b;

    move-result-object v1

    .line 120
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/q$a$a;->b()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3c

    const/4 v8, 0x0

    move-object v0, p1

    .line 118
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    goto/16 :goto_0

    .line 122
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/c/q$a$g;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 123
    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/q$a$g;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/q$a$g;->a()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v1, p1

    .line 122
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    goto/16 :goto_0

    .line 125
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/c/q$a$b;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 126
    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/q$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/q$a$b;->a()Z

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x37

    const/4 v9, 0x0

    move-object v1, p1

    .line 125
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    goto :goto_0

    .line 128
    :cond_2
    sget-object v0, Lcom/swedbank/mobile/app/transfer/c/q$a$d;->a:Lcom/swedbank/mobile/app/transfer/c/q$a$d;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xb

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    goto :goto_0

    .line 133
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/transfer/c/q$a$f;->a:Lcom/swedbank/mobile/app/transfer/c/q$a$f;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x23

    const/4 v9, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    goto :goto_0

    .line 138
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/c/q$a$e;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 141
    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/q$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/q$a$e;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v7

    const/16 v8, 0xb

    const/4 v9, 0x0

    move-object v1, p1

    .line 138
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    goto :goto_0

    .line 143
    :cond_5
    sget-object v0, Lcom/swedbank/mobile/app/transfer/c/q$a$c;->a:Lcom/swedbank/mobile/app/transfer/c/q$a$c;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    const/4 v8, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/q;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/q$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/c/f$b;->a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/c/q$a;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/transfer/request/PaymentRequestViewState;Lcom/swedbank/mobile/app/transfer/request/PaymentRequestViewState$PartialState;)Lcom/swedbank/mobile/app/transfer/request/PaymentRequestViewState;"

    return-object v0
.end method
