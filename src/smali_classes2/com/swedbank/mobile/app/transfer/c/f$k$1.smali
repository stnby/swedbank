.class final Lcom/swedbank/mobile/app/transfer/c/f$k$1;
.super Ljava/lang/Object;
.source "PaymentRequestPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/c/f$k;->a(Lcom/swedbank/mobile/app/transfer/c/l;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/c/f$k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/c/f$k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/f$k$1;->a:Lcom/swedbank/mobile/app/transfer/c/f$k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/c/q$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$b;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/swedbank/mobile/app/transfer/c/q$a$f;->a:Lcom/swedbank/mobile/app/transfer/c/q$a$f;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(PartialState.QuerySuccessful)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/f$k$1;->a:Lcom/swedbank/mobile/app/transfer/c/f$k;

    iget-object v0, v0, Lcom/swedbank/mobile/app/transfer/c/f$k;->a:Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/f;->b(Lcom/swedbank/mobile/app/transfer/c/f;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v1

    .line 57
    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/q$a$e;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/transfer/c/q$a$e;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 58
    sget-object v3, Lcom/swedbank/mobile/app/transfer/c/q$a$c;->a:Lcom/swedbank/mobile/app/transfer/c/q$a$c;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 56
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/c/f$k$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
