.class public final Lcom/swedbank/mobile/app/transfer/c/a/m;
.super Ljava/lang/Object;
.source "PaymentRequestOpeningViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/transfer/c/a/m$a;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, v1, v0, v2, v0}, Lcom/swedbank/mobile/app/transfer/c/a/m;-><init>(ZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(ZLcom/swedbank/mobile/business/util/e;)V
    .locals 0
    .param p2    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->b:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method

.method public synthetic constructor <init>(ZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    const/4 p1, 0x1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    .line 7
    check-cast p2, Lcom/swedbank/mobile/business/util/e;

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/c/a/m;-><init>(ZLcom/swedbank/mobile/business/util/e;)V

    return-void
.end method


# virtual methods
.method public final a(ZLcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/transfer/c/a/m;
    .locals 1
    .param p2    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/transfer/c/a/m;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lcom/swedbank/mobile/app/transfer/c/a/m;

    invoke-direct {v0, p1, p2}, Lcom/swedbank/mobile/app/transfer/c/a/m;-><init>(ZLcom/swedbank/mobile/business/util/e;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->a:Z

    return v0
.end method

.method public final b()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->b:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/transfer/c/a/m;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/a/m;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->a:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/c/a/m;->a:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->b:Lcom/swedbank/mobile/business/util/e;

    iget-object p1, p1, Lcom/swedbank/mobile/app/transfer/c/a/m;->b:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->b:Lcom/swedbank/mobile/business/util/e;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentRequestOpeningViewState(loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/a/m;->b:Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
