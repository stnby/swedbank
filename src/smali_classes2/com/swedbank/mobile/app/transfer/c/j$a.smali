.class final Lcom/swedbank/mobile/app/transfer/c/j$a;
.super Lkotlin/e/b/k;
.source "PaymentRequestRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/c/j;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/request/a;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/app/Activity;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/c/j;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/request/a;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/c/j;Lcom/swedbank/mobile/business/transfer/request/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->a:Lcom/swedbank/mobile/app/transfer/c/j;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->b:Lcom/swedbank/mobile/business/transfer/request/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->d:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 13
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    check-cast p1, Landroid/content/Context;

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->a:Lcom/swedbank/mobile/app/transfer/c/j;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->b:Lcom/swedbank/mobile/business/transfer/request/a;

    .line 38
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->c:Ljava/lang/String;

    .line 39
    iget-object v3, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->d:Ljava/lang/String;

    .line 67
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/j;->a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;

    move-result-object v5

    sget v6, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_template_name:I

    const/4 v7, 0x1

    new-array v8, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v2, v8, v9

    invoke-virtual {v5, v6, v8}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-static {v4}, Lkotlin/j/n;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 71
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/j;->a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;

    move-result-object v2

    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_template_account:I

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/request/a;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v9

    invoke-virtual {v2, v5, v6}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    invoke-static {v4}, Lkotlin/j/n;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 73
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/j;->a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;

    move-result-object v2

    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_template_amount:I

    new-array v6, v7, [Ljava/lang/Object;

    .line 74
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v10, Ljava/math/BigDecimal;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/request/a;->b()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    invoke-static {v10, v9, v9, v11, v12}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v10, 0x20

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/request/a;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v9

    .line 73
    invoke-virtual {v2, v5, v6}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/request/a;->d()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v2, v7

    if-eqz v2, :cond_0

    .line 76
    invoke-static {v4}, Lkotlin/j/n;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 77
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/j;->a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;

    move-result-object v2

    sget v5, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_template_description:I

    new-array v6, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/transfer/request/a;->d()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v9

    invoke-virtual {v2, v5, v6}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    :cond_0
    invoke-static {v4}, Lkotlin/j/n;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 80
    invoke-static {v4}, Lkotlin/j/n;->a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 81
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/j;->a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_template_link:I

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply {\n\u2026ink, url))\n  }.toString()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/j$a;->a:Lcom/swedbank/mobile/app/transfer/c/j;

    invoke-static {v1}, Lcom/swedbank/mobile/app/transfer/c/j;->a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->payment_request_share_dialog_description:I

    invoke-virtual {v1, v2}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026share_dialog_description)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p1, v0, v1}, Lcom/swedbank/mobile/app/w/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 23
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/c/j$a;->a(Landroid/app/Activity;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
