.class public final Lcom/swedbank/mobile/app/transfer/c/a/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PaymentRequestOpeningViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/c/a/j;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorViews"

    const-string v4, "getErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitle"

    const-string v4, "getErrorTitle()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/c/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorBtn"

    const-string v4, "getErrorBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/c/a/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->g:Lio/reactivex/o;

    .line 26
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->b:Lkotlin/f/c;

    .line 27
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_opening_loading_view:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->c:Lkotlin/f/c;

    .line 28
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_opening_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->d:Lkotlin/f/c;

    .line 29
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_opening_error_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->e:Lkotlin/f/c;

    .line 30
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->payment_request_opening_error_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->f:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->f()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/widget/TextView;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/content/Context;
    .locals 0

    .line 23
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final d()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/widget/Button;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->h()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final h()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/k;->g:Lio/reactivex/o;

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->c()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeW\u2026olbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/c/a/m;)V
    .locals 16
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/a/m;->a()Z

    move-result v2

    const/4 v3, 0x0

    const/16 v4, 0x8

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :cond_0
    const/16 v2, 0x8

    .line 56
    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 39
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/transfer/c/a/m;->b()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 60
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->a(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 61
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->b(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->c(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x0

    .line 62
    check-cast v5, Ljava/lang/Integer;

    .line 65
    instance-of v5, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v5, :cond_2

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 67
    move-object v6, v5

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    xor-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_1

    move-object v7, v5

    check-cast v7, Ljava/lang/Iterable;

    const-string v2, "\n"

    move-object v8, v2

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3e

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 68
    :cond_1
    sget v5, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 70
    :cond_2
    instance-of v5, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v5, :cond_4

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Throwable;

    .line 71
    invoke-static {v5}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v5, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    const-string v5, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 72
    invoke-static {v2, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 73
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->d(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    instance-of v0, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const/16 v3, 0x8

    .line 75
    :goto_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 71
    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 77
    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->a(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 78
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->d(Lcom/swedbank/mobile/app/transfer/c/a/k;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    :goto_3
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/c/a/k;->a(Lcom/swedbank/mobile/app/transfer/c/a/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/c/a/k;->h()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 55
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
