.class final Lcom/swedbank/mobile/app/transfer/c/f$i;
.super Ljava/lang/Object;
.source "PaymentRequestPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/c/f;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/aa<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/c/f;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/c/f;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/f$i;->a:Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/c/l;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/c/l;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/app/transfer/c/q$a$g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/f$i;->a:Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/f;->a(Lcom/swedbank/mobile/app/transfer/c/f;)Lcom/swedbank/mobile/business/transfer/request/b;

    move-result-object v0

    .line 42
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/c/l;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/c/l;->b()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/request/b;->a(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 43
    sget-object v0, Lcom/swedbank/mobile/app/transfer/c/f$i$1;->a:Lcom/swedbank/mobile/app/transfer/c/f$i$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/c/f$i;->a(Lcom/swedbank/mobile/app/transfer/c/l;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
