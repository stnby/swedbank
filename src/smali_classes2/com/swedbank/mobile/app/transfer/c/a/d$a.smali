.class final synthetic Lcom/swedbank/mobile/app/transfer/c/a/d$a;
.super Lkotlin/e/b/i;
.source "PaymentRequestOpeningPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/c/a/d;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/transfer/c/a/m;",
        "Lcom/swedbank/mobile/app/transfer/c/a/m$a;",
        "Lcom/swedbank/mobile/app/transfer/c/a/m;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/c/a/d;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/c/a/m;Lcom/swedbank/mobile/app/transfer/c/a/m$a;)Lcom/swedbank/mobile/app/transfer/c/a/m;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/c/a/m$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/d$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/transfer/c/a/d;

    .line 52
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/c/a/m$a$b;

    if-eqz v0, :cond_0

    .line 53
    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/a/m$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/a/m$a$b;->a()Z

    move-result p2

    const/4 v0, 0x0

    .line 52
    invoke-virtual {p1, p2, v0}, Lcom/swedbank/mobile/app/transfer/c/a/m;->a(ZLcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/transfer/c/a/m;

    move-result-object p1

    goto :goto_0

    .line 55
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/c/a/m$a$a;

    if-eqz v0, :cond_1

    .line 56
    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/a/m$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/c/a/m$a$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p2

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p1, v0, p2}, Lcom/swedbank/mobile/app/transfer/c/a/m;->a(ZLcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/transfer/c/a/m;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/a/m;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/c/a/m$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/c/a/d$a;->a(Lcom/swedbank/mobile/app/transfer/c/a/m;Lcom/swedbank/mobile/app/transfer/c/a/m$a;)Lcom/swedbank/mobile/app/transfer/c/a/m;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/c/a/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/transfer/request/opening/PaymentRequestOpeningViewState;Lcom/swedbank/mobile/app/transfer/request/opening/PaymentRequestOpeningViewState$PartialState;)Lcom/swedbank/mobile/app/transfer/request/opening/PaymentRequestOpeningViewState;"

    return-object v0
.end method
