.class public final Lcom/swedbank/mobile/app/transfer/c/q;
.super Ljava/lang/Object;
.source "PaymentRequestViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/transfer/c/q$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/app/transfer/b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Z

.field private final d:Z

.field private final e:Z

.field private final f:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3f

    const/4 v8, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/transfer/c/q;-><init>(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/b;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;ZZZ",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "accounts"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    iput-boolean p4, p0, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    iput-boolean p5, p0, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    iput-object p6, p0, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V
    .locals 4

    and-int/lit8 p8, p7, 0x1

    const/4 v0, 0x0

    if-eqz p8, :cond_0

    .line 11
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/transfer/b;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    .line 12
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p2

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move v2, p3

    :goto_0
    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    move v3, p4

    :goto_1
    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    goto :goto_2

    :cond_4
    move v1, p5

    :goto_2
    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    .line 16
    move-object p6, v0

    check-cast p6, Lcom/swedbank/mobile/business/util/e;

    :cond_5
    move-object v0, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v2

    move p6, v3

    move p7, v1

    move-object p8, v0

    invoke-direct/range {p2 .. p8}, Lcom/swedbank/mobile/app/transfer/c/q;-><init>(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/transfer/c/q;Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/c/q;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/swedbank/mobile/app/transfer/c/q;->a(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/transfer/c/q;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/app/transfer/b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/app/transfer/c/q;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/transfer/b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/b;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;ZZZ",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/transfer/c/q;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "accounts"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/app/transfer/c/q;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/app/transfer/c/q;-><init>(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;)V

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/transfer/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 13
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_4

    instance-of v1, p1, Lcom/swedbank/mobile/app/transfer/c/q;

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/q;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    iget-object v3, p1, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    if-ne v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    iget-object p1, p1, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    return v2

    :cond_4
    :goto_3
    return v0
.end method

.method public final f()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaymentRequestViewState(selectedAccount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->a:Lcom/swedbank/mobile/app/transfer/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", submissionEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", closeVisible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", loading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", queryError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/q;->f:Lcom/swedbank/mobile/business/util/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
