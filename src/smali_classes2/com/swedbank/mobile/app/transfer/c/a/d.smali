.class public final Lcom/swedbank/mobile/app/transfer/c/a/d;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PaymentRequestOpeningPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/c/a/j;",
        "Lcom/swedbank/mobile/app/transfer/c/a/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/transfer/request/opening/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/request/opening/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/request/opening/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/a/d;->a:Lcom/swedbank/mobile/business/transfer/request/opening/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/c/a/d;)Lcom/swedbank/mobile/business/transfer/request/opening/d;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/c/a/d;->a:Lcom/swedbank/mobile/business/transfer/request/opening/d;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/a/d;->a:Lcom/swedbank/mobile/business/transfer/request/opening/d;

    .line 18
    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/request/opening/d;->b()Lio/reactivex/o;

    move-result-object v0

    .line 19
    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/d$d;->a:Lcom/swedbank/mobile/app/transfer/c/a/d$d;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/a/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/c/a/f;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/a/d$b;->a:Lcom/swedbank/mobile/app/transfer/c/a/d$b;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/transfer/c/a/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 22
    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/a/d$c;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/transfer/c/a/d$c;-><init>(Lcom/swedbank/mobile/app/transfer/c/a/d;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 26
    sget-object v2, Lcom/swedbank/mobile/app/transfer/c/a/d$e;->a:Lcom/swedbank/mobile/app/transfer/c/a/d$e;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/transfer/c/a/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 27
    new-instance v3, Lcom/swedbank/mobile/app/transfer/c/a/d$f;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/transfer/c/a/d$f;-><init>(Lcom/swedbank/mobile/app/transfer/c/a/d;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 33
    check-cast v0, Lio/reactivex/s;

    .line 34
    check-cast v1, Lio/reactivex/s;

    .line 35
    check-cast v2, Lio/reactivex/s;

    .line 32
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026eam,\n        retryStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v1, Lcom/swedbank/mobile/app/transfer/c/a/m;

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3, v4}, Lcom/swedbank/mobile/app/transfer/c/a/m;-><init>(ZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/a/d$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/transfer/c/a/d;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/transfer/c/a/d$a;-><init>(Lcom/swedbank/mobile/app/transfer/c/a/d;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 51
    new-instance v3, Lcom/swedbank/mobile/app/transfer/c/a/e;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/c/a/e;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 52
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
