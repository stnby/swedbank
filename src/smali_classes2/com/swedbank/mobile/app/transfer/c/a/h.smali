.class public final Lcom/swedbank/mobile/app/transfer/c/a/h;
.super Lcom/swedbank/mobile/architect/a/h;
.source "PaymentRequestOpeningRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/request/opening/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_request_opening"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_request_opening"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0, p1, p3, p2}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    return-void
.end method
