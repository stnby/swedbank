.class public final Lcom/swedbank/mobile/app/transfer/c/f;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PaymentRequestPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/c/m;",
        "Lcom/swedbank/mobile/app/transfer/c/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/ad;

.field private final b:Lcom/swedbank/mobile/business/transfer/request/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/transfer/request/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/request/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/f;->b:Lcom/swedbank/mobile/business/transfer/request/b;

    .line 20
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/f;->a:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/c/f;)Lcom/swedbank/mobile/business/transfer/request/b;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/c/f;->b:Lcom/swedbank/mobile/business/transfer/request/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/c/f;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/c/f;->a:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 11

    .line 24
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/f;->b:Lcom/swedbank/mobile/business/transfer/request/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/transfer/request/b;->b()Lio/reactivex/o;

    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/c/f;->b:Lcom/swedbank/mobile/business/transfer/request/b;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/transfer/request/b;->a()Lio/reactivex/o;

    move-result-object v1

    .line 117
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    .line 118
    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/f$a;

    invoke-direct {v2}, Lcom/swedbank/mobile/app/transfer/c/f$a;-><init>()V

    check-cast v2, Lio/reactivex/c/c;

    .line 117
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/f$e;->a:Lcom/swedbank/mobile/app/transfer/c/f$e;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 39
    sget-object v1, Lcom/swedbank/mobile/app/transfer/c/f$h;->a:Lcom/swedbank/mobile/app/transfer/c/f$h;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/transfer/c/f;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 40
    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/f$i;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/transfer/c/f$i;-><init>(Lcom/swedbank/mobile/app/transfer/c/f;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->o(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "action(PaymentRequestVie\u2026onEnabled(it) }\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v2, Lcom/swedbank/mobile/app/transfer/c/f$j;->a:Lcom/swedbank/mobile/app/transfer/c/f$j;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/transfer/c/f;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 47
    new-instance v3, Lcom/swedbank/mobile/app/transfer/c/f$k;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/transfer/c/f$k;-><init>(Lcom/swedbank/mobile/app/transfer/c/f;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 64
    sget-object v3, Lcom/swedbank/mobile/app/transfer/c/f$c;->a:Lcom/swedbank/mobile/app/transfer/c/f$c;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/transfer/c/f;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 65
    new-instance v4, Lcom/swedbank/mobile/app/transfer/c/f$d;

    iget-object v5, p0, Lcom/swedbank/mobile/app/transfer/c/f;->b:Lcom/swedbank/mobile/business/transfer/request/b;

    invoke-direct {v4, v5}, Lcom/swedbank/mobile/app/transfer/c/f$d;-><init>(Lcom/swedbank/mobile/business/transfer/request/b;)V

    check-cast v4, Lkotlin/e/a/b;

    new-instance v5, Lcom/swedbank/mobile/app/transfer/c/h;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/transfer/c/h;-><init>(Lkotlin/e/a/b;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v3, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 66
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 67
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 69
    sget-object v4, Lcom/swedbank/mobile/app/transfer/c/f$f;->a:Lcom/swedbank/mobile/app/transfer/c/f$f;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/transfer/c/f;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 70
    new-instance v5, Lcom/swedbank/mobile/app/transfer/c/f$g;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/transfer/c/f$g;-><init>(Lcom/swedbank/mobile/app/transfer/c/f;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 71
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 72
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    const/4 v5, 0x5

    .line 74
    new-array v5, v5, [Lio/reactivex/s;

    .line 75
    check-cast v0, Lio/reactivex/s;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    .line 76
    check-cast v1, Lio/reactivex/s;

    const/4 v0, 0x1

    aput-object v1, v5, v0

    .line 77
    check-cast v2, Lio/reactivex/s;

    const/4 v0, 0x2

    aput-object v2, v5, v0

    .line 78
    check-cast v3, Lio/reactivex/s;

    const/4 v0, 0x3

    aput-object v3, v5, v0

    .line 79
    check-cast v4, Lio/reactivex/s;

    const/4 v0, 0x4

    aput-object v4, v5, v0

    .line 74
    invoke-static {v5}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026am,\n        finishStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance v1, Lcom/swedbank/mobile/app/transfer/c/q;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3f

    const/4 v10, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v10}, Lcom/swedbank/mobile/app/transfer/c/q;-><init>(Lcom/swedbank/mobile/app/transfer/b;Ljava/util/List;ZZZLcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/transfer/c/f$b;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/transfer/c/f$b;-><init>(Lcom/swedbank/mobile/app/transfer/c/f;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 119
    new-instance v3, Lcom/swedbank/mobile/app/transfer/c/g;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/c/g;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 120
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
