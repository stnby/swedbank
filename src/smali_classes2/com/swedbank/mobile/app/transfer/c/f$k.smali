.class final Lcom/swedbank/mobile/app/transfer/c/f$k;
.super Ljava/lang/Object;
.source "PaymentRequestPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/c/f;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/c/f;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/c/f;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/f$k;->a:Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/c/l;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/transfer/c/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/transfer/c/l;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/c/q$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/f$k;->a:Lcom/swedbank/mobile/app/transfer/c/f;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/c/f;->a(Lcom/swedbank/mobile/app/transfer/c/f;)Lcom/swedbank/mobile/business/transfer/request/b;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/c/l;->a()Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/c/l;->b()Ljava/lang/String;

    move-result-object p1

    .line 49
    invoke-interface {v0, v1, p1}, Lcom/swedbank/mobile/business/transfer/request/b;->b(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object p1

    .line 53
    new-instance v0, Lcom/swedbank/mobile/app/transfer/c/f$k$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/transfer/c/f$k$1;-><init>(Lcom/swedbank/mobile/app/transfer/c/f$k;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 61
    sget-object v0, Lcom/swedbank/mobile/app/transfer/c/q$a$d;->a:Lcom/swedbank/mobile/app/transfer/c/q$a$d;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/transfer/c/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/c/f$k;->a(Lcom/swedbank/mobile/app/transfer/c/l;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
