.class public final Lcom/swedbank/mobile/app/transfer/c/j;
.super Lcom/swedbank/mobile/architect/a/h;
.source "PaymentRequestRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/transfer/request/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/data/device/a;

.field private final f:Landroid/app/Application;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;Landroid/app/Application;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_request"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_payment_request"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/device/a;",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p3, p5, p4}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/c/j;->e:Lcom/swedbank/mobile/data/device/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/c/j;->f:Landroid/app/Application;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/c/j;)Landroid/app/Application;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/c/j;->f:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/swedbank/mobile/business/transfer/request/a;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/request/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "customerName"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "creationInput"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "url"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/c/j;->e:Lcom/swedbank/mobile/data/device/a;

    .line 35
    new-instance v1, Lcom/swedbank/mobile/app/transfer/c/j$a;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/swedbank/mobile/app/transfer/c/j$a;-><init>(Lcom/swedbank/mobile/app/transfer/c/j;Lcom/swedbank/mobile/business/transfer/request/a;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {v0, p1, v1, p2, p1}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    return-void
.end method
