.class public final Lcom/swedbank/mobile/app/transfer/m$a;
.super Lkotlin/e/b/k;
.source "TransferRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/transfer/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/architect/a/h;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/architect/a/h;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/a/h;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/m$a;->a:Lcom/swedbank/mobile/architect/a/h;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/architect/a/h;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/a/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/payment/form/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m$a;->a:Lcom/swedbank/mobile/architect/a/h;

    if-ne p1, v0, :cond_2

    .line 137
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/search/h;

    if-nez v0, :cond_2

    .line 138
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/detailed/d;

    if-nez v0, :cond_2

    .line 139
    instance-of v0, p1, Lcom/swedbank/mobile/business/transfer/request/f;

    if-nez v0, :cond_2

    .line 140
    instance-of p1, p1, Lcom/swedbank/mobile/business/transfer/request/opening/h;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/swedbank/mobile/architect/a/h;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/m$a;->a(Lcom/swedbank/mobile/architect/a/h;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
