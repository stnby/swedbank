.class public final Lcom/swedbank/mobile/app/transfer/a/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "TransferDetailedViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/transfer/a/j;
.implements Lcom/swedbank/mobile/architect/a/b/i;
.implements Lcom/swedbank/mobile/core/ui/ao;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/b/a;",
        "Lcom/swedbank/mobile/app/transfer/a/j;",
        "Lcom/swedbank/mobile/architect/a/b/i<",
        "Lcom/swedbank/mobile/app/transfer/a/n;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/ao<",
        "Lcom/swedbank/mobile/app/transfer/a/n;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/widget/u;"
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lcom/swedbank/mobile/core/ui/aj;

.field private final g:I

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final i:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/transfer/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "transferList"

    const-string v4, "getTransferList()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/transfer/a/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/core/ui/aj;ILjava/util/List;Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/aj;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Ljavax/inject/Named;
            value = "transfer_detailed_title_res"
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/aj;",
            "I",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transitionAwareRenderer"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rendererProviders"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->f:Lcom/swedbank/mobile/core/ui/aj;

    iput p2, p0, Lcom/swedbank/mobile/app/transfer/a/k;->g:I

    iput-object p3, p0, Lcom/swedbank/mobile/app/transfer/a/k;->h:Ljava/util/List;

    iput-object p4, p0, Lcom/swedbank/mobile/app/transfer/a/k;->i:Lio/reactivex/o;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->c:Lkotlin/f/c;

    .line 43
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_detailed_refresh:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->d:Lkotlin/f/c;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/transfer/a$e;->transfer_detailed_list:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->e:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/a/k;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/a/k;)Landroid/content/Context;
    .locals 0

    .line 33
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/transfer/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    .line 65
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "refreshView.refreshes()\n\u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/transfer/a/n;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/app/transfer/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/a/n;->b()Lcom/swedbank/mobile/app/plugins/list/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/a/n;->a()Z

    move-result v0

    .line 103
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->a(Lcom/swedbank/mobile/app/transfer/a/k;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 73
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/a/n;->c()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/a/n;->d()Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    if-eqz v0, :cond_4

    .line 112
    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->b(Lcom/swedbank/mobile/app/transfer/a/k;)Landroid/content/Context;

    move-result-object v1

    .line 113
    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->error_general_error:I

    .line 116
    instance-of v3, v0, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v3, :cond_2

    check-cast v0, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 118
    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    move-object v4, v0

    check-cast v4, Ljava/lang/Iterable;

    const-string v0, "\n"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 119
    :cond_1
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :cond_2
    instance-of v2, v0, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_3

    check-cast v0, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 122
    invoke-static {v0}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const-string v1, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 123
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    goto :goto_2

    .line 122
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    if-eqz p1, :cond_5

    .line 127
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p1

    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->b(Lcom/swedbank/mobile/app/transfer/a/k;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_5
    const/4 p1, 0x0

    :goto_1
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    :goto_2
    if-eqz v0, :cond_6

    .line 131
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-static {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->b(Lcom/swedbank/mobile/app/transfer/a/k;)Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/transfer/a$h;->retry_btn:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {p1, v1}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 129
    invoke-virtual {p0, v0, p1}, Lcom/swedbank/mobile/app/transfer/a/k;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_3

    .line 132
    :cond_6
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :goto_3
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/swedbank/mobile/app/transfer/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/a/k;->b(Lcom/swedbank/mobile/app/transfer/a/n;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->i:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(Lcom/swedbank/mobile/app/transfer/a/n;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/transfer/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->f:Lcom/swedbank/mobile/core/ui/aj;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/core/ui/aj;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/swedbank/mobile/app/transfer/a/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/a/k;->a(Lcom/swedbank/mobile/app/transfer/a/n;)V

    return-void
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 68
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 5

    .line 100
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 101
    new-instance v1, Lcom/swedbank/mobile/app/transfer/a/k$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/transfer/a/k$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/transfer/a/l;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/a/l;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 102
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/transfer/a/k;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/k;->f:Lcom/swedbank/mobile/core/ui/aj;

    .line 51
    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/architect/a/b/a;

    const/4 v2, 0x1

    .line 50
    invoke-interface {v0, v1, v2}, Lcom/swedbank/mobile/core/ui/aj;->a(Lcom/swedbank/mobile/architect/a/b/a;Z)V

    .line 53
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->h:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Ljava/util/List;)V

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    .line 55
    iget v1, p0, Lcom/swedbank/mobile/app/transfer/a/k;->g:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 56
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->h()Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 58
    invoke-direct {p0}, Lcom/swedbank/mobile/app/transfer/a/k;->g()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x2

    .line 59
    new-array v1, v1, [I

    sget v3, Lcom/swedbank/mobile/app/transfer/a$b;->brand_orange:I

    const/4 v4, 0x0

    aput v3, v1, v4

    sget v3, Lcom/swedbank/mobile/app/transfer/a$b;->brand_turqoise:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 60
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewStartOffset()I

    move-result v1

    int-to-float v1, v1

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float v1, v1, v3

    float-to-int v1, v1

    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v4

    int-to-float v4, v4

    mul-float v4, v4, v3

    float-to-int v3, v4

    invoke-virtual {v0, v2, v1, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZII)V

    return-void
.end method
