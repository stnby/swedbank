.class public final Lcom/swedbank/mobile/app/transfer/a/a;
.super Ljava/lang/Object;
.source "TransferDetailedBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Lcom/swedbank/mobile/app/plugins/list/c;

.field private c:Lcom/swedbank/mobile/business/transfer/detailed/c;

.field private final d:Lcom/swedbank/mobile/a/ac/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/ac/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/ac/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/a;->d:Lcom/swedbank/mobile/a/ac/a/a$a;

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/swedbank/mobile/app/transfer/a/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/a/a;

    .line 19
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/a/a;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/app/transfer/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/plugins/list/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "pluginDetails"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/a/a;

    .line 23
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/a/a;->b:Lcom/swedbank/mobile/app/plugins/list/c;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/app/transfer/a/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/transfer/detailed/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/transfer/a/a;

    .line 27
    iput-object p1, v0, Lcom/swedbank/mobile/app/transfer/a/a;->c:Lcom/swedbank/mobile/business/transfer/detailed/c;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/a;->d:Lcom/swedbank/mobile/a/ac/a/a$a;

    .line 31
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/a/a;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/a/a$a;->b(I)Lcom/swedbank/mobile/a/ac/a/a$a;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/a/a;->b:Lcom/swedbank/mobile/app/plugins/list/c;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/a/a$a;->b(Lcom/swedbank/mobile/app/plugins/list/c;)Lcom/swedbank/mobile/a/ac/a/a$a;

    move-result-object v0

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/a/a;->c:Lcom/swedbank/mobile/business/transfer/detailed/c;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/ac/a/a$a;->b(Lcom/swedbank/mobile/business/transfer/detailed/c;)Lcom/swedbank/mobile/a/ac/a/a$a;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/a/a$a;->a()Lcom/swedbank/mobile/a/ac/a/a;

    move-result-object v0

    .line 41
    invoke-interface {v0}, Lcom/swedbank/mobile/a/ac/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 37
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot build transfer detailed node without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 34
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot build transfer detailed node without plugin details"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 31
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot build transfer detailed node without title res"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
