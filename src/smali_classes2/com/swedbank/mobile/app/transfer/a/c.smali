.class public final Lcom/swedbank/mobile/app/transfer/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "TransferDetailedPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/transfer/a/j;",
        "Lcom/swedbank/mobile/app/transfer/a/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;

.field private final b:Lcom/swedbank/mobile/core/ui/ad;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/swedbank/mobile/business/transfer/detailed/a;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/swedbank/mobile/business/transfer/detailed/a;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation runtime Ljavax/inject/Named;
            value = "for_transfer_detailed"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/transfer/detailed/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lkotlin/e/a/b<",
            "Ljava/lang/Object;",
            "Lcom/swedbank/mobile/app/plugins/list/g;",
            ">;>;",
            "Lcom/swedbank/mobile/business/transfer/detailed/a;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dataMappers"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/c;->c:Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/a/c;->d:Lcom/swedbank/mobile/business/transfer/detailed/a;

    .line 25
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/c;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 26
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/c;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/transfer/a/c;)Lcom/swedbank/mobile/business/transfer/detailed/a;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/a/c;->d:Lcom/swedbank/mobile/business/transfer/detailed/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/transfer/a/c;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/a/c;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/transfer/a/c;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/transfer/a/c;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/c;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v0

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/transfer/a/c$k;->a:Lcom/swedbank/mobile/app/transfer/a/c$k;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/transfer/a/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/transfer/a/f;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/swedbank/mobile/app/transfer/a/c;->d:Lcom/swedbank/mobile/business/transfer/detailed/a;

    .line 34
    invoke-interface {v1}, Lcom/swedbank/mobile/business/transfer/detailed/a;->c()Lio/reactivex/o;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/swedbank/mobile/app/transfer/a/c;->c:Ljava/util/Map;

    .line 122
    new-instance v3, Lcom/swedbank/mobile/app/transfer/a/c$a;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/a/c$a;-><init>(Ljava/util/Map;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 123
    new-instance v2, Lcom/swedbank/mobile/app/transfer/a/c$b;

    invoke-direct {v2}, Lcom/swedbank/mobile/app/transfer/a/c$b;-><init>()V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "map { it.toMappedData(da\u2026t, hasMoreToLoad) }\n    }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 45
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 46
    sget-object v4, Lcom/swedbank/mobile/app/transfer/a/c$h;->a:Lcom/swedbank/mobile/app/transfer/a/c$h;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/transfer/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    sget-object v5, Lcom/swedbank/mobile/app/transfer/a/c$i;->a:Lcom/swedbank/mobile/app/transfer/a/c$i;

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    check-cast v4, Lio/reactivex/s;

    .line 44
    invoke-static {v3, v4}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v3

    .line 47
    new-instance v4, Lcom/swedbank/mobile/app/transfer/a/c$j;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/transfer/a/c$j;-><init>(Lcom/swedbank/mobile/app/transfer/a/c;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    .line 65
    sget-object v4, Lcom/swedbank/mobile/app/transfer/a/c$d;->a:Lcom/swedbank/mobile/app/transfer/a/c$d;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/transfer/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 66
    new-instance v5, Lcom/swedbank/mobile/app/transfer/a/c$e;

    iget-object v6, p0, Lcom/swedbank/mobile/app/transfer/a/c;->d:Lcom/swedbank/mobile/business/transfer/detailed/a;

    invoke-direct {v5, v6}, Lcom/swedbank/mobile/app/transfer/a/c$e;-><init>(Lcom/swedbank/mobile/business/transfer/detailed/a;)V

    check-cast v5, Lkotlin/e/a/b;

    new-instance v6, Lcom/swedbank/mobile/app/transfer/a/e;

    invoke-direct {v6, v5}, Lcom/swedbank/mobile/app/transfer/a/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v4, v6}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 67
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 68
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    .line 70
    sget-object v5, Lcom/swedbank/mobile/app/transfer/a/c$f;->a:Lcom/swedbank/mobile/app/transfer/a/c$f;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/transfer/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 71
    new-instance v6, Lcom/swedbank/mobile/app/transfer/a/c$g;

    invoke-direct {v6, p0}, Lcom/swedbank/mobile/app/transfer/a/c$g;-><init>(Lcom/swedbank/mobile/app/transfer/a/c;)V

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 72
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 73
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    const/4 v6, 0x5

    .line 75
    new-array v6, v6, [Lio/reactivex/s;

    .line 76
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v6, v2

    const/4 v0, 0x1

    .line 77
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v6, v0

    const/4 v0, 0x2

    .line 78
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v6, v0

    const/4 v0, 0x3

    .line 79
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v6, v0

    const/4 v0, 0x4

    .line 80
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v6, v0

    .line 75
    invoke-static {v6}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026m,\n        actionsStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v1, Lcom/swedbank/mobile/app/transfer/a/n;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/transfer/a/n;-><init>(ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/transfer/a/c$c;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/transfer/a/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/transfer/a/c$c;-><init>(Lcom/swedbank/mobile/app/transfer/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 125
    new-instance v3, Lcom/swedbank/mobile/app/transfer/a/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/transfer/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 126
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
