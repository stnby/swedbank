.class final Lcom/swedbank/mobile/app/transfer/a/c$j$1;
.super Ljava/lang/Object;
.source "TransferDetailedPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/a/c$j;->a(Ljava/lang/Boolean;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/a/c$j;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/a/c$j;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/c$j$1;->a:Lcom/swedbank/mobile/app/transfer/a/c$j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/s;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/s<",
            "+",
            "Lcom/swedbank/mobile/app/transfer/a/n$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/a/c$j$1;->a:Lcom/swedbank/mobile/app/transfer/a/c$j;

    iget-object p1, p1, Lcom/swedbank/mobile/app/transfer/a/c$j;->a:Lcom/swedbank/mobile/app/transfer/a/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/a/c;->b(Lcom/swedbank/mobile/app/transfer/a/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    goto :goto_0

    .line 54
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/c$j$1;->a:Lcom/swedbank/mobile/app/transfer/a/c$j;

    iget-object v0, v0, Lcom/swedbank/mobile/app/transfer/a/c$j;->a:Lcom/swedbank/mobile/app/transfer/a/c;

    .line 55
    new-instance v2, Lcom/swedbank/mobile/app/transfer/a/n$a$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/transfer/a/n$a$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object v4, v2

    check-cast v4, Lcom/swedbank/mobile/app/transfer/a/n$a;

    .line 122
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/a/c;->c(Lcom/swedbank/mobile/app/transfer/a/c;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v3

    .line 127
    sget-object v5, Lcom/swedbank/mobile/app/transfer/a/n$a$c;->a:Lcom/swedbank/mobile/app/transfer/a/n$a$c;

    const-wide/16 v6, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    .line 122
    invoke-static/range {v3 .. v9}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 125
    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/a/c;->b(Lcom/swedbank/mobile/app/transfer/a/c;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/ui/x;->d(Z)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "messageRenderStream.disp\u2026ream.forceLoading(false))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/s;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/a/c$j$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/s;

    move-result-object p1

    return-object p1
.end method
