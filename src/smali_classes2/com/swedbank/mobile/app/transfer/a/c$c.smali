.class final synthetic Lcom/swedbank/mobile/app/transfer/a/c$c;
.super Lkotlin/e/b/i;
.source "TransferDetailedPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/transfer/a/n;",
        "Lcom/swedbank/mobile/app/transfer/a/n$a;",
        "Lcom/swedbank/mobile/app/transfer/a/n;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/a/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/transfer/a/n;Lcom/swedbank/mobile/app/transfer/a/n$a;)Lcom/swedbank/mobile/app/transfer/a/n;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/transfer/a/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/transfer/a/n$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/a/c$c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/transfer/a/c;

    .line 123
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/a/n$a$e;

    if-eqz v0, :cond_1

    .line 124
    check-cast p2, Lcom/swedbank/mobile/app/transfer/a/n$a$e;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/a/n$a$e;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/transfer/a/n;->a(Lcom/swedbank/mobile/app/transfer/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    goto/16 :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p1

    .line 128
    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/transfer/a/n;->a(Lcom/swedbank/mobile/app/transfer/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    goto :goto_0

    .line 131
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/a/n$a$a;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    .line 132
    new-instance v3, Lcom/swedbank/mobile/app/plugins/list/b;

    .line 133
    check-cast p2, Lcom/swedbank/mobile/app/transfer/a/n$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/a/n$a$a;->a()Ljava/util/List;

    move-result-object v0

    .line 134
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/a/n$a$a;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object v1

    .line 135
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/a/n$a$a;->c()Z

    move-result p2

    .line 132
    invoke-direct {v3, v0, v1, p2}, Lcom/swedbank/mobile/app/plugins/list/b;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;Z)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    move-object v1, p1

    .line 131
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/transfer/a/n;->a(Lcom/swedbank/mobile/app/transfer/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    goto :goto_0

    .line 136
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/a/n$a$b;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 137
    check-cast p2, Lcom/swedbank/mobile/app/transfer/a/n$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/a/n$a$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object v1, p1

    .line 136
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/transfer/a/n;->a(Lcom/swedbank/mobile/app/transfer/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    goto :goto_0

    .line 138
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/transfer/a/n$a$c;->a:Lcom/swedbank/mobile/app/transfer/a/n$a$c;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/transfer/a/n;->a(Lcom/swedbank/mobile/app/transfer/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    goto :goto_0

    .line 141
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/transfer/a/n$a$d;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 142
    check-cast p2, Lcom/swedbank/mobile/app/transfer/a/n$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/transfer/a/n$a$d;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v5

    const/4 v4, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object v1, p1

    .line 141
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/transfer/a/n;->a(Lcom/swedbank/mobile/app/transfer/a/n;ZLcom/swedbank/mobile/app/plugins/list/b;Lcom/swedbank/mobile/business/util/e;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/app/transfer/a/n;

    check-cast p2, Lcom/swedbank/mobile/app/transfer/a/n$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/transfer/a/c$c;->a(Lcom/swedbank/mobile/app/transfer/a/n;Lcom/swedbank/mobile/app/transfer/a/n$a;)Lcom/swedbank/mobile/app/transfer/a/n;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/transfer/a/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/transfer/detailed/TransferDetailedViewState;Lcom/swedbank/mobile/app/transfer/detailed/TransferDetailedViewState$PartialState;)Lcom/swedbank/mobile/app/transfer/detailed/TransferDetailedViewState;"

    return-object v0
.end method
