.class final Lcom/swedbank/mobile/app/transfer/h$o;
.super Ljava/lang/Object;
.source "TransferPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/h;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/h;

.field final synthetic b:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/h;Landroid/content/res/Resources;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/h$o;->a:Lcom/swedbank/mobile/app/transfer/h;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/h$o;->b:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/o;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/transfer/s$a$h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/h$o;->a:Lcom/swedbank/mobile/app/transfer/h;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/h;->c(Lcom/swedbank/mobile/app/transfer/h;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v1

    .line 96
    new-instance v2, Lcom/swedbank/mobile/app/transfer/s$a$h;

    .line 97
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/h$o;->b:Landroid/content/res/Resources;

    sget v3, Lcom/swedbank/mobile/app/transfer/a$h;->payment_successs_snackbar_title:I

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 98
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->f()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->c()Ljava/math/BigDecimal;

    move-result-object v5

    const/4 v6, 0x1

    aput-object v5, v4, v6

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;->d()Ljava/lang/String;

    move-result-object p1

    const/4 v5, 0x2

    aput-object p1, v4, v5

    .line 97
    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 96
    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/transfer/s$a$h;-><init>(Ljava/lang/String;)V

    .line 99
    new-instance v3, Lcom/swedbank/mobile/app/transfer/s$a$h;

    const/4 p1, 0x0

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/transfer/s$a$h;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 95
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/h$o;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
