.class final Lcom/swedbank/mobile/app/transfer/m$h;
.super Lkotlin/e/b/k;
.source "TransferRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/transfer/m;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/transfer/m;

.field final synthetic b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/transfer/m;Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/transfer/m$h;->a:Lcom/swedbank/mobile/app/transfer/m;

    iput-object p2, p0, Lcom/swedbank/mobile/app/transfer/m$h;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/transfer/m$h;->c:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object p1, p0, Lcom/swedbank/mobile/app/transfer/m$h;->a:Lcom/swedbank/mobile/app/transfer/m;

    invoke-static {p1}, Lcom/swedbank/mobile/app/transfer/m;->a(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/app/transfer/payment/form/j;

    move-result-object p1

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m$h;->a:Lcom/swedbank/mobile/app/transfer/m;

    invoke-static {v0}, Lcom/swedbank/mobile/app/transfer/m;->b(Lcom/swedbank/mobile/app/transfer/m;)Lcom/swedbank/mobile/business/transfer/payment/form/g;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a(Lcom/swedbank/mobile/business/transfer/payment/form/g;)Lcom/swedbank/mobile/app/transfer/payment/form/j;

    move-result-object p1

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m$h;->b:Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a(Lcom/swedbank/mobile/business/transfer/payment/PaymentInput;)Lcom/swedbank/mobile/app/transfer/payment/form/j;

    move-result-object p1

    .line 62
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/transfer/m$h;->c:Z

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a(Z)Lcom/swedbank/mobile/app/transfer/payment/form/j;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/transfer/payment/form/j;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 64
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m$h;->a:Lcom/swedbank/mobile/app/transfer/m;

    .line 144
    invoke-static {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/app/transfer/m$h;->a:Lcom/swedbank/mobile/app/transfer/m;

    .line 145
    new-instance v1, Lcom/swedbank/mobile/app/transfer/m$b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/transfer/m$b;-><init>(Lcom/swedbank/mobile/architect/a/h;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/transfer/m;->a(Lcom/swedbank/mobile/app/transfer/m;Lkotlin/e/a/b;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/transfer/m$h;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
