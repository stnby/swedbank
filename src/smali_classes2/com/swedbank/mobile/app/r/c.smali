.class public final Lcom/swedbank/mobile/app/r/c;
.super Lcom/swedbank/mobile/business/authentication/f$a;
.source "PushAuthenticationExplanation.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/UserPushMessage;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/UserPushMessage;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/UserPushMessage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/business/authentication/f$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iget-object p1, p0, Lcom/swedbank/mobile/app/r/c;->a:Lcom/swedbank/mobile/business/push/UserPushMessage;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/push/UserPushMessage;->c()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method
