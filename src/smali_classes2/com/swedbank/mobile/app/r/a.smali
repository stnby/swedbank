.class public final Lcom/swedbank/mobile/app/r/a;
.super Ljava/lang/Object;
.source "GenericPushNotificationProviderImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/push/handling/d;
.implements Lcom/swedbank/mobile/data/device/h;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Landroid/app/Application;

.field private final c:Lcom/squareup/moshi/n;

.field private final d:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/moshi/n;Lcom/swedbank/mobile/architect/business/a/c;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/moshi/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/moshi/n;",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jsonMapper"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flowManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/a;->b:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/r/a;->c:Lcom/squareup/moshi/n;

    iput-object p3, p0, Lcom/swedbank/mobile/app/r/a;->d:Lcom/swedbank/mobile/architect/business/a/c;

    const-string p1, "com.swedbank.mobile.GENERIC_PUSH_NOTIFICATION"

    .line 35
    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/b;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/a;->a:Ljava/util/Set;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/r/a;)Lcom/squareup/moshi/n;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/a;->c:Lcom/squareup/moshi/n;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/r/a;)Landroid/app/Application;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/a;->b:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/r/a;)Lcom/swedbank/mobile/architect/business/a/c;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/a;->d:Lcom/swedbank/mobile/architect/business/a/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/push/UserPushMessage;)Lcom/swedbank/mobile/business/e/m;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/UserPushMessage;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/swedbank/mobile/app/r/a$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/r/a$a;-><init>(Lcom/swedbank/mobile/app/r/a;Lcom/swedbank/mobile/business/push/UserPushMessage;)V

    check-cast v0, Lcom/swedbank/mobile/business/e/m;

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/a;->a:Ljava/util/Set;

    return-object v0
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "generic_business_notification_message"

    .line 53
    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "generic_business_notification_message"

    .line 72
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 73
    invoke-static {p0}, Lcom/swedbank/mobile/app/r/a;->a(Lcom/swedbank/mobile/app/r/a;)Lcom/squareup/moshi/n;

    move-result-object v0

    .line 74
    const-class v1, Lcom/swedbank/mobile/business/push/UserPushMessage;

    invoke-virtual {v0, v1}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/push/UserPushMessage;

    if-eqz p1, :cond_0

    .line 76
    invoke-static {p0}, Lcom/swedbank/mobile/app/r/a;->c(Lcom/swedbank/mobile/app/r/a;)Lcom/swedbank/mobile/architect/business/a/c;

    move-result-object v0

    new-instance v1, Lcom/swedbank/mobile/business/navigation/e;

    .line 78
    new-instance v2, Lcom/swedbank/mobile/app/r/c;

    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/r/c;-><init>(Lcom/swedbank/mobile/business/push/UserPushMessage;)V

    check-cast v2, Lcom/swedbank/mobile/business/authentication/f;

    .line 76
    invoke-direct {v1, p1, v2}, Lcom/swedbank/mobile/business/navigation/e;-><init>(Lcom/swedbank/mobile/business/push/UserPushMessage;Lcom/swedbank/mobile/business/authentication/f;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    :cond_0
    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
