.class public final Lcom/swedbank/mobile/app/r/b/d;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PushOnboardingPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/r/b/j;",
        "Lcom/swedbank/mobile/app/r/b/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/push/onboarding/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/onboarding/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/onboarding/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/b/d;->a:Lcom/swedbank/mobile/business/push/onboarding/a;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 13
    sget-object v0, Lcom/swedbank/mobile/app/r/b/d$b;->a:Lcom/swedbank/mobile/app/r/b/d$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/r/b/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 14
    new-instance v1, Lcom/swedbank/mobile/app/r/b/d$c;

    iget-object v2, p0, Lcom/swedbank/mobile/app/r/b/d;->a:Lcom/swedbank/mobile/business/push/onboarding/a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/r/b/d$c;-><init>(Lcom/swedbank/mobile/business/push/onboarding/a;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/r/b/e;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/r/b/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/swedbank/mobile/app/r/b/d;->a:Lcom/swedbank/mobile/business/push/onboarding/a;

    .line 19
    invoke-interface {v1}, Lcom/swedbank/mobile/business/push/onboarding/a;->c()Lio/reactivex/o;

    move-result-object v1

    .line 20
    sget-object v2, Lcom/swedbank/mobile/app/r/b/d$a;->a:Lcom/swedbank/mobile/app/r/b/d$a;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/app/r/b/f;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/r/b/f;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 23
    check-cast v0, Lio/reactivex/s;

    .line 24
    check-cast v1, Lio/reactivex/s;

    .line 22
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026     forceDisabledStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
