.class public final Lcom/swedbank/mobile/app/r/b/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PushOnboardingViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/r/b/j;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/r/b/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "pluginContainer"

    const-string v4, "getPluginContainer()Landroid/view/ViewGroup;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/r/b/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "switchView"

    const-string v4, "getSwitchView()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/r/b/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    const v0, 0x7f0a01c9

    .line 20
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->b(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/r/b/k;->b:Lkotlin/f/c;

    const v0, 0x7f0a01ca

    .line 21
    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->b(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/r/b/k;->c:Lkotlin/f/c;

    const/4 v0, 0x1

    .line 23
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/b/c/b;->a(Ljava/lang/Object;)Lcom/b/c/b;

    move-result-object v0

    const-string v1, "BehaviorRelay.createDefa\u2026AULT_PUSH_SERVICE_STATUS)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/r/b/k;->d:Lcom/b/c/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/r/b/k;)Lcom/b/c/b;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/b/k;->d:Lcom/b/c/b;

    return-object p0
.end method

.method private final b()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/r/b/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/r/b/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final c()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/r/b/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/r/b/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/b/k;->d:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/r/b/m;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/r/b/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/app/r/b/k;->c()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 44
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/b/m;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 45
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/Boolean;)V

    .line 47
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/b/m;->a()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/r/b/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/r/b/k;->a(Lcom/swedbank/mobile/app/r/b/m;)V

    return-void
.end method

.method protected e()V
    .locals 6

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/r/b/k;->b()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 29
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0d00a2

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 54
    :goto_0
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v5, "LayoutInflater.from(this)"

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Landroid/view/View;

    const v1, 0x7f0a01ca

    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    .line 31
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/Boolean;)V

    .line 32
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    .line 56
    new-instance v2, Lcom/swedbank/mobile/app/r/b/k$a;

    invoke-direct {v2, v0, p0}, Lcom/swedbank/mobile/app/r/b/k$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Lcom/swedbank/mobile/app/r/b/k;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 53
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.View"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_1
    return-void
.end method
