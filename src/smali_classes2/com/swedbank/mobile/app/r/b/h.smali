.class public final Lcom/swedbank/mobile/app/r/b/h;
.super Lcom/swedbank/mobile/architect/a/h;
.source "PushOnboardingRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/push/onboarding/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/onboarding/a/a;

.field private final f:Lcom/swedbank/mobile/business/onboarding/error/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/onboarding/a/a;Lcom/swedbank/mobile/business/onboarding/error/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/onboarding/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/onboarding/error/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_push_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_push_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_push_onboarding"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/onboarding/a/a;",
            "Lcom/swedbank/mobile/business/onboarding/error/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "onboardingErrorHandlerBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingErrorHandlerListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p3, p5, p4}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/b/h;->e:Lcom/swedbank/mobile/app/onboarding/a/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/r/b/h;->f:Lcom/swedbank/mobile/business/onboarding/error/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 35
    sget-object v0, Lcom/swedbank/mobile/app/r/b/h$a;->a:Lcom/swedbank/mobile/app/r/b/h$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/b/h;->e:Lcom/swedbank/mobile/app/onboarding/a/a;

    .line 26
    new-instance v1, Lcom/swedbank/mobile/app/r/b/c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/r/b/c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/app/onboarding/a/k;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/onboarding/a/a;->a(Lcom/swedbank/mobile/app/onboarding/a/k;)Lcom/swedbank/mobile/app/onboarding/a/a;

    move-result-object p1

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/b/h;->f:Lcom/swedbank/mobile/business/onboarding/error/c;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/onboarding/a/a;->a(Lcom/swedbank/mobile/business/onboarding/error/c;)Lcom/swedbank/mobile/app/onboarding/a/a;

    move-result-object p1

    .line 28
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/onboarding/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 34
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method
