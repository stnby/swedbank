.class public final Lcom/swedbank/mobile/app/r/b/a;
.super Ljava/lang/Object;
.source "PushOnboardingBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Lcom/swedbank/mobile/business/onboarding/f;",
            "+",
            "Lcom/swedbank/mobile/business/push/onboarding/e;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/a/x/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/x/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/x/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/b/a;->b:Lcom/swedbank/mobile/a/x/b/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/onboarding/f;)Lcom/swedbank/mobile/app/r/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/onboarding/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "onboardingStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/r/b/a;

    .line 19
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/r/b/a;->a:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/b/a;->b:Lcom/swedbank/mobile/a/x/b/a$a;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/r/b/a;->a:Lcom/swedbank/mobile/business/util/e;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/x/b/a$a;->b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/x/b/a$a;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/a/x/b/a$a;->a()Lcom/swedbank/mobile/a/x/b/a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/x/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build push onboarding node without specifying onboarding listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
