.class public final Lcom/swedbank/mobile/app/r/a$a;
.super Ljava/lang/Object;
.source "GenericPushNotificationProviderImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/e/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/r/a;->a(Lcom/swedbank/mobile/business/push/UserPushMessage;)Lcom/swedbank/mobile/business/e/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/r/a;

.field final synthetic b:Lcom/swedbank/mobile/business/push/UserPushMessage;

.field private final c:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/r/a;Lcom/swedbank/mobile/business/push/UserPushMessage;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/push/UserPushMessage;",
            ")V"
        }
    .end annotation

    .line 41
    iput-object p1, p0, Lcom/swedbank/mobile/app/r/a$a;->a:Lcom/swedbank/mobile/app/r/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/r/a$a;->b:Lcom/swedbank/mobile/business/push/UserPushMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "generic_business_notification_channel_id"

    .line 42
    iput-object v0, p0, Lcom/swedbank/mobile/app/r/a$a;->c:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/swedbank/mobile/app/r/a;->b(Lcom/swedbank/mobile/app/r/a;)Landroid/app/Application;

    move-result-object p1

    const v0, 0x7f110137

    invoke-virtual {p1, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "application.getString(R.\u2026otification_channel_name)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/a$a;->d:Ljava/lang/String;

    .line 44
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/push/UserPushMessage;->b()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/a$a;->e:Ljava/lang/String;

    .line 45
    invoke-virtual {p2}, Lcom/swedbank/mobile/business/push/UserPushMessage;->c()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/a$a;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/a$a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/a$a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/a$a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/a$a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public e()Landroid/os/Bundle;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x1

    .line 47
    new-array v0, v0, [Lkotlin/k;

    const-string v1, "generic_business_notification_message"

    .line 48
    iget-object v2, p0, Lcom/swedbank/mobile/app/r/a$a;->b:Lcom/swedbank/mobile/business/push/UserPushMessage;

    iget-object v3, p0, Lcom/swedbank/mobile/app/r/a$a;->a:Lcom/swedbank/mobile/app/r/a;

    invoke-static {v3}, Lcom/swedbank/mobile/app/r/a;->a(Lcom/swedbank/mobile/app/r/a;)Lcom/squareup/moshi/n;

    move-result-object v3

    .line 71
    const-class v4, Lcom/swedbank/mobile/business/push/UserPushMessage;

    invoke-virtual {v3, v4}, Lcom/squareup/moshi/n;->a(Ljava/lang/Class;)Lcom/squareup/moshi/JsonAdapter;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/moshi/JsonAdapter;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "jsonMapper.adapter(T::class.java).toJson(this)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v1, v2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 47
    invoke-static {v0}, Landroidx/core/os/b;->a([Lkotlin/k;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method
