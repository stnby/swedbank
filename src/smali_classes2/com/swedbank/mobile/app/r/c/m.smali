.class public final Lcom/swedbank/mobile/app/r/c/m;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PushPreferencesViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/r/c/l;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/app/r/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/core/ui/widget/k;

.field private final g:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/r/c/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/r/c/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "preferencesList"

    const-string v4, "getPreferencesList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/r/c/m;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 5
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m;->g:Lio/reactivex/o;

    const p1, 0x7f0a02dd

    .line 30
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m;->c:Lkotlin/f/c;

    const p1, 0x7f0a0263

    .line 31
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m;->d:Lkotlin/f/c;

    .line 34
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<PushPreferenceAction>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m;->e:Lcom/b/c/c;

    .line 35
    new-instance p1, Lcom/swedbank/mobile/core/ui/widget/k;

    const/4 v0, 0x2

    .line 36
    new-array v0, v0, [Lkotlin/k;

    const/4 v1, 0x0

    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 39
    sget-object v3, Lcom/swedbank/mobile/app/r/c/m$b;->a:Lcom/swedbank/mobile/app/r/c/m$b;

    check-cast v3, Lkotlin/e/a/m;

    const v4, 0x7f0d0040

    .line 37
    invoke-static {v4, v3}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 45
    new-instance v3, Lcom/swedbank/mobile/app/r/c/m$c;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/r/c/m$c;-><init>(Lcom/swedbank/mobile/app/r/c/m;)V

    check-cast v3, Lkotlin/e/a/m;

    const v4, 0x7f0d0041

    .line 43
    invoke-static {v4, v3}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v2

    aput-object v2, v0, v1

    .line 36
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object v0

    const/4 v2, 0x0

    .line 35
    invoke-direct {p1, v2, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m;->f:Lcom/swedbank/mobile/core/ui/widget/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/r/c/m;)Lcom/swedbank/mobile/core/ui/widget/k;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/c/m;->f:Lcom/swedbank/mobile/core/ui/widget/k;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/r/c/m;)Landroid/content/Context;
    .locals 0

    .line 27
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/r/c/m;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method private final c()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/m;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/r/c/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/r/c/m;)Lcom/b/c/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/c/m;->e:Lcom/b/c/c;

    return-object p0
.end method

.method private final f()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/m;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/r/c/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/r/c/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/m;->e:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/r/c/p;)V
    .locals 13
    .param p1    # Lcom/swedbank/mobile/app/r/c/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/c/p;->a()Lkotlin/k;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/app/r/c/m;

    .line 114
    invoke-virtual {v0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/f$b;

    .line 115
    invoke-static {v2}, Lcom/swedbank/mobile/app/r/c/m;->a(Lcom/swedbank/mobile/app/r/c/m;)Lcom/swedbank/mobile/core/ui/widget/k;

    move-result-object v2

    .line 116
    check-cast v3, Ljava/lang/Iterable;

    .line 117
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 118
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 119
    check-cast v5, Lcom/swedbank/mobile/app/r/c/c;

    .line 120
    new-instance v6, Lcom/swedbank/mobile/core/ui/widget/i;

    .line 123
    instance-of v7, v5, Lcom/swedbank/mobile/app/r/c/c$b;

    if-eqz v7, :cond_0

    const/4 v7, 0x1

    goto :goto_1

    .line 124
    :cond_0
    instance-of v7, v5, Lcom/swedbank/mobile/app/r/c/c$a;

    if-eqz v7, :cond_1

    const/4 v7, 0x0

    .line 120
    :goto_1
    invoke-direct {v6, v5, v7}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 125
    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 126
    :cond_2
    check-cast v4, Ljava/util/List;

    .line 115
    invoke-virtual {v2, v4, v0}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    .line 85
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/c/p;->b()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 134
    invoke-static {p0}, Lcom/swedbank/mobile/app/r/c/m;->b(Lcom/swedbank/mobile/app/r/c/m;)Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    .line 135
    move-object v3, v2

    check-cast v3, Ljava/lang/Integer;

    .line 138
    instance-of v3, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v3, :cond_5

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 140
    move-object v3, p1

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v1

    if-eqz v3, :cond_4

    move-object v4, p1

    check-cast v4, Ljava/lang/Iterable;

    const-string p1, "\n"

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 141
    :cond_4
    sget p1, Lcom/swedbank/mobile/core/a$f;->error_general_error:I

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 143
    :cond_5
    instance-of v3, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v3, :cond_6

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 144
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    const-string v0, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 145
    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 147
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-direct {v0, v2, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 133
    invoke-virtual {p0, p1, v0}, Lcom/swedbank/mobile/app/r/c/m;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_3

    .line 144
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 148
    :cond_7
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/r/c/m;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :goto_3
    return-void
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/app/r/c/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/r/c/m;->a(Lcom/swedbank/mobile/app/r/c/p;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/m;->g:Lio/reactivex/o;

    .line 81
    invoke-direct {p0}, Lcom/swedbank/mobile/app/r/c/m;->c()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks\n      .mergeW\u2026olbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/m;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 9

    .line 111
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 112
    new-instance v1, Lcom/swedbank/mobile/app/r/c/m$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/r/c/m$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/r/c/n;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/r/c/n;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 113
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/r/c/m;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 68
    invoke-direct {p0}, Lcom/swedbank/mobile/app/r/c/m;->f()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    .line 69
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    const/4 v1, 0x0

    .line 70
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$f;)V

    .line 71
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/f;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v2, "context"

    invoke-static {v3, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v2, Lcom/swedbank/mobile/core/ui/widget/g$c;

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v5, v4, v5

    invoke-direct {v2, v4}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v6, v2

    check-cast v6, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v4, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v2, v1

    .line 71
    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 73
    iget-object v1, p0, Lcom/swedbank/mobile/app/r/c/m;->f:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 74
    invoke-direct {p0}, Lcom/swedbank/mobile/app/r/c/m;->c()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method
