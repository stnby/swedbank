.class final Lcom/swedbank/mobile/app/r/c/b;
.super Ljava/lang/Object;
.source "PushPreferencesPresenter.kt"


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/r/c/b;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;ZILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/util/e<",
            "+",
            "Ljava/lang/Throwable;",
            "+",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;Z)V"
        }
    .end annotation

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/app/r/c/b;->b:Lcom/swedbank/mobile/business/util/e;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/r/c/b;->c:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;ZILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 104
    move-object p1, v0

    check-cast p1, Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 105
    move-object p2, v0

    check-cast p2, Lcom/swedbank/mobile/business/util/e;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    const/4 p3, 0x0

    .line 106
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/r/c/b;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;Z)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/business/util/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/b;->b:Lcom/swedbank/mobile/business/util/e;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/r/c/b;->c:Z

    return v0
.end method
