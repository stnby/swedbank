.class public final Lcom/swedbank/mobile/app/r/c/g;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PushPreferencesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/r/c/l;",
        "Lcom/swedbank/mobile/app/r/c/p;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/ad;

.field private final b:Lcom/swedbank/mobile/business/push/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/push/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/push/preferences/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/g;->b:Lcom/swedbank/mobile/business/push/preferences/a;

    .line 23
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/g;->a:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/r/c/g;)Lcom/swedbank/mobile/business/push/preferences/a;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/c/g;->b:Lcom/swedbank/mobile/business/push/preferences/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/r/c/g;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/r/c/g;->a:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 26
    sget-object v0, Lcom/swedbank/mobile/app/r/c/g$d;->a:Lcom/swedbank/mobile/app/r/c/g$d;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/r/c/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/swedbank/mobile/app/r/c/g$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/r/c/g$e;-><init>(Lcom/swedbank/mobile/app/r/c/g;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/reactivex/o;->m()Lio/reactivex/o;

    move-result-object v0

    .line 47
    sget-object v1, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 49
    iget-object v2, p0, Lcom/swedbank/mobile/app/r/c/g;->b:Lcom/swedbank/mobile/business/push/preferences/a;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/push/preferences/a;->a()Lio/reactivex/o;

    move-result-object v2

    .line 52
    new-instance v9, Lcom/swedbank/mobile/app/r/c/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x7

    const/4 v8, 0x0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/app/r/c/b;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/util/e;ZILkotlin/e/b/g;)V

    invoke-virtual {v0, v9}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v3

    const-string v4, "preferenceActionStream.s\u2026PreferenceChangeStatus())"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {v1, v2, v3}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v1

    .line 53
    sget-object v2, Lcom/swedbank/mobile/app/r/c/g$g;->a:Lcom/swedbank/mobile/app/r/c/g$g;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observables\n        .com\u2026inToggleClick))\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {}, Lcom/swedbank/mobile/app/r/c/d;->a()Lkotlin/e/a/m;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v1, v3, v2, v4, v3}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 62
    sget-object v2, Lcom/swedbank/mobile/app/r/c/g$h;->a:Lcom/swedbank/mobile/app/r/c/g$h;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 65
    new-instance v2, Lcom/swedbank/mobile/app/r/c/g$f;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/r/c/g$f;-><init>(Lcom/swedbank/mobile/app/r/c/g;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 74
    sget-object v2, Lcom/swedbank/mobile/app/r/c/g$b;->a:Lcom/swedbank/mobile/app/r/c/g$b;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/r/c/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 75
    new-instance v4, Lcom/swedbank/mobile/app/r/c/g$c;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/r/c/g$c;-><init>(Lcom/swedbank/mobile/app/r/c/g;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 76
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 77
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 80
    check-cast v1, Lio/reactivex/s;

    .line 81
    check-cast v0, Lio/reactivex/s;

    .line 82
    check-cast v2, Lio/reactivex/s;

    .line 79
    invoke-static {v1, v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026\n        backClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    new-instance v1, Lcom/swedbank/mobile/app/r/c/p;

    const/4 v2, 0x3

    invoke-direct {v1, v3, v3, v2, v3}, Lcom/swedbank/mobile/app/r/c/p;-><init>(Lkotlin/k;Lcom/swedbank/mobile/business/util/e;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/r/c/g$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/r/c/g;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/r/c/g$a;-><init>(Lcom/swedbank/mobile/app/r/c/g;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 108
    new-instance v3, Lcom/swedbank/mobile/app/r/c/h;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/r/c/h;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 109
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
