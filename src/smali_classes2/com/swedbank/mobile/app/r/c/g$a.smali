.class final synthetic Lcom/swedbank/mobile/app/r/c/g$a;
.super Lkotlin/e/b/i;
.source "PushPreferencesPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/r/c/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/r/c/p;",
        "Lcom/swedbank/mobile/app/r/c/p$a;",
        "Lcom/swedbank/mobile/app/r/c/p;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/r/c/g;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/r/c/p;Lcom/swedbank/mobile/app/r/c/p$a;)Lcom/swedbank/mobile/app/r/c/p;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/r/c/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/r/c/p$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/g$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/r/c/g;

    .line 109
    instance-of v0, p2, Lcom/swedbank/mobile/app/r/c/p$a$b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 110
    check-cast p2, Lcom/swedbank/mobile/app/r/c/p$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/p$a$b;->a()Lkotlin/k;

    move-result-object p2

    const/4 v0, 0x2

    .line 109
    invoke-static {p1, p2, v1, v0, v1}, Lcom/swedbank/mobile/app/r/c/p;->a(Lcom/swedbank/mobile/app/r/c/p;Lkotlin/k;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/r/c/p;

    move-result-object p1

    goto :goto_0

    .line 111
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/r/c/p$a$c;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 112
    check-cast p2, Lcom/swedbank/mobile/app/r/c/p$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/p$a$c;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p2

    .line 111
    invoke-static {p1, v1, p2, v2, v1}, Lcom/swedbank/mobile/app/r/c/p;->a(Lcom/swedbank/mobile/app/r/c/p;Lkotlin/k;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/r/c/p;

    move-result-object p1

    goto :goto_0

    .line 113
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/app/r/c/p$a$a;->a:Lcom/swedbank/mobile/app/r/c/p$a$a;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-static {p1, v1, v1, v2, v1}, Lcom/swedbank/mobile/app/r/c/p;->a(Lcom/swedbank/mobile/app/r/c/p;Lkotlin/k;Lcom/swedbank/mobile/business/util/e;ILjava/lang/Object;)Lcom/swedbank/mobile/app/r/c/p;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/r/c/p;

    check-cast p2, Lcom/swedbank/mobile/app/r/c/p$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/r/c/g$a;->a(Lcom/swedbank/mobile/app/r/c/p;Lcom/swedbank/mobile/app/r/c/p$a;)Lcom/swedbank/mobile/app/r/c/p;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/r/c/g;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/push/preferences/PushPreferencesViewState;Lcom/swedbank/mobile/app/push/preferences/PushPreferencesViewState$PartialState;)Lcom/swedbank/mobile/app/push/preferences/PushPreferencesViewState;"

    return-object v0
.end method
