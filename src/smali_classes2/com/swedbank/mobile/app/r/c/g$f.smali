.class final Lcom/swedbank/mobile/app/r/c/g$f;
.super Ljava/lang/Object;
.source "PushPreferencesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/r/c/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/r/c/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/r/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/g$f;->a:Lcom/swedbank/mobile/app/r/c/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/r/c/b;)Lio/reactivex/o;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/r/c/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/r/c/b;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/r/c/p$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/c/b;->b()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/r/c/g$f;->a:Lcom/swedbank/mobile/app/r/c/g;

    .line 68
    new-instance v1, Lcom/swedbank/mobile/app/r/c/p$a$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/c/b;->b()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/r/c/p$a$c;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    move-object v3, v1

    check-cast v3, Lcom/swedbank/mobile/app/r/c/p$a;

    .line 108
    invoke-static {v0}, Lcom/swedbank/mobile/app/r/c/g;->b(Lcom/swedbank/mobile/app/r/c/g;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v2

    .line 110
    sget-object v4, Lcom/swedbank/mobile/app/r/c/p$a$a;->a:Lcom/swedbank/mobile/app/r/c/p$a$a;

    const-wide/16 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 108
    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 69
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/c/b;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/swedbank/mobile/app/r/c/p$a$a;->a:Lcom/swedbank/mobile/app/r/c/p$a$a;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(PartialState.DismissQueryError)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_1
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.empty()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/r/c/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/r/c/g$f;->a(Lcom/swedbank/mobile/app/r/c/b;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
