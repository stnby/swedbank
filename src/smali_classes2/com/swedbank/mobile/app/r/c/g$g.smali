.class final Lcom/swedbank/mobile/app/r/c/g$g;
.super Ljava/lang/Object;
.source "PushPreferencesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/r/c/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/r/c/g$g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/r/c/g$g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/r/c/g$g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/r/c/g$g;->a:Lcom/swedbank/mobile/app/r/c/g$g;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/r/c/g$g;->a(Lkotlin/k;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlin/k;)Ljava/util/List;
    .locals 10
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/Boolean;",
            "Lcom/swedbank/mobile/app/r/c/b;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/r/c/c$b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/r/c/b;

    .line 54
    new-instance v0, Lcom/swedbank/mobile/app/r/c/c$b;

    const-string v2, "mainToggle"

    .line 58
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/r/c/b;->a()Ljava/lang/String;

    move-result-object p1

    const-string v1, "mainToggle"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    .line 59
    sget-object p1, Lcom/swedbank/mobile/app/r/c/a$a;->a:Lcom/swedbank/mobile/app/r/c/a$a;

    move-object v7, p1

    check-cast v7, Lcom/swedbank/mobile/app/r/c/a;

    const v3, 0x7f1101ef

    const/4 v4, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v1, v0

    .line 54
    invoke-direct/range {v1 .. v9}, Lcom/swedbank/mobile/app/r/c/c$b;-><init>(Ljava/lang/String;ILjava/lang/Integer;ZZLcom/swedbank/mobile/app/r/c/a;ILkotlin/e/b/g;)V

    invoke-static {v0}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
