.class final Lcom/swedbank/mobile/app/r/c/m$c;
.super Lkotlin/e/b/k;
.source "PushPreferencesViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/r/c/m;-><init>(Lio/reactivex/o;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/r/c/c$b;",
        ">;",
        "Lcom/swedbank/mobile/app/r/c/c$b;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/r/c/m;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/r/c/m;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/r/c/m$c;->a:Lcom/swedbank/mobile/app/r/c/m;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/r/c/c$b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/r/c/m$c;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/r/c/c$b;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/r/c/c$b;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/r/c/c$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/r/c/c$b;",
            ">;",
            "Lcom/swedbank/mobile/app/r/c/c$b;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "info"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    .line 48
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/c$b;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 49
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/c$b;->c()Ljava/lang/Integer;

    move-result-object v1

    .line 50
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/c$b;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 47
    invoke-virtual {p1, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 51
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/c$b;->e()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setLoading(Z)V

    .line 53
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/c$b;->f()Lcom/swedbank/mobile/app/r/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/r/c/c$b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p1, Landroid/view/View;

    .line 111
    new-instance v0, Lcom/swedbank/mobile/app/r/c/m$c$a;

    invoke-direct {v0, p0, p2}, Lcom/swedbank/mobile/app/r/c/m$c$a;-><init>(Lcom/swedbank/mobile/app/r/c/m$c;Lcom/swedbank/mobile/app/r/c/c$b;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 57
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x0

    .line 58
    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setClickable(Z)V

    :goto_0
    return-void

    .line 46
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.core.ui.widget.OnOffSettingView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
