.class public final Lcom/swedbank/mobile/app/o/a;
.super Ljava/lang/Object;
.source "OldAppMigrationBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/i/c;

.field private final b:Lcom/swedbank/mobile/a/r/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/r/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/r/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/o/a;->b:Lcom/swedbank/mobile/a/r/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/i/c;)Lcom/swedbank/mobile/app/o/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/i/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/o/a;

    .line 15
    iput-object p1, v0, Lcom/swedbank/mobile/app/o/a;->a:Lcom/swedbank/mobile/business/i/c;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/app/o/a;->b:Lcom/swedbank/mobile/a/r/a$a;

    .line 19
    iget-object v1, p0, Lcom/swedbank/mobile/app/o/a;->a:Lcom/swedbank/mobile/business/i/c;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/r/a$a;->b(Lcom/swedbank/mobile/business/i/c;)Lcom/swedbank/mobile/a/r/a$a;

    move-result-object v0

    .line 22
    invoke-interface {v0}, Lcom/swedbank/mobile/a/r/a$a;->a()Lcom/swedbank/mobile/a/r/a;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/swedbank/mobile/a/r/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 19
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build OldAppMigrationNode without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
