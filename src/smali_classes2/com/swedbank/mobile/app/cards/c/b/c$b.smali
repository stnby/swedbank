.class final Lcom/swedbank/mobile/app/cards/c/b/c$b;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsListPresenter.kt"

# interfaces
.implements Lio/reactivex/c/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/c/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/d<",
        "Lcom/swedbank/mobile/app/cards/list/r;",
        "Lcom/swedbank/mobile/app/cards/list/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/c/b/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/cards/c/b/c$b;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/c/b/c$b;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/c/b/c$b;->a:Lcom/swedbank/mobile/app/cards/c/b/c$b;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/cards/list/r;Lcom/swedbank/mobile/app/cards/list/r;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/cards/list/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/list/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "prev"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curr"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/cards/list/r;

    check-cast p2, Lcom/swedbank/mobile/app/cards/list/r;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/c/b/c$b;->a(Lcom/swedbank/mobile/app/cards/list/r;Lcom/swedbank/mobile/app/cards/list/r;)Z

    move-result p1

    return p1
.end method
