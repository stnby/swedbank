.class public final Lcom/swedbank/mobile/app/cards/c/b/b;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsListBuilder_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/c/b/a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/b/a$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/b/a$a;",
            ">;)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/b/b;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/c/b/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/a/e/d/b/a$a;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/c/b/b;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/b/b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/c/b/b;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/c/b/a;
    .locals 2

    .line 18
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/b/a;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/c/b/b;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/a/e/d/b/a$a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/cards/c/b/a;-><init>(Lcom/swedbank/mobile/a/e/d/b/a$a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/c/b/b;->a()Lcom/swedbank/mobile/app/cards/c/b/a;

    move-result-object v0

    return-object v0
.end method
