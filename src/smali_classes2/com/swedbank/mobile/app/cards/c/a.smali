.class public final Lcom/swedbank/mobile/app/cards/c/a;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Lcom/swedbank/mobile/a/e/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/a/e/d/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/d/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/d/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/a;->b:Lcom/swedbank/mobile/a/e/d/a$a;

    .line 15
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/a;->a:Lcom/swedbank/mobile/business/util/l;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/a/e/d/b;)Lcom/swedbank/mobile/app/cards/c/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/d/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "emptyState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/c/a;

    .line 18
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/c/a;->a:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/c/a;->b:Lcom/swedbank/mobile/a/e/d/a$a;

    .line 22
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/c/a;->a:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/d/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/e/d/a$a;

    move-result-object v0

    .line 23
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/d/a$a;->a()Lcom/swedbank/mobile/a/e/d/a;

    move-result-object v0

    .line 24
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/d/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0
.end method
