.class final Lcom/swedbank/mobile/app/cards/c/b/c$n;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsListPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/c/b/c;->c()[Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/c/b/c$n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/cards/c/b/c$n;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/c/b/c$n;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/c/b/c$n;->a:Lcom/swedbank/mobile/app/cards/c/b/c$n;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/cards/list/r$a$a;
    .locals 3
    .param p1    # Lcom/swedbank/mobile/core/ui/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/j<",
            "+",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/list/r$a$a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 119
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 120
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 121
    check-cast v2, Lcom/swedbank/mobile/business/cards/a;

    .line 72
    invoke-static {v2}, Lcom/swedbank/mobile/app/cards/d;->a(Lcom/swedbank/mobile/business/cards/a;)Lcom/swedbank/mobile/app/cards/c;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 73
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    .line 71
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/r$a$a;

    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/cards/list/r$a$a;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/core/ui/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/c/b/c$n;->a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/cards/list/r$a$a;

    move-result-object p1

    return-object p1
.end method
