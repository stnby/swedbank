.class final Lcom/swedbank/mobile/app/cards/c/b/c$o;
.super Ljava/lang/Object;
.source "NotAuthenticatedCardsListPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/c/b/c;->c()[Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/c/b/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/c/b/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/b/c$o;->a:Lcom/swedbank/mobile/app/cards/c/b/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/list/r$a;",
            ">;)",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/list/r$a;",
            ">;"
        }
    .end annotation

    const-string v0, "cardsStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    move-object v0, p1

    check-cast v0, Lio/reactivex/s;

    const-wide/16 v1, 0x1

    .line 79
    invoke-virtual {p1, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object p1

    .line 80
    new-instance v1, Lcom/swedbank/mobile/app/cards/c/b/c$o$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/c/b/c$o$1;-><init>(Lcom/swedbank/mobile/app/cards/c/b/c$o;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 76
    invoke-static {v0, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/c/b/c$o;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
