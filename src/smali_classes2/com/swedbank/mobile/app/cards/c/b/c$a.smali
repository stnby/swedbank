.class final synthetic Lcom/swedbank/mobile/app/cards/c/b/c$a;
.super Lkotlin/e/b/i;
.source "NotAuthenticatedCardsListPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/c/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/cards/list/r;",
        "Lcom/swedbank/mobile/app/cards/list/r$a;",
        "Lcom/swedbank/mobile/app/cards/list/r;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/c/b/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/cards/list/r;Lcom/swedbank/mobile/app/cards/list/r$a;)Lcom/swedbank/mobile/app/cards/list/r;
    .locals 10
    .param p1    # Lcom/swedbank/mobile/app/cards/list/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/list/r$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/c/b/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/cards/c/b/c;

    .line 120
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/list/r$a$f;

    if-eqz v0, :cond_2

    .line 122
    check-cast p2, Lcom/swedbank/mobile/app/cards/list/r$a$f;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    .line 123
    :cond_0
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$f;->a()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x36

    const/4 v8, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/list/r;->a(Lcom/swedbank/mobile/app/cards/list/r;ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3e

    const/4 v8, 0x0

    move-object v0, p1

    .line 126
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/list/r;->a(Lcom/swedbank/mobile/app/cards/list/r;ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    goto/16 :goto_0

    .line 129
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/list/r$a$a;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 130
    check-cast p2, Lcom/swedbank/mobile/app/cards/list/r$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$a;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$a;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object p2

    invoke-static {v0, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3b

    const/4 v9, 0x0

    move-object v1, p1

    .line 129
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/cards/list/r;->a(Lcom/swedbank/mobile/app/cards/list/r;ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    goto/16 :goto_0

    .line 131
    :cond_3
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/r$a$c;->a:Lcom/swedbank/mobile/app/cards/list/r$a$c;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    .line 132
    :cond_4
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/list/r$a$b;

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 134
    check-cast p2, Lcom/swedbank/mobile/app/cards/list/r$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x36

    const/4 v9, 0x0

    move-object v1, p1

    .line 132
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/cards/list/r;->a(Lcom/swedbank/mobile/app/cards/list/r;ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    goto :goto_0

    .line 135
    :cond_5
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/r$a$e;->a:Lcom/swedbank/mobile/app/cards/list/r$a$e;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 139
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->c()Lkotlin/k;

    move-result-object p2

    if-eqz p2, :cond_8

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->c()Lkotlin/k;

    move-result-object p2

    invoke-virtual {p2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_6

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x37

    const/4 v8, 0x0

    move-object v0, p1

    .line 140
    invoke-static/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/list/r;->a(Lcom/swedbank/mobile/app/cards/list/r;ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    goto :goto_0

    .line 142
    :cond_7
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/list/r$a$d;

    if-eqz v0, :cond_9

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 143
    check-cast p2, Lcom/swedbank/mobile/app/cards/list/r$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$d;->a()Lcom/swedbank/mobile/business/e/p$c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/list/r$a$d;->b()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {v0, p2}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x2f

    const/4 v9, 0x0

    move-object v1, p1

    .line 142
    invoke-static/range {v1 .. v9}, Lcom/swedbank/mobile/app/cards/list/r;->a(Lcom/swedbank/mobile/app/cards/list/r;ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    :cond_8
    :goto_0
    return-object p1

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/cards/list/r;

    check-cast p2, Lcom/swedbank/mobile/app/cards/list/r$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/c/b/c$a;->a(Lcom/swedbank/mobile/app/cards/list/r;Lcom/swedbank/mobile/app/cards/list/r$a;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/cards/c/b/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/cards/list/CardsListViewState;Lcom/swedbank/mobile/app/cards/list/CardsListViewState$PartialState;)Lcom/swedbank/mobile/app/cards/list/CardsListViewState;"

    return-object v0
.end method
