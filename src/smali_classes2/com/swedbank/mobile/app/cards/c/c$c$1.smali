.class final Lcom/swedbank/mobile/app/cards/c/c$c$1;
.super Lkotlin/e/b/k;
.source "NotAuthenticatedCardsRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/c/c$c;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/c/c$c;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/c/c$c;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->a:Lcom/swedbank/mobile/app/cards/c/c$c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 7
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->a:Lcom/swedbank/mobile/app/cards/c/c$c;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/c/c$c;->b:Lcom/swedbank/mobile/business/cards/wallet/w;

    .line 99
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->a:Lcom/swedbank/mobile/app/cards/c/c$c;

    iget-object v1, v1, Lcom/swedbank/mobile/app/cards/c/c$c;->a:Lcom/swedbank/mobile/app/cards/c/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/c/c;->g(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v1

    .line 100
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->a:Lcom/swedbank/mobile/app/cards/c/c$c;

    iget-object v2, v2, Lcom/swedbank/mobile/app/cards/c/c$c;->a:Lcom/swedbank/mobile/app/cards/c/c;

    invoke-static {v2}, Lcom/swedbank/mobile/app/cards/c/c;->i(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object v2

    .line 101
    iget-object v3, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->b:Lio/reactivex/k;

    const-string v4, "emitter"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    check-cast p1, Ljava/lang/Iterable;

    .line 110
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Lcom/swedbank/mobile/architect/a/h;

    .line 109
    instance-of v6, v6, Lcom/swedbank/mobile/business/general/confirmation/e;

    if-eqz v6, :cond_0

    goto :goto_0

    :cond_1
    move-object v4, v5

    .line 112
    :goto_0
    check-cast v4, Lcom/swedbank/mobile/architect/a/h;

    if-eqz v4, :cond_2

    .line 113
    invoke-virtual {v4}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    move-object v5, p1

    check-cast v5, Lcom/swedbank/mobile/business/general/confirmation/d;

    :cond_2
    if-eqz v5, :cond_4

    .line 115
    invoke-interface {v5}, Lcom/swedbank/mobile/business/general/confirmation/d;->i()Ljava/lang/String;

    move-result-object p1

    const-string v4, "wallet_general_error_notification_listener"

    invoke-static {p1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_3

    goto :goto_1

    .line 128
    :cond_3
    invoke-interface {v3, v5}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_4

    .line 123
    :cond_4
    :goto_1
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$d;->a:Lcom/swedbank/mobile/business/cards/wallet/w$d;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object p1, Lcom/swedbank/mobile/app/cards/f/b/b/a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/a;

    check-cast p1, Lcom/swedbank/mobile/app/f/c$b;

    goto :goto_3

    .line 124
    :cond_5
    instance-of p1, v0, Lcom/swedbank/mobile/business/cards/wallet/w$a;

    if-eqz p1, :cond_6

    goto :goto_2

    .line 125
    :cond_6
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$c;->a:Lcom/swedbank/mobile/business/cards/wallet/w$c;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    :goto_2
    sget-object p1, Lcom/swedbank/mobile/app/cards/f/b/b/f;->a:Lcom/swedbank/mobile/app/cards/f/b/b/f;

    check-cast p1, Lcom/swedbank/mobile/app/f/c$b;

    .line 126
    :goto_3
    check-cast p1, Lcom/swedbank/mobile/app/f/c;

    invoke-virtual {v1, p1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    .line 121
    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    const/4 v0, 0x0

    .line 120
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/f/a;->a(Z)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    const-string v0, "wallet_general_error_notification_listener"

    .line 119
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/f/a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 117
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-interface {v3, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/c/c$c$1;->a:Lcom/swedbank/mobile/app/cards/c/c$c;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/c/c$c;->a:Lcom/swedbank/mobile/app/cards/c/c;

    .line 127
    invoke-static {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :goto_4
    return-void

    .line 126
    :cond_7
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/w$b;->a:Lcom/swedbank/mobile/business/cards/wallet/w$b;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Not handled errors are not handled"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/c/c$c$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
