.class public Lcom/swedbank/mobile/app/cards/c/b/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "NotAuthenticatedCardsListPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/list/k;",
        "Lcom/swedbank/mobile/app/cards/list/r;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/cards/notauth/list/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/notauth/list/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/notauth/list/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    .line 21
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->a:Lcom/swedbank/mobile/core/ui/x;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/c/b/c;)Lcom/swedbank/mobile/business/cards/notauth/list/d;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v0

    .line 26
    sget-object v1, Lcom/swedbank/mobile/app/cards/c/b/c$k;->a:Lcom/swedbank/mobile/app/cards/c/b/c$k;

    check-cast v1, Lkotlin/e/a/b;

    if-eqz v1, :cond_0

    new-instance v2, Lcom/swedbank/mobile/app/cards/c/b/g;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/cards/c/b/g;-><init>(Lkotlin/e/a/b;)V

    move-object v1, v2

    :cond_0
    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/cards/c/b/c$g;->a:Lcom/swedbank/mobile/app/cards/c/b/c$g;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/c/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 29
    new-instance v2, Lcom/swedbank/mobile/app/cards/c/b/c$h;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/cards/c/b/c$h;-><init>(Lcom/swedbank/mobile/app/cards/c/b/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 33
    sget-object v2, Lcom/swedbank/mobile/app/cards/c/b/c$e;->a:Lcom/swedbank/mobile/app/cards/c/b/c$e;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/c/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 34
    new-instance v3, Lcom/swedbank/mobile/app/cards/c/b/c$f;

    iget-object v4, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/cards/c/b/c$f;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/d;)V

    check-cast v3, Lkotlin/e/a/b;

    new-instance v4, Lcom/swedbank/mobile/app/cards/c/b/f;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/cards/c/b/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 36
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 38
    sget-object v3, Lcom/swedbank/mobile/app/cards/c/b/c$i;->a:Lcom/swedbank/mobile/app/cards/c/b/c$i;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/cards/c/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 39
    new-instance v4, Lcom/swedbank/mobile/app/cards/c/b/c$j;

    iget-object v5, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    invoke-direct {v4, v5}, Lcom/swedbank/mobile/app/cards/c/b/c$j;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/d;)V

    check-cast v4, Lkotlin/e/a/b;

    new-instance v5, Lcom/swedbank/mobile/app/cards/c/b/f;

    invoke-direct {v5, v4}, Lcom/swedbank/mobile/app/cards/c/b/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v3, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 40
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 41
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 43
    sget-object v4, Lcom/swedbank/mobile/app/cards/c/b/c$c;->a:Lcom/swedbank/mobile/app/cards/c/b/c$c;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/cards/c/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 44
    new-instance v5, Lcom/swedbank/mobile/app/cards/c/b/c$d;

    iget-object v6, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    invoke-direct {v5, v6}, Lcom/swedbank/mobile/app/cards/c/b/c$d;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/d;)V

    check-cast v5, Lkotlin/e/a/b;

    new-instance v6, Lcom/swedbank/mobile/app/cards/c/b/f;

    invoke-direct {v6, v5}, Lcom/swedbank/mobile/app/cards/c/b/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v6, Lio/reactivex/c/g;

    invoke-virtual {v4, v6}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v4

    .line 45
    invoke-virtual {v4}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v4

    .line 46
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    .line 48
    sget-object v5, Lcom/swedbank/mobile/app/cards/c/b/c$l;->a:Lcom/swedbank/mobile/app/cards/c/b/c$l;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/cards/c/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 49
    new-instance v6, Lcom/swedbank/mobile/app/cards/c/b/c$m;

    iget-object v7, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/cards/c/b/c$m;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/d;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/cards/c/b/f;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/cards/c/b/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 50
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 51
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 53
    new-instance v6, Lkotlin/e/b/x;

    const/4 v7, 0x7

    invoke-direct {v6, v7}, Lkotlin/e/b/x;-><init>(I)V

    .line 54
    check-cast v0, Lio/reactivex/s;

    invoke-virtual {v6, v0}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 55
    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v6, v1}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 56
    check-cast v2, Lio/reactivex/s;

    invoke-virtual {v6, v2}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 57
    check-cast v3, Lio/reactivex/s;

    invoke-virtual {v6, v3}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 58
    check-cast v4, Lio/reactivex/s;

    invoke-virtual {v6, v4}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 59
    check-cast v5, Lio/reactivex/s;

    invoke-virtual {v6, v5}, Lkotlin/e/b/x;->b(Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/c/b/c;->c()[Lio/reactivex/o;

    move-result-object v0

    invoke-virtual {v6, v0}, Lkotlin/e/b/x;->a(Ljava/lang/Object;)V

    invoke-virtual {v6}, Lkotlin/e/b/x;->a()I

    move-result v0

    new-array v0, v0, [Lio/reactivex/s;

    invoke-virtual {v6, v0}, Lkotlin/e/b/x;->a([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/reactivex/s;

    .line 53
    invoke-static {v0}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026*bindAdditionalActions())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/c/b/c;->b()Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object v1

    new-instance v2, Lcom/swedbank/mobile/app/cards/c/b/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/cards/c/b/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/cards/c/b/c$a;-><init>(Lcom/swedbank/mobile/app/cards/c/b/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 119
    new-instance v3, Lcom/swedbank/mobile/app/cards/c/b/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/cards/c/b/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 120
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v1, Lcom/swedbank/mobile/app/cards/c/b/c$b;->a:Lcom/swedbank/mobile/app/cards/c/b/c$b;

    check-cast v1, Lio/reactivex/c/d;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/d;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026, curr -> prev === curr }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method

.method protected b()Lcom/swedbank/mobile/app/cards/list/r;
    .locals 10
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 86
    new-instance v9, Lcom/swedbank/mobile/app/cards/list/r;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/list/r;-><init>(ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILkotlin/e/b/g;)V

    return-object v9
.end method

.method protected c()[Lio/reactivex/o;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/cards/list/r$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x1

    .line 66
    new-array v1, v0, [Lio/reactivex/o;

    .line 67
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->b:Lcom/swedbank/mobile/business/cards/notauth/list/d;

    .line 68
    invoke-interface {v2}, Lcom/swedbank/mobile/business/cards/notauth/list/d;->m()Lio/reactivex/o;

    move-result-object v2

    .line 69
    invoke-static {}, Lcom/swedbank/mobile/app/cards/c/b/e;->a()Lkotlin/e/a/m;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v4, v3, v0, v4}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    .line 70
    sget-object v3, Lcom/swedbank/mobile/app/cards/c/b/c$n;->a:Lcom/swedbank/mobile/app/cards/c/b/c$n;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 75
    new-instance v3, Lcom/swedbank/mobile/app/cards/c/b/c$o;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/cards/c/b/c$o;-><init>(Lcom/swedbank/mobile/app/cards/c/b/c;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 83
    iget-object v3, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->a:Lcom/swedbank/mobile/core/ui/x;

    const/4 v5, 0x0

    invoke-static {v3, v5, v0, v4}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {v2, v0}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v2, "interactor\n          .ob\u2026ngRenderStream.loading())"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    aput-object v0, v1, v5

    return-object v1
.end method

.method protected final d()Lcom/swedbank/mobile/core/ui/x;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/c/b/c;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object v0
.end method
