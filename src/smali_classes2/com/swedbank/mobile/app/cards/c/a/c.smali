.class public final Lcom/swedbank/mobile/app/cards/c/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "NotAuthenticatedEmptyCardsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/c/a/g;",
        "Lcom/swedbank/mobile/app/cards/c/a/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/notauth/empty/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/notauth/empty/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/notauth/empty/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/c/a/c;->a:Lcom/swedbank/mobile/business/cards/notauth/empty/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/c/a/c;)Lcom/swedbank/mobile/business/cards/notauth/empty/a;
    .locals 0

    .line 7
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/a/c;->a:Lcom/swedbank/mobile/business/cards/notauth/empty/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 2

    .line 12
    sget-object v0, Lcom/swedbank/mobile/app/cards/c/a/c$a;->a:Lcom/swedbank/mobile/app/cards/c/a/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/c/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 13
    new-instance v1, Lcom/swedbank/mobile/app/cards/c/a/c$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/c/a/c$b;-><init>(Lcom/swedbank/mobile/app/cards/c/a/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "action(NotAuthenticatedE\u2026tedEmptyCardsViewState>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
