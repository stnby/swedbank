.class public final Lcom/swedbank/mobile/app/cards/c/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "NotAuthenticatedCardsRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/notauth/d;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/cards/c/b/a;

.field private final f:Lcom/swedbank/mobile/app/cards/f/c/c;

.field private final g:Lcom/swedbank/mobile/app/f/a;

.field private final h:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lkotlin/h/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation
.end field

.field private final j:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

.field private final k:Lcom/swedbank/mobile/business/cards/notauth/list/h;

.field private final l:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private final m:Lcom/swedbank/mobile/business/general/confirmation/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/c/b/a;Lcom/swedbank/mobile/app/cards/f/c/c;Lcom/swedbank/mobile/app/f/a;Lkotlin/e/a/a;Lkotlin/h/b;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Lcom/swedbank/mobile/business/cards/notauth/list/h;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 16
    .param p1    # Lcom/swedbank/mobile/app/cards/c/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/f/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/e/a/a;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lkotlin/h/b;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/cards/notauth/list/h;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "outdated_card_shortcut_help_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_general_error_notification_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_not_authenticated_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/cards/c/b/a;",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;",
            "Lkotlin/h/b<",
            "*>;",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            "Lcom/swedbank/mobile/business/cards/notauth/list/h;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    const-string v0, "notAuthCardsListBuilder"

    invoke-static {v7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPreconditionsResolverBuilder"

    invoke-static {v8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogBuilder"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emptyStateBuilder"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emptyStateRouterType"

    invoke-static {v11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPreconditionsResolverListener"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notAuthCardsListListener"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outdatedCardShortcutHelpListener"

    invoke-static {v14, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletGeneralErrorNotificationListener"

    invoke-static {v15, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    move-object/from16 v1, p10

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    move-object/from16 v2, p11

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    .line 46
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object v7, v6, Lcom/swedbank/mobile/app/cards/c/c;->e:Lcom/swedbank/mobile/app/cards/c/b/a;

    iput-object v8, v6, Lcom/swedbank/mobile/app/cards/c/c;->f:Lcom/swedbank/mobile/app/cards/f/c/c;

    iput-object v9, v6, Lcom/swedbank/mobile/app/cards/c/c;->g:Lcom/swedbank/mobile/app/f/a;

    iput-object v10, v6, Lcom/swedbank/mobile/app/cards/c/c;->h:Lkotlin/e/a/a;

    iput-object v11, v6, Lcom/swedbank/mobile/app/cards/c/c;->i:Lkotlin/h/b;

    iput-object v12, v6, Lcom/swedbank/mobile/app/cards/c/c;->j:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    iput-object v13, v6, Lcom/swedbank/mobile/app/cards/c/c;->k:Lcom/swedbank/mobile/business/cards/notauth/list/h;

    iput-object v14, v6, Lcom/swedbank/mobile/app/cards/c/c;->l:Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object v15, v6, Lcom/swedbank/mobile/app/cards/c/c;->m:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/c/c;)Lkotlin/h/b;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->i:Lkotlin/h/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/c/c;Lkotlin/e/a/b;)V
    .locals 0

    .line 34
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/c/c;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/app/cards/c/b/a;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->e:Lcom/swedbank/mobile/app/cards/c/b/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/c/c;Lkotlin/e/a/b;)V
    .locals 0

    .line 34
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/c/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/business/cards/notauth/list/h;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->k:Lcom/swedbank/mobile/business/cards/notauth/list/h;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/c/c;)Lkotlin/e/a/a;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->h:Lkotlin/e/a/a;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/app/cards/f/c/c;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->f:Lcom/swedbank/mobile/app/cards/f/c/c;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->j:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->g:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->l:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/cards/c/c;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/c/c;->m:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/cards/wallet/w;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/w;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/c$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/cards/c/c$c;-><init>(Lcom/swedbank/mobile/app/cards/c/c;Lcom/swedbank/mobile/business/cards/wallet/w;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.create { emitter -\u2026achAsChild() })\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 48
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/c$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/c/c$d;-><init>(Lcom/swedbank/mobile/app/cards/c/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/c/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)V"
        }
    .end annotation

    const-string v0, "notSatisfiedPreconditions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/c$f;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/cards/c/c$f;-><init>(Lcom/swedbank/mobile/app/cards/c/c;Ljava/util/List;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/c/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 61
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/c$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/c/c$e;-><init>(Lcom/swedbank/mobile/app/cards/c/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/c/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 109
    sget-object v0, Lcom/swedbank/mobile/app/cards/c/c$a;->a:Lcom/swedbank/mobile/app/cards/c/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 83
    new-instance v0, Lcom/swedbank/mobile/app/cards/c/c$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/c/c$b;-><init>(Lcom/swedbank/mobile/app/cards/c/c;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026achAsChild() })\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()V
    .locals 1

    const-string v0, "outdated_card_shortcut_help_listener"

    .line 93
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/g;->a(Ljava/lang/String;)Lkotlin/e/a/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/c/c;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public f()V
    .locals 1

    const-string v0, "wallet_general_error_notification_listener"

    .line 106
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/g;->a(Ljava/lang/String;)Lkotlin/e/a/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/c/c;->c(Lkotlin/e/a/b;)V

    return-void
.end method
