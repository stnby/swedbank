.class final Lcom/swedbank/mobile/app/cards/f/b/b/d$c;
.super Lkotlin/e/b/k;
.source "WalletPaymentBackgroundRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/b/b/d;->a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

.field final synthetic b:Lcom/swedbank/mobile/business/util/e;

.field final synthetic c:Z

.field final synthetic d:Lio/reactivex/x;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/b/b/d;Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->b:Lcom/swedbank/mobile/business/util/e;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->d:Lio/reactivex/x;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    check-cast p1, Ljava/lang/Iterable;

    .line 111
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 61
    instance-of v1, v1, Lcom/swedbank/mobile/business/cards/wallet/payment/m;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 113
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 62
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;-><init>(Lcom/swedbank/mobile/app/cards/f/b/b/d$c;)V

    if-nez v0, :cond_2

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->b:Lcom/swedbank/mobile/business/util/e;

    .line 82
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->c:Z

    .line 83
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->d:Lio/reactivex/x;

    .line 80
    invoke-virtual {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/l;

    .line 87
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/l;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->b:Lcom/swedbank/mobile/business/util/e;

    invoke-static {v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/h;->a(Lcom/swedbank/mobile/business/util/e;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 88
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->b()V

    .line 90
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->b:Lcom/swedbank/mobile/business/util/e;

    .line 91
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->c:Z

    .line 92
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->d:Lio/reactivex/x;

    .line 89
    invoke-virtual {p1, v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V

    goto :goto_1

    .line 95
    :cond_3
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->b:Lcom/swedbank/mobile/business/util/e;

    invoke-interface {v0, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/l;->a(Lcom/swedbank/mobile/business/util/e;)V

    .line 96
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->d:Lio/reactivex/x;

    if-eqz p1, :cond_4

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    :cond_4
    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
