.class public final Lcom/swedbank/mobile/app/cards/f/b/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "WalletPaymentRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/m;


# instance fields
.field private final e:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/data/device/a;

.field private final g:Lcom/swedbank/mobile/core/a/c;

.field private final h:Lcom/swedbank/mobile/business/e/b;

.field private final i:Lcom/swedbank/mobile/app/cards/f/b/a/b;

.field private final j:Lcom/swedbank/mobile/app/cards/f/b/d/a;

.field private final k:Lcom/swedbank/mobile/app/cards/f/b/c/a;

.field private final l:Lcom/swedbank/mobile/app/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/app/a/d<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/swedbank/mobile/app/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/app/a/d<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

.field private final o:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

.field private final p:Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/core/a/c;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/app/cards/f/b/a/b;Lcom/swedbank/mobile/app/cards/f/b/d/a;Lcom/swedbank/mobile/app/cards/f/b/c/a;Lcom/swedbank/mobile/app/a/d;Lcom/swedbank/mobile/app/a/d;Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 18
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/cards/f/b/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/cards/f/b/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/app/cards/f/b/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/app/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "trackWalletPaySuccessEvent"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/app/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "trackWalletPayTryAgainEvent"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_payment"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p13    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/core/a/c;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/app/cards/f/b/a/b;",
            "Lcom/swedbank/mobile/app/cards/f/b/d/a;",
            "Lcom/swedbank/mobile/app/cards/f/b/c/a;",
            "Lcom/swedbank/mobile/app/a/d<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;",
            ">;",
            "Lcom/swedbank/mobile/app/a/d<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p7

    move-object/from16 v14, p8

    move-object/from16 v15, p9

    move-object/from16 v5, p10

    move-object/from16 v4, p11

    const-string v0, "activityManager"

    invoke-static {v7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsManager"

    invoke-static {v8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentAuthenticationBuilder"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentTapBuilder"

    invoke-static {v11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentResultBuilder"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trackWalletPaySuccessEvent"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trackWalletPayTryAgainEvent"

    invoke-static {v14, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentAuthenticationListener"

    invoke-static {v15, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentTapListener"

    invoke-static {v5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentResultListener"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    move-object/from16 v1, p12

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    move-object/from16 v2, p13

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/16 v16, 0x4

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v4, v16

    move-object/from16 v5, v17

    .line 47
    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object v7, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    iput-object v8, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->g:Lcom/swedbank/mobile/core/a/c;

    iput-object v9, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->h:Lcom/swedbank/mobile/business/e/b;

    iput-object v10, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->i:Lcom/swedbank/mobile/app/cards/f/b/a/b;

    iput-object v11, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->j:Lcom/swedbank/mobile/app/cards/f/b/d/a;

    iput-object v12, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->k:Lcom/swedbank/mobile/app/cards/f/b/c/a;

    iput-object v13, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->l:Lcom/swedbank/mobile/app/a/d;

    iput-object v14, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->m:Lcom/swedbank/mobile/app/a/d;

    iput-object v15, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->n:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

    move-object/from16 v0, p10

    iput-object v0, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->o:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    move-object/from16 v0, p11

    iput-object v0, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->p:Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;

    .line 48
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<WalletTapState>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v6, Lcom/swedbank/mobile/app/cards/f/b/c;->e:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/data/device/a;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    return-object p0
.end method

.method private final a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V
    .locals 1

    .line 116
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/c$g;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/cards/f/b/c$g;-><init>(Lcom/swedbank/mobile/app/cards/f/b/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/app/cards/f/b/d/a;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->j:Lcom/swedbank/mobile/app/cards/f/b/d/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->o:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/b/c/c;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->e:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/app/cards/f/b/a/b;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->i:Lcom/swedbank/mobile/app/cards/f/b/a/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->n:Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/core/a/c;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->g:Lcom/swedbank/mobile/core/a/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 4

    .line 93
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c$d;->a:Lcom/swedbank/mobile/app/cards/f/b/c$d;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->k:Lcom/swedbank/mobile/app/cards/f/b/c/a;

    .line 150
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->p:Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/c/a;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/d;)Lcom/swedbank/mobile/app/cards/f/b/c/a;

    move-result-object v0

    .line 151
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/f/b/c/a;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;)Lcom/swedbank/mobile/app/cards/f/b/c/a;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/b/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 174
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    .line 155
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->l:Lcom/swedbank/mobile/app/a/d;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/a/d;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 156
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    if-eqz v0, :cond_1

    .line 157
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    if-eq v1, v2, :cond_1

    .line 158
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->g:Lcom/swedbank/mobile/core/a/c;

    new-instance v2, Lcom/swedbank/mobile/app/cards/x;

    .line 159
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;->a()Ljava/lang/String;

    move-result-object v3

    .line 160
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;->b()Ljava/lang/String;

    move-result-object p1

    .line 161
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;->d()I

    move-result v0

    .line 158
    invoke-direct {v2, v3, p1, v0}, Lcom/swedbank/mobile/app/cards/x;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    check-cast v2, Lcom/swedbank/mobile/core/a/a;

    invoke-interface {v1, v2}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "selectedCardForPayment"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    .line 101
    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/payment/n$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "selectedCardForPayment"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "event"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    .line 106
    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V

    .line 109
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->m:Lcom/swedbank/mobile/app/a/d;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/app/a/d;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Lkotlin/e/a/a;)V
    .locals 9
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "afterDisplay"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->h:Lcom/swedbank/mobile/business/e/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    .line 56
    invoke-interface {v0}, Lcom/swedbank/mobile/data/device/a;->b()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 57
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "activityManager\n        \u2026()\n              .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 58
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/c$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/cards/f/b/c$a;-><init>(Lkotlin/e/a/a;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 168
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    .line 62
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    invoke-interface {p1}, Lcom/swedbank/mobile/data/device/a;->c()V

    :goto_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .line 172
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/b/c$e;->a:Lcom/swedbank/mobile/app/cards/f/b/c$e;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    if-eqz p1, :cond_0

    .line 145
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->g:Lcom/swedbank/mobile/core/a/c;

    sget-object v0, Lcom/swedbank/mobile/app/cards/q;->a:Lcom/swedbank/mobile/app/cards/q;

    check-cast v0, Lcom/swedbank/mobile/core/a/a;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .line 99
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/b/c;->r()V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "selectedCardForPayment"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    .line 112
    invoke-direct {p0, p1, v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V

    return-void
.end method

.method public b(Lkotlin/e/a/a;)V
    .locals 9
    .param p1    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "afterDisplay"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->h:Lcom/swedbank/mobile/business/e/b;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/e/b;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c$b;->a:Lcom/swedbank/mobile/app/cards/f/b/c$b;

    check-cast v1, Lkotlin/e/a/b;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2, v3}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    .line 73
    invoke-interface {p1}, Lkotlin/e/a/a;->f_()Ljava/lang/Object;

    goto :goto_0

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    .line 77
    invoke-interface {v0}, Lcom/swedbank/mobile/data/device/a;->b()Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 78
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v3

    const-string v0, "activityManager\n        \u2026es()\n            .take(1)"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 79
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/c$c;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/c$c;-><init>(Lcom/swedbank/mobile/app/cards/f/b/c;Lkotlin/e/a/a;)V

    move-object v6, v0

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object p1

    .line 170
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lio/reactivex/b/c;)V

    .line 87
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c;->f:Lcom/swedbank/mobile/data/device/a;

    invoke-interface {p1}, Lcom/swedbank/mobile/data/device/a;->c()V

    :goto_0
    return-void
.end method

.method public c()V
    .locals 1

    .line 133
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/c$f;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/f/b/c$f;-><init>(Lcom/swedbank/mobile/app/cards/f/b/c;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method
