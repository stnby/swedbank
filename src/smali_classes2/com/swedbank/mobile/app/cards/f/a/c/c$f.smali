.class final Lcom/swedbank/mobile/app/cards/f/a/c/c$f;
.super Ljava/lang/Object;
.source "WalletOnboardingDigitizationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/a/c/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/n<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/f/a/c/c$f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/c/c$f;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/f/a/c/c$f;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/a/c/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/c/c$f;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/j;
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/b;",
            ">;)",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    check-cast p1, Ljava/lang/Iterable;

    .line 58
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/business/cards/wallet/b;

    .line 27
    instance-of v2, v2, Lcom/swedbank/mobile/business/cards/wallet/b$a;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/b;

    if-eqz v0, :cond_2

    .line 29
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/c/j;

    const/4 v2, 0x0

    .line 30
    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/b$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/b$a;->b()Ljava/lang/Throwable;

    move-result-object v0

    const/4 v3, 0x1

    .line 29
    invoke-direct {p1, v2, v0, v3, v1}, Lcom/swedbank/mobile/app/cards/f/a/c/j;-><init>(ZLjava/lang/Throwable;ILkotlin/e/b/g;)V

    .line 60
    invoke-static {p1}, Lio/reactivex/j;->b(Ljava/lang/Object;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.just(this)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 32
    :cond_2
    invoke-static {}, Lio/reactivex/j;->a()Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.empty()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/a/c/c$f;->a(Ljava/util/List;)Lio/reactivex/j;

    move-result-object p1

    return-object p1
.end method
