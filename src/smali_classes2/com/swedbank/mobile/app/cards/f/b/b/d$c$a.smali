.class final Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;
.super Lkotlin/e/b/k;
.source "WalletPaymentBackgroundRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/q;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a(Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/q<",
        "Lcom/swedbank/mobile/business/util/e<",
        "Ljava/lang/String;",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lio/reactivex/x<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/f/b/b/d$c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/b/b/d$c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d$c;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/business/util/e;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    check-cast p3, Lio/reactivex/x;

    invoke-virtual {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/x;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;Z",
            "Lio/reactivex/x<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
            ">;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d$c;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

    .line 66
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d$c;

    iget-object v1, v1, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->b(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/app/cards/f/b/a;

    move-result-object v1

    .line 67
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d$c;

    iget-object v2, v2, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

    invoke-static {v2}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->c(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/app/cards/f/b/a;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/app/cards/f/b/a;

    move-result-object v1

    .line 68
    invoke-virtual {v1, p1, p2}, Lcom/swedbank/mobile/app/cards/f/b/a;->a(Lcom/swedbank/mobile/business/util/e;Z)Lcom/swedbank/mobile/app/cards/f/b/a;

    move-result-object p2

    .line 69
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/f/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p2

    if-eqz p3, :cond_0

    .line 71
    invoke-virtual {p2}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v1

    invoke-interface {p3, v1}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    .line 72
    :cond_0
    iget-object p3, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c$a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d$c;

    iget-object p3, p3, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d;

    invoke-static {p3}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->d(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/app/a/d;

    move-result-object p3

    .line 112
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/n;

    .line 74
    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/n;->a()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 113
    :cond_1
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 115
    :goto_0
    invoke-virtual {p3, p1}, Lcom/swedbank/mobile/app/a/d;->c(Ljava/lang/Object;)V

    .line 116
    invoke-static {v0, p2}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void

    .line 114
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
