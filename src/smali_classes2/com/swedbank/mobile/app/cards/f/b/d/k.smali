.class public final Lcom/swedbank/mobile/app/cards/f/b/d/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "WalletPaymentTapViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/f/b/d/j;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/os/Vibrator;

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/d/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardView"

    const-string v4, "getCardView()Lcom/swedbank/mobile/app/cards/CardLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/d/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "tapInfoView"

    const-string v4, "getTapInfoView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/d/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/d/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "tryAgainViews"

    const-string v4, "getTryAgainViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->h:Lio/reactivex/o;

    .line 29
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_view:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->b:Lkotlin/f/c;

    .line 30
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_tap_info:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->c:Lkotlin/f/c;

    .line 31
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_tap_cancel:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->d:Lkotlin/f/c;

    .line 32
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_tap_try_again_views:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->e:Lkotlin/f/c;

    .line 34
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->f:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/d/k;)Lcom/swedbank/mobile/app/cards/CardLayout;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->c()Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/b/d/k;)Landroid/widget/TextView;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->d()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/b/d/k;)Landroid/view/View;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->g()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final c()Lcom/swedbank/mobile/app/cards/CardLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/CardLayout;

    return-object v0
.end method

.method private final d()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final f()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final g()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->f:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/cards/f/b/d/n;)V
    .locals 9
    .param p1    # Lcom/swedbank/mobile/app/cards/f/b/d/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/b/d/n;->a()Lcom/swedbank/mobile/app/cards/c;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/app/cards/f/b/d/k;

    .line 84
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a(Lcom/swedbank/mobile/app/cards/f/b/d/k;)Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object v2

    .line 85
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->c()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->d()Ljava/lang/String;

    move-result-object v4

    .line 87
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->e()Ljava/lang/String;

    move-result-object v5

    .line 88
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->h()Z

    move-result v6

    .line 89
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->f()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v7

    .line 90
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->g()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v8

    .line 84
    invoke-virtual/range {v2 .. v8}, Lcom/swedbank/mobile/app/cards/CardLayout;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/g;)V

    .line 50
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/b/d/n;->b()Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    move-result-object v0

    .line 93
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->b(Lcom/swedbank/mobile/app/cards/f/b/d/k;)Landroid/widget/TextView;

    move-result-object v1

    .line 94
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/b/d/l;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 97
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_try_again_tap_info:I

    goto :goto_0

    .line 96
    :pswitch_0
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_after_auth_tap_info:I

    goto :goto_0

    .line 95
    :pswitch_1
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_tap_info:I

    .line 93
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 100
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->c(Lcom/swedbank/mobile/app/cards/f/b/d/k;)Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    const/4 v3, 0x4

    if-ne v0, v2, :cond_1

    const/4 v0, 0x4

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 51
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/b/d/n;->c()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 52
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->g:Landroid/os/Vibrator;

    if-nez p1, :cond_2

    const-string v0, "vibrator"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    .line 102
    :cond_2
    new-array v0, v3, [J

    fill-array-data v0, :array_0

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 54
    :cond_3
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->f:Lcom/b/c/c;

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 8
        0x0
        0x46
        0x46
        0x46
    .end array-data
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/b/d/n;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->a(Lcom/swedbank/mobile/app/cards/f/b/d/n;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->f()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 82
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->h:Lio/reactivex/o;

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {v1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "cancelBtn\n      .clicks(\u2026   .mergeWith(backClicks)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected e()V
    .locals 2

    .line 38
    invoke-super {p0}, Lcom/swedbank/mobile/architect/a/b/a;->e()V

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/b/d/k;->n()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    .line 81
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/k;->g:Landroid/os/Vibrator;

    return-void

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.os.Vibrator"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
