.class public final Lcom/swedbank/mobile/app/cards/f/b/c/h;
.super Lcom/swedbank/mobile/architect/a/b/a/a;
.source "WalletPaymentResultTransition.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/f/b/c/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/c/h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/f/b/c/h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/b/c/h;->a:Lcom/swedbank/mobile/app/cards/f/b/c/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)Landroidx/l/n;
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 24
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_transition_target:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026g_card_transition_target)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget v1, Lcom/swedbank/mobile/app/cards/n$f;->anim_default_dur:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v1, v1

    const/4 v3, 0x1

    if-eqz p4, :cond_0

    .line 28
    sget p2, Lcom/swedbank/mobile/app/cards/n$h;->tag_wallet_payment_transition_target:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "resources.getString(R.st\u2026ayment_transition_target)"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    sget p3, Lcom/swedbank/mobile/app/cards/n$f;->anim_short_dur:I

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    int-to-long p3, p1

    .line 31
    new-instance p1, Landroidx/l/r;

    invoke-direct {p1}, Landroidx/l/r;-><init>()V

    .line 32
    new-instance v4, Lcom/swedbank/mobile/core/ui/a/h;

    invoke-direct {v4}, Lcom/swedbank/mobile/core/ui/a/h;-><init>()V

    .line 33
    invoke-virtual {v4, v1, v2}, Lcom/swedbank/mobile/core/ui/a/h;->setDuration(J)Landroidx/l/n;

    move-result-object v1

    .line 34
    invoke-virtual {v1, v0}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object v0

    .line 32
    invoke-virtual {p1, v0}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 35
    new-instance v0, Landroidx/l/d;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroidx/l/d;-><init>(I)V

    .line 36
    invoke-virtual {v0, p3, p4}, Landroidx/l/d;->setDuration(J)Landroidx/l/n;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p2}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object v0

    .line 35
    invoke-virtual {p1, v0}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 38
    new-instance v0, Landroidx/l/d;

    invoke-direct {v0, v3}, Landroidx/l/d;-><init>(I)V

    .line 39
    invoke-virtual {v0, p3, p4}, Landroidx/l/d;->setDuration(J)Landroidx/l/n;

    move-result-object p3

    .line 40
    invoke-virtual {p3, p2}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 38
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    const-string p2, "TransitionSet()\n        \u2026paymentTransitionTarget))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/l/n;

    goto/16 :goto_2

    :cond_0
    if-eqz p3, :cond_1

    if-eqz p2, :cond_1

    .line 58
    sget p4, Lcom/swedbank/mobile/app/cards/n$e;->cards_list:I

    invoke-virtual {p2, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    if-eqz p4, :cond_1

    .line 60
    sget v4, Lcom/swedbank/mobile/app/cards/n$e;->card_view:I

    invoke-virtual {p3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/swedbank/mobile/app/cards/CardLayout;

    if-eqz p3, :cond_1

    .line 61
    invoke-virtual {p3}, Lcom/swedbank/mobile/app/cards/CardLayout;->b()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 62
    invoke-virtual {p4, p3}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_2

    .line 65
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_full_loading:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_2

    const/16 p4, 0x8

    invoke-virtual {p3, p4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const/4 p3, 0x0

    if-eqz p2, :cond_3

    .line 67
    sget p4, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_root_view:I

    invoke-virtual {p2, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_3
    move-object p2, p3

    :goto_0
    const/4 p4, 0x0

    if-eqz p2, :cond_4

    const/4 p2, 0x1

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    .line 45
    :goto_1
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_list_transition_target:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v4, "resources.getString(R.st\u2026d_list_transition_target)"

    invoke-static {p1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v4, Landroidx/l/r;

    invoke-direct {v4}, Landroidx/l/r;-><init>()V

    .line 47
    invoke-virtual {v4, v1, v2}, Landroidx/l/r;->a(J)Landroidx/l/r;

    move-result-object v4

    .line 48
    new-instance v5, Lcom/swedbank/mobile/core/ui/a/b;

    invoke-direct {v5, p4, p2, v3, p3}, Lcom/swedbank/mobile/core/ui/a/b;-><init>(ZZILkotlin/e/b/g;)V

    .line 49
    invoke-virtual {v5, v0}, Lcom/swedbank/mobile/core/ui/a/b;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 48
    invoke-virtual {v4, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p2

    .line 50
    new-instance p3, Lcom/swedbank/mobile/core/ui/a/f;

    invoke-direct {p3}, Lcom/swedbank/mobile/core/ui/a/f;-><init>()V

    .line 51
    invoke-virtual {p3, v1, v2}, Lcom/swedbank/mobile/core/ui/a/f;->setDuration(J)Landroidx/l/n;

    move-result-object p3

    .line 52
    invoke-virtual {p3, p1}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p1

    .line 53
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->navigation_bar:I

    invoke-virtual {p1, p3}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    move-result-object p1

    .line 50
    invoke-virtual {p2, p1}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    const-string p2, "TransitionSet()\n        \u2026get(R.id.navigation_bar))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/l/n;

    :goto_2
    return-object p1
.end method
