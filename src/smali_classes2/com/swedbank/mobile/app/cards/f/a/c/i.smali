.class public final Lcom/swedbank/mobile/app/cards/f/a/c/i;
.super Ljava/lang/Object;
.source "WalletOnboardingDigitizationViewImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/c/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/c/i;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/c/i;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;>;)",
            "Lcom/swedbank/mobile/app/cards/f/a/c/i;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/c/i;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/f/a/c/i;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/f/a/c/h;
    .locals 2

    .line 19
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/c/h;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/c/i;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/o;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/cards/f/a/c/h;-><init>(Lio/reactivex/o;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/a/c/i;->a()Lcom/swedbank/mobile/app/cards/f/a/c/h;

    move-result-object v0

    return-object v0
.end method
