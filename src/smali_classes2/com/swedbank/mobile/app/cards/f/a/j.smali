.class public final Lcom/swedbank/mobile/app/cards/f/a/j;
.super Lcom/swedbank/mobile/architect/a/b/a/b;
.source "WalletOnboardingStepTransition.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/ViewGroup;Landroid/view/View;ZLandroid/view/View;Z)Landroid/animation/Animator;
    .locals 9
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x3f266666    # 0.65f

    const-wide/16 v0, 0xdc

    const/high16 v2, 0x40000000    # 2.0f

    const-wide/16 v3, 0x96

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-eqz p5, :cond_6

    .line 23
    sget p5, Lcom/swedbank/mobile/app/cards/n$h;->tag_wallet_onboarding_step:I

    invoke-static {p4, p5}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;I)Z

    move-result p5

    if-eqz p5, :cond_4

    .line 92
    new-instance p5, Landroid/animation/AnimatorSet;

    invoke-direct {p5}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz p3, :cond_0

    .line 94
    sget-object p3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v0, v6, [F

    fill-array-data v0, :array_0

    invoke-static {p2, p3, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    check-cast p2, Landroid/animation/Animator;

    invoke-virtual {p5, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_0
    if-eqz p4, :cond_3

    .line 98
    instance-of p2, p4, Landroid/view/ViewGroup;

    if-eqz p2, :cond_2

    .line 99
    check-cast p4, Landroid/view/ViewGroup;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    new-array p1, p1, [Landroid/animation/Animator;

    .line 101
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    const/4 p3, 0x0

    :goto_0
    if-ge p3, p2, :cond_1

    .line 102
    invoke-virtual {p4, p3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "getChildAt(index)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v7, [F

    aput v5, v2, v8

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    aput-object v0, p1, p3

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    .line 106
    :cond_1
    array-length p2, p1

    invoke-static {p1, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/animation/Animator;

    invoke-virtual {p5, p1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_1

    .line 108
    :cond_2
    sget-object p2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array p3, v7, [F

    aput p1, p3, v8

    invoke-static {p4, p2, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    check-cast p1, Landroid/animation/Animator;

    invoke-virtual {p5, p1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 111
    :cond_3
    :goto_1
    check-cast p5, Landroid/animation/Animator;

    return-object p5

    .line 112
    :cond_4
    new-instance p1, Landroid/animation/AnimatorSet;

    invoke-direct {p1}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz p3, :cond_5

    if-eqz p2, :cond_5

    .line 114
    new-array p3, v6, [Landroid/animation/Animator;

    .line 115
    sget-object p4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array p5, v6, [F

    fill-array-data p5, :array_1

    invoke-static {p2, p4, p5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p4

    .line 118
    invoke-virtual {p4, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object p4

    const-string p5, "it"

    .line 117
    invoke-static {p4, p5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object p5

    check-cast p5, Landroid/animation/TimeInterpolator;

    invoke-virtual {p4, p5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast p4, Landroid/animation/Animator;

    aput-object p4, p3, v8

    .line 120
    sget-object p4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array p5, v6, [F

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v2

    aput v3, p5, v8

    aput v5, p5, v7

    invoke-static {p2, p4, p5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    .line 123
    invoke-virtual {p2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object p2

    const-string p4, "it"

    .line 122
    invoke-static {p2, p4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object p4

    check-cast p4, Landroid/animation/TimeInterpolator;

    invoke-virtual {p2, p4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast p2, Landroid/animation/Animator;

    aput-object p2, p3, v7

    .line 114
    invoke-virtual {p1, p3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 127
    :cond_5
    check-cast p1, Landroid/animation/Animator;

    return-object p1

    .line 28
    :cond_6
    sget p5, Lcom/swedbank/mobile/app/cards/n$h;->tag_wallet_onboarding_step:I

    invoke-static {p2, p5}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;I)Z

    move-result p5

    if-eqz p5, :cond_b

    .line 128
    new-instance p5, Landroid/animation/AnimatorSet;

    invoke-direct {p5}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz p3, :cond_7

    .line 130
    sget-object p3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v0, v6, [F

    fill-array-data v0, :array_2

    invoke-static {p2, p3, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    check-cast p2, Landroid/animation/Animator;

    invoke-virtual {p5, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_7
    if-eqz p4, :cond_a

    .line 134
    instance-of p2, p4, Landroid/view/ViewGroup;

    if-eqz p2, :cond_9

    .line 135
    check-cast p4, Landroid/view/ViewGroup;

    invoke-virtual {p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    new-array p1, p1, [Landroid/animation/Animator;

    .line 137
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    const/4 p3, 0x0

    :goto_2
    if-ge p3, p2, :cond_8

    .line 138
    invoke-virtual {p4, p3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "getChildAt(index)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v2, v7, [F

    aput v5, v2, v8

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    aput-object v0, p1, p3

    add-int/lit8 p3, p3, 0x1

    goto :goto_2

    .line 142
    :cond_8
    array-length p2, p1

    invoke-static {p1, p2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Landroid/animation/Animator;

    invoke-virtual {p5, p1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    goto :goto_3

    .line 144
    :cond_9
    sget-object p2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array p3, v7, [F

    aput p1, p3, v8

    invoke-static {p4, p2, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    check-cast p1, Landroid/animation/Animator;

    invoke-virtual {p5, p1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 147
    :cond_a
    :goto_3
    check-cast p5, Landroid/animation/Animator;

    return-object p5

    .line 148
    :cond_b
    new-instance p1, Landroid/animation/AnimatorSet;

    invoke-direct {p1}, Landroid/animation/AnimatorSet;-><init>()V

    if-eqz p4, :cond_c

    .line 150
    new-array p2, v6, [Landroid/animation/Animator;

    .line 151
    sget-object p3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array p5, v6, [F

    fill-array-data p5, :array_3

    invoke-static {p4, p3, p5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p3

    .line 154
    invoke-virtual {p3, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object p3

    const-string p5, "it"

    .line 153
    invoke-static {p3, p5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object p5

    check-cast p5, Landroid/animation/TimeInterpolator;

    invoke-virtual {p3, p5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast p3, Landroid/animation/Animator;

    aput-object p3, p2, v8

    .line 156
    sget-object p3, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array p5, v6, [F

    aput v5, p5, v8

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v2

    aput v3, p5, v7

    invoke-static {p4, p3, p5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p3

    .line 159
    invoke-virtual {p3, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object p3

    const-string p4, "it"

    .line 158
    invoke-static {p3, p4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->b()Landroid/view/animation/Interpolator;

    move-result-object p4

    check-cast p4, Landroid/animation/TimeInterpolator;

    invoke-virtual {p3, p4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    check-cast p3, Landroid/animation/Animator;

    aput-object p3, p2, v7

    .line 150
    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 163
    :cond_c
    check-cast p1, Landroid/animation/Animator;

    return-object p1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method protected a(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 36
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x0

    .line 37
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method
