.class public final Lcom/swedbank/mobile/app/cards/f/a/f/e;
.super Lcom/swedbank/mobile/app/cards/f/a/h;
.source "WalletOnboardingPreconditionsRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/a/a;


# instance fields
.field private final e:Lcom/swedbank/mobile/business/general/confirmation/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_preconditions"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_preconditions"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cancelConfirmationDialogBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelConfirmationDialogListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/cards/f/a/h;-><init>(Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/a/f/e;->e:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-void
.end method
