.class public final Lcom/swedbank/mobile/app/cards/f/a/g/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "WalletOnboardingRegistrationViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/f/a/g/g;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/d;

.field private final j:Lkotlin/f/c;

.field private final k:Lcom/swedbank/mobile/data/device/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "retryBtn"

    const-string v4, "getRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "notEligibleCompleteBtn"

    const-string v4, "getNotEligibleCompleteBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "notEligibleTitle"

    const-string v4, "getNotEligibleTitle()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "notEligibleDescription"

    const-string v4, "getNotEligibleDescription()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "retryViews"

    const-string v4, "getRetryViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "invalidityViews"

    const-string v4, "getInvalidityViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorInformationViews"

    const-string v4, "getErrorInformationViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->k:Lcom/swedbank/mobile/data/device/a;

    .line 26
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->b:Lkotlin/f/c;

    .line 27
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_retry:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->c:Lkotlin/f/c;

    .line 28
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_cancel:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d:Lkotlin/f/c;

    .line 29
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_not_eligible_complete:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e:Lkotlin/f/c;

    .line 30
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_error_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->f:Lkotlin/f/c;

    .line 31
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_error_description:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->g:Lkotlin/f/c;

    .line 33
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_retry_views:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->h:Lkotlin/f/c;

    .line 34
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/g/h$a;

    invoke-direct {p1, p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h$a;-><init>(Lcom/swedbank/mobile/app/cards/f/a/g/h;)V

    check-cast p1, Lkotlin/e/a/a;

    invoke-static {p1}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->i:Lkotlin/d;

    .line 35
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_registration_error_information_views:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->j:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->q()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->k()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->i()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->j()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Lcom/swedbank/mobile/data/device/a;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->k:Lcom/swedbank/mobile/data/device/a;

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->p()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final h()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/Button;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->h()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final i()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final j()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final k()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final p()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->i:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final q()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/h;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->f()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 108
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/cards/f/a/g/l;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/cards/f/a/g/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/g/l;->a()Z

    move-result v0

    .line 111
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 48
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/g/l;->b()Ljava/lang/Throwable;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    .line 114
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->c(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 116
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->c(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 118
    instance-of v4, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    if-eqz v4, :cond_2

    .line 119
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v4

    sget v5, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_network_problem_title:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 120
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v4

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$NetworkProblem;->b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;

    move-result-object v0

    sget-object v5, Lcom/swedbank/mobile/app/cards/f/a/g/i;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$a;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    .line 122
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_network_problem_description:I

    goto :goto_1

    .line 121
    :pswitch_1
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_no_network_description:I

    .line 122
    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 125
    :cond_2
    sget-object v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$GoogleServicesNotAvailable;

    invoke-static {v0, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v0

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_google_services_not_available_title:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 127
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v0

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_google_services_not_available_description:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 128
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->f(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Lcom/swedbank/mobile/data/device/a;

    move-result-object v0

    sget-object v4, Lcom/swedbank/mobile/app/cards/f/a/g/h$b;->a:Lcom/swedbank/mobile/app/cards/f/a/g/h$b;

    check-cast v4, Lkotlin/e/a/b;

    const/4 v5, 0x0

    invoke-static {v0, v5, v4, v1, v5}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    goto :goto_2

    .line 133
    :cond_3
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v0

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_unknown_problem_title:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 134
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v0

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_unknown_problem_description:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 49
    :goto_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/g/l;->c()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_4

    .line 140
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->g(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 142
    :cond_4
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->g(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 143
    instance-of v4, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    if-eqz v4, :cond_8

    .line 144
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v4

    sget v5, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_not_eligible_title:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 145
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v4

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;->b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    move-result-object v5

    .line 146
    instance-of v6, v5, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    if-eqz v6, :cond_6

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$UnsupportedDevice;->b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$b;->a()Lcom/swedbank/mobile/business/e/l;

    move-result-object v0

    .line 147
    sget-object v5, Lcom/swedbank/mobile/app/cards/f/a/g/j;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/e/l;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_1

    .line 164
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_2
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_debuggable:I

    goto :goto_3

    .line 163
    :pswitch_3
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_stacktrace:I

    goto :goto_3

    .line 162
    :pswitch_4
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_connection:I

    goto :goto_3

    .line 161
    :pswitch_5
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_device_files:I

    goto :goto_3

    .line 160
    :pswitch_6
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_strings:I

    goto :goto_3

    .line 159
    :pswitch_7
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_proc:I

    goto :goto_3

    .line 158
    :pswitch_8
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_memory:I

    goto :goto_3

    .line 157
    :pswitch_9
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_alcatel:I

    goto :goto_3

    .line 156
    :pswitch_a
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_test_keys:I

    goto :goto_3

    .line 155
    :pswitch_b
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_props:I

    goto :goto_3

    .line 154
    :pswitch_c
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_su:I

    goto :goto_3

    .line 153
    :pswitch_d
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_mgm_apps:I

    goto :goto_3

    .line 152
    :pswitch_e
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_dang_apps:I

    goto :goto_3

    .line 151
    :pswitch_f
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_cloak_apps:I

    goto :goto_3

    .line 150
    :pswitch_10
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_bin:I

    goto :goto_3

    .line 149
    :pswitch_11
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_bbox:I

    goto :goto_3

    .line 148
    :pswitch_12
    sget v0, Lcom/swedbank/mobile/core/a$f;->root_reason_general:I

    goto :goto_3

    .line 146
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.swedbank.mobile.business.cards.wallet.onboarding.registration.RegistrationError.UnsupportedDeviceReason.Rooted"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 166
    :cond_6
    sget-object v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError$b$a;

    invoke-static {v5, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_not_eligible_description:I

    :goto_3
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_4

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 169
    :cond_8
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->d(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v0

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_unknown_problem_title:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 170
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->e(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/widget/TextView;

    move-result-object v0

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_registration_unknown_problem_description:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 50
    :goto_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/g/l;->b()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/g/l;->c()Ljava/lang/Throwable;

    move-result-object p1

    if-eqz p1, :cond_9

    goto :goto_5

    :cond_9
    const/4 v1, 0x0

    .line 174
    :cond_a
    :goto_5
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->b(Lcom/swedbank/mobile/app/cards/f/a/g/h;)Landroid/view/View;

    move-result-object p1

    if-eqz v1, :cond_b

    const/4 v2, 0x0

    :cond_b
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/a/g/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->a(Lcom/swedbank/mobile/app/cards/f/a/g/l;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->g()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 109
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/h;->h()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 110
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method protected e()V
    .locals 0

    .line 42
    invoke-super {p0}, Lcom/swedbank/mobile/architect/a/b/a;->e()V

    .line 43
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/ap;->a(Lcom/swedbank/mobile/architect/a/b/a;)V

    return-void
.end method
