.class final Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;
.super Ljava/lang/Object;
.source "WalletPaymentTapPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/b/d/c$f;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;->a:Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/cards/f/b/d/n$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/b/d/d;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 36
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$b;->a:Lcom/swedbank/mobile/app/cards/f/b/d/n$a$b;

    new-instance v1, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V

    invoke-static {v0, v1}, Lio/reactivex/o;->a(Ljava/lang/Object;Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(PartialS\u2026artialState.TapState(it))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(PartialState.TapState(it))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/d/c$f$1;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
