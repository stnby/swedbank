.class public final Lcom/swedbank/mobile/app/cards/f/b/d/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletPaymentTapPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/b/d/j;",
        "Lcom/swedbank/mobile/app/cards/f/b/d/n;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/d/c;)Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 12

    .line 19
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/b/d/c$b;->a:Lcom/swedbank/mobile/app/cards/f/b/d/c$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/f/b/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 20
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/b/d/c$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/b/d/c$c;-><init>(Lcom/swedbank/mobile/app/cards/f/b/d/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;

    .line 25
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/a;->b()Lio/reactivex/o;

    move-result-object v1

    .line 26
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/b/d/c$d;->a:Lcom/swedbank/mobile/app/cards/f/b/d/c$d;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/b/d/c$e;->a:Lcom/swedbank/mobile/app/cards/f/b/d/c$e;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/b/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    const-wide/16 v3, 0x1

    .line 29
    invoke-virtual {v2, v3, v4}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v2

    .line 30
    new-instance v5, Lcom/swedbank/mobile/app/cards/f/b/d/c$f;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/cards/f/b/d/c$f;-><init>(Lcom/swedbank/mobile/app/cards/f/b/d/c;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v2, v5}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 42
    check-cast v0, Lio/reactivex/s;

    .line 43
    check-cast v1, Lio/reactivex/s;

    .line 44
    check-cast v2, Lio/reactivex/s;

    .line 41
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026,\n        tapRetryStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/b/d/n;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf

    const/4 v11, 0x0

    move-object v5, v1

    invoke-direct/range {v5 .. v11}, Lcom/swedbank/mobile/app/cards/f/b/d/n;-><init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;ZLjava/lang/Throwable;ILkotlin/e/b/g;)V

    .line 47
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/b/d/c$a;

    move-object v5, p0

    check-cast v5, Lcom/swedbank/mobile/app/cards/f/b/d/c;

    invoke-direct {v2, v5}, Lcom/swedbank/mobile/app/cards/f/b/d/c$a;-><init>(Lcom/swedbank/mobile/app/cards/f/b/d/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 61
    new-instance v5, Lcom/swedbank/mobile/app/cards/f/b/d/e;

    invoke-direct {v5, v2}, Lcom/swedbank/mobile/app/cards/f/b/d/e;-><init>(Lkotlin/e/a/m;)V

    check-cast v5, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v5}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v3, v4}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
