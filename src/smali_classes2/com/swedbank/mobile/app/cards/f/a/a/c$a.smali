.class final synthetic Lcom/swedbank/mobile/app/cards/f/a/a/c$a;
.super Lkotlin/e/b/i;
.source "WalletOnboardingContactlessPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/a/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/cards/f/a/a/k;",
        "Lcom/swedbank/mobile/app/cards/f/a/a/k$a;",
        "Lcom/swedbank/mobile/app/cards/f/a/a/k;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/a/a/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/cards/f/a/a/k;Lcom/swedbank/mobile/app/cards/f/a/a/k$a;)Lcom/swedbank/mobile/app/cards/f/a/a/k;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/cards/f/a/a/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/f/a/a/k$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/a/c;

    .line 36
    invoke-static {v0, p1, p2}, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a(Lcom/swedbank/mobile/app/cards/f/a/a/c;Lcom/swedbank/mobile/app/cards/f/a/a/k;Lcom/swedbank/mobile/app/cards/f/a/a/k$a;)Lcom/swedbank/mobile/app/cards/f/a/a/k;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/a/a/k;

    check-cast p2, Lcom/swedbank/mobile/app/cards/f/a/a/k$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/f/a/a/c$a;->a(Lcom/swedbank/mobile/app/cards/f/a/a/k;Lcom/swedbank/mobile/app/cards/f/a/a/k$a;)Lcom/swedbank/mobile/app/cards/f/a/a/k;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/cards/f/a/a/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/cards/wallet/onboarding/contactless/WalletOnboardingContactlessViewState;Lcom/swedbank/mobile/app/cards/wallet/onboarding/contactless/WalletOnboardingContactlessViewState$PartialState;)Lcom/swedbank/mobile/app/cards/wallet/onboarding/contactless/WalletOnboardingContactlessViewState;"

    return-object v0
.end method
