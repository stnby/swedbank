.class public final Lcom/swedbank/mobile/app/cards/f/b/c/m;
.super Ljava/lang/Object;
.source "WalletPaymentResultViewState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/cards/f/b/c/m$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/app/cards/c;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private final c:Z

.field private final d:Ljava/lang/Throwable;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/cards/f/b/c/m;-><init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;ILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;)V
    .locals 0
    .param p1    # Lcom/swedbank/mobile/app/cards/c;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->d:Ljava/lang/Throwable;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;ILkotlin/e/b/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 7
    move-object p1, v0

    check-cast p1, Lcom/swedbank/mobile/app/cards/c;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 8
    move-object p2, v0

    check-cast p2, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    const/4 p3, 0x0

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 10
    move-object p4, v0

    check-cast p4, Ljava/lang/Throwable;

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/cards/f/b/c/m;-><init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic a(Lcom/swedbank/mobile/app/cards/f/b/c/m;Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/f/b/c/m;
    .locals 0
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->d:Ljava/lang/Throwable;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;)Lcom/swedbank/mobile/app/cards/f/b/c/m;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/app/cards/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 7
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;)Lcom/swedbank/mobile/app/cards/f/b/c/m;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/cards/c;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/c/m;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/swedbank/mobile/app/cards/f/b/c/m;-><init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;)V

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x1

    if-eq p0, p1, :cond_2

    instance-of v1, p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    iget-object v3, p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    iget-object v3, p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    iget-boolean v3, p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->d:Ljava/lang/Throwable;

    iget-object p1, p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;->d:Ljava/lang/Throwable;

    invoke-static {v1, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    return v2

    :cond_2
    :goto_1
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->d:Ljava/lang/Throwable;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WalletPaymentResultViewState(card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a:Lcom/swedbank/mobile/app/cards/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emitFeedback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", fatalError="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/m;->d:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
