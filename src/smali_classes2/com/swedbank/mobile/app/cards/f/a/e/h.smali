.class public final Lcom/swedbank/mobile/app/cards/f/a/e/h;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "WalletOnboardingNfcViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/f/a/e/g;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "nfcEnabledIllustration"

    const-string v4, "getNfcEnabledIllustration()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "nfcDisabledIllustration"

    const-string v4, "getNfcDisabledIllustration()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "title"

    const-string v4, "getTitle()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "message"

    const-string v4, "getMessage()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "actionBtn"

    const-string v4, "getActionBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/e/h;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->h:Lio/reactivex/o;

    .line 22
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_nfc_how_illustration:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->b:Lkotlin/f/c;

    .line 23
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_nfc_illustration:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->c:Lkotlin/f/c;

    .line 24
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_nfc_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->d:Lkotlin/f/c;

    .line 25
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_nfc_message:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->e:Lkotlin/f/c;

    .line 26
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_nfc_action_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->f:Lkotlin/f/c;

    .line 27
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_nfc_cancel_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->g:Lkotlin/f/c;

    return-void
.end method

.method private final d()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final f()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final h()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final i()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final j()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->i()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 55
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/cards/f/a/e/j;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/cards/f/a/e/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/e/j;->a()Z

    move-result p1

    const/16 v0, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 40
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->d()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 41
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->f()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 42
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->g()Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_nfc_enabled_title:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 43
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->h()Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_nfc_enabled_text:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->i()Landroid/widget/Button;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_nfc_enabled_action_btn:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 46
    :cond_0
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->d()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 47
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->f()Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 48
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->g()Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_nfc_disabled_title:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 49
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->h()Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_nfc_disabled_text:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 50
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->i()Landroid/widget/Button;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_nfc_disabled_action_btn:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    :goto_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/a/e/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->a(Lcom/swedbank/mobile/app/cards/f/a/e/j;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/e/h;->j()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 56
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/h;->h:Lio/reactivex/o;

    return-object v0
.end method

.method protected e()V
    .locals 0

    .line 34
    invoke-super {p0}, Lcom/swedbank/mobile/architect/a/b/a;->e()V

    .line 35
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/ap;->a(Lcom/swedbank/mobile/architect/a/b/a;)V

    return-void
.end method
