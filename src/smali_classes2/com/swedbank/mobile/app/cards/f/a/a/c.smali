.class public final Lcom/swedbank/mobile/app/cards/f/a/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletOnboardingContactlessPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/a/h;",
        "Lcom/swedbank/mobile/app/cards/f/a/a/k;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/a/c;Lcom/swedbank/mobile/app/cards/f/a/a/k;Lcom/swedbank/mobile/app/cards/f/a/a/k$a;)Lcom/swedbank/mobile/app/cards/f/a/a/k;
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a(Lcom/swedbank/mobile/app/cards/f/a/a/k;Lcom/swedbank/mobile/app/cards/f/a/a/k$a;)Lcom/swedbank/mobile/app/cards/f/a/a/k;

    move-result-object p0

    return-object p0
.end method

.method private final a(Lcom/swedbank/mobile/app/cards/f/a/a/k;Lcom/swedbank/mobile/app/cards/f/a/a/k$a;)Lcom/swedbank/mobile/app/cards/f/a/a/k;
    .locals 7

    .line 43
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$b;->a:Lcom/swedbank/mobile/app/cards/f/a/a/k$a$b;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v0}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->a(ZLjava/lang/String;Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/cards/f/a/a/k;

    move-result-object p1

    goto :goto_0

    .line 47
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$a;

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    .line 49
    check-cast p2, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$a;->a()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p1

    .line 47
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->a(Lcom/swedbank/mobile/app/cards/f/a/a/k;ZLjava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/f/a/a/k;

    move-result-object p1

    goto :goto_0

    .line 50
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$c;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 52
    check-cast p2, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$c;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object p2

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/w/b;->a()Ljava/lang/Throwable;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p1

    .line 50
    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->a(Lcom/swedbank/mobile/app/cards/f/a/a/k;ZLjava/lang/String;Ljava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/f/a/a/k;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/a/c;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 16
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/a/a/c$e;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c$e;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 17
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/a/c$f;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/a/c$f;-><init>(Lcom/swedbank/mobile/app/cards/f/a/a/c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/c$b;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c$b;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 28
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/a/a/c$c;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c$c;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 26
    invoke-static {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 29
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/a/a/c$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/cards/f/a/a/c$d;-><init>(Lcom/swedbank/mobile/app/cards/f/a/a/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 30
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 33
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(enableC\u2026lessStream, cancelStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/a/k;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/swedbank/mobile/app/cards/f/a/a/k;-><init>(ZLjava/lang/String;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    .line 36
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/a/a/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/cards/f/a/a/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/cards/f/a/a/c$a;-><init>(Lcom/swedbank/mobile/app/cards/f/a/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 56
    new-instance v3, Lcom/swedbank/mobile/app/cards/f/a/a/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/cards/f/a/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 57
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Lio/reactivex/o;->h()Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(enableC\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
