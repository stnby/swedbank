.class public final Lcom/swedbank/mobile/app/cards/f/a/f/a;
.super Ljava/lang/Object;
.source "WalletOnboardingPreconditionRegistryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;


# instance fields
.field private final a:Lcom/swedbank/mobile/app/cards/f/a/d/a;

.field private final b:Lcom/swedbank/mobile/app/cards/f/a/b/a;

.field private final c:Lcom/swedbank/mobile/app/cards/f/a/e/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/f/a/d/a;Lcom/swedbank/mobile/app/cards/f/a/b/a;Lcom/swedbank/mobile/app/cards/f/a/e/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/cards/f/a/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/f/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/cards/f/a/e/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletOnboardingLockScreenBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingDefaultPayAppBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingNfcBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f/a;->a:Lcom/swedbank/mobile/app/cards/f/a/d/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/a/f/a;->b:Lcom/swedbank/mobile/app/cards/f/a/b/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/f/a/f/a;->c:Lcom/swedbank/mobile/app/cards/f/a/e/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/f/a;)Lcom/swedbank/mobile/app/cards/f/a/d/a;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/f/a;->a:Lcom/swedbank/mobile/app/cards/f/a/d/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/a/f/a;)Lcom/swedbank/mobile/app/cards/f/a/b/a;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/f/a;->b:Lcom/swedbank/mobile/app/cards/f/a/b/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/a/f/a;)Lcom/swedbank/mobile/app/cards/f/a/e/a;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/f/a;->c:Lcom/swedbank/mobile/app/cards/f/a/e/a;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 17
    new-instance v0, Lkotlin/j;

    const-string v1, "First time onboarding steps not relevant for preconditions"

    invoke-direct {v0, v1}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 19
    new-instance v0, Lkotlin/j;

    const-string v1, "Canceled onboarding steps not relevant for preconditions"

    invoke-direct {v0, v1}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public c()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    new-instance v0, Lkotlin/j;

    const-string v1, "Completed onboarding steps not relevant for preconditions"

    invoke-direct {v0, v1}, Lkotlin/j;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x3

    .line 23
    new-array v0, v0, [Lkotlin/e/a/a;

    .line 24
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/f/a$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/f/a$a;-><init>(Lcom/swedbank/mobile/app/cards/f/a/f/a;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 25
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/f/a$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/f/a$b;-><init>(Lcom/swedbank/mobile/app/cards/f/a/f/a;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 26
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/f/a$c;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/f/a$c;-><init>(Lcom/swedbank/mobile/app/cards/f/a/f/a;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 23
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
