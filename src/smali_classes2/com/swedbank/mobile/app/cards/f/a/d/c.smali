.class public final Lcom/swedbank/mobile/app/cards/f/a/d/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletOnboardingLockScreenPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/d/g;",
        "Lcom/swedbank/mobile/app/cards/f/a/d/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/d/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/d/c;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/d/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 13
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/a/d/c$a;->a:Lcom/swedbank/mobile/app/cards/f/a/d/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/f/a/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 14
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/d/c$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/d/c$b;-><init>(Lcom/swedbank/mobile/app/cards/f/a/d/c;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 18
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/d/c$e;->a:Lcom/swedbank/mobile/app/cards/f/a/d/c$e;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/f/a/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 19
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/a/d/c$f;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/cards/f/a/d/c$f;-><init>(Lcom/swedbank/mobile/app/cards/f/a/d/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 20
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 23
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/a/d/c$c;->a:Lcom/swedbank/mobile/app/cards/f/a/d/c$c;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/a/d/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 24
    new-instance v3, Lcom/swedbank/mobile/app/cards/f/a/d/c$d;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/cards/f/a/d/c$d;-><init>(Lcom/swedbank/mobile/app/cards/f/a/d/c;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 25
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 26
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 28
    iget-object v3, p0, Lcom/swedbank/mobile/app/cards/f/a/d/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;

    .line 29
    invoke-interface {v3}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/a;->c()Lio/reactivex/w;

    move-result-object v3

    .line 30
    sget-object v4, Lcom/swedbank/mobile/app/cards/f/a/d/c$g;->a:Lcom/swedbank/mobile/app/cards/f/a/d/c$g;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object v3

    .line 31
    invoke-virtual {v3}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v3

    .line 33
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    check-cast v2, Lio/reactivex/s;

    check-cast v3, Lio/reactivex/s;

    invoke-static {v0, v1, v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(authent\u2026, lockScreenStatusStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
