.class final Lcom/swedbank/mobile/app/cards/f/b/c$g;
.super Lkotlin/e/b/k;
.source "WalletPaymentRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/b/c;->a(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/f/b/c;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/b/c;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    .line 168
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c$g$a;->a:Lcom/swedbank/mobile/app/cards/f/b/c$g$a;

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    .line 119
    check-cast p1, Ljava/lang/Iterable;

    .line 170
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 119
    instance-of v1, v1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/d;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 172
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-nez v0, :cond_2

    .line 126
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    .line 121
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->b(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/app/cards/f/b/d/a;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/b/c;->c(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/app/cards/f/b/d/a;

    move-result-object v0

    .line 123
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/b/c;->d(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/b/c/c;

    move-result-object v1

    check-cast v1, Lio/reactivex/o;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a(Lio/reactivex/o;)Lcom/swedbank/mobile/app/cards/f/b/d/a;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/cards/f/b/d/a;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/app/cards/f/b/d/a;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 173
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 128
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/b/c;->d(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/b/c/c;

    move-result-object p1

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c$g;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/c$g;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
