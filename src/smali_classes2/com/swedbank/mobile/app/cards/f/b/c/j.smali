.class public final Lcom/swedbank/mobile/app/cards/f/b/c/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "WalletPaymentResultViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/f/b/c/i;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x7

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardView"

    const-string v4, "getCardView()Lcom/swedbank/mobile/app/cards/CardLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "successResultIcon"

    const-string v4, "getSuccessResultIcon()Lcom/airbnb/lottie/LottieAnimationView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "failureResultIcon"

    const-string v4, "getFailureResultIcon()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "resultInfo"

    const-string v4, "getResultInfo()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "resultFailureTitle"

    const-string v4, "getResultFailureTitle()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "sukInfo"

    const-string v4, "getSukInfo()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/b/c/j;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "ackBtn"

    const-string v4, "getAckBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->j:Lio/reactivex/o;

    .line 33
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_view:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->b:Lkotlin/f/c;

    .line 34
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_result_success_icon:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c:Lkotlin/f/c;

    .line 35
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_result_failure_icon:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d:Lkotlin/f/c;

    .line 36
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_result_info:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->e:Lkotlin/f/c;

    .line 37
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_result_failure_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f:Lkotlin/f/c;

    .line 38
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_result_suk_info:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g:Lkotlin/f/c;

    .line 39
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_payment_result_acknowledge:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->h:Lkotlin/f/c;

    .line 41
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->i:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/swedbank/mobile/app/cards/CardLayout;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d()Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/content/Context;
    .locals 0

    .line 30
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->j()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->h()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final d()Lcom/swedbank/mobile/app/cards/CardLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/CardLayout;

    return-object v0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/airbnb/lottie/LottieAnimationView;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f()Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/ImageView;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method private final f()Lcom/airbnb/lottie/LottieAnimationView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    return-object v0
.end method

.method private final g()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->i()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final h()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final i()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final j()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final k()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->i:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/cards/f/b/c/m;)V
    .locals 11
    .param p1    # Lcom/swedbank/mobile/app/cards/f/b/c/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/b/c/m;->a()Lcom/swedbank/mobile/app/cards/c;

    move-result-object v0

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 152
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/swedbank/mobile/app/cards/CardLayout;->setVisibility(I)V

    .line 154
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object v4

    .line 155
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->c()Ljava/lang/String;

    move-result-object v5

    .line 156
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->d()Ljava/lang/String;

    move-result-object v6

    .line 157
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->e()Ljava/lang/String;

    move-result-object v7

    .line 158
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->h()Z

    move-result v8

    .line 159
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->f()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v9

    .line 160
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->g()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v10

    .line 154
    invoke-virtual/range {v4 .. v10}, Lcom/swedbank/mobile/app/cards/CardLayout;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/g;)V

    .line 162
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->m()Lcom/swedbank/mobile/app/cards/o;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 164
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->b(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/content/Context;

    move-result-object v0

    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_no_suk:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "context().getString(R.string.wallet_no_suk)"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 168
    :cond_0
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/o;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->b(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_low_suk:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "context().getString(R.st\u2026ow_suk, count.toString())"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 172
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->c(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 162
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 177
    :cond_3
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/CardLayout;->setVisibility(I)V

    .line 49
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/b/c/m;->b()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    move-result-object v0

    .line 51
    instance-of v3, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    const/16 v4, 0x8

    if-eqz v3, :cond_4

    .line 180
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->b(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_info:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->e(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setVisibility(I)V

    .line 182
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 52
    :cond_4
    instance-of v5, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    if-eqz v5, :cond_5

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;->c()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;

    move-result-object v5

    .line 187
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->e(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/airbnb/lottie/LottieAnimationView;->setVisibility(I)V

    .line 188
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/ImageView;

    move-result-object v4

    sget v6, Lcom/swedbank/mobile/app/cards/n$d;->ic_payment_failed:I

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 190
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/b/c/k;->a:[I

    invoke-virtual {v5}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a$a;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    goto/16 :goto_1

    .line 235
    :pswitch_0
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_transaction_limit_exceeded_failure_title:I

    .line 236
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_transaction_limit_exceeded_failure_info:I

    .line 243
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 244
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 246
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->f(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/ImageView;

    move-result-object v2

    sget v4, Lcom/swedbank/mobile/app/cards/n$d;->ic_payment_attention:I

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 229
    :pswitch_1
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_no_payment_tokens_failure_title:I

    .line 230
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_no_payment_tokens_failure_info:I

    .line 231
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 232
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 223
    :pswitch_2
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_user_security_violation_failure_title:I

    .line 224
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_user_security_violation_failure_info:I

    .line 225
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 226
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 217
    :pswitch_3
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_user_changed_failure_title:I

    .line 218
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_user_changed_failure_info:I

    .line 219
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 220
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 211
    :pswitch_4
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_technical_failure_title:I

    .line 212
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_technical_failure_info:I

    .line 213
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 214
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 205
    :pswitch_5
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_card_failure_title:I

    .line 206
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_card_failure_info:I

    .line 207
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 208
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 199
    :pswitch_6
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_nfc_failure_title:I

    .line 200
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_nfc_failure_info:I

    .line 201
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 202
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 193
    :pswitch_7
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_terminal_failure_title:I

    .line 194
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->wallet_payment_result_terminal_failure_info:I

    .line 195
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->g(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    .line 196
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->d(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 54
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/b/c/m;->c()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 257
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->b(Lcom/swedbank/mobile/app/cards/f/b/c/j;)Landroid/content/Context;

    move-result-object p1

    const-string v2, "vibrator"

    .line 258
    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_7

    check-cast p1, Landroid/os/Vibrator;

    const/4 v2, -0x1

    if-eqz v3, :cond_6

    const/4 v0, 0x2

    .line 261
    new-array v0, v0, [J

    fill-array-data v0, :array_0

    invoke-virtual {p1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    goto :goto_2

    .line 262
    :cond_6
    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$a;

    if-eqz v0, :cond_8

    .line 263
    new-array v0, v1, [J

    fill-array-data v0, :array_1

    invoke-virtual {p1, v0, v2}, Landroid/os/Vibrator;->vibrate([JI)V

    goto :goto_2

    .line 258
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.os.Vibrator"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 58
    :cond_8
    :goto_2
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->i:Lcom/b/c/c;

    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-virtual {p1, v0}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 8
        0x0
        0x7d
    .end array-data

    :array_1
    .array-data 8
        0x0
        0x46
        0x46
        0x46
    .end array-data
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/b/c/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->a(Lcom/swedbank/mobile/app/cards/f/b/c/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/b/c/j;->k()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 150
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/j;->j:Lio/reactivex/o;

    return-object v0
.end method
