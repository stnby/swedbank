.class final Lcom/swedbank/mobile/app/cards/f/c/e$a;
.super Lkotlin/e/b/k;
.source "WalletPreconditionsResolverRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/c/e;->a(Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/f/c/e;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/c/e;Ljava/util/List;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->a:Lcom/swedbank/mobile/app/cards/f/c/e;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->b:Ljava/util/List;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->c:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    check-cast p1, Ljava/lang/Iterable;

    .line 57
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 58
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 56
    instance-of v0, v0, Lcom/swedbank/mobile/business/general/confirmation/e;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 38
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->a:Lcom/swedbank/mobile/app/cards/f/c/e;

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->a:Lcom/swedbank/mobile/app/cards/f/c/e;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/c/e;->a(Lcom/swedbank/mobile/app/cards/f/c/e;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->a:Lcom/swedbank/mobile/app/cards/f/c/e;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/c/e;->b(Lcom/swedbank/mobile/app/cards/f/c/e;)Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/c/a;

    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/cards/f/c/a;-><init>(Ljava/util/List;)V

    check-cast v1, Lcom/swedbank/mobile/app/f/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 37
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/c/e$a;->c:Z

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Z)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 61
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/c/e$a;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
