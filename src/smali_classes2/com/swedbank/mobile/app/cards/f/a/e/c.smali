.class public final Lcom/swedbank/mobile/app/cards/f/a/e/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletOnboardingNfcPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/e/g;",
        "Lcom/swedbank/mobile/app/cards/f/a/e/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/e/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/e/c;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 13
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/e/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;

    .line 14
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/nfc/a;->b()Lio/reactivex/o;

    move-result-object v0

    .line 15
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/e/c$f;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 17
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/e/c$a;->a:Lcom/swedbank/mobile/app/cards/f/a/e/c$a;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/f/a/e/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 18
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/a/e/c$b;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/cards/f/a/e/c$b;-><init>(Lcom/swedbank/mobile/app/cards/f/a/e/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 19
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 20
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 24
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/a/e/c$c;->a:Lcom/swedbank/mobile/app/cards/f/a/e/c$c;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/a/e/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 25
    sget-object v3, Lcom/swedbank/mobile/app/cards/f/a/e/c$d;->a:Lcom/swedbank/mobile/app/cards/f/a/e/c$d;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/cards/f/a/e/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 23
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 26
    new-instance v3, Lcom/swedbank/mobile/app/cards/f/a/e/c$e;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/cards/f/a/e/c$e;-><init>(Lcom/swedbank/mobile/app/cards/f/a/e/c;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 27
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 31
    check-cast v0, Lio/reactivex/s;

    .line 32
    check-cast v1, Lio/reactivex/s;

    .line 33
    check-cast v2, Lio/reactivex/s;

    .line 30
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026       cancelClickStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
