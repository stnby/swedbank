.class public final Lcom/swedbank/mobile/app/cards/f/a/b;
.super Ljava/lang/Object;
.source "WalletOnboardingBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/util/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Boolean;

.field private d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private e:Z

.field private final f:Lcom/swedbank/mobile/a/e/g/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/g/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/g/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->f:Lcom/swedbank/mobile/a/e/g/a/a$a;

    .line 17
    sget-object p1, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p1, Lcom/swedbank/mobile/business/util/l;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->a:Lcom/swedbank/mobile/business/util/l;

    .line 20
    sget-object p1, Lcom/swedbank/mobile/app/cards/f/a/e;->a:Lcom/swedbank/mobile/app/cards/f/a/e;

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/app/cards/f/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/b;

    .line 42
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/a/b;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/cards/f/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/l<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/f/a/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/b;

    .line 24
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/a/b;->a:Lcom/swedbank/mobile/business/util/l;

    return-object v0
.end method

.method public final a(Ljava/util/List;Z)Lcom/swedbank/mobile/app/cards/f/a/b;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/swedbank/mobile/app/cards/f/a/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardsToDigitize"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/a/b;

    .line 31
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/a/b;->b:Ljava/util/List;

    .line 32
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/a/b;->c:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->f:Lcom/swedbank/mobile/a/e/g/a/a$a;

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->a:Lcom/swedbank/mobile/business/util/l;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/a$a;->b(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/a/e/g/a/a$a;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/a$a;->b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/a/a$a;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/a$a;->d(Z)Lcom/swedbank/mobile/a/e/g/a/a$a;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->d:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/a$a;->b(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/a/e/g/a/a$a;

    move-result-object v0

    .line 54
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/a/b;->e:Z

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/a/a$a;->c(Z)Lcom/swedbank/mobile/a/e/g/a/a$a;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/a/a$a;->a()Lcom/swedbank/mobile/a/e/g/a/a;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 50
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "One must explicitly define whether cards are digitized in the background or not"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 47
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display wallet onboarding when there is no cards to digitize"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
