.class public final Lcom/swedbank/mobile/app/cards/f/a/d/e;
.super Lcom/swedbank/mobile/architect/a/h;
.source "WalletOnboardingLockScreenRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/lockscreen/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/g/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/g/m;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/g/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_lockscreen"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_lockscreen"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/g/m;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "lockScreenManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p2, p4, p3}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/d/e;->e:Lcom/swedbank/mobile/app/g/m;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/d/e;->e:Lcom/swedbank/mobile/app/g/m;

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a;->a:Lcom/swedbank/mobile/app/cards/f/a/a;

    check-cast v1, Lcom/swedbank/mobile/app/g/f;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/app/g/m;->a(Lcom/swedbank/mobile/app/g/f;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method

.method public b()Lio/reactivex/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/d/e;->e:Lcom/swedbank/mobile/app/g/m;

    invoke-interface {v0}, Lcom/swedbank/mobile/app/g/m;->b()Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
