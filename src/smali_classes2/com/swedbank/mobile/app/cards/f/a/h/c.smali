.class public final Lcom/swedbank/mobile/app/cards/f/a/h/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "WalletOnboardingValidateUserRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/validateuser/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/g/m;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/g/m;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/g/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_onboarding_validate_user"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/g/m;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "lockScreenManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    .line 21
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/h/c;->e:Lcom/swedbank/mobile/app/g/m;

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/h/c;->e:Lcom/swedbank/mobile/app/g/m;

    .line 24
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a;->a:Lcom/swedbank/mobile/app/cards/f/a/a;

    check-cast v1, Lcom/swedbank/mobile/app/g/f;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/app/g/m;->a(Lcom/swedbank/mobile/app/g/f;)Lio/reactivex/w;

    move-result-object v0

    return-object v0
.end method
