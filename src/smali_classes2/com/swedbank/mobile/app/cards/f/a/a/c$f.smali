.class final Lcom/swedbank/mobile/app/cards/f/a/a/c$f;
.super Ljava/lang/Object;
.source "WalletOnboardingContactlessPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/a/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/f/a/a/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/a/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/o;
    .locals 1
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/f/a/a/k$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/a/a/c;->a(Lcom/swedbank/mobile/app/cards/f/a/a/c;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/contactless/a;->c()Lio/reactivex/j;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object p1

    .line 20
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/a/a/c$f$1;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c$f$1;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 21
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/a/a/c$f$2;->a:Lcom/swedbank/mobile/app/cards/f/a/a/c$f$2;

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->j(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/a/a/k$a$b;->a:Lcom/swedbank/mobile/app/cards/f/a/a/k$a$b;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/a/a/c$f;->a(Lkotlin/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
