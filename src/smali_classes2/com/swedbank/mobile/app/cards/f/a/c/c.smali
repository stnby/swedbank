.class public final Lcom/swedbank/mobile/app/cards/f/a/c/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletOnboardingDigitizationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/c/g;",
        "Lcom/swedbank/mobile/app/cards/f/a/c/j;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;


# direct methods
.method public constructor <init>(ZLcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;)V
    .locals 1
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "digitize_cards_in_background"
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/cards/f/a/c/c;->a:Z

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/a/c/c;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/c/c;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/c/c;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 19
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/cards/f/a/c/c;->a:Z

    if-eqz v0, :cond_0

    return-void

    .line 24
    :cond_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/c/c;->b:Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/digitization/a;->b()Lio/reactivex/w;

    move-result-object v0

    .line 26
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/c/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/c/c$f;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->b(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/c/j;

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4, v3, v4}, Lcom/swedbank/mobile/app/cards/f/a/c/j;-><init>(ZLjava/lang/Throwable;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 37
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/c/c$g;->a:Lcom/swedbank/mobile/app/cards/f/a/c/c$g;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->j(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 39
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/c/c$d;->a:Lcom/swedbank/mobile/app/cards/f/a/c/c$d;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/f/a/c/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 40
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/a/c/c$e;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/cards/f/a/c/c$e;-><init>(Lio/reactivex/o;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 44
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/a/c/c$a;->a:Lcom/swedbank/mobile/app/cards/f/a/c/c$a;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/a/c/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 45
    sget-object v3, Lcom/swedbank/mobile/app/cards/f/a/c/c$b;->a:Lcom/swedbank/mobile/app/cards/f/a/c/c$b;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/cards/f/a/c/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 43
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 46
    new-instance v3, Lcom/swedbank/mobile/app/cards/f/a/c/c$c;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/cards/f/a/c/c$c;-><init>(Lcom/swedbank/mobile/app/cards/f/a/c/c;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 48
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 51
    check-cast v0, Lio/reactivex/s;

    .line 52
    check-cast v1, Lio/reactivex/s;

    .line 53
    check-cast v2, Lio/reactivex/s;

    .line 50
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026am,\n        cancelStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
