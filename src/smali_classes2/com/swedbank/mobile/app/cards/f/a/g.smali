.class public final Lcom/swedbank/mobile/app/cards/f/a/g;
.super Ljava/lang/Object;
.source "WalletOnboardingPluginRegistryImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/i/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/h/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/g/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/i/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/h/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/g/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/a/a;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->a:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->b:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->c:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->d:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->e:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->f:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->g:Ljavax/inject/Provider;

    .line 48
    iput-object p8, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->h:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/g;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/i/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/h/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/g/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/c/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/e/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/a/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/f/a/g;"
        }
    .end annotation

    .line 65
    new-instance v9, Lcom/swedbank/mobile/app/cards/f/a/g;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/f/a/g;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/f/a/f;
    .locals 10

    .line 53
    new-instance v9, Lcom/swedbank/mobile/app/cards/f/a/f;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/app/cards/f/a/i/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/app/cards/f/a/h/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/app/cards/f/a/d/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/app/cards/f/a/g/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/app/cards/f/a/c/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/app/cards/f/a/b/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/app/cards/f/a/e/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g;->h:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/swedbank/mobile/app/cards/f/a/a/a;

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/f/a/f;-><init>(Lcom/swedbank/mobile/app/cards/f/a/i/a;Lcom/swedbank/mobile/app/cards/f/a/h/a;Lcom/swedbank/mobile/app/cards/f/a/d/a;Lcom/swedbank/mobile/app/cards/f/a/g/a;Lcom/swedbank/mobile/app/cards/f/a/c/a;Lcom/swedbank/mobile/app/cards/f/a/b/a;Lcom/swedbank/mobile/app/cards/f/a/e/a;Lcom/swedbank/mobile/app/cards/f/a/a/a;)V

    return-object v9
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/a/g;->a()Lcom/swedbank/mobile/app/cards/f/a/f;

    move-result-object v0

    return-object v0
.end method
