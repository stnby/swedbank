.class final synthetic Lcom/swedbank/mobile/app/cards/f/b/d/c$a;
.super Lkotlin/e/b/i;
.source "WalletPaymentTapPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/b/d/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/cards/f/b/d/n;",
        "Lcom/swedbank/mobile/app/cards/f/b/d/n$a;",
        "Lcom/swedbank/mobile/app/cards/f/b/d/n;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/b/d/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/cards/f/b/d/n;Lcom/swedbank/mobile/app/cards/f/b/d/n$a;)Lcom/swedbank/mobile/app/cards/f/b/d/n;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/cards/f/b/d/n;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/f/b/d/n$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/d/c;

    .line 62
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$a;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$a;->a()Lcom/swedbank/mobile/app/cards/c;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/cards/f/b/d/n;->a(Lcom/swedbank/mobile/app/cards/f/b/d/n;Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;ZLjava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/f/b/d/n;

    move-result-object p1

    goto :goto_0

    .line 63
    :cond_0
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$b;->a:Lcom/swedbank/mobile/app/cards/f/b/d/n$a$b;

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object v1, p1

    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/cards/f/b/d/n;->a(Lcom/swedbank/mobile/app/cards/f/b/d/n;Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;ZLjava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/f/b/d/n;

    move-result-object p1

    goto :goto_0

    .line 64
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    .line 65
    check-cast p2, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/f/b/d/n$a$c;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x9

    const/4 v7, 0x0

    move-object v1, p1

    .line 64
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/cards/f/b/d/n;->a(Lcom/swedbank/mobile/app/cards/f/b/d/n;Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;ZLjava/lang/Throwable;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/f/b/d/n;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 14
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/b/d/n;

    check-cast p2, Lcom/swedbank/mobile/app/cards/f/b/d/n$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/f/b/d/c$a;->a(Lcom/swedbank/mobile/app/cards/f/b/d/n;Lcom/swedbank/mobile/app/cards/f/b/d/n$a;)Lcom/swedbank/mobile/app/cards/f/b/d/n;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/cards/f/b/d/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/cards/wallet/payment/tap/WalletPaymentTapViewState;Lcom/swedbank/mobile/app/cards/wallet/payment/tap/WalletPaymentTapViewState$PartialState;)Lcom/swedbank/mobile/app/cards/wallet/payment/tap/WalletPaymentTapViewState;"

    return-object v0
.end method
