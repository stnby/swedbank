.class public final Lcom/swedbank/mobile/app/cards/f/c/f;
.super Ljava/lang/Object;
.source "WalletPreconditionsResolverRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/c/e;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->a:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->b:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->c:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->d:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->e:Ljavax/inject/Provider;

    .line 37
    iput-object p6, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->f:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/c/f;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/f/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/f/c/f;"
        }
    .end annotation

    .line 51
    new-instance v7, Lcom/swedbank/mobile/app/cards/f/c/f;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/cards/f/c/f;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/f/c/e;
    .locals 8

    .line 42
    new-instance v7, Lcom/swedbank/mobile/app/cards/f/c/e;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/app/f/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/app/cards/f/a/f/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/business/general/confirmation/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/architect/business/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/f;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/architect/a/b/g;

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/cards/f/c/e;-><init>(Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/app/cards/f/a/f/c;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v7
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/c/f;->a()Lcom/swedbank/mobile/app/cards/f/c/e;

    move-result-object v0

    return-object v0
.end method
