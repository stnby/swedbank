.class public final Lcom/swedbank/mobile/app/cards/f/b/a;
.super Ljava/lang/Object;
.source "WalletPaymentBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

.field private b:Lcom/swedbank/mobile/business/util/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Lcom/swedbank/mobile/a/e/g/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/g/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/g/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/a;->d:Lcom/swedbank/mobile/a/e/g/b/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/app/cards/f/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/a;

    .line 18
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/b/a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/util/e;Z)Lcom/swedbank/mobile/app/cards/f/b/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;Z)",
            "Lcom/swedbank/mobile/app/cards/f/b/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/a;

    .line 22
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/b/a;->b:Lcom/swedbank/mobile/business/util/e;

    .line 23
    iput-boolean p2, v0, Lcom/swedbank/mobile/app/cards/f/b/a;->c:Z

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/a;->d:Lcom/swedbank/mobile/a/e/g/b/a$a;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/a;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/a$a;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/k;)Lcom/swedbank/mobile/a/e/g/b/a$a;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/a;->b:Lcom/swedbank/mobile/business/util/e;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/a$a;->b(Lcom/swedbank/mobile/business/util/e;)Lcom/swedbank/mobile/a/e/g/b/a$a;

    move-result-object v0

    .line 33
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/cards/f/b/a;->c:Z

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/a$a;->b(Z)Lcom/swedbank/mobile/a/e/g/b/a$a;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/b/a$a;->a()Lcom/swedbank/mobile/a/e/g/b/a;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display wallet payment without input"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 27
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display wallet payment without parent node"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
