.class public final Lcom/swedbank/mobile/app/cards/f/a/g/k;
.super Ljava/lang/Object;
.source "WalletOnboardingRegistrationViewImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/g/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/k;->a:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/f/a/g/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/f/a/g/k;"
        }
    .end annotation

    .line 23
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/g/k;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/f/a/g/k;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/f/a/g/h;
    .locals 2

    .line 18
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/g/h;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/k;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/data/device/a;

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/cards/f/a/g/h;-><init>(Lcom/swedbank/mobile/data/device/a;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/f/a/g/k;->a()Lcom/swedbank/mobile/app/cards/f/a/g/h;

    move-result-object v0

    return-object v0
.end method
