.class public final Lcom/swedbank/mobile/app/cards/f/a/a/a;
.super Ljava/lang/Object;
.source "WalletOnboardingContactlessBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private final a:Lcom/swedbank/mobile/a/e/g/a/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/g/a/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/g/a/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/a;->a:Lcom/swedbank/mobile/a/e/g/a/a/a$a;

    return-void
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/a;->a:Lcom/swedbank/mobile/a/e/g/a/a/a$a;

    .line 19
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/a/a/a$a;->a()Lcom/swedbank/mobile/a/e/g/a/a/a;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/swedbank/mobile/a/e/g/a/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0
.end method

.method public b()Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/a;->a:Lcom/swedbank/mobile/a/e/g/a/a/a$a;

    .line 15
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/a/a/a$a;->a()Lcom/swedbank/mobile/a/e/g/a/a/a;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lcom/swedbank/mobile/a/e/g/a/a/a;->e()Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;

    move-result-object v0

    return-object v0
.end method
