.class public final Lcom/swedbank/mobile/app/cards/f/b/b/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "WalletPaymentBackgroundRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/payment/background/e;


# instance fields
.field private final e:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/business/e/b;

.field private final g:Lcom/swedbank/mobile/app/cards/f/b/a;

.field private final h:Lcom/swedbank/mobile/app/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/swedbank/mobile/business/cards/wallet/payment/k;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/e/b;Lcom/swedbank/mobile/app/cards/f/b/a;Lcom/swedbank/mobile/app/a/d;Lcom/swedbank/mobile/business/cards/wallet/payment/k;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/cards/f/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "trackWalletCardSelectedForPaymentEvent"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/cards/wallet/payment/k;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_payment_background"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_wallet_payment_background"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/business/e/b;",
            "Lcom/swedbank/mobile/app/cards/f/b/a;",
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/k;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityLifecycleMonitor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trackWalletCardSelectedForPaymentEvent"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPaymentListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    .line 36
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->e:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->f:Lcom/swedbank/mobile/business/e/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->g:Lcom/swedbank/mobile/app/cards/f/b/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->h:Lcom/swedbank/mobile/app/a/d;

    iput-object p5, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/business/e/b;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->f:Lcom/swedbank/mobile/business/e/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/b/d;Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V

    return-void
.end method

.method private final a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;Z",
            "Lio/reactivex/x<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
            ">;)V"
        }
    .end annotation

    .line 60
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/cards/f/b/b/d$c;-><init>(Lcom/swedbank/mobile/app/cards/f/b/b/d;Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/app/cards/f/b/a;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->g:Lcom/swedbank/mobile/app/cards/f/b/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/business/cards/wallet/payment/k;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->i:Lcom/swedbank/mobile/business/cards/wallet/payment/k;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/f/b/b/d;)Lcom/swedbank/mobile/app/a/d;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->h:Lcom/swedbank/mobile/app/a/d;

    return-object p0
.end method


# virtual methods
.method public a(Lcom/swedbank/mobile/business/util/e;)Lio/reactivex/w;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;)",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/l;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/b/b/d$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/b/d$b;-><init>(Lcom/swedbank/mobile/app/cards/f/b/b/d;Lcom/swedbank/mobile/business/util/e;)V

    check-cast v0, Lio/reactivex/z;

    invoke-static {v0}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.create {\n    toPa\u2026        emitter = it)\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->e:Lcom/swedbank/mobile/architect/business/a/c;

    sget-object v1, Lcom/swedbank/mobile/business/cards/list/b;->a:Lcom/swedbank/mobile/business/cards/list/b;

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/w;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/d;->e:Lcom/swedbank/mobile/architect/business/a/c;

    .line 107
    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/z;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/z;-><init>(Lcom/swedbank/mobile/business/cards/wallet/w;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/business/util/e;Z)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/util/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/e<",
            "Ljava/lang/String;",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/n;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2, v0}, Lcom/swedbank/mobile/app/cards/f/b/b/d;->a(Lcom/swedbank/mobile/business/util/e;ZLio/reactivex/x;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 111
    sget-object v0, Lcom/swedbank/mobile/app/cards/f/b/b/d$a;->a:Lcom/swedbank/mobile/app/cards/f/b/b/d$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
