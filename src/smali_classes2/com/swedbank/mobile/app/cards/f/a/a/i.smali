.class public final Lcom/swedbank/mobile/app/cards/f/a/a/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "WalletOnboardingContactlessViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/f/a/a/h;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x6

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "loadingView"

    const-string v4, "getLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentViews"

    const-string v4, "getContentViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "titleView"

    const-string v4, "getTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "textView"

    const-string v4, "getTextView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "enableContactlessBtn"

    const-string v4, "getEnableContactlessBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/f/a/a/i;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cancelBtn"

    const-string v4, "getCancelBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->h:Lio/reactivex/o;

    .line 21
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_contactless_loading:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->b:Lkotlin/f/c;

    .line 22
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_contactless_content_views:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->c:Lkotlin/f/c;

    .line 23
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_contactless_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->d:Lkotlin/f/c;

    .line 24
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_contactless_text:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->e:Lkotlin/f/c;

    .line 25
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_contactless_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->f:Lkotlin/f/c;

    .line 26
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->wallet_onboarding_contactless_cancel_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->g:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/view/View;
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->d()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/TextView;
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->g()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/TextView;
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->h()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final d()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/Button;
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->i()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final g()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final h()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final i()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final j()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->i()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 66
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/cards/f/a/a/k;)V
    .locals 4
    .param p1    # Lcom/swedbank/mobile/app/cards/f/a/a/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->a()Z

    move-result v0

    .line 68
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->f()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 40
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->a()Z

    move-result v0

    if-nez v0, :cond_5

    .line 41
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/a/k;->c()Ljava/lang/Throwable;

    move-result-object p1

    if-eqz p1, :cond_3

    :cond_2
    const/4 v3, 0x1

    :cond_3
    if-eqz v3, :cond_4

    .line 78
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->b(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_contactless_error_title:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 79
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->c(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_contactless_error_text:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 80
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->d(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/Button;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_contactless_enable_retry:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    .line 82
    :cond_4
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->b(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_contactless_title:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 83
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->c(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/TextView;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_contactless_text:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 84
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->d(Lcom/swedbank/mobile/app/cards/f/a/a/i;)Landroid/widget/Button;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_contactless_enable:I

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    :cond_5
    :goto_2
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/app/cards/f/a/a/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->a(Lcom/swedbank/mobile/app/cards/f/a/a/k;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/f/a/a/i;->j()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 67
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/a/i;->h:Lio/reactivex/o;

    return-object v0
.end method

.method protected e()V
    .locals 0

    .line 33
    invoke-super {p0}, Lcom/swedbank/mobile/architect/a/b/a;->e()V

    .line 34
    invoke-static {p0}, Lcom/swedbank/mobile/core/ui/ap;->a(Lcom/swedbank/mobile/architect/a/b/a;)V

    return-void
.end method
