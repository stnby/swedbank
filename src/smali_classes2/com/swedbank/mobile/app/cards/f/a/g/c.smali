.class public final Lcom/swedbank/mobile/app/cards/f/a/g/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletOnboardingRegistrationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/a/g/g;",
        "Lcom/swedbank/mobile/app/cards/f/a/g/l;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/g/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/g/c;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 8

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/a/g/c;->a:Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;

    .line 15
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/c;->b()Lio/reactivex/b;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 17
    new-instance v7, Lcom/swedbank/mobile/app/cards/f/a/g/l;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/f/a/g/l;-><init>(ZLjava/lang/Throwable;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v7}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 18
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/g/c$f;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->j(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/a/g/c$d;->a:Lcom/swedbank/mobile/app/cards/f/a/g/c$d;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/f/a/g/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 28
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/a/g/c$e;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/cards/f/a/g/c$e;-><init>(Lio/reactivex/o;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 32
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/a/g/c$a;->a:Lcom/swedbank/mobile/app/cards/f/a/g/c$a;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/a/g/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 33
    sget-object v3, Lcom/swedbank/mobile/app/cards/f/a/g/c$b;->a:Lcom/swedbank/mobile/app/cards/f/a/g/c$b;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/cards/f/a/g/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 31
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 34
    new-instance v3, Lcom/swedbank/mobile/app/cards/f/a/g/c$c;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/cards/f/a/g/c$c;-><init>(Lcom/swedbank/mobile/app/cards/f/a/g/c;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 35
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 36
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 38
    check-cast v0, Lio/reactivex/s;

    check-cast v1, Lio/reactivex/s;

    check-cast v2, Lio/reactivex/s;

    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(registr\u2026etryStream, cancelStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
