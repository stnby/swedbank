.class public final Lcom/swedbank/mobile/app/cards/f/b/b/b;
.super Ljava/lang/Object;
.source "WalletPaymentBackgroundBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

.field private final b:Lcom/swedbank/mobile/a/e/g/b/b/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/g/b/b/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/g/b/b/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/b;->b:Lcom/swedbank/mobile/a/e/g/b/b/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/a;)Lcom/swedbank/mobile/app/cards/f/b/b/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/b/b;->b:Lcom/swedbank/mobile/a/e/g/b/b/a$a;

    .line 21
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/b/b;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/a;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/b/a$a;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/a;)Lcom/swedbank/mobile/a/e/g/b/b/a$a;

    move-result-object v0

    .line 24
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/b/b/a$a;->a()Lcom/swedbank/mobile/a/e/g/b/b/a;

    move-result-object v0

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/b/b/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 21
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display wallet payment background without parent node"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
