.class final Lcom/swedbank/mobile/app/cards/f/b/c$f;
.super Lkotlin/e/b/k;
.source "WalletPaymentRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/b/c;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/f/b/c;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/f/b/c;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$f;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    check-cast p1, Ljava/lang/Iterable;

    .line 169
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 170
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 168
    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/payment/auth/d;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 137
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$f;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    .line 135
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c$f;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/f/b/c;->e(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/app/cards/f/b/a/b;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$f;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/f/b/c;->f(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/b/a/b;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/auth/a;)Lcom/swedbank/mobile/app/cards/f/b/a/b;

    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/b/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 173
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    .line 140
    :cond_3
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c$f;->a:Lcom/swedbank/mobile/app/cards/f/b/c;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/f/b/c;->g(Lcom/swedbank/mobile/app/cards/f/b/c;)Lcom/swedbank/mobile/core/a/c;

    move-result-object p1

    sget-object v0, Lcom/swedbank/mobile/app/cards/p;->a:Lcom/swedbank/mobile/app/cards/p;

    check-cast v0, Lcom/swedbank/mobile/core/a/a;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/core/a/c;->a(Lcom/swedbank/mobile/core/a/a;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/b/c$f;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
