.class public final Lcom/swedbank/mobile/app/cards/f/a/f;
.super Ljava/lang/Object;
.source "WalletOnboardingPluginRegistryImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/wallet/onboarding/i;


# instance fields
.field private final a:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lkotlin/e/a/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/app/cards/f/a/i/a;

.field private final h:Lcom/swedbank/mobile/app/cards/f/a/h/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/f/a/i/a;Lcom/swedbank/mobile/app/cards/f/a/h/a;Lcom/swedbank/mobile/app/cards/f/a/d/a;Lcom/swedbank/mobile/app/cards/f/a/g/a;Lcom/swedbank/mobile/app/cards/f/a/c/a;Lcom/swedbank/mobile/app/cards/f/a/b/a;Lcom/swedbank/mobile/app/cards/f/a/e/a;Lcom/swedbank/mobile/app/cards/f/a/a/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/cards/f/a/i/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/f/a/h/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/cards/f/a/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/cards/f/a/g/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/cards/f/a/c/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/app/cards/f/a/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/app/cards/f/a/e/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/app/cards/f/a/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletOnboardingWelcomeBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingValidateUserBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingLockScreenBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingRegistrationBuilder"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingDigitizationBuilder"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingDefaultPayAppBuilder"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingNfcBuilder"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingContactlessBuilder"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->g:Lcom/swedbank/mobile/app/cards/f/a/i/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->h:Lcom/swedbank/mobile/app/cards/f/a/h/a;

    .line 26
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/f$f;

    invoke-direct {p1, p3}, Lcom/swedbank/mobile/app/cards/f/a/f$f;-><init>(Lcom/swedbank/mobile/app/cards/f/a/d/a;)V

    check-cast p1, Lkotlin/e/a/a;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->a:Lkotlin/e/a/a;

    .line 27
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/f$h;

    invoke-direct {p1, p4}, Lcom/swedbank/mobile/app/cards/f/a/f$h;-><init>(Lcom/swedbank/mobile/app/cards/f/a/g/a;)V

    check-cast p1, Lkotlin/e/a/a;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->b:Lkotlin/e/a/a;

    .line 28
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/f$g;

    invoke-direct {p1, p7}, Lcom/swedbank/mobile/app/cards/f/a/f$g;-><init>(Lcom/swedbank/mobile/app/cards/f/a/e/a;)V

    check-cast p1, Lkotlin/e/a/a;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->c:Lkotlin/e/a/a;

    .line 29
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/f$c;

    invoke-direct {p1, p5}, Lcom/swedbank/mobile/app/cards/f/a/f$c;-><init>(Lcom/swedbank/mobile/app/cards/f/a/c/a;)V

    check-cast p1, Lkotlin/e/a/a;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->d:Lkotlin/e/a/a;

    .line 30
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/f$b;

    invoke-direct {p1, p6}, Lcom/swedbank/mobile/app/cards/f/a/f$b;-><init>(Lcom/swedbank/mobile/app/cards/f/a/b/a;)V

    check-cast p1, Lkotlin/e/a/a;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->e:Lkotlin/e/a/a;

    .line 31
    new-instance p1, Lcom/swedbank/mobile/app/cards/f/a/f$a;

    invoke-direct {p1, p8}, Lcom/swedbank/mobile/app/cards/f/a/f$a;-><init>(Lcom/swedbank/mobile/app/cards/f/a/a/a;)V

    check-cast p1, Lkotlin/e/a/a;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->f:Lkotlin/e/a/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/a/f;)Lcom/swedbank/mobile/app/cards/f/a/i/a;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->g:Lcom/swedbank/mobile/app/cards/f/a/i/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/f/a/f;)Lcom/swedbank/mobile/app/cards/f/a/h/a;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->h:Lcom/swedbank/mobile/app/cards/f/a/h/a;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x6

    .line 33
    new-array v0, v0, [Lkotlin/e/a/a;

    .line 34
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/f$e;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/f$e;-><init>(Lcom/swedbank/mobile/app/cards/f/a/f;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->a:Lkotlin/e/a/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 36
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->b:Lkotlin/e/a/a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 37
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->c:Lkotlin/e/a/a;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->d:Lkotlin/e/a/a;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->e:Lkotlin/e/a/a;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 33
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x6

    .line 41
    new-array v0, v0, [Lkotlin/e/a/a;

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->f:Lkotlin/e/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 43
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->a:Lkotlin/e/a/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->b:Lkotlin/e/a/a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->c:Lkotlin/e/a/a;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 46
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->d:Lkotlin/e/a/a;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 47
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->e:Lkotlin/e/a/a;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 41
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x3

    .line 49
    new-array v0, v0, [Lkotlin/e/a/a;

    .line 50
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->f:Lkotlin/e/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 51
    new-instance v1, Lcom/swedbank/mobile/app/cards/f/a/f$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/f/a/f$d;-><init>(Lcom/swedbank/mobile/app/cards/f/a/f;)V

    check-cast v1, Lkotlin/e/a/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 52
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->d:Lkotlin/e/a/a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 49
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/e/a/a<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/k;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const/4 v0, 0x3

    .line 54
    new-array v0, v0, [Lkotlin/e/a/a;

    .line 55
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->a:Lkotlin/e/a/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 56
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->e:Lkotlin/e/a/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 57
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/a/f;->c:Lkotlin/e/a/a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 54
    invoke-static {v0}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
