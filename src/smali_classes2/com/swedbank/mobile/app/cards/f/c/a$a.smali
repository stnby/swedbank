.class final Lcom/swedbank/mobile/app/cards/f/c/a$a;
.super Lkotlin/e/b/k;
.source "WalletPreconditionsNotSatisfiedDialogInformation.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/c/a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/business/cards/wallet/ab;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/a$a;->a:Landroid/content/res/Resources;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/ab;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ab;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "notSatisfiedPrecondition"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/a$a;->a:Landroid/content/res/Resources;

    sget-object v1, Lcom/swedbank/mobile/app/cards/f/c/b;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/wallet/ab;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    .line 24
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_preconditions_not_satisfied_reason_default_pay_app:I

    goto :goto_0

    .line 23
    :pswitch_1
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_preconditions_not_satisfied_reason_secure_lockscreen:I

    goto :goto_0

    .line 22
    :pswitch_2
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_preconditions_not_satisfied_reason_nfc:I

    .line 21
    :goto_0
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "resources.getString(when\u2026efault_pay_app\n        })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/ab;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/c/a$a;->a(Lcom/swedbank/mobile/business/cards/wallet/ab;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
