.class public final Lcom/swedbank/mobile/app/cards/f/b/d/a;
.super Ljava/lang/Object;
.source "WalletPaymentTapBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

.field private c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

.field private d:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/a/e/g/b/d/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/g/b/d/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/g/b/d/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->e:Lcom/swedbank/mobile/a/e/g/b/d/a$a;

    .line 18
    sget-object p1, Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    .line 19
    invoke-static {}, Lio/reactivex/o;->e()Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.empty()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->d:Lio/reactivex/o;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/app/cards/f/b/d/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;

    .line 26
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/app/cards/f/b/d/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "startWithState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;

    .line 30
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    return-object v0
.end method

.method public final a(Lio/reactivex/o;)Lcom/swedbank/mobile/app/cards/f/b/d/a;
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/f/b/d/a;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "tapStateStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;

    .line 34
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->d:Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/cards/f/b/d/a;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "selectedCardForTap"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;

    .line 22
    iput-object p1, v0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->e:Lcom/swedbank/mobile/a/e/g/b/d/a$a;

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->b:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/d/a$a;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/c;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/d/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->c:Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/d/a$a;->b(Lcom/swedbank/mobile/business/cards/wallet/payment/tap/e;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/d/a;->d:Lio/reactivex/o;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/b/d/a$a;->b(Lio/reactivex/o;)Lcom/swedbank/mobile/a/e/g/b/d/a$a;

    move-result-object v0

    .line 46
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/b/d/a$a;->a()Lcom/swedbank/mobile/a/e/g/b/d/a;

    move-result-object v0

    .line 47
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/b/d/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display WalletPaymentTap node without a selected card"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 38
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display WalletPaymentTap node without parent node"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
