.class public final Lcom/swedbank/mobile/app/cards/f/b/c/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WalletPaymentResultPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/f/b/c/i;",
        "Lcom/swedbank/mobile/app/cards/f/b/c/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/f/b/c/c;)Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .line 17
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;

    .line 18
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;->b()Lio/reactivex/o;

    move-result-object v0

    .line 19
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/c$g;->a:Lcom/swedbank/mobile/app/cards/f/b/c/c$g;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 22
    sget-object v1, Lcom/swedbank/mobile/app/cards/f/b/c/c$b;->a:Lcom/swedbank/mobile/app/cards/f/b/c/c$b;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 23
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/b/c/c$c;->a:Lcom/swedbank/mobile/app/cards/f/b/c/c$c;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 21
    invoke-static {v1, v2}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    new-instance v2, Lcom/swedbank/mobile/app/cards/f/b/c/c$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/cards/f/b/c/c$d;-><init>(Lcom/swedbank/mobile/app/cards/f/b/c/c;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 28
    sget-object v2, Lcom/swedbank/mobile/app/cards/f/b/c/c$e;->a:Lcom/swedbank/mobile/app/cards/f/b/c/c$e;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    const-wide/16 v3, 0x1

    .line 29
    invoke-virtual {v2, v3, v4}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object v2

    .line 30
    sget-object v3, Lcom/swedbank/mobile/app/cards/f/b/c/c$f;->a:Lcom/swedbank/mobile/app/cards/f/b/c/c$f;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 33
    check-cast v0, Lio/reactivex/s;

    .line 34
    check-cast v1, Lio/reactivex/s;

    .line 35
    check-cast v2, Lio/reactivex/s;

    .line 32
    invoke-static {v0, v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 36
    new-instance v8, Lcom/swedbank/mobile/app/cards/f/b/c/m;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/b/c/c;->a:Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/wallet/payment/result/b;->a()Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/app/cards/f/b/c/m;-><init>(Lcom/swedbank/mobile/app/cards/c;Lcom/swedbank/mobile/business/cards/wallet/payment/result/a;ZLjava/lang/Throwable;ILkotlin/e/b/g;)V

    new-instance v1, Lcom/swedbank/mobile/app/cards/f/b/c/c$a;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/app/cards/f/b/c/c;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/cards/f/b/c/c$a;-><init>(Lcom/swedbank/mobile/app/cards/f/b/c/c;)V

    check-cast v1, Lkotlin/e/a/m;

    new-instance v2, Lcom/swedbank/mobile/app/cards/f/b/c/d;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/cards/f/b/c/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v2, Lio/reactivex/c/c;

    invoke-virtual {v0, v8, v2}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026), this::viewStateReduce)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
