.class final Lcom/swedbank/mobile/app/cards/f/a/g/c$f;
.super Ljava/lang/Object;
.source "WalletOnboardingRegistrationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/f/a/g/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lcom/swedbank/mobile/app/cards/f/a/g/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/f/a/g/c$f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/g/c$f;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/f/a/g/c$f;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/f/a/g/c$f;->a:Lcom/swedbank/mobile/app/cards/f/a/g/c$f;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/cards/f/a/g/l;
    .locals 13
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    if-eqz v0, :cond_1

    .line 21
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/wallet/onboarding/registration/RegistrationError;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/g/l;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, v0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/f/a/g/l;-><init>(ZLjava/lang/Throwable;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    goto :goto_0

    .line 22
    :cond_0
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/g/l;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x3

    const/4 v12, 0x0

    move-object v7, v0

    move-object v10, p1

    invoke-direct/range {v7 .. v12}, Lcom/swedbank/mobile/app/cards/f/a/g/l;-><init>(ZLjava/lang/Throwable;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    goto :goto_0

    .line 23
    :cond_1
    new-instance v0, Lcom/swedbank/mobile/app/cards/f/a/g/l;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, v0

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/f/a/g/l;-><init>(ZLjava/lang/Throwable;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    :goto_0
    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/f/a/g/c$f;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/cards/f/a/g/l;

    move-result-object p1

    return-object p1
.end method
