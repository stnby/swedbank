.class public final Lcom/swedbank/mobile/app/cards/f/c/c;
.super Ljava/lang/Object;
.source "WalletPreconditionsResolverBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/swedbank/mobile/a/e/g/c/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/g/c/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/g/c/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/c;->c:Lcom/swedbank/mobile/a/e/g/c/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/app/cards/f/c/c;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/c;->a:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/swedbank/mobile/app/cards/f/c/c;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/f/c/c;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "notSatisfiedPreconditions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/f/c/c;->b:Ljava/util/List;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/f/c/c;->c:Lcom/swedbank/mobile/a/e/g/c/a$a;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/c/c;->a:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/c/a$a;->b(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/a/e/g/c/a$a;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/f/c/c;->b:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/g/c/a$a;->b(Ljava/util/List;)Lcom/swedbank/mobile/a/e/g/c/a$a;

    move-result-object v0

    .line 33
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/c/a$a;->a()Lcom/swedbank/mobile/a/e/g/c/a;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/g/c/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No resolution needing preconditions provided"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 27
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build preconditions resolver without a listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
