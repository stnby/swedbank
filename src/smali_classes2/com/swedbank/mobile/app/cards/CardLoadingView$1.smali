.class final Lcom/swedbank/mobile/app/cards/CardLoadingView$1;
.super Lkotlin/e/b/k;
.source "CardLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/CardLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/CardLoadingView;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/CardLoadingView;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;
    .locals 10
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "metrics"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x10

    int-to-float v0, v0

    const/4 v1, 0x1

    .line 98
    invoke-static {v1, v0, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    .line 99
    invoke-static {v1, v0, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    const/16 v0, 0xf

    int-to-float v0, v0

    const/4 v2, 0x2

    .line 100
    invoke-static {v2, v0, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    .line 36
    sget v0, Lcom/swedbank/mobile/app/cards/n$c;->card_item_group_vertical_margin:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float p1, v2

    .line 101
    invoke-static {v1, p1, p2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 39
    new-instance p1, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;

    iget-object p2, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    move-object v9, p2

    check-cast v9, Landroid/view/View;

    move-object v2, p1

    move-object v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/cards/CardLoadingView$1;FFFFILandroid/view/View;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
