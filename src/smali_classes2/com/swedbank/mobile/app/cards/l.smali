.class public final Lcom/swedbank/mobile/app/cards/l;
.super Lcom/swedbank/mobile/architect/a/h;
.source "CardsRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/n;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/cards/list/b;

.field private final f:Lcom/swedbank/mobile/app/cards/f/a/b;

.field private final g:Lcom/swedbank/mobile/app/cards/f/c/c;

.field private final h:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/list/b;Lcom/swedbank/mobile/app/cards/f/a/b;Lcom/swedbank/mobile/app/cards/f/c/c;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/cards/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/cards/f/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/cards/list/b;",
            "Lcom/swedbank/mobile/app/cards/f/a/b;",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "cardsListBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPreconditionsResolverBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPreconditionsResolverListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p5

    move-object v3, p6

    .line 37
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/l;->e:Lcom/swedbank/mobile/app/cards/list/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/l;->f:Lcom/swedbank/mobile/app/cards/f/a/b;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/l;->g:Lcom/swedbank/mobile/app/cards/f/c/c;

    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/l;->h:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/l;)Lcom/swedbank/mobile/app/cards/list/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/l;->e:Lcom/swedbank/mobile/app/cards/list/b;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/l;Lkotlin/e/a/b;)V
    .locals 0

    .line 30
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/l;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/l;)Lcom/swedbank/mobile/app/cards/f/a/b;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/l;->f:Lcom/swedbank/mobile/app/cards/f/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/l;Lkotlin/e/a/b;)V
    .locals 0

    .line 30
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/l;->c(Lkotlin/e/a/b;)V

    return-void
.end method


# virtual methods
.method public a()Lio/reactivex/w;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/business/cards/list/j;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    new-instance v0, Lcom/swedbank/mobile/app/cards/l$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/l$c;-><init>(Lcom/swedbank/mobile/app/cards/l;)V

    check-cast v0, Lio/reactivex/z;

    invoke-static {v0}, Lio/reactivex/w;->a(Lio/reactivex/z;)Lio/reactivex/w;

    move-result-object v0

    const-string v1, "Single.create { emitter \u2026e, emitter = emitter)\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)V"
        }
    .end annotation

    const-string v0, "notSatisfiedPreconditions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/l;->g:Lcom/swedbank/mobile/app/cards/f/c/c;

    .line 78
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/l;->h:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/c/c;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/app/cards/f/c/c;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/f/c/c;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/cards/f/c/c;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/c/c;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 91
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public a(Ljava/util/List;Z)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "cardsEligibleForDigitization"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    new-instance v0, Lcom/swedbank/mobile/app/cards/l$d;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/cards/l$d;-><init>(Lcom/swedbank/mobile/app/cards/l;Ljava/util/List;Z)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/l;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    const/4 v0, 0x0

    .line 86
    check-cast v0, Lio/reactivex/x;

    .line 87
    new-instance v1, Lcom/swedbank/mobile/app/cards/l$a;

    invoke-direct {v1, p0, p1, v0}, Lcom/swedbank/mobile/app/cards/l$a;-><init>(Lcom/swedbank/mobile/app/cards/l;ZLio/reactivex/x;)V

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {p0, v1}, Lcom/swedbank/mobile/app/cards/l;->a(Lcom/swedbank/mobile/app/cards/l;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 92
    sget-object v0, Lcom/swedbank/mobile/app/cards/l$b;->a:Lcom/swedbank/mobile/app/cards/l$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method
