.class public final Lcom/swedbank/mobile/app/cards/CardLayout;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "CardLayout.kt"


# static fields
.field static final synthetic g:[Lkotlin/h/g;


# instance fields
.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private final m:Lkotlin/f/c;

.field private final n:Lkotlin/f/c;

.field private final o:Lkotlin/f/c;

.field private final p:Lkotlin/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x9

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardImage"

    const-string v4, "getCardImage()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "statusView"

    const-string v4, "getStatusView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "settingsIconView"

    const-string v4, "getSettingsIconView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contactlessView"

    const-string v4, "getContactlessView()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardNumber"

    const-string v4, "getCardNumber()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardHolderName"

    const-string v4, "getCardHolderName()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardGroup"

    const-string v4, "getCardGroup()Landroid/widget/ImageView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "greyscalableViews"

    const-string v4, "getGreyscalableViews()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/CardLayout;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "greyscalePaint"

    const-string v4, "getGreyscalePaint()Landroid/graphics/Paint;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/cards/CardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/cards/CardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_image:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->h:Lkotlin/f/c;

    .line 31
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_status:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->i:Lkotlin/f/c;

    .line 32
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_settings_icon:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->j:Lkotlin/f/c;

    .line 33
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_contactless:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->k:Lkotlin/f/c;

    .line 34
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_number:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->l:Lkotlin/f/c;

    .line 35
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_holder_name:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->m:Lkotlin/f/c;

    .line 36
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_group:I

    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->n:Lkotlin/f/c;

    const/4 p3, 0x5

    .line 37
    new-array p3, p3, [I

    .line 38
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_image:I

    const/4 v1, 0x0

    aput v0, p3, v1

    .line 39
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_group:I

    const/4 v2, 0x1

    aput v0, p3, v2

    .line 40
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_number:I

    const/4 v3, 0x2

    aput v0, p3, v3

    .line 41
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_holder_name:I

    const/4 v3, 0x3

    aput v0, p3, v3

    .line 42
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_contactless:I

    const/4 v3, 0x4

    aput v0, p3, v3

    .line 37
    invoke-static {p0, p3}, Lcom/swedbank/mobile/core/ui/am;->a(Landroid/view/View;[I)Lkotlin/f/c;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->o:Lkotlin/f/c;

    .line 43
    sget-object p3, Lcom/swedbank/mobile/app/cards/CardLayout$a;->a:Lcom/swedbank/mobile/app/cards/CardLayout$a;

    check-cast p3, Lkotlin/e/a/a;

    invoke-static {p3}, Lkotlin/e;->a(Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p3

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->p:Lkotlin/d;

    .line 53
    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/cards/CardLayout;->setClipToOutline(Z)V

    .line 54
    sget p3, Lcom/swedbank/mobile/app/cards/n$g;->widget_card:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    .line 346
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const-string v5, "LayoutInflater.from(this)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 345
    invoke-virtual {v4, p3, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    if-eqz p3, :cond_2

    if-eqz p2, :cond_1

    .line 57
    sget-object p3, Lcom/swedbank/mobile/app/cards/n$i;->CardLayout:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 59
    :try_start_0
    sget p2, Lcom/swedbank/mobile/app/cards/n$i;->CardLayout_settingsIconVisible:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    .line 60
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getSettingsIconView()Landroid/view/View;

    move-result-object p3

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x4

    :goto_0
    invoke-virtual {p3, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_1

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    :cond_1
    :goto_1
    return-void

    .line 345
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 27
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 28
    sget p3, Lcom/swedbank/mobile/app/cards/n$a;->cardStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/cards/CardLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/TextView;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getStatusView()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/CardLayout;II)V
    .locals 0

    .line 25
    invoke-super {p0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->onMeasure(II)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/graphics/Paint;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getGreyscalePaint()Landroid/graphics/Paint;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getGreyscalableViews()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/ImageView;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getCardGroup()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/ImageView;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getCardImage()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/ImageView;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getContactlessView()Landroid/widget/ImageView;

    move-result-object p0

    return-object p0
.end method

.method private final getCardGroup()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->n:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getCardHolderName()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->m:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getCardImage()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getCardNumber()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->l:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final getContactlessView()Landroid/widget/ImageView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private final getGreyscalableViews()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->o:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final getGreyscalePaint()Landroid/graphics/Paint;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->p:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    return-object v0
.end method

.method private final getSettingsIconView()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getStatusView()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLayout;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/CardLayout;->g:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final setViewsGreyscaled(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 129
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->b(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/graphics/Paint;

    move-result-object p1

    .line 130
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x2

    .line 131
    invoke-virtual {v1, v2, p1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0

    .line 134
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 135
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/g;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/cards/f;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/cards/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "number"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardHolderName"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "status"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/CardLayout;->setTag(Ljava/lang/Object;)V

    .line 81
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getCardNumber()Landroid/widget/TextView;

    move-result-object p1

    invoke-static {p2}, Lcom/swedbank/mobile/app/cards/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getCardHolderName()Landroid/widget/TextView;

    move-result-object p1

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getContactlessView()Landroid/widget/ImageView;

    move-result-object p1

    const/4 p2, 0x4

    const/4 p3, 0x0

    if-eqz p4, :cond_0

    const/4 p4, 0x0

    goto :goto_0

    :cond_0
    const/4 p4, 0x4

    :goto_0
    invoke-virtual {p1, p4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 195
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->a(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/TextView;

    move-result-object p1

    .line 196
    sget-object p4, Lcom/swedbank/mobile/app/cards/a;->a:[I

    invoke-virtual {p5}, Lcom/swedbank/mobile/app/cards/f;->ordinal()I

    move-result p5

    aget p4, p4, p5

    const/4 p5, 0x2

    const/4 v0, 0x0

    packed-switch p4, :pswitch_data_0

    .line 255
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto/16 :goto_4

    .line 237
    :pswitch_0
    sget p2, Lcom/swedbank/mobile/app/cards/n$h;->card_status_default_for_mobile_contactless:I

    .line 238
    sget p4, Lcom/swedbank/mobile/app/cards/n$b;->brand_magenta:I

    .line 239
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 241
    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 250
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 251
    invoke-virtual {p2, p3, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_1

    .line 218
    :pswitch_1
    sget p2, Lcom/swedbank/mobile/app/cards/n$h;->card_status_not_activated:I

    .line 219
    sget p4, Lcom/swedbank/mobile/app/cards/n$b;->brand_blue:I

    .line 220
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 222
    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 226
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->b(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/graphics/Paint;

    move-result-object p1

    .line 227
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    .line 228
    invoke-virtual {p3, p5, p1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_2

    .line 199
    :pswitch_2
    sget p2, Lcom/swedbank/mobile/app/cards/n$h;->card_status_blocked:I

    .line 200
    sget p4, Lcom/swedbank/mobile/app/cards/n$b;->label_red:I

    .line 201
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 203
    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 207
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->b(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/graphics/Paint;

    move-result-object p1

    .line 208
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->c(Lcom/swedbank/mobile/app/cards/CardLayout;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    .line 209
    invoke-virtual {p3, p5, p1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_3

    .line 263
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 264
    invoke-virtual {p2, p3, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_4

    .line 271
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->d(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/ImageView;

    move-result-object p1

    invoke-virtual {p6}, Lcom/swedbank/mobile/business/cards/g;->a()Lcom/swedbank/mobile/business/cards/d;

    move-result-object p2

    sget-object p3, Lcom/swedbank/mobile/app/cards/a;->b:[I

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/cards/d;->ordinal()I

    move-result p2

    aget p2, p3, p2

    packed-switch p2, :pswitch_data_1

    .line 277
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_3
    sget p2, Lcom/swedbank/mobile/app/cards/n$b;->transparent:I

    goto :goto_5

    .line 276
    :pswitch_4
    sget p2, Lcom/swedbank/mobile/app/cards/n$d;->amex_logo:I

    goto :goto_5

    .line 275
    :pswitch_5
    sget p2, Lcom/swedbank/mobile/app/cards/n$d;->ic_mastercard_logo:I

    goto :goto_5

    .line 274
    :pswitch_6
    sget p2, Lcom/swedbank/mobile/app/cards/n$d;->ic_visa_electron_logo:I

    goto :goto_5

    .line 273
    :pswitch_7
    sget p2, Lcom/swedbank/mobile/app/cards/n$d;->ic_visa_logo:I

    goto :goto_5

    .line 272
    :pswitch_8
    sget p2, Lcom/swedbank/mobile/app/cards/n$d;->ic_maestro_logo:I

    .line 277
    :goto_5
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 279
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->e(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/ImageView;

    move-result-object p1

    sget-object p2, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {p2, p6}, Lcom/swedbank/mobile/app/cards/g;->a(Lcom/swedbank/mobile/business/cards/g;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 280
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget-object p2, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {p2, p6}, Lcom/swedbank/mobile/app/cards/g;->b(Lcom/swedbank/mobile/business/cards/g;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    .line 281
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->f(Lcom/swedbank/mobile/app/cards/CardLayout;)Landroid/widget/ImageView;

    move-result-object p2

    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 88
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/CardLayout;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 1

    .line 71
    new-instance v0, Lcom/swedbank/mobile/app/cards/CardLayout$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/CardLayout$b;-><init>(Lcom/swedbank/mobile/app/cards/CardLayout;)V

    check-cast v0, Lkotlin/e/a/m;

    .line 68
    invoke-static {p1, p2, v0}, Lcom/swedbank/mobile/app/cards/b;->a(IILkotlin/e/a/m;)V

    return-void
.end method
