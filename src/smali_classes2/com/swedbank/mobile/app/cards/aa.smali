.class public final Lcom/swedbank/mobile/app/cards/aa;
.super Lcom/swedbank/mobile/app/a/d;
.source "WalletPaySuccessEventTracker.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/app/a/d<",
        "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/core/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0, p2}, Lcom/swedbank/mobile/app/a/d;-><init>(Lcom/swedbank/mobile/core/a/c;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/aa;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    return-void
.end method


# virtual methods
.method protected a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;)Lio/reactivex/w;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/core/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/aa;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 15
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->l()Lio/reactivex/w;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/swedbank/mobile/app/cards/aa$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/cards/aa$a;-><init>(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/w;->e(Lio/reactivex/c/h;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "walletRepository\n      .\u2026      )\n        }\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 0

    .line 10
    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/aa;->a(Lcom/swedbank/mobile/business/cards/wallet/payment/result/a$b;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
