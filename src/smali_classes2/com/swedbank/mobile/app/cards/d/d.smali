.class public final Lcom/swedbank/mobile/app/cards/d/d;
.super Lcom/swedbank/mobile/architect/a/h;
.source "OrderCardRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/cards/order/c;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/f/a/b;

.field private final f:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/f/a/b;Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/business/general/confirmation/bottom/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_order_card"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_order_card"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/f/a/b;",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/business/general/confirmation/bottom/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bottomSheetDialogBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flowManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomSheetDialogListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    .line 25
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/d/d;->e:Lcom/swedbank/mobile/app/f/a/b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/d/d;->f:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/d/d;->g:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/d/d;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/d/d;->e:Lcom/swedbank/mobile/app/f/a/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/d/d;)Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/d/d;->g:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 26
    new-instance v0, Lcom/swedbank/mobile/app/cards/d/d$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/d/d$b;-><init>(Lcom/swedbank/mobile/app/cards/d/d;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/d/d;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 41
    sget-object v0, Lcom/swedbank/mobile/app/cards/d/d$a;->a:Lcom/swedbank/mobile/app/cards/d/d$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/d/d;->f:Lcom/swedbank/mobile/architect/business/a/c;

    new-instance v1, Lcom/swedbank/mobile/app/j/b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/j/b;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return-void
.end method
