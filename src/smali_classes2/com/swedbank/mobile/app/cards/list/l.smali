.class public final Lcom/swedbank/mobile/app/cards/list/l;
.super Landroidx/recyclerview/widget/RecyclerView$x;
.source "CardsListAdapter.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/app/cards/CardLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lcom/swedbank/mobile/app/cards/CardLayout;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/CardLayout;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardLayout"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    check-cast p1, Landroid/view/View;

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$x;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/l;->a:Lcom/swedbank/mobile/app/cards/CardLayout;

    return-void
.end method


# virtual methods
.method public final a()Lcom/swedbank/mobile/app/cards/CardLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/l;->a:Lcom/swedbank/mobile/app/cards/CardLayout;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/cards/c;)V
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/cards/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/l;->a:Lcom/swedbank/mobile/app/cards/CardLayout;

    .line 118
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->c()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->d()Ljava/lang/String;

    move-result-object v3

    .line 120
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->e()Ljava/lang/String;

    move-result-object v4

    .line 121
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->h()Z

    move-result v5

    .line 122
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->f()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v6

    .line 123
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->g()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v7

    .line 117
    invoke-virtual/range {v1 .. v7}, Lcom/swedbank/mobile/app/cards/CardLayout;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/g;)V

    return-void
.end method
