.class public final Lcom/swedbank/mobile/app/cards/list/m$o;
.super Ljava/lang/Object;
.source "CardsListRecyclerView.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/list/m;->a(Lkotlin/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

.field final synthetic b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

.field final synthetic c:Lcom/swedbank/mobile/app/cards/list/m$p;

.field final synthetic d:Ljava/util/List;

.field final synthetic e:Lcom/swedbank/mobile/app/cards/list/m;

.field final synthetic f:Lkotlin/k;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/m$p;Ljava/util/List;Lcom/swedbank/mobile/app/cards/list/m;Lkotlin/k;)V
    .locals 0

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->c:Lcom/swedbank/mobile/app/cards/list/m$p;

    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->d:Ljava/util/List;

    iput-object p5, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->e:Lcom/swedbank/mobile/app/cards/list/m;

    iput-object p6, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->f:Lkotlin/k;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->a:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 13

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->a:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 140
    move-object v1, v0

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->c:Lcom/swedbank/mobile/app/cards/list/m$p;

    invoke-virtual {v2, v1}, Lcom/swedbank/mobile/app/cards/list/m$p;->a(I)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-eqz v0, :cond_13

    .line 141
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->e:Lcom/swedbank/mobile/app/cards/list/m;

    iget-object v3, p0, Lcom/swedbank/mobile/app/cards/list/m$o;->d:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/c;

    .line 148
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object v7

    .line 149
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->k(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 150
    invoke-virtual {v9}, Lcom/swedbank/mobile/app/cards/list/r;->e()Lkotlin/k;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_1
    const/4 v4, 0x1

    const/4 v10, 0x0

    if-nez v3, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    .line 152
    :goto_2
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->e(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v5

    sget-object v6, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->g()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/swedbank/mobile/app/cards/g;->d(Lcom/swedbank/mobile/business/cards/g;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 156
    invoke-virtual {v1}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_12

    check-cast v5, Landroid/view/ViewGroup;

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->c()Landroidx/l/r;

    move-result-object v6

    check-cast v6, Landroidx/l/n;

    invoke-static {v5, v6}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 159
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v5

    const/4 v11, 0x4

    .line 160
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v6, ""

    .line 161
    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 163
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v5

    const/16 v6, 0x8

    .line 164
    invoke-virtual {v5, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 165
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 167
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v5

    .line 168
    invoke-virtual {v5, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 169
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 171
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v5

    .line 172
    invoke-virtual {v5, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 173
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    sget-object v5, Lkotlin/s;->a:Lkotlin/s;

    .line 176
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->f()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v5

    sget-object v6, Lcom/swedbank/mobile/app/cards/list/n;->a:[I

    invoke-virtual {v5}, Lcom/swedbank/mobile/app/cards/f;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    if-eqz v3, :cond_f

    .line 227
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 229
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 230
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_3

    .line 232
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 233
    invoke-virtual {v4, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 234
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 235
    check-cast v4, Landroid/view/View;

    .line 236
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$o$2;

    invoke-direct {v5, v3, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$2;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_3

    .line 199
    :pswitch_0
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_not_activated_extra_info_btn:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "context.getString(R.stri\u2026activated_extra_info_btn)"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 203
    check-cast v2, Ljava/lang/CharSequence;

    .line 204
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v2

    .line 205
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 206
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 207
    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 208
    check-cast v2, Landroid/view/View;

    .line 209
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$o$1;

    invoke-direct {v5, v3, v4, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$1;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 178
    :pswitch_1
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_blocked_extra_info:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context.getString(R.stri\u2026.card_blocked_extra_info)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 179
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 180
    check-cast v2, Ljava/lang/CharSequence;

    .line 193
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 194
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 195
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 242
    :cond_3
    :goto_3
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v3

    .line 243
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 244
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 248
    :cond_4
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->b()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 250
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_retry:I

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026let_card_provision_retry)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 254
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_failed:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 255
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v5

    .line 256
    invoke-virtual {v5, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 257
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 259
    check-cast v5, Landroid/view/View;

    .line 260
    new-instance v6, Lcom/swedbank/mobile/app/cards/list/m$o$3;

    invoke-direct {v6, v2, v4, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$3;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v6, Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v3, :cond_f

    .line 266
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 267
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 268
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 275
    :cond_5
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->j()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v3

    sget-object v5, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    if-eq v3, v5, :cond_6

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v3

    sget-object v5, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    if-eq v3, v5, :cond_6

    const/4 v3, 0x1

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    :goto_4
    if-eqz v3, :cond_d

    .line 278
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v3

    sget-object v5, Lcom/swedbank/mobile/app/cards/list/n;->b:[I

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/cards/t;->ordinal()I

    move-result v3

    aget v3, v5, v3

    packed-switch v3, :pswitch_data_1

    .line 445
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not renderable state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 440
    :pswitch_2
    sget-object v2, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    goto/16 :goto_6

    .line 335
    :pswitch_3
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->m()Lcom/swedbank/mobile/app/cards/o;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 336
    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 339
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/o;->c()Z

    move-result v6

    if-nez v6, :cond_8

    .line 342
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 343
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_7

    .line 345
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 346
    invoke-virtual {v4, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 347
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 348
    check-cast v4, Landroid/view/View;

    .line 349
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$o$6;

    invoke-direct {v5, v3, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$6;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 353
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 355
    :cond_7
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v3

    .line 356
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 363
    :cond_8
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v6

    if-nez v6, :cond_9

    .line 365
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_no_suk:I

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "resources.getString(R.string.wallet_no_suk)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 366
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 367
    move-object v4, v2

    check-cast v4, Ljava/lang/CharSequence;

    .line 380
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v4

    .line 381
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 382
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    const/4 v6, 0x0

    goto :goto_5

    .line 387
    :cond_9
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/o;->b()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 389
    sget v6, Lcom/swedbank/mobile/app/cards/n$h;->wallet_low_suk:I

    new-array v8, v4, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v10

    invoke-virtual {v5, v6, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "resources.getString(R.st\u2026ow_suk, count.toString())"

    invoke-static {v3, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 390
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 391
    move-object v6, v2

    check-cast v6, Ljava/lang/CharSequence;

    .line 404
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v6

    .line 405
    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 406
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 407
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    :cond_a
    const/4 v6, 0x1

    .line 415
    :goto_5
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_pay_btn:I

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "resources.getString(R.string.wallet_pay_btn)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v3

    check-cast v4, Ljava/lang/CharSequence;

    .line 420
    check-cast v2, Ljava/lang/CharSequence;

    .line 421
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v2

    .line 422
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 423
    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 424
    invoke-virtual {v2, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 425
    check-cast v2, Landroid/view/View;

    .line 426
    new-instance v12, Lcom/swedbank/mobile/app/cards/list/m$o$7;

    move-object v3, v12

    move v5, v6

    move-object v6, v1

    move-object v8, v0

    invoke-direct/range {v3 .. v8}, Lcom/swedbank/mobile/app/cards/list/m$o$7;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Landroid/content/Context;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v12, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 430
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 438
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 335
    :cond_b
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 311
    :pswitch_4
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_retry:I

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026let_card_provision_retry)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 315
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_failed:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 316
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v5

    .line 317
    invoke-virtual {v5, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 318
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 319
    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 320
    check-cast v5, Landroid/view/View;

    .line 321
    new-instance v6, Lcom/swedbank/mobile/app/cards/list/m$o$5;

    invoke-direct {v6, v2, v4, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$5;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v6, Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v3, :cond_f

    .line 327
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 328
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 329
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 330
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_6

    .line 283
    :pswitch_5
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 284
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_c

    .line 286
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 287
    invoke-virtual {v4, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 288
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 289
    check-cast v4, Landroid/view/View;

    .line 290
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$o$4;

    invoke-direct {v5, v3, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$4;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 294
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 296
    :cond_c
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v3

    .line 297
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_6

    .line 448
    :cond_d
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 449
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_enable_mobile_contactless:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "context.getString(R.stri\u2026nable_mobile_contactless)"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 453
    check-cast v2, Ljava/lang/CharSequence;

    .line 454
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v2

    .line 455
    invoke-virtual {v2, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 456
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 457
    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 458
    check-cast v2, Landroid/view/View;

    .line 459
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$o$8;

    invoke-direct {v5, v3, v4, v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$o$8;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 463
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_6

    .line 471
    :cond_e
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v3

    if-nez v3, :cond_f

    .line 472
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_not_eligible_for_digitization_extra_info:I

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context.getString(R.stri\u2026_digitization_extra_info)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 473
    check-cast v2, Ljava/lang/CharSequence;

    .line 486
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 487
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 488
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    :cond_f
    :goto_6
    if-eqz v9, :cond_11

    .line 498
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v0

    .line 499
    invoke-virtual {v9}, Lcom/swedbank/mobile/app/cards/list/r;->e()Lkotlin/k;

    move-result-object v2

    if-eqz v0, :cond_10

    if-eqz v2, :cond_10

    .line 500
    invoke-virtual {v2}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 501
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setVisibility(I)V

    .line 502
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 503
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object v3

    .line 504
    invoke-virtual {v2}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/e/p$c;

    sget-object v2, Lcom/swedbank/mobile/app/cards/list/n;->c:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/e/p$c;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_2

    .line 509
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_other:I

    goto :goto_7

    .line 508
    :pswitch_6
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_rooted:I

    goto :goto_7

    .line 507
    :pswitch_7
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_no_hce:I

    goto :goto_7

    .line 506
    :pswitch_8
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_no_nfc:I

    goto :goto_7

    .line 505
    :pswitch_9
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_os_api_level:I

    .line 504
    :goto_7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(when\u2026reason_other\n          })"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v1

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    .line 511
    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;

    .line 512
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_acknowledge:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "resources.getString(R.st\u2026not_eligible_acknowledge)"

    invoke-static {v0, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    .line 511
    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;-><init>(Ljava/lang/CharSequence;)V

    move-object v6, v1

    check-cast v6, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;

    const/4 v7, 0x2

    const/4 v8, 0x0

    .line 503
    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a(Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;ILjava/lang/Object;)V

    goto :goto_8

    .line 514
    :cond_10
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object v0

    invoke-virtual {v0, v11}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setVisibility(I)V

    .line 517
    :goto_8
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    .line 141
    :cond_11
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_9

    .line 156
    :cond_12
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    :goto_9
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method
