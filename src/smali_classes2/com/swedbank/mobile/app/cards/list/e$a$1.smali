.class final Lcom/swedbank/mobile/app/cards/list/e$a$1;
.super Ljava/lang/Object;
.source "CardsListPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/list/e$a;->a(Lio/reactivex/o;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Lio/reactivex/o<",
        "TT;>;",
        "Lio/reactivex/s<",
        "TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/list/e$a;

.field final synthetic b:Lio/reactivex/o;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/list/e$a;Lio/reactivex/o;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/e$a$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/e$a$1;->b:Lio/reactivex/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lio/reactivex/o;)Lio/reactivex/o;
    .locals 2
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;)",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    const-string v0, "firstQueryCardsStream"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->b()Lio/reactivex/c/k;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/swedbank/mobile/app/cards/list/e$a$1$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/list/e$a$1$1;-><init>(Lcom/swedbank/mobile/app/cards/list/e$a$1;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 71
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->a()Lio/reactivex/c/k;

    move-result-object v1

    invoke-virtual {p1, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    .line 72
    new-instance v1, Lcom/swedbank/mobile/app/cards/list/e$a$1$2;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/list/e$a$1$2;-><init>(Lcom/swedbank/mobile/app/cards/list/e$a$1;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {p1, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    check-cast p1, Lio/reactivex/s;

    .line 54
    invoke-static {v0, p1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lio/reactivex/o;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/e$a$1;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
