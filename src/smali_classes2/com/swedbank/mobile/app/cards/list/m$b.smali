.class final synthetic Lcom/swedbank/mobile/app/cards/list/m$b;
.super Lkotlin/e/b/i;
.source "CardsListViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/list/m;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/app/cards/c;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/list/m;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/cards/c;)V
    .locals 12
    .param p1    # Lcom/swedbank/mobile/app/cards/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m$b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/cards/list/m;

    .line 433
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object v5

    .line 434
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->k(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object v7

    const/4 v1, 0x0

    if-eqz v7, :cond_0

    .line 435
    invoke-virtual {v7}, Lcom/swedbank/mobile/app/cards/list/r;->e()Lkotlin/k;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    const/4 v3, 0x1

    const/4 v8, 0x0

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 437
    :goto_1
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->e(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v4

    sget-object v6, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->g()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/swedbank/mobile/app/cards/g;->d(Lcom/swedbank/mobile/business/cards/g;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 441
    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_11

    check-cast v4, Landroid/view/ViewGroup;

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->c()Landroidx/l/r;

    move-result-object v6

    check-cast v6, Landroidx/l/n;

    invoke-static {v4, v6}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 444
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v4

    const/4 v9, 0x4

    .line 445
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v6, ""

    .line 446
    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 448
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    const/16 v6, 0x8

    .line 449
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 450
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 451
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 452
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 453
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 454
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 456
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 457
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 458
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 459
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 461
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->f()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v4

    sget-object v6, Lcom/swedbank/mobile/app/cards/list/n;->a:[I

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/f;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    if-eqz v2, :cond_e

    .line 512
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 514
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 515
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    .line 517
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v3

    .line 518
    invoke-virtual {v3, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 519
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 520
    check-cast v3, Landroid/view/View;

    .line 521
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$e;

    invoke-direct {v4, v2, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$e;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 525
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_2

    .line 484
    :pswitch_0
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_not_activated_extra_info_btn:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "context.getString(R.stri\u2026activated_extra_info_btn)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 488
    check-cast v1, Ljava/lang/CharSequence;

    .line 489
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v1

    .line 490
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 491
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 492
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 493
    check-cast v1, Landroid/view/View;

    .line 494
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$j;

    invoke-direct {v4, v2, v3, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$j;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 498
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 463
    :pswitch_1
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_blocked_extra_info:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026.card_blocked_extra_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 464
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 465
    check-cast v1, Ljava/lang/CharSequence;

    .line 478
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 479
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 480
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 481
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 527
    :cond_2
    :goto_2
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 528
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 529
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 530
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 533
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 535
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_retry:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026let_card_provision_retry)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 539
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_failed:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 540
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 541
    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 542
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 543
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 544
    check-cast v4, Landroid/view/View;

    .line 545
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$h;

    invoke-direct {v5, v1, v3, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$h;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 549
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v2, :cond_e

    .line 551
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 552
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 553
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 554
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 560
    :cond_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v2

    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    if-eq v2, v4, :cond_5

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v2

    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    if-eq v2, v4, :cond_5

    const/4 v2, 0x1

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_c

    .line 563
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v2

    sget-object v4, Lcom/swedbank/mobile/app/cards/list/n;->b:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/t;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_1

    .line 730
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not renderable state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 725
    :pswitch_2
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    goto/16 :goto_5

    .line 620
    :pswitch_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->m()Lcom/swedbank/mobile/app/cards/o;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 621
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 624
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->c()Z

    move-result v6

    if-nez v6, :cond_7

    .line 627
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 628
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    .line 630
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v3

    .line 631
    invoke-virtual {v3, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 632
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 633
    check-cast v3, Landroid/view/View;

    .line 634
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$g;

    invoke-direct {v4, v2, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$g;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 638
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 640
    :cond_6
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 641
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 642
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 648
    :cond_7
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v6

    if-nez v6, :cond_8

    .line 650
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_no_suk:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "resources.getString(R.string.wallet_no_suk)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 651
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 652
    move-object v3, v1

    check-cast v3, Ljava/lang/CharSequence;

    .line 665
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v3

    .line 666
    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 667
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 668
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    const/4 v3, 0x0

    goto :goto_4

    .line 672
    :cond_8
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->b()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 674
    sget v6, Lcom/swedbank/mobile/app/cards/n$h;->wallet_low_suk:I

    new-array v10, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v8

    invoke-virtual {v4, v6, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "resources.getString(R.st\u2026ow_suk, count.toString())"

    invoke-static {v2, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 675
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 676
    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    .line 689
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v6

    .line 690
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 691
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 700
    :cond_9
    :goto_4
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_pay_btn:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "resources.getString(R.string.wallet_pay_btn)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 705
    check-cast v1, Ljava/lang/CharSequence;

    .line 706
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v1

    .line 707
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 708
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 709
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 710
    move-object v10, v1

    check-cast v10, Landroid/view/View;

    .line 711
    new-instance v11, Lcom/swedbank/mobile/app/cards/list/m$d;

    move-object v1, v11

    move-object v4, v0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/list/m$d;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Landroid/content/Context;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v11, Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 715
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    .line 723
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 620
    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 596
    :pswitch_4
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_retry:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026let_card_provision_retry)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 600
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_failed:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 601
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 602
    invoke-virtual {v4, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 603
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 604
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 605
    check-cast v4, Landroid/view/View;

    .line 606
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$i;

    invoke-direct {v5, v1, v3, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$i;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 610
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v2, :cond_e

    .line 612
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 613
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 614
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 568
    :pswitch_5
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 569
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    .line 571
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v3

    .line 572
    invoke-virtual {v3, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 573
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 574
    check-cast v3, Landroid/view/View;

    .line 575
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$f;

    invoke-direct {v4, v2, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$f;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 579
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 581
    :cond_b
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 582
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 583
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 584
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_5

    .line 733
    :cond_c
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 734
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_enable_mobile_contactless:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "context.getString(R.stri\u2026nable_mobile_contactless)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 738
    check-cast v1, Ljava/lang/CharSequence;

    .line 739
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v1

    .line 740
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setVisibility(I)V

    .line 741
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 742
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 743
    check-cast v1, Landroid/view/View;

    .line 744
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$c;

    invoke-direct {v4, v2, v3, v0, p1}, Lcom/swedbank/mobile/app/cards/list/m$c;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 748
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_5

    .line 756
    :cond_d
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v2

    if-nez v2, :cond_e

    .line 757
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_not_eligible_for_digitization_extra_info:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026_digitization_extra_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 758
    check-cast v1, Ljava/lang/CharSequence;

    .line 771
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 772
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 773
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 774
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    :cond_e
    :goto_5
    if-eqz v7, :cond_10

    .line 783
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result p1

    .line 784
    invoke-virtual {v7}, Lcom/swedbank/mobile/app/cards/list/r;->e()Lkotlin/k;

    move-result-object v1

    if-eqz p1, :cond_f

    if-eqz v1, :cond_f

    .line 785
    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_f

    .line 786
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object p1

    invoke-virtual {p1, v8}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setVisibility(I)V

    .line 787
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 788
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object v2

    .line 789
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/e/p$c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/n;->c:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/e/p$c;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_2

    .line 794
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_other:I

    goto :goto_6

    .line 793
    :pswitch_6
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_rooted:I

    goto :goto_6

    .line 792
    :pswitch_7
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_no_hce:I

    goto :goto_6

    .line 791
    :pswitch_8
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_no_nfc:I

    goto :goto_6

    .line 790
    :pswitch_9
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_os_api_level:I

    .line 789
    :goto_6
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(when\u2026reason_other\n          })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    .line 796
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;

    .line 797
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_acknowledge:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "resources.getString(R.st\u2026not_eligible_acknowledge)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 796
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;-><init>(Ljava/lang/CharSequence;)V

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 788
    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a(Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;ILjava/lang/Object;)V

    goto :goto_7

    .line 799
    :cond_f
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object p1

    invoke-virtual {p1, v9}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setVisibility(I)V

    .line 802
    :goto_7
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    :cond_10
    return-void

    .line 441
    :cond_11
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/app/cards/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$b;->a(Lcom/swedbank/mobile/app/cards/c;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "renderSelectedCardExtraInfo"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "renderSelectedCardExtraInfo(Lcom/swedbank/mobile/app/cards/CardOverview;)V"

    return-object v0
.end method
