.class public final Lcom/swedbank/mobile/app/cards/list/r$a$a;
.super Lcom/swedbank/mobile/app/cards/list/r$a;
.source "CardsListViewState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/cards/list/r$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Landroidx/recyclerview/widget/f$b;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/f$b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ")V"
        }
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/cards/list/r$a;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/r$a$a;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/r$a$a;->b:Landroidx/recyclerview/widget/f$b;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/r$a$a;->a:Ljava/util/List;

    return-object v0
.end method

.method public final b()Landroidx/recyclerview/widget/f$b;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/r$a$a;->b:Landroidx/recyclerview/widget/f$b;

    return-object v0
.end method
