.class public final Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;
.super Ljava/lang/Object;
.source "CardsListRecyclerView.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

.field final synthetic b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)V
    .locals 0

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;->b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;->a:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;->a:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 139
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;->b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;->b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Landroidx/recyclerview/widget/o;

    move-result-object v1

    .line 141
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/o;->a(Landroidx/recyclerview/widget/RecyclerView$i;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 143
    invoke-virtual {v1, v0, v2}, Landroidx/recyclerview/widget/o;->a(Landroidx/recyclerview/widget/RecyclerView$i;Landroid/view/View;)[I

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 145
    aget v2, v0, v1

    const/4 v3, 0x1

    if-nez v2, :cond_0

    aget v2, v0, v3

    if-eqz v2, :cond_1

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;->b:Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    aget v1, v0, v1

    aget v0, v0, v3

    invoke-virtual {v2, v1, v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->scrollBy(II)V

    :cond_1
    return-void
.end method
