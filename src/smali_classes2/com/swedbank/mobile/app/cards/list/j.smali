.class public final Lcom/swedbank/mobile/app/cards/list/j;
.super Ljava/lang/Object;
.source "CardsListTransition.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/b/a/f;


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/list/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/j;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/list/j;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/list/j;->a:Lcom/swedbank/mobile/app/cards/list/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 56
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    sget v1, Lcom/swedbank/mobile/app/cards/n$e;->root_layout:I

    if-ne v0, v1, :cond_0

    return-object p1

    .line 59
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of v0, p1, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    move-object p1, v1

    :cond_1
    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_2

    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/cards/list/j;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v1

    :cond_2
    return-object v1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/j;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/cards/list/j;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 0

    .line 12
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->a(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V
    .locals 9
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p5    # Lkotlin/e/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            "Z",
            "Lkotlin/e/a/a<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transitionCompleted"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-static {p0, p1}, Lcom/swedbank/mobile/app/cards/list/j;->a(Lcom/swedbank/mobile/app/cards/list/j;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz p4, :cond_1

    .line 64
    sget v1, Lcom/swedbank/mobile/app/cards/n$e;->tag_key_selected_card_id:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 65
    sget v2, Lcom/swedbank/mobile/app/cards/n$e;->cards_list:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    if-eqz v2, :cond_2

    if-eqz v1, :cond_0

    .line 66
    check-cast v1, Ljava/lang/String;

    .line 67
    invoke-virtual {v2, v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Ljava/lang/String;)V

    .line 69
    sget v1, Lcom/swedbank/mobile/app/cards/n$e;->tag_key_selected_card_id:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 66
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.business.cards.CardId /* = kotlin.String */"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    if-eqz p3, :cond_2

    .line 73
    sget v1, Lcom/swedbank/mobile/app/cards/n$e;->cards_list:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    if-eqz v1, :cond_2

    .line 74
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c()Ljava/lang/String;

    move-result-object v1

    .line 75
    sget v2, Lcom/swedbank/mobile/app/cards/n$e;->tag_key_selected_card_id:I

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 24
    :cond_2
    :goto_0
    sget-object v3, Lcom/swedbank/mobile/architect/a/b/a/d;->a:Lcom/swedbank/mobile/architect/a/b/a/d;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    move-object v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/swedbank/mobile/architect/a/b/a/d;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;ZLkotlin/e/a/a;)V

    return-void
.end method

.method public b()V
    .locals 0

    .line 12
    invoke-static {p0}, Lcom/swedbank/mobile/architect/a/b/a/f$a;->b(Lcom/swedbank/mobile/architect/a/b/a/f;)V

    return-void
.end method
