.class public final Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "CardsListRecyclerView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/app/cards/list/a;

.field private final b:Lcom/swedbank/mobile/app/cards/list/d;

.field private final c:Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

.field private final d:Landroidx/recyclerview/widget/o;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance p2, Lcom/swedbank/mobile/app/cards/list/a;

    invoke-direct {p2}, Lcom/swedbank/mobile/app/cards/list/a;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    .line 23
    new-instance p2, Lcom/swedbank/mobile/app/cards/list/d;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/app/cards/list/d;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b:Lcom/swedbank/mobile/app/cards/list/d;

    .line 30
    new-instance p2, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 p3, 0x0

    .line 31
    invoke-virtual {p2, p3}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->b(I)V

    .line 32
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_item_card_layout:I

    invoke-virtual {p2, v0}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->a(I)V

    .line 33
    move-object v0, p2

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 30
    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c:Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    const/4 p2, 0x1

    .line 36
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->setHasFixedSize(Z)V

    .line 37
    new-instance p2, Landroidx/recyclerview/widget/o;

    invoke-direct {p2}, Landroidx/recyclerview/widget/o;-><init>()V

    .line 38
    move-object v0, p0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p2, v0}, Landroidx/recyclerview/widget/o;->a(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 37
    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->d:Landroidx/recyclerview/widget/o;

    .line 40
    iget-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 41
    iget-object p2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b:Lcom/swedbank/mobile/app/cards/list/d;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 42
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/p;

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c:Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$i;

    sget v1, Lcom/swedbank/mobile/app/cards/n$e;->card_item_card_layout:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p2, p1, v0, v1}, Lcom/swedbank/mobile/core/ui/widget/p;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView$i;Ljava/lang/Integer;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 44
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$f;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroidx/recyclerview/widget/e;

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/e;->a(Z)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 19
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 20
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/app/cards/list/a;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c:Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Landroidx/recyclerview/widget/o;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->d:Landroidx/recyclerview/widget/o;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/a;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 157
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/app/cards/list/a;

    move-result-object v0

    .line 158
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/list/a;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 159
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 160
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->scrollToPosition(I)V

    .line 161
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/app/cards/list/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/list/a;->c(I)V

    .line 163
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object p1

    new-instance v0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$c;

    invoke-direct {v0, p0, p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$c;-><init>(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)V

    check-cast v0, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 82
    :cond_0
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->e:Ljava/lang/String;

    :cond_1
    :goto_0
    return-void
.end method

.method public final a(Lkotlin/k;)V
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/list/a;->a(Lkotlin/k;)V

    const/4 p1, 0x0

    .line 54
    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->f:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    move-object v1, p0

    check-cast v1, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    .line 139
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/app/cards/list/a;

    move-result-object v2

    .line 140
    invoke-virtual {v2, v0}, Lcom/swedbank/mobile/app/cards/list/a;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 142
    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->scrollToPosition(I)V

    .line 143
    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)Lcom/swedbank/mobile/app/cards/list/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/swedbank/mobile/app/cards/list/a;->c(I)V

    .line 145
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;

    invoke-direct {v2, v1, v1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$b;-><init>(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;)V

    check-cast v2, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 56
    :cond_0
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->e:Ljava/lang/String;

    return-void
.end method

.method public final b()I
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c:Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    invoke-virtual {v0}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->o()I

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 62
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b()I

    move-result v0

    .line 63
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 64
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/cards/list/a;->getItemCount()I

    move-result v2

    if-gez v0, :cond_0

    goto :goto_0

    :cond_0
    if-le v2, v0, :cond_1

    .line 65
    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/app/cards/list/a;->a(I)Lcom/swedbank/mobile/app/cards/c;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/c;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 69
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()V
    .locals 3

    .line 73
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b()I

    move-result v0

    .line 74
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c:Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;

    invoke-virtual {v1, v0}, Lcom/swedbank/mobile/util/widget/CenterZoomLinearLayoutManager;->c(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    sget v1, Lcom/swedbank/mobile/app/cards/n$e;->card_item_card_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "cardLayout"

    .line 155
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_transition_target:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$i;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$i;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/LinearLayoutManager;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$i;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 123
    instance-of v0, p1, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;

    if-nez v0, :cond_0

    .line 124
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 127
    :cond_0
    check-cast p1, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->f:Ljava/lang/String;

    .line 128
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;->a()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 129
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;->b()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/list/a;->a(Ljava/util/List;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 117
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;

    .line 118
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 119
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a:Lcom/swedbank/mobile/app/cards/list/a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/list/a;->b()Ljava/util/List;

    move-result-object v2

    .line 120
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->c()Ljava/lang/String;

    move-result-object v3

    .line 117
    invoke-direct {v0, v1, v2, v3}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView$a;-><init>(Landroid/os/Parcelable;Ljava/util/List;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method
