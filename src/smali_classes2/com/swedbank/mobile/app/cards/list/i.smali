.class public final Lcom/swedbank/mobile/app/cards/list/i;
.super Ljava/lang/Object;
.source "CardsListRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/list/h;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/d/b;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/k;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final q:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/d/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 79
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->a:Ljavax/inject/Provider;

    move-object v1, p2

    .line 80
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->b:Ljavax/inject/Provider;

    move-object v1, p3

    .line 81
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->c:Ljavax/inject/Provider;

    move-object v1, p4

    .line 82
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->d:Ljavax/inject/Provider;

    move-object v1, p5

    .line 83
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->e:Ljavax/inject/Provider;

    move-object v1, p6

    .line 84
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->f:Ljavax/inject/Provider;

    move-object v1, p7

    .line 85
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->g:Ljavax/inject/Provider;

    move-object v1, p8

    .line 86
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->h:Ljavax/inject/Provider;

    move-object v1, p9

    .line 87
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->i:Ljavax/inject/Provider;

    move-object v1, p10

    .line 88
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->j:Ljavax/inject/Provider;

    move-object v1, p11

    .line 89
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->k:Ljavax/inject/Provider;

    move-object v1, p12

    .line 90
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->l:Ljavax/inject/Provider;

    move-object v1, p13

    .line 91
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->m:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 92
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->n:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 93
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->o:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 94
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->p:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 95
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->q:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 96
    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->r:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/list/i;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/data/device/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/a/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/a/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/f/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/cards/d/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/details/k;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/list/i;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 121
    new-instance v19, Lcom/swedbank/mobile/app/cards/list/i;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/swedbank/mobile/app/cards/list/i;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v19
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/list/h;
    .locals 22

    move-object/from16 v0, p0

    .line 101
    new-instance v20, Lcom/swedbank/mobile/app/cards/list/h;

    move-object/from16 v1, v20

    iget-object v2, v0, Lcom/swedbank/mobile/app/cards/list/i;->a:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    iget-object v3, v0, Lcom/swedbank/mobile/app/cards/list/i;->b:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/data/device/a;

    iget-object v4, v0, Lcom/swedbank/mobile/app/cards/list/i;->c:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/architect/business/a/c;

    iget-object v5, v0, Lcom/swedbank/mobile/app/cards/list/i;->d:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/swedbank/mobile/app/cards/a/c;

    iget-object v6, v0, Lcom/swedbank/mobile/app/cards/list/i;->e:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/swedbank/mobile/app/cards/f/a/b;

    iget-object v7, v0, Lcom/swedbank/mobile/app/cards/list/i;->f:Ljavax/inject/Provider;

    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/swedbank/mobile/app/cards/f/c/c;

    iget-object v8, v0, Lcom/swedbank/mobile/app/cards/list/i;->g:Ljavax/inject/Provider;

    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/swedbank/mobile/app/f/a;

    iget-object v9, v0, Lcom/swedbank/mobile/app/cards/list/i;->h:Ljavax/inject/Provider;

    invoke-interface {v9}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/swedbank/mobile/app/cards/d/b;

    iget-object v10, v0, Lcom/swedbank/mobile/app/cards/list/i;->i:Ljavax/inject/Provider;

    invoke-interface {v10}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/swedbank/mobile/app/a/d;

    iget-object v11, v0, Lcom/swedbank/mobile/app/cards/list/i;->j:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/swedbank/mobile/business/cards/details/k;

    iget-object v12, v0, Lcom/swedbank/mobile/app/cards/list/i;->k:Ljavax/inject/Provider;

    invoke-interface {v12}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iget-object v13, v0, Lcom/swedbank/mobile/app/cards/list/i;->l:Ljavax/inject/Provider;

    invoke-interface {v13}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    iget-object v14, v0, Lcom/swedbank/mobile/app/cards/list/i;->m:Ljavax/inject/Provider;

    invoke-interface {v14}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/swedbank/mobile/business/general/confirmation/c;

    iget-object v15, v0, Lcom/swedbank/mobile/app/cards/list/i;->n:Ljavax/inject/Provider;

    invoke-interface {v15}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/swedbank/mobile/business/general/confirmation/c;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->o:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/swedbank/mobile/business/cards/order/b;

    iget-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->p:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/swedbank/mobile/architect/business/c;

    iget-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->q:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/swedbank/mobile/architect/a/b/f;

    iget-object v1, v0, Lcom/swedbank/mobile/app/cards/list/i;->r:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/swedbank/mobile/architect/a/b/g;

    move-object/from16 v1, v21

    invoke-direct/range {v1 .. v19}, Lcom/swedbank/mobile/app/cards/list/h;-><init>(Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/app/cards/a/c;Lcom/swedbank/mobile/app/cards/f/a/b;Lcom/swedbank/mobile/app/cards/f/c/c;Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/app/cards/d/b;Lcom/swedbank/mobile/app/a/d;Lcom/swedbank/mobile/business/cards/details/k;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/cards/order/b;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v20
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/i;->a()Lcom/swedbank/mobile/app/cards/list/h;

    move-result-object v0

    return-object v0
.end method
