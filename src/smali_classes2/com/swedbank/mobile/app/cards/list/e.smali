.class public final Lcom/swedbank/mobile/app/cards/list/e;
.super Lcom/swedbank/mobile/app/cards/c/b/c;
.source "CardsListPresenter.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/list/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/list/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/list/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    move-object v0, p1

    check-cast v0, Lcom/swedbank/mobile/business/cards/notauth/list/d;

    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/cards/c/b/c;-><init>(Lcom/swedbank/mobile/business/cards/notauth/list/d;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/e;->a:Lcom/swedbank/mobile/business/cards/list/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/e;)Lcom/swedbank/mobile/business/cards/list/d;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/e;->a:Lcom/swedbank/mobile/business/cards/list/d;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/list/e;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 18
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/e;->d()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected b()Lcom/swedbank/mobile/app/cards/list/r;
    .locals 10
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 22
    new-instance v9, Lcom/swedbank/mobile/app/cards/list/r;

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/e;->a:Lcom/swedbank/mobile/business/cards/list/d;

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/d;->e()Z

    move-result v2

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3d

    const/4 v8, 0x0

    move-object v0, v9

    .line 22
    invoke-direct/range {v0 .. v8}, Lcom/swedbank/mobile/app/cards/list/r;-><init>(ZZLkotlin/k;Lcom/swedbank/mobile/business/util/e;Lkotlin/k;ZILkotlin/e/b/g;)V

    return-object v9
.end method

.method protected c()[Lio/reactivex/o;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/cards/list/r$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 28
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 29
    sget-object v1, Lcom/swedbank/mobile/app/cards/list/e$c;->a:Lcom/swedbank/mobile/app/cards/list/e$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/cards/list/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 27
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/swedbank/mobile/app/cards/list/e$d;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/list/e$d;-><init>(Lcom/swedbank/mobile/app/cards/list/e;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable\n        .merg\u2026ream.loading())\n        }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-static {v0}, Lcom/b/a/b;->a(Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/e;->a:Lcom/swedbank/mobile/business/cards/list/d;

    .line 49
    invoke-interface {v1}, Lcom/swedbank/mobile/business/cards/list/d;->m()Lio/reactivex/o;

    move-result-object v1

    .line 50
    new-instance v2, Lcom/swedbank/mobile/app/cards/list/e$a;

    invoke-direct {v2, p0, v0}, Lcom/swedbank/mobile/app/cards/list/e$a;-><init>(Lcom/swedbank/mobile/app/cards/list/e;Lio/reactivex/o;)V

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->k(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "interactor\n        .obse\u2026              }\n        }"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/swedbank/mobile/app/cards/c/b/e;->a()Lkotlin/e/a/m;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v4, v2, v3, v4}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v1

    .line 82
    sget-object v2, Lcom/swedbank/mobile/app/cards/list/e$b;->a:Lcom/swedbank/mobile/app/cards/list/e$b;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 88
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/list/e;->a:Lcom/swedbank/mobile/business/cards/list/d;

    .line 89
    invoke-interface {v2}, Lcom/swedbank/mobile/business/cards/list/d;->g()Lio/reactivex/o;

    move-result-object v2

    .line 90
    sget-object v4, Lcom/swedbank/mobile/app/cards/list/e$g;->a:Lcom/swedbank/mobile/app/cards/list/e$g;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v2, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 96
    sget-object v4, Lcom/swedbank/mobile/app/cards/list/e$e;->a:Lcom/swedbank/mobile/app/cards/list/e$e;

    check-cast v4, Lkotlin/e/a/b;

    invoke-virtual {p0, v4}, Lcom/swedbank/mobile/app/cards/list/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v4

    .line 97
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/e$f;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/cards/list/e$f;-><init>(Lcom/swedbank/mobile/app/cards/list/e;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->c(Lio/reactivex/c/h;)Lio/reactivex/b;

    move-result-object v4

    .line 98
    invoke-virtual {v4}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v4

    .line 100
    sget-object v5, Lcom/swedbank/mobile/app/cards/list/e$j;->a:Lcom/swedbank/mobile/app/cards/list/e$j;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/cards/list/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 101
    new-instance v6, Lcom/swedbank/mobile/app/cards/list/e$k;

    iget-object v7, p0, Lcom/swedbank/mobile/app/cards/list/e;->a:Lcom/swedbank/mobile/business/cards/list/d;

    invoke-direct {v6, v7}, Lcom/swedbank/mobile/app/cards/list/e$k;-><init>(Lcom/swedbank/mobile/business/cards/list/d;)V

    check-cast v6, Lkotlin/e/a/b;

    new-instance v7, Lcom/swedbank/mobile/app/cards/list/f;

    invoke-direct {v7, v6}, Lcom/swedbank/mobile/app/cards/list/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 102
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 103
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    .line 105
    sget-object v6, Lcom/swedbank/mobile/app/cards/list/e$h;->a:Lcom/swedbank/mobile/app/cards/list/e$h;

    check-cast v6, Lkotlin/e/a/b;

    invoke-virtual {p0, v6}, Lcom/swedbank/mobile/app/cards/list/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v6

    .line 106
    new-instance v7, Lcom/swedbank/mobile/app/cards/list/e$i;

    invoke-direct {v7, p0}, Lcom/swedbank/mobile/app/cards/list/e$i;-><init>(Lcom/swedbank/mobile/app/cards/list/e;)V

    check-cast v7, Lio/reactivex/c/g;

    invoke-virtual {v6, v7}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v6

    .line 107
    invoke-virtual {v6}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v6

    .line 108
    invoke-virtual {v6}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v6

    .line 110
    sget-object v7, Lcom/swedbank/mobile/app/cards/list/e$l;->a:Lcom/swedbank/mobile/app/cards/list/e$l;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {p0, v7}, Lcom/swedbank/mobile/app/cards/list/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    .line 111
    new-instance v8, Lcom/swedbank/mobile/app/cards/list/e$m;

    invoke-direct {v8, p0}, Lcom/swedbank/mobile/app/cards/list/e$m;-><init>(Lcom/swedbank/mobile/app/cards/list/e;)V

    check-cast v8, Lio/reactivex/c/g;

    invoke-virtual {v7, v8}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v7

    .line 112
    invoke-virtual {v7}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v7

    .line 113
    invoke-virtual {v7}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v7

    const/4 v8, 0x7

    .line 115
    new-array v8, v8, [Lio/reactivex/o;

    const-string v9, "allCardsStream"

    .line 116
    invoke-static {v1, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x0

    aput-object v1, v8, v9

    aput-object v0, v8, v3

    const-string v0, "digitizationPossibilityStream"

    .line 118
    invoke-static {v2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    aput-object v2, v8, v0

    const-string v0, "digitizationNotPossibleAcknowledgementStream"

    .line 119
    invoke-static {v4, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    aput-object v4, v8, v0

    const-string v0, "provisionRetryStream"

    .line 120
    invoke-static {v5, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    aput-object v5, v8, v0

    const-string v0, "orderCardStream"

    .line 121
    invoke-static {v6, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x5

    aput-object v6, v8, v0

    const-string v0, "viewBusinessCardsStream"

    .line 122
    invoke-static {v7, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x6

    aput-object v7, v8, v0

    return-object v8
.end method
