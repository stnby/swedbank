.class public final Lcom/swedbank/mobile/app/cards/list/h;
.super Lcom/swedbank/mobile/business/i/f;
.source "CardsListRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/j/d;
.implements Lcom/swedbank/mobile/business/cards/list/k;


# instance fields
.field private final e:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/data/device/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/swedbank/mobile/app/cards/a/c;

.field private final i:Lcom/swedbank/mobile/app/cards/f/a/b;

.field private final j:Lcom/swedbank/mobile/app/cards/f/c/c;

.field private final k:Lcom/swedbank/mobile/app/f/a;

.field private final l:Lcom/swedbank/mobile/app/cards/d/b;

.field private final m:Lcom/swedbank/mobile/app/a/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/swedbank/mobile/business/cards/details/k;

.field private final o:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private final p:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

.field private final q:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private final r:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private final s:Lcom/swedbank/mobile/business/cards/order/b;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/app/cards/a/c;Lcom/swedbank/mobile/app/cards/f/a/b;Lcom/swedbank/mobile/app/cards/f/c/c;Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/app/cards/d/b;Lcom/swedbank/mobile/app/a/d;Lcom/swedbank/mobile/business/cards/details/k;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/cards/order/b;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 16
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/cards/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/cards/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/app/cards/f/c/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/app/cards/d/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/app/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "trackWalletPayButtonEvent"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/business/cards/details/k;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p13    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "outdated_card_shortcut_help_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p14    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "wallet_general_error_notification_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p15    # Lcom/swedbank/mobile/business/cards/order/b;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p16    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p17    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_cards_list"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p18    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/app/cards/a/c;",
            "Lcom/swedbank/mobile/app/cards/f/a/b;",
            "Lcom/swedbank/mobile/app/cards/f/c/c;",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/app/cards/d/b;",
            "Lcom/swedbank/mobile/app/a/d<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/swedbank/mobile/business/cards/details/k;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "context"

    invoke-static {v1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityManager"

    invoke-static {v2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flowManager"

    invoke-static {v3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardDetailsBuilder"

    invoke-static {v4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingBuilder"

    invoke-static {v5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPreconditionsResolverBuilder"

    invoke-static {v6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogBuilder"

    invoke-static {v7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderCardBuilder"

    invoke-static {v8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trackWalletPayButtonEvent"

    invoke-static {v9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardDetailsListener"

    invoke-static {v10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingListener"

    invoke-static {v11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletPreconditionsResolverListener"

    invoke-static {v12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outdatedCardShortcutHelpListener"

    invoke-static {v13, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletGeneralErrorNotificationListener"

    invoke-static {v14, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderCardListener"

    invoke-static {v15, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    move-object/from16 v14, p18

    invoke-static {v14, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v13, p16

    .line 72
    invoke-direct {v0, v13, v14, v15}, Lcom/swedbank/mobile/business/i/f;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/h;->e:Landroid/app/Application;

    iput-object v2, v0, Lcom/swedbank/mobile/app/cards/list/h;->f:Lcom/swedbank/mobile/data/device/a;

    iput-object v3, v0, Lcom/swedbank/mobile/app/cards/list/h;->g:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object v4, v0, Lcom/swedbank/mobile/app/cards/list/h;->h:Lcom/swedbank/mobile/app/cards/a/c;

    iput-object v5, v0, Lcom/swedbank/mobile/app/cards/list/h;->i:Lcom/swedbank/mobile/app/cards/f/a/b;

    iput-object v6, v0, Lcom/swedbank/mobile/app/cards/list/h;->j:Lcom/swedbank/mobile/app/cards/f/c/c;

    iput-object v7, v0, Lcom/swedbank/mobile/app/cards/list/h;->k:Lcom/swedbank/mobile/app/f/a;

    iput-object v8, v0, Lcom/swedbank/mobile/app/cards/list/h;->l:Lcom/swedbank/mobile/app/cards/d/b;

    iput-object v9, v0, Lcom/swedbank/mobile/app/cards/list/h;->m:Lcom/swedbank/mobile/app/a/d;

    iput-object v10, v0, Lcom/swedbank/mobile/app/cards/list/h;->n:Lcom/swedbank/mobile/business/cards/details/k;

    iput-object v11, v0, Lcom/swedbank/mobile/app/cards/list/h;->o:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-object v12, v0, Lcom/swedbank/mobile/app/cards/list/h;->p:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/h;->q:Lcom/swedbank/mobile/business/general/confirmation/c;

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/h;->r:Lcom/swedbank/mobile/business/general/confirmation/c;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/swedbank/mobile/app/cards/list/h;->s:Lcom/swedbank/mobile/business/cards/order/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/h;->k:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/h;Lkotlin/e/a/b;)V
    .locals 0

    .line 53
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/h;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/h;->q:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/h;->r:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/app/cards/d/b;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/h;->l:Lcom/swedbank/mobile/app/cards/d/b;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/business/cards/order/b;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/h;->s:Lcom/swedbank/mobile/business/cards/order/b;

    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/swedbank/mobile/business/cards/details/l;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->h:Lcom/swedbank/mobile/app/cards/a/c;

    .line 74
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/a/c;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/cards/a/c;

    move-result-object p1

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->n:Lcom/swedbank/mobile/business/cards/details/k;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/cards/a/c;->a(Lcom/swedbank/mobile/business/cards/details/k;)Lcom/swedbank/mobile/app/cards/a/c;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/a/c;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 195
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->b(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/details/l;

    return-object p1
.end method

.method public a(Ljava/lang/String;ZZ)Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->i:Lcom/swedbank/mobile/app/cards/f/a/b;

    if-eqz p3, :cond_0

    .line 86
    invoke-static {p1}, Lcom/swedbank/mobile/business/util/m;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/l;

    move-result-object p3

    goto :goto_0

    :cond_0
    sget-object p3, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    check-cast p3, Lcom/swedbank/mobile/business/util/l;

    :goto_0
    invoke-virtual {v0, p3}, Lcom/swedbank/mobile/app/cards/f/a/b;->a(Lcom/swedbank/mobile/business/util/l;)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object p3

    .line 88
    invoke-static {p1}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 87
    invoke-virtual {p3, p1, p2}, Lcom/swedbank/mobile/app/cards/f/a/b;->a(Ljava/util/List;Z)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object p1

    .line 90
    iget-object p2, p0, Lcom/swedbank/mobile/app/cards/list/h;->o:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    invoke-virtual {p1, p2}, Lcom/swedbank/mobile/app/cards/f/a/b;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 198
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->b(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/cards/wallet/onboarding/h;

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/business/cards/wallet/w;)Lio/reactivex/j;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/w;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/w;",
            ")",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/h$f;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/cards/list/h$f;-><init>(Lcom/swedbank/mobile/app/cards/list/h;Lcom/swedbank/mobile/business/cards/wallet/w;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.create { emitter -\u2026achAsChild() })\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 196
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/h$a;->a:Lcom/swedbank/mobile/app/cards/list/h$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/wallet/ab;",
            ">;)V"
        }
    .end annotation

    const-string v0, "notSatisfiedPreconditions"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->j:Lcom/swedbank/mobile/app/cards/f/c/c;

    .line 97
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/h;->p:Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/c/c;->a(Lcom/swedbank/mobile/business/cards/wallet/preconditions/d;)Lcom/swedbank/mobile/app/cards/f/c/c;

    move-result-object v0

    .line 98
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/cards/f/c/c;->a(Ljava/util/List;)Lcom/swedbank/mobile/app/cards/f/c/c;

    move-result-object p1

    .line 99
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/f/c/c;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 201
    invoke-static {p0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 199
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/h$c;->a:Lcom/swedbank/mobile/app/cards/list/h$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {p0, p1}, Lcom/swedbank/mobile/app/j/d$a;->a(Lcom/swedbank/mobile/app/j/d;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 202
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/h$d;->a:Lcom/swedbank/mobile/app/cards/list/h$d;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->g:Lcom/swedbank/mobile/architect/business/a/c;

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/e;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/business/cards/wallet/payment/e;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->m:Lcom/swedbank/mobile/app/a/d;

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/a/d;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public d()Lio/reactivex/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/j<",
            "Lcom/swedbank/mobile/business/general/confirmation/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 109
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/h$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/list/h$e;-><init>(Lcom/swedbank/mobile/app/cards/list/h;)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object v0

    const-string v1, "Maybe.create { emitter -\u2026achAsChild() })\n    }\n  }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public e()V
    .locals 1

    const-string v0, "outdated_card_shortcut_help_listener"

    .line 119
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/g;->a(Ljava/lang/String;)Lkotlin/e/a/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/list/h;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public f()V
    .locals 1

    const-string v0, "wallet_general_error_notification_listener"

    .line 132
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/g;->a(Ljava/lang/String;)Lkotlin/e/a/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/list/h;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public g()V
    .locals 1

    .line 134
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/h$g;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/list/h$g;-><init>(Lcom/swedbank/mobile/app/cards/list/h;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/list/h;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public h()V
    .locals 1

    .line 204
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/h$b;->a:Lcom/swedbank/mobile/app/cards/list/h$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public i()Landroid/app/Application;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->e:Landroid/app/Application;

    return-object v0
.end method

.method public j()Lcom/swedbank/mobile/data/device/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h;->f:Lcom/swedbank/mobile/data/device/a;

    return-object v0
.end method
