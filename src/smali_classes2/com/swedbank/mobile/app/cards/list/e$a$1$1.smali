.class final Lcom/swedbank/mobile/app/cards/list/e$a$1$1;
.super Ljava/lang/Object;
.source "CardsListPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/list/e$a$1;->a(Lio/reactivex/o;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/list/e$a$1;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/list/e$a$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/e$a$1$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)Lio/reactivex/o;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;)",
            "Lio/reactivex/o<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/a;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/list/e$a$1$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a$1;

    iget-object p1, p1, Lcom/swedbank/mobile/app/cards/list/e$a$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a;

    iget-object p1, p1, Lcom/swedbank/mobile/app/cards/list/e$a;->b:Lio/reactivex/o;

    .line 61
    sget-object v0, Lcom/swedbank/mobile/app/cards/list/e$a$1$1$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a$1$1$1;

    check-cast v0, Lio/reactivex/c/k;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 65
    invoke-virtual {p1, v0, v1}, Lio/reactivex/o;->d(J)Lio/reactivex/o;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object p1

    .line 67
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/e$a$1$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a$1;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/list/e$a$1;->a:Lcom/swedbank/mobile/app/cards/list/e$a;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/list/e$a;->a:Lcom/swedbank/mobile/app/cards/list/e;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/e;->a(Lcom/swedbank/mobile/app/cards/list/e;)Lcom/swedbank/mobile/business/cards/list/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/list/d;->m()Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/b;->a(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/e$a$1$1;->a(Ljava/util/List;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
