.class public final Lcom/swedbank/mobile/app/cards/list/m;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "CardsListViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/list/k;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final A:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final C:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private final m:Lkotlin/f/c;

.field private final n:Lkotlin/f/c;

.field private final o:Lkotlin/f/c;

.field private final p:Lkotlin/f/c;

.field private final q:Lkotlin/f/c;

.field private final r:Lkotlin/f/c;

.field private final s:Lkotlin/f/c;

.field private final t:Lkotlin/f/c;

.field private final u:Lkotlin/f/c;

.field private final v:Lkotlin/f/c;

.field private w:Lcom/swedbank/mobile/core/ui/widget/t;

.field private x:Lcom/swedbank/mobile/app/cards/list/r;

.field private final y:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x15

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardsList"

    const-string v4, "getCardsList()Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardPrimaryActionButton"

    const-string v4, "getCardPrimaryActionButton()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardSecondaryActionButton"

    const-string v4, "getCardSecondaryActionButton()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "extraInfoView"

    const-string v4, "getExtraInfoView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "extraInfoBtn"

    const-string v4, "getExtraInfoBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "balanceGroup"

    const-string v4, "getBalanceGroup()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "balanceView"

    const-string v4, "getBalanceView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "creditGroup"

    const-string v4, "getCreditGroup()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "creditView"

    const-string v4, "getCreditView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "orderCardButton"

    const-string v4, "getOrderCardButton()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "informationView"

    const-string v4, "getInformationView()Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "privateNoCardsViews"

    const-string v4, "getPrivateNoCardsViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "privateNoCardsMessage"

    const-string v4, "getPrivateNoCardsMessage()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "businessNoCardsViews"

    const-string v4, "getBusinessNoCardsViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "businessViewCardsButton"

    const-string v4, "getBusinessViewCardsButton()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenLoadingView"

    const-string v4, "getFullScreenLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 40
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->toolbar:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->b:Lkotlin/f/c;

    .line 41
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_refresh:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->c:Lkotlin/f/c;

    .line 42
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->d:Lkotlin/f/c;

    .line 43
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_card_primary_action_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->e:Lkotlin/f/c;

    .line 44
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_card_secondary_action_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->f:Lkotlin/f/c;

    .line 45
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_card_extra_info_text:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->g:Lkotlin/f/c;

    .line 46
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_card_extra_info_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->h:Lkotlin/f/c;

    .line 47
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_balance_group:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->i:Lkotlin/f/c;

    .line 48
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_balance:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->j:Lkotlin/f/c;

    .line 49
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_credit_group:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->k:Lkotlin/f/c;

    .line 50
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_credit:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->l:Lkotlin/f/c;

    .line 51
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_order_card_action_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->m:Lkotlin/f/c;

    .line 52
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_information_overlay:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->n:Lkotlin/f/c;

    .line 53
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_error_title:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->o:Lkotlin/f/c;

    .line 54
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_error_retry_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->p:Lkotlin/f/c;

    .line 56
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_no_cards_group:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->q:Lkotlin/f/c;

    .line 57
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_no_cards_message:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->r:Lkotlin/f/c;

    .line 58
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_no_cards_group_business:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->s:Lkotlin/f/c;

    .line 59
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_view_business_cards_btn:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->t:Lkotlin/f/c;

    .line 60
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_error_group:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->u:Lkotlin/f/c;

    .line 61
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_full_loading:I

    invoke-static {p0, v0}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->v:Lkotlin/f/c;

    .line 65
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<DigitizedCardId>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->y:Lcom/b/c/c;

    .line 66
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<CardId>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->z:Lcom/b/c/c;

    .line 67
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<DigitizedCardId>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->A:Lcom/b/c/c;

    .line 68
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<CardId>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->B:Lcom/b/c/c;

    .line 69
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<CardId>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->C:Lcom/b/c/c;

    return-void
.end method

.method private final A()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->q:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final B()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->r:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final C()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->s:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final D()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->t:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final E()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->u:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final F()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->v:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->F()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/m;Lkotlin/k;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/cards/list/m;->a(Lkotlin/k;)V

    return-void
.end method

.method private final a(Lkotlin/k;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;)V"
        }
    .end annotation

    .line 214
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r()Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    move-result-object v2

    const/4 v0, 0x0

    .line 215
    invoke-virtual {v2, v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->setVisibility(I)V

    .line 216
    invoke-virtual {v2, p1}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Lkotlin/k;)V

    .line 217
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/util/List;

    .line 218
    new-instance v3, Lcom/swedbank/mobile/app/cards/list/m$p;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/cards/list/m$p;-><init>(Ljava/util/List;)V

    .line 220
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->b()I

    move-result v1

    .line 222
    invoke-virtual {v3, v1}, Lcom/swedbank/mobile/app/cards/list/m$p;->a(I)Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/cards/c;

    .line 599
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object v5

    .line 600
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->k(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/app/cards/list/r;

    move-result-object v7

    const/4 v1, 0x0

    if-eqz v7, :cond_0

    .line 601
    invoke-virtual {v7}, Lcom/swedbank/mobile/app/cards/list/r;->e()Lkotlin/k;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    const/4 v3, 0x1

    if-nez v2, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 603
    :goto_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->e(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v4

    sget-object v6, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->g()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/swedbank/mobile/app/cards/g;->d(Lcom/swedbank/mobile/business/cards/g;)I

    move-result v6

    invoke-virtual {v4, v6}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 607
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_10

    check-cast v4, Landroid/view/ViewGroup;

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->c()Landroidx/l/r;

    move-result-object v6

    check-cast v6, Landroidx/l/n;

    invoke-static {v4, v6}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 610
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v4

    const/4 v8, 0x4

    .line 611
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v6, ""

    .line 612
    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 614
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    const/16 v6, 0x8

    .line 615
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 616
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 617
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 618
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 619
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 620
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 621
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 622
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 623
    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 624
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 625
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 627
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->f()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v4

    sget-object v6, Lcom/swedbank/mobile/app/cards/list/n;->a:[I

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/f;->ordinal()I

    move-result v4

    aget v4, v6, v4

    packed-switch v4, :pswitch_data_0

    if-eqz v2, :cond_e

    .line 678
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 680
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 681
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    .line 683
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v3

    .line 684
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 685
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 686
    check-cast v3, Landroid/view/View;

    .line 687
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$q;

    invoke-direct {v4, v2, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$q;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 691
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_2

    .line 650
    :pswitch_0
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_not_activated_extra_info_btn:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "context.getString(R.stri\u2026activated_extra_info_btn)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 654
    check-cast v1, Ljava/lang/CharSequence;

    .line 655
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v1

    .line 656
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 657
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 658
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 659
    check-cast v1, Landroid/view/View;

    .line 660
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$v;

    invoke-direct {v4, v2, v3, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$v;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 664
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 629
    :pswitch_1
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_blocked_extra_info:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026.card_blocked_extra_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 630
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 631
    check-cast v1, Ljava/lang/CharSequence;

    .line 644
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 645
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 646
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 647
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 693
    :cond_2
    :goto_2
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 694
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 695
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 696
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 699
    :cond_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 701
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_retry:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026let_card_provision_retry)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 705
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_failed:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 706
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 707
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 708
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 709
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 710
    check-cast v4, Landroid/view/View;

    .line 711
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$t;

    invoke-direct {v5, v1, v3, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$t;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 715
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v2, :cond_e

    .line 717
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 718
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 719
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 720
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 726
    :cond_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v2

    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    if-eq v2, v4, :cond_5

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v2

    sget-object v4, Lcom/swedbank/mobile/business/cards/t;->f:Lcom/swedbank/mobile/business/cards/t;

    if-eq v2, v4, :cond_5

    const/4 v2, 0x1

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_c

    .line 729
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v2

    sget-object v4, Lcom/swedbank/mobile/app/cards/list/n;->b:[I

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/t;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_1

    .line 896
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not renderable state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->l()Lcom/swedbank/mobile/business/cards/t;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 891
    :pswitch_2
    sget-object v1, Lcom/swedbank/mobile/business/util/i;->a:Lcom/swedbank/mobile/business/util/i;

    goto/16 :goto_5

    .line 786
    :pswitch_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->m()Lcom/swedbank/mobile/app/cards/o;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 787
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 790
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->c()Z

    move-result v6

    if-nez v6, :cond_7

    .line 793
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 794
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    .line 796
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v3

    .line 797
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 798
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 799
    check-cast v3, Landroid/view/View;

    .line 800
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$s;

    invoke-direct {v4, v2, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$s;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 804
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 806
    :cond_6
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 807
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 808
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 809
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 814
    :cond_7
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v6

    if-nez v6, :cond_8

    .line 816
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_no_suk:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "resources.getString(R.string.wallet_no_suk)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 817
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 818
    move-object v3, v1

    check-cast v3, Ljava/lang/CharSequence;

    .line 831
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v3

    .line 832
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 833
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 834
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    const/4 v3, 0x0

    goto :goto_4

    .line 838
    :cond_8
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->b()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 840
    sget v6, Lcom/swedbank/mobile/app/cards/n$h;->wallet_low_suk:I

    new-array v9, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/o;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v9, v0

    invoke-virtual {v4, v6, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "resources.getString(R.st\u2026ow_suk, count.toString())"

    invoke-static {v2, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 841
    invoke-static {}, Lcom/swedbank/mobile/business/util/u;->c()Lkotlin/e/a/a;

    .line 842
    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    .line 855
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v6

    .line 856
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 857
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 858
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 866
    :cond_9
    :goto_4
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_pay_btn:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "resources.getString(R.string.wallet_pay_btn)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 871
    check-cast v1, Ljava/lang/CharSequence;

    .line 872
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v1

    .line 873
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 874
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 875
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 876
    move-object v9, v1

    check-cast v9, Landroid/view/View;

    .line 877
    new-instance v10, Lcom/swedbank/mobile/app/cards/list/m$l;

    move-object v1, v10

    move-object v4, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/list/m$l;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Landroid/content/Context;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v10, Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 881
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    .line 889
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 786
    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 762
    :pswitch_4
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_retry:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026let_card_provision_retry)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 766
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_card_provision_failed:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 767
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v4

    .line 768
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 769
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 770
    invoke-virtual {v4, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 771
    check-cast v4, Landroid/view/View;

    .line 772
    new-instance v5, Lcom/swedbank/mobile/app/cards/list/m$u;

    invoke-direct {v5, v1, v3, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$u;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 776
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    if-eqz v2, :cond_e

    .line 778
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 779
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 780
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 781
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_5

    .line 734
    :pswitch_5
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_digitization_in_progress_info:I

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026ization_in_progress_info)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 735
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_cancel_digitization_in_progress:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    .line 737
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v3

    .line 738
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 739
    invoke-virtual {v3, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 740
    check-cast v3, Landroid/view/View;

    .line 741
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$r;

    invoke-direct {v4, v2, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$r;-><init>(Ljava/lang/CharSequence;Lcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 745
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    .line 747
    :cond_b
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v2

    .line 748
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 749
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 750
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_5

    .line 899
    :cond_c
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 900
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->wallet_onboarding_enable_mobile_contactless:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "context.getString(R.stri\u2026nable_mobile_contactless)"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 904
    check-cast v1, Ljava/lang/CharSequence;

    .line 905
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v1

    .line 906
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 907
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 908
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 909
    check-cast v1, Landroid/view/View;

    .line 910
    new-instance v4, Lcom/swedbank/mobile/app/cards/list/m$k;

    invoke-direct {v4, v2, v3, p0, p1}, Lcom/swedbank/mobile/app/cards/list/m$k;-><init>(Ljava/lang/CharSequence;ZLcom/swedbank/mobile/app/cards/list/m;Lcom/swedbank/mobile/app/cards/c;)V

    check-cast v4, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 914
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_5

    .line 922
    :cond_d
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result v2

    if-nez v2, :cond_e

    .line 923
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_not_eligible_for_digitization_extra_info:I

    invoke-virtual {v5, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026_digitization_extra_info)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/CharSequence;

    .line 924
    check-cast v1, Ljava/lang/CharSequence;

    .line 937
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v1

    .line 938
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 939
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 940
    sget-object v1, Lkotlin/s;->a:Lkotlin/s;

    :cond_e
    :goto_5
    if-eqz v7, :cond_12

    .line 949
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/c;->i()Z

    move-result p1

    .line 950
    invoke-virtual {v7}, Lcom/swedbank/mobile/app/cards/list/r;->e()Lkotlin/k;

    move-result-object v1

    if-eqz p1, :cond_f

    if-eqz v1, :cond_f

    .line 951
    invoke-virtual {v1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_f

    .line 952
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setVisibility(I)V

    .line 953
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 954
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object v2

    .line 955
    invoke-virtual {v1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/e/p$c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/n;->c:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/e/p$c;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_2

    .line 960
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_other:I

    goto :goto_6

    .line 959
    :pswitch_6
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_rooted:I

    goto :goto_6

    .line 958
    :pswitch_7
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_no_hce:I

    goto :goto_6

    .line 957
    :pswitch_8
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_no_nfc:I

    goto :goto_6

    .line 956
    :pswitch_9
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_reason_os_api_level:I

    .line 955
    :goto_6
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(when\u2026reason_other\n          })"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v0

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    .line 962
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;

    .line 963
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->wallet_not_eligible_acknowledge:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "resources.getString(R.st\u2026not_eligible_acknowledge)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 962
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a$a;-><init>(Ljava/lang/CharSequence;)V

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;

    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 954
    invoke-static/range {v2 .. v7}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a(Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView$a;ILjava/lang/Object;)V

    goto :goto_7

    .line 965
    :cond_f
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object p1

    invoke-virtual {p1, v8}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->setVisibility(I)V

    .line 968
    :goto_7
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_8

    .line 607
    :cond_10
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 970
    :cond_11
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    new-instance v8, Lcom/swedbank/mobile/app/cards/list/m$o;

    move-object v0, v8

    move-object v1, v2

    move-object v5, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/app/cards/list/m$o;-><init>(Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;Lcom/swedbank/mobile/app/cards/list/m$p;Ljava/util/List;Lcom/swedbank/mobile/app/cards/list/m;Lkotlin/k;)V

    check-cast v8, Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v7, v8}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 229
    :cond_12
    :goto_8
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
    .end packed-switch
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->q()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/m;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->w:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez p0, :cond_0

    const-string v0, "snackbarRenderer"

    invoke-static {v0}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/appcompat/widget/Toolbar;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r()Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->E()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->y()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->C()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->A()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/app/cards/list/r;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->x:Lcom/swedbank/mobile/app/cards/list/r;

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/b/c/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->B:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/b/c/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->z:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/b/c/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->C:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic o(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/b/c/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->y:Lcom/b/c/c;

    return-object p0
.end method

.method private final p()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic p(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/b/c/c;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/m;->A:Lcom/b/c/c;

    return-object p0
.end method

.method private final q()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method public static final synthetic q(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->x()Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final r()Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    return-object v0
.end method

.method private final s()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final t()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method public static final synthetic t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->u()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->v()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final u()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final v()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final w()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->m:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final x()Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->n:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    return-object v0
.end method

.method private final y()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->o:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final z()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->p:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/list/m;->a:[Lkotlin/h/g;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 103
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->q()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 104
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->z()Landroid/widget/Button;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 448
    new-instance v2, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v2, Lio/reactivex/o;

    check-cast v2, Lio/reactivex/s;

    .line 105
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/m;->w:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v1, :cond_0

    const-string v3, "snackbarRenderer"

    invoke-static {v3}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 102
    invoke-static {v0, v2, v1}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/cards/list/r;)V
    .locals 14
    .param p1    # Lcom/swedbank/mobile/app/cards/list/r;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/m;->x:Lcom/swedbank/mobile/app/cards/list/r;

    .line 451
    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_14

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->c()Landroidx/l/r;

    move-result-object v1

    check-cast v1, Landroidx/l/n;

    invoke-static {v0, v1}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 453
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->j(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 454
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->i(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 455
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->g(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 456
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->a(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->a()Z

    move-result v0

    .line 125
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->c()Lkotlin/k;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 126
    :goto_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->c()Lkotlin/k;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->c()Lkotlin/k;

    move-result-object v5

    invoke-virtual {v5}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v5, 0x1

    .line 127
    :goto_2
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    .line 128
    :goto_3
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->f()Z

    move-result v7

    if-eqz v0, :cond_4

    if-nez v5, :cond_5

    :cond_4
    if-eqz v2, :cond_6

    if-nez v6, :cond_6

    .line 460
    :cond_5
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->a(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 461
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->b(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 462
    invoke-virtual {v0, v4}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 463
    invoke-virtual {v0, v4}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_7

    .line 467
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->b(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 468
    invoke-virtual {v0, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 469
    invoke-virtual {v0, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_4

    .line 473
    :cond_7
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->b(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 474
    invoke-virtual {v0, v4}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 475
    invoke-virtual {v0, v3}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    :goto_4
    if-nez v7, :cond_8

    .line 480
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->b(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 481
    invoke-virtual {v0, v4}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 482
    invoke-virtual {v0, v4}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 130
    :cond_8
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->a()Z

    move-result v0

    .line 131
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->b()Z

    move-result v2

    .line 132
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->c()Lkotlin/k;

    move-result-object v5

    .line 133
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/r;->d()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    if-eqz v5, :cond_c

    .line 486
    invoke-virtual {v5}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    if-eqz v6, :cond_c

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    xor-int/2addr v6, v3

    if-ne v6, v3, :cond_c

    .line 487
    invoke-static {p0, v5}, Lcom/swedbank/mobile/app/cards/list/m;->a(Lcom/swedbank/mobile/app/cards/list/m;Lkotlin/k;)V

    if-eqz p1, :cond_12

    .line 490
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object v0

    .line 491
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->cards_list_general_loading_error:I

    .line 494
    instance-of v2, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v2, :cond_a

    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 496
    move-object v4, v2

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v3, v4

    if-eqz v3, :cond_9

    move-object v4, v2

    check-cast v4, Ljava/lang/Iterable;

    const-string v0, "\n"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x3e

    const/4 v12, 0x0

    invoke-static/range {v4 .. v12}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 497
    :cond_9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 499
    :cond_a
    instance-of v1, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v1, :cond_b

    move-object v1, p1

    check-cast v1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    .line 500
    invoke-static {v1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_5
    const-string v1, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 501
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    .line 503
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->d(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    .line 504
    new-instance v2, Lcom/swedbank/mobile/app/cards/list/m$m;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/cards/list/m$m;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lkotlin/e/a/b;

    .line 505
    new-instance v3, Lcom/swedbank/mobile/app/cards/list/m$n;

    invoke-direct {v3, p0, v0}, Lcom/swedbank/mobile/app/cards/list/m$n;-><init>(Lcom/swedbank/mobile/app/cards/list/m;Ljava/lang/CharSequence;)V

    check-cast v3, Lkotlin/e/a/b;

    .line 503
    invoke-virtual {v1, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/t;->a(Lkotlin/e/a/b;Lkotlin/e/a/b;)V

    goto/16 :goto_7

    .line 500
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_c
    if-nez v0, :cond_12

    .line 513
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->e(Lcom/swedbank/mobile/app/cards/list/m;)Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    sget v6, Lcom/swedbank/mobile/app/cards/n$h;->cards_view_title:I

    invoke-virtual {v0, v6}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 514
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->f(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    move-result-object v0

    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->setVisibility(I)V

    .line 516
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->t(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v0

    .line 517
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v6, ""

    .line 518
    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->u(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v0

    .line 521
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v6, 0x0

    .line 522
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 524
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v0

    .line 525
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 526
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->s(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/Button;

    move-result-object v0

    .line 529
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 530
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_10

    .line 534
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->g(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 535
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->h(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/widget/TextView;

    move-result-object v0

    .line 536
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->c(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/content/Context;

    move-result-object v1

    .line 537
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->cards_list_general_loading_error:I

    .line 540
    instance-of v4, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v4, :cond_e

    move-object v4, p1

    check-cast v4, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 542
    move-object v5, v4

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    xor-int/2addr v3, v5

    if-eqz v3, :cond_d

    move-object v5, v4

    check-cast v5, Ljava/lang/Iterable;

    const-string v1, "\n"

    move-object v6, v1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    .line 543
    :cond_d
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    .line 545
    :cond_e
    instance-of v2, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v2, :cond_f

    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Throwable;

    .line 546
    invoke-static {v2}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_6
    const-string v2, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 547
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 548
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 546
    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_10
    if-eqz v5, :cond_12

    if-eqz v2, :cond_11

    .line 551
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->i(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    .line 552
    :cond_11
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->j(Lcom/swedbank/mobile/app/cards/list/m;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_12
    :goto_7
    if-nez p1, :cond_13

    .line 557
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/m;->d(Lcom/swedbank/mobile/app/cards/list/m;)Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :cond_13
    return-void

    .line 451
    :cond_14
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 38
    check-cast p1, Lcom/swedbank/mobile/app/cards/list/r;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/m;->a(Lcom/swedbank/mobile/app/cards/list/r;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 107
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r()Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->B:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 109
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->x()Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/OverlayInformationView;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()V
    .locals 6

    .line 433
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 434
    new-instance v1, Lcom/swedbank/mobile/app/cards/list/m$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/cards/list/m$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/cards/list/o;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/cards/list/o;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 435
    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->w:Lcom/swedbank/mobile/core/ui/widget/t;

    .line 73
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->q()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v1

    const/4 v2, 0x2

    div-int/2addr v1, v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZI)V

    .line 75
    new-array v1, v2, [I

    sget v2, Lcom/swedbank/mobile/app/cards/n$b;->brand_orange:I

    const/4 v4, 0x0

    aput v2, v1, v4

    sget v2, Lcom/swedbank/mobile/app/cards/n$b;->brand_turqoise:I

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 78
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->p()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    .line 79
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->cards_view_title:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    .line 82
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->r()Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/widget/o;->a(Landroidx/recyclerview/widget/RecyclerView;)Lio/reactivex/o;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/swedbank/mobile/app/cards/list/m$b;

    move-object v2, p0

    check-cast v2, Lcom/swedbank/mobile/app/cards/list/m;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/cards/list/m$b;-><init>(Lcom/swedbank/mobile/app/cards/list/m;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/cards/list/p;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/cards/list/p;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->c(Lio/reactivex/c/g;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "cardsList.observeSelecte\u2026derSelectedCardExtraInfo)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 436
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 86
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->B()Landroid/widget/TextView;

    move-result-object v0

    .line 87
    invoke-static {v0, v3}, Landroidx/core/widget/i;->b(Landroid/widget/TextView;I)V

    .line 88
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/m;->n()Landroid/content/Context;

    move-result-object v1

    .line 438
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 90
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->cards_list_no_cards_message:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 91
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->no_cards_message_highlight:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "context.getString(R.stri\u2026_cards_message_highlight)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lkotlin/j/n;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    xor-int/2addr v3, v5

    if-eqz v3, :cond_0

    .line 93
    move-object v3, v2

    check-cast v3, Ljava/lang/Appendable;

    invoke-static {v3}, Lkotlin/j/n;->a(Ljava/lang/Appendable;)Ljava/lang/Appendable;

    .line 94
    sget v3, Lcom/swedbank/mobile/app/cards/n$b;->brand_yellow:I

    .line 441
    invoke-static {v1, v3}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v1

    .line 442
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v3, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 443
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 95
    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 445
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 447
    :cond_0
    new-instance v1, Landroid/text/SpannedString;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {v1, v2}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public f()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 114
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->w()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 449
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public g()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->z:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public h()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->A:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public i()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->y:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public j()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/m;->C:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public k()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 115
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/list/m;->D()Landroid/view/View;

    move-result-object v0

    .line 450
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
