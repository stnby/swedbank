.class public final Lcom/swedbank/mobile/app/cards/list/a;
.super Landroidx/recyclerview/widget/RecyclerView$a;
.source "CardsListAdapter.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/widget/q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/swedbank/mobile/app/cards/list/l;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/widget/q<",
        "Lcom/swedbank/mobile/app/cards/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Ljava/lang/Integer;

.field private e:Landroidx/recyclerview/widget/f$b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 22
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    .line 23
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<CardOverview>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->a:Lcom/b/c/c;

    .line 24
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/a;)Lcom/b/c/c;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/a;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/list/a;Ljava/lang/Integer;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/a;->d:Ljava/lang/Integer;

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/list/a;)Ljava/lang/Integer;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/list/a;->d:Ljava/lang/Integer;

    return-object p0
.end method


# virtual methods
.method public a(I)Lcom/swedbank/mobile/app/cards/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/cards/c;

    return-object p1
.end method

.method public a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/cards/list/l;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "parent.context"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/swedbank/mobile/app/cards/n$g;->item_card:I

    .line 135
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const-string v1, "LayoutInflater.from(this)"

    invoke-static {p2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 134
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_1

    check-cast p2, Landroid/view/ViewGroup;

    .line 68
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result p1

    int-to-float p1, p1

    const v1, 0x3f4ccccd    # 0.8f

    mul-float p1, p1, v1

    float-to-int p1, p1

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 71
    :cond_0
    new-instance p1, Lcom/swedbank/mobile/app/cards/list/l;

    .line 73
    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_item_card_layout:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "rootView.findViewById(R.id.card_item_card_layout)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/swedbank/mobile/app/cards/CardLayout;

    .line 71
    invoke-direct {p1, p2, v0}, Lcom/swedbank/mobile/app/cards/list/l;-><init>(Landroid/view/ViewGroup;Lcom/swedbank/mobile/app/cards/CardLayout;)V

    return-object p1

    .line 134
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    .line 128
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, -0x1

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 129
    check-cast v3, Lcom/swedbank/mobile/app/cards/c;

    .line 38
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    .line 133
    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 39
    move-object v0, p1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-eq v0, v4, :cond_2

    const/4 v1, 0x1

    :cond_2
    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/app/cards/list/l;I)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/cards/list/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/app/cards/c;

    .line 78
    invoke-virtual {p1, v6}, Lcom/swedbank/mobile/app/cards/list/l;->a(Lcom/swedbank/mobile/app/cards/c;)V

    .line 80
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/cards/list/l;->a()Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object v2

    .line 81
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/CardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_transition_target:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string p1, "resources.getString(R.st\u2026g_card_transition_target)"

    invoke-static {v3, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/cards/CardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    invoke-static {p0}, Lcom/swedbank/mobile/app/cards/list/a;->b(Lcom/swedbank/mobile/app/cards/list/a;)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    .line 137
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p2, :cond_1

    .line 138
    check-cast v1, Ljava/lang/Integer;

    invoke-static {p0, v1}, Lcom/swedbank/mobile/app/cards/list/a;->a(Lcom/swedbank/mobile/app/cards/list/a;Ljava/lang/Integer;)V

    .line 139
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_transition_target:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 142
    :cond_1
    :goto_0
    invoke-virtual {v2, v1}, Lcom/swedbank/mobile/app/cards/CardLayout;->setTransitionName(Ljava/lang/String;)V

    .line 83
    move-object p1, v2

    check-cast p1, Landroid/view/View;

    .line 143
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/a$a;

    move-object v1, v0

    move-object v4, p0

    move v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/cards/list/a$a;-><init>(Lcom/swedbank/mobile/app/cards/CardLayout;Ljava/lang/String;Lcom/swedbank/mobile/app/cards/list/a;ILcom/swedbank/mobile/app/cards/c;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 61
    iput-boolean v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->c:Z

    .line 62
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    .line 63
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/a;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Lkotlin/k;)V
    .locals 3
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cards"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    .line 43
    invoke-virtual {p1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iput-object v1, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    .line 44
    invoke-virtual {p1}, Lkotlin/k;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/f$b;

    .line 45
    iget-boolean v2, p0, Lcom/swedbank/mobile/app/cards/list/a;->c:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 46
    iput-boolean v2, p0, Lcom/swedbank/mobile/app/cards/list/a;->c:Z

    .line 47
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_4

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/a;->notifyDataSetChanged()V

    goto :goto_1

    .line 50
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    if-nez v1, :cond_2

    goto :goto_0

    .line 53
    :cond_2
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/list/a;->e:Landroidx/recyclerview/widget/f$b;

    if-eq p1, v1, :cond_4

    .line 54
    move-object p1, p0

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v1, p1}, Landroidx/recyclerview/widget/f$b;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_1

    .line 51
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/list/a;->notifyDataSetChanged()V

    .line 57
    :cond_4
    :goto_1
    iput-object v1, p0, Lcom/swedbank/mobile/app/cards/list/a;->e:Landroidx/recyclerview/widget/f$b;

    return-void
.end method

.method public synthetic b(I)Ljava/lang/Object;
    .locals 0

    .line 22
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/a;->a(I)Lcom/swedbank/mobile/app/cards/c;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/cards/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->c:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 91
    :goto_0
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/a;->d:Ljava/lang/Integer;

    return-void
.end method

.method public getItemCount()I
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$x;I)V
    .locals 0

    .line 22
    check-cast p1, Lcom/swedbank/mobile/app/cards/list/l;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/list/a;->a(Lcom/swedbank/mobile/app/cards/list/l;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$x;
    .locals 0

    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/list/a;->a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/cards/list/l;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$x;

    return-object p1
.end method
