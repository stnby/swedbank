.class final Lcom/swedbank/mobile/app/cards/list/e$d$1;
.super Ljava/lang/Object;
.source "CardsListPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/list/e$d;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/list/e$d;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/list/e$d;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/e$d$1;->a:Lcom/swedbank/mobile/app/cards/list/e$d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
    .locals 5
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/cards/list/r$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 36
    sget-object p1, Lcom/swedbank/mobile/app/cards/list/r$a$c;->a:Lcom/swedbank/mobile/app/cards/list/r$a$c;

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/e$d$1;->a:Lcom/swedbank/mobile/app/cards/list/e$d;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/list/e$d;->a:Lcom/swedbank/mobile/app/cards/list/e;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/e;->b(Lcom/swedbank/mobile/app/cards/list/e;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 38
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    .line 39
    new-instance v0, Lcom/swedbank/mobile/app/cards/list/r$a$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/cards/list/r$a$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 40
    sget-object p1, Lcom/swedbank/mobile/app/cards/list/r$a$e;->a:Lcom/swedbank/mobile/app/cards/list/r$a$e;

    const-wide/16 v2, 0xdac

    .line 131
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v4}, Lio/reactivex/o;->a(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/o;

    move-result-object v2

    .line 130
    new-instance v3, Lcom/swedbank/mobile/app/cards/list/e$d$1$a;

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/cards/list/e$d$1$a;-><init>(Ljava/lang/Object;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 129
    invoke-virtual {p1, v0}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable\n    .timer(di\u2026    .startWith(startWith)"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/e$d$1;->a:Lcom/swedbank/mobile/app/cards/list/e$d;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/list/e$d;->a:Lcom/swedbank/mobile/app/cards/list/e;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/e;->b(Lcom/swedbank/mobile/app/cards/list/e;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/e$d$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
