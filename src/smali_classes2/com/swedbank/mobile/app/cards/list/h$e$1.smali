.class final Lcom/swedbank/mobile/app/cards/list/h$e$1;
.super Lkotlin/e/b/k;
.source "CardsListRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/list/h$e;->a(Lio/reactivex/k;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/list/h$e;

.field final synthetic b:Lio/reactivex/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/list/h$e;Lio/reactivex/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/list/h$e$1;->a:Lcom/swedbank/mobile/app/cards/list/h$e;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/list/h$e$1;->b:Lio/reactivex/k;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 6
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h$e$1;->a:Lcom/swedbank/mobile/app/cards/list/h$e;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/list/h$e;->a:Lcom/swedbank/mobile/app/cards/list/h;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/list/h;->a(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/list/h$e$1;->a:Lcom/swedbank/mobile/app/cards/list/h$e;

    iget-object v1, v1, Lcom/swedbank/mobile/app/cards/list/h$e;->a:Lcom/swedbank/mobile/app/cards/list/h;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/list/h;->b(Lcom/swedbank/mobile/app/cards/list/h;)Lcom/swedbank/mobile/business/general/confirmation/c;

    move-result-object v1

    .line 114
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/list/h$e$1;->b:Lio/reactivex/k;

    const-string v3, "emitter"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    check-cast p1, Ljava/lang/Iterable;

    .line 196
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/swedbank/mobile/architect/a/h;

    .line 195
    instance-of v5, v5, Lcom/swedbank/mobile/business/general/confirmation/e;

    if-eqz v5, :cond_0

    goto :goto_0

    :cond_1
    move-object v3, v4

    .line 198
    :goto_0
    check-cast v3, Lcom/swedbank/mobile/architect/a/h;

    if-eqz v3, :cond_2

    .line 199
    invoke-virtual {v3}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lcom/swedbank/mobile/business/general/confirmation/d;

    :cond_2
    const/4 p1, 0x1

    if-eqz v4, :cond_4

    .line 201
    invoke-interface {v4}, Lcom/swedbank/mobile/business/general/confirmation/d;->i()Ljava/lang/String;

    move-result-object v3

    const-string v5, "outdated_card_shortcut_help_listener"

    invoke-static {v3, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, p1

    if-eqz v3, :cond_3

    goto :goto_1

    .line 216
    :cond_3
    invoke-interface {v2, v4}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    goto :goto_2

    .line 214
    :cond_4
    :goto_1
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/f/a;->a(Z)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    .line 213
    sget-object v0, Lcom/swedbank/mobile/app/cards/e/a;->a:Lcom/swedbank/mobile/app/cards/e/a;

    check-cast v0, Lcom/swedbank/mobile/app/f/c;

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    .line 212
    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    const-string v0, "outdated_card_shortcut_help_listener"

    .line 211
    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/f/a;->a(Ljava/lang/String;)Lcom/swedbank/mobile/app/f/a;

    move-result-object p1

    .line 210
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 209
    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-interface {v2, v0}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/list/h$e$1;->a:Lcom/swedbank/mobile/app/cards/list/h$e;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/list/h$e;->a:Lcom/swedbank/mobile/app/cards/list/h;

    .line 215
    invoke-static {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :goto_2
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/list/h$e$1;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
