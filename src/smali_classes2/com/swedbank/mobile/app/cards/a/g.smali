.class public final Lcom/swedbank/mobile/app/cards/a/g;
.super Lcom/swedbank/mobile/architect/a/d;
.source "CardDetailsPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/cards/a/n;",
        "Lcom/swedbank/mobile/app/cards/a/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/x;

.field private final b:Lcom/swedbank/mobile/core/ui/ad;

.field private final c:Landroid/app/Application;

.field private final d:Lcom/swedbank/mobile/business/cards/details/h;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/cards/details/h;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/details/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g;->c:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/g;->d:Lcom/swedbank/mobile/business/cards/details/h;

    .line 25
    invoke-static {}, Lcom/swedbank/mobile/core/ui/ab;->a()Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 26
    new-instance p1, Lcom/swedbank/mobile/core/ui/ad;

    invoke-direct {p1}, Lcom/swedbank/mobile/core/ui/ad;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/a/g;Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/s$a;)Lcom/swedbank/mobile/app/cards/a/s;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/s$a;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object p0

    return-object p0
.end method

.method private final a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/s$a;)Lcom/swedbank/mobile/app/cards/a/s;
    .locals 17

    move-object/from16 v0, p2

    .line 186
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$a;

    if-eqz v1, :cond_0

    .line 187
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$a;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$a;->a()Lcom/swedbank/mobile/app/cards/a/b;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xff6

    const/4 v15, 0x0

    move-object/from16 v1, p1

    .line 186
    invoke-static/range {v1 .. v15}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 189
    :cond_0
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$c;

    if-eqz v1, :cond_2

    .line 190
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$c;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3f9

    const/4 v15, 0x0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v15}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xffd

    const/4 v15, 0x0

    move-object/from16 v1, p1

    .line 195
    invoke-static/range {v1 .. v15}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 198
    :cond_2
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$b;

    if-eqz v1, :cond_3

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 200
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$b;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$b;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xff9

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 198
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 201
    :cond_3
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$q;

    if-eqz v1, :cond_4

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 202
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$q;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$q;->a()Z

    move-result v6

    const/4 v5, 0x0

    const/4 v13, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    const/4 v7, 0x0

    const/16 v15, 0x3e3

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 201
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 207
    :cond_4
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$r;

    if-eqz v1, :cond_5

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 208
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$r;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$r;->a()Ljava/lang/String;

    move-result-object v7

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xfe7

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 207
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 210
    :cond_5
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$p;

    if-eqz v1, :cond_6

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 211
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$p;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$p;->a()Ljava/lang/String;

    move-result-object v13

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v6, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xbf7

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 210
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 213
    :cond_6
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$e;

    if-eqz v1, :cond_7

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 214
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$e;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$e;->a()Z

    move-result v8

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3db

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 213
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 218
    :cond_7
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$d;

    if-eqz v1, :cond_8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 219
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$d;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$d;->a()Ljava/lang/String;

    move-result-object v13

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v8, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xbdf

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 218
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 221
    :cond_8
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$m;

    if-eqz v1, :cond_9

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 222
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$m;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$m;->a()Z

    move-result v9

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3bb

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 221
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 226
    :cond_9
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$l;

    if-eqz v1, :cond_a

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 227
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$l;->a()Ljava/lang/String;

    move-result-object v13

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xbbf

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 226
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 229
    :cond_a
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$o;

    if-eqz v1, :cond_b

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 230
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$o;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$o;->a()Z

    move-result v10

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v5, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x37b

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 229
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 234
    :cond_b
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$n;

    if-eqz v1, :cond_c

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 235
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$n;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$n;->a()Ljava/lang/String;

    move-result-object v13

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xb7f

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 234
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 237
    :cond_c
    sget-object v1, Lcom/swedbank/mobile/app/cards/a/s$a$i;->a:Lcom/swedbank/mobile/app/cards/a/s$a$i;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xfef

    const/16 v16, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto/16 :goto_1

    .line 239
    :cond_d
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$f;

    if-eqz v1, :cond_e

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 240
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$f;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$f;->a()Z

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xeff

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 239
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto :goto_1

    .line 241
    :cond_e
    sget-object v1, Lcom/swedbank/mobile/app/cards/a/s$a$h;->a:Lcom/swedbank/mobile/app/cards/a/s$a$h;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    goto :goto_0

    .line 242
    :cond_f
    sget-object v1, Lcom/swedbank/mobile/app/cards/a/s$a$j;->a:Lcom/swedbank/mobile/app/cards/a/s$a$j;

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3fb

    const/16 v16, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto :goto_1

    .line 246
    :cond_10
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$g;

    if-eqz v1, :cond_11

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 247
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$g;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$g;->a()Z

    move-result v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xdff

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 246
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    goto :goto_1

    .line 248
    :cond_11
    instance-of v1, v0, Lcom/swedbank/mobile/app/cards/a/s$a$k;

    if-eqz v1, :cond_12

    const/4 v3, 0x0

    .line 249
    check-cast v0, Lcom/swedbank/mobile/app/cards/a/s$a$k;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/a/s$a$k;->a()Lcom/swedbank/mobile/app/w/b;

    move-result-object v14

    const/4 v5, 0x0

    const/4 v13, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v15, 0x201

    const/16 v16, 0x0

    move-object/from16 v2, p1

    .line 248
    invoke-static/range {v2 .. v16}, Lcom/swedbank/mobile/app/cards/a/s;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILjava/lang/Object;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_12
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/business/cards/details/h;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/g;->d:Lcom/swedbank/mobile/business/cards/details/h;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/x;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/g;->a:Lcom/swedbank/mobile/core/ui/x;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/ad;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/g;->b:Lcom/swedbank/mobile/core/ui/ad;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/a/g;)Landroid/app/Application;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/g;->c:Landroid/app/Application;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 18

    move-object/from16 v0, p0

    .line 29
    iget-object v1, v0, Lcom/swedbank/mobile/app/cards/a/g;->a:Lcom/swedbank/mobile/core/ui/x;

    .line 30
    invoke-interface {v1}, Lcom/swedbank/mobile/core/ui/x;->a()Lio/reactivex/o;

    move-result-object v1

    .line 31
    sget-object v2, Lcom/swedbank/mobile/app/cards/a/g$e;->a:Lcom/swedbank/mobile/app/cards/a/g$e;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/app/cards/a/i;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/cards/a/i;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 35
    sget-object v2, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v2}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    check-cast v2, Lio/reactivex/s;

    .line 36
    sget-object v3, Lcom/swedbank/mobile/app/cards/a/g$f;->a:Lcom/swedbank/mobile/app/cards/a/g$f;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    check-cast v3, Lio/reactivex/s;

    .line 34
    invoke-static {v2, v3}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v2

    .line 37
    new-instance v3, Lcom/swedbank/mobile/app/cards/a/g$g;

    invoke-direct {v3, v0}, Lcom/swedbank/mobile/app/cards/a/g$g;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 56
    sget-object v3, Lcom/swedbank/mobile/app/cards/a/g$c;->a:Lcom/swedbank/mobile/app/cards/a/g$c;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 57
    new-instance v4, Lcom/swedbank/mobile/app/cards/a/g$d;

    invoke-direct {v4, v0}, Lcom/swedbank/mobile/app/cards/a/g$d;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v4, Lio/reactivex/c/g;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 58
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 59
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 61
    sget-object v4, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 63
    iget-object v4, v0, Lcom/swedbank/mobile/app/cards/a/g;->d:Lcom/swedbank/mobile/business/cards/details/h;

    invoke-interface {v4}, Lcom/swedbank/mobile/business/cards/details/h;->f()Lio/reactivex/o;

    move-result-object v4

    .line 64
    iget-object v5, v0, Lcom/swedbank/mobile/app/cards/a/g;->d:Lcom/swedbank/mobile/business/cards/details/h;

    invoke-interface {v5}, Lcom/swedbank/mobile/business/cards/details/h;->g()Lio/reactivex/o;

    move-result-object v5

    .line 262
    check-cast v4, Lio/reactivex/s;

    check-cast v5, Lio/reactivex/s;

    .line 263
    new-instance v6, Lcom/swedbank/mobile/app/cards/a/g$a;

    invoke-direct {v6}, Lcom/swedbank/mobile/app/cards/a/g$a;-><init>()V

    check-cast v6, Lio/reactivex/c/c;

    .line 262
    invoke-static {v4, v5, v6}, Lio/reactivex/o;->a(Lio/reactivex/s;Lio/reactivex/s;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v4

    const-string v5, "Observables\n        .com\u2026e\n            }\n        )"

    .line 263
    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    new-instance v5, Lcom/swedbank/mobile/app/cards/a/g$w;

    invoke-direct {v5, v0}, Lcom/swedbank/mobile/app/cards/a/g$w;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v5, Lio/reactivex/c/h;

    invoke-virtual {v4, v5}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v4

    const-string v5, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object v5, Lcom/swedbank/mobile/app/cards/a/g$u;->a:Lcom/swedbank/mobile/app/cards/a/g$u;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {v0, v5}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 72
    new-instance v6, Lcom/swedbank/mobile/app/cards/a/g$v;

    invoke-direct {v6, v0}, Lcom/swedbank/mobile/app/cards/a/g$v;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v5

    .line 91
    sget-object v6, Lcom/swedbank/mobile/app/cards/a/g$h;->a:Lcom/swedbank/mobile/app/cards/a/g$h;

    check-cast v6, Lkotlin/e/a/b;

    invoke-virtual {v0, v6}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v6

    .line 92
    new-instance v7, Lcom/swedbank/mobile/app/cards/a/g$i;

    invoke-direct {v7, v0}, Lcom/swedbank/mobile/app/cards/a/g$i;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v7, Lio/reactivex/c/h;

    invoke-virtual {v6, v7}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v6

    .line 104
    sget-object v7, Lcom/swedbank/mobile/app/cards/a/g$m;->a:Lcom/swedbank/mobile/app/cards/a/g$m;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {v0, v7}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    .line 105
    new-instance v8, Lcom/swedbank/mobile/app/cards/a/g$n;

    invoke-direct {v8, v0}, Lcom/swedbank/mobile/app/cards/a/g$n;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v8, Lio/reactivex/c/h;

    invoke-virtual {v7, v8}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v7

    .line 117
    iget-object v8, v0, Lcom/swedbank/mobile/app/cards/a/g;->d:Lcom/swedbank/mobile/business/cards/details/h;

    .line 118
    invoke-interface {v8}, Lcom/swedbank/mobile/business/cards/details/h;->k()Lio/reactivex/o;

    move-result-object v8

    .line 119
    sget-object v9, Lcom/swedbank/mobile/app/cards/a/g$l;->a:Lcom/swedbank/mobile/app/cards/a/g$l;

    check-cast v9, Lkotlin/e/a/b;

    if-eqz v9, :cond_1

    new-instance v10, Lcom/swedbank/mobile/app/cards/a/i;

    invoke-direct {v10, v9}, Lcom/swedbank/mobile/app/cards/a/i;-><init>(Lkotlin/e/a/b;)V

    move-object v9, v10

    :cond_1
    check-cast v9, Lio/reactivex/c/h;

    invoke-virtual {v8, v9}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v8

    .line 121
    sget-object v9, Lcom/swedbank/mobile/app/cards/a/g$q;->a:Lcom/swedbank/mobile/app/cards/a/g$q;

    check-cast v9, Lkotlin/e/a/b;

    invoke-virtual {v0, v9}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v9

    .line 122
    new-instance v10, Lcom/swedbank/mobile/app/cards/a/g$r;

    invoke-direct {v10, v0}, Lcom/swedbank/mobile/app/cards/a/g$r;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v10, Lio/reactivex/c/h;

    invoke-virtual {v9, v10}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v9

    .line 137
    sget-object v10, Lcom/swedbank/mobile/app/cards/a/g$j;->a:Lcom/swedbank/mobile/app/cards/a/g$j;

    check-cast v10, Lkotlin/e/a/b;

    invoke-virtual {v0, v10}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v10

    .line 138
    new-instance v11, Lcom/swedbank/mobile/app/cards/a/g$k;

    invoke-direct {v11, v0}, Lcom/swedbank/mobile/app/cards/a/g$k;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v11, Lio/reactivex/c/h;

    invoke-virtual {v10, v11}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v10

    .line 146
    sget-object v11, Lcom/swedbank/mobile/app/cards/a/g$o;->a:Lcom/swedbank/mobile/app/cards/a/g$o;

    check-cast v11, Lkotlin/e/a/b;

    invoke-virtual {v0, v11}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v11

    .line 147
    new-instance v12, Lcom/swedbank/mobile/app/cards/a/g$p;

    invoke-direct {v12, v0}, Lcom/swedbank/mobile/app/cards/a/g$p;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v12, Lio/reactivex/c/g;

    invoke-virtual {v11, v12}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v11

    .line 148
    invoke-virtual {v11}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v11

    .line 149
    invoke-virtual {v11}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v11

    .line 151
    sget-object v12, Lcom/swedbank/mobile/app/cards/a/g$s;->a:Lcom/swedbank/mobile/app/cards/a/g$s;

    check-cast v12, Lkotlin/e/a/b;

    invoke-virtual {v0, v12}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v12

    .line 152
    new-instance v13, Lcom/swedbank/mobile/app/cards/a/g$t;

    invoke-direct {v13, v0}, Lcom/swedbank/mobile/app/cards/a/g$t;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v13, Lio/reactivex/c/g;

    invoke-virtual {v12, v13}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v12

    .line 153
    invoke-virtual {v12}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v12

    .line 154
    invoke-virtual {v12}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v12

    const/16 v13, 0xc

    .line 156
    new-array v13, v13, [Lio/reactivex/s;

    const/4 v14, 0x0

    .line 157
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v13, v14

    const/4 v1, 0x1

    .line 158
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v13, v1

    const/4 v1, 0x2

    .line 159
    check-cast v4, Lio/reactivex/s;

    aput-object v4, v13, v1

    const/4 v1, 0x3

    .line 160
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v13, v1

    const/4 v1, 0x4

    .line 161
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v13, v1

    const/4 v1, 0x5

    .line 162
    check-cast v6, Lio/reactivex/s;

    aput-object v6, v13, v1

    const/4 v1, 0x6

    .line 163
    check-cast v7, Lio/reactivex/s;

    aput-object v7, v13, v1

    const/4 v1, 0x7

    .line 164
    check-cast v8, Lio/reactivex/s;

    aput-object v8, v13, v1

    const/16 v1, 0x8

    .line 165
    check-cast v9, Lio/reactivex/s;

    aput-object v9, v13, v1

    const/16 v1, 0x9

    .line 166
    check-cast v10, Lio/reactivex/s;

    aput-object v10, v13, v1

    const/16 v1, 0xa

    .line 167
    check-cast v11, Lio/reactivex/s;

    aput-object v11, v13, v1

    const/16 v1, 0xb

    .line 168
    check-cast v12, Lio/reactivex/s;

    aput-object v12, v13, v1

    .line 156
    invoke-static {v13}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "Observable.mergeArray(\n \u2026\n        orderCardStream)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    new-instance v2, Lcom/swedbank/mobile/app/cards/a/s;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xfff

    const/16 v17, 0x0

    move-object v3, v2

    invoke-direct/range {v3 .. v17}, Lcom/swedbank/mobile/app/cards/a/s;-><init>(Lcom/swedbank/mobile/app/cards/a/b;ZLcom/swedbank/mobile/business/util/e;ZLjava/lang/String;ZZZZZLjava/lang/String;Lcom/swedbank/mobile/app/w/b;ILkotlin/e/b/g;)V

    new-instance v3, Lcom/swedbank/mobile/app/cards/a/g$b;

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/app/cards/a/g;

    invoke-direct {v3, v4}, Lcom/swedbank/mobile/app/cards/a/g$b;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v3, Lkotlin/e/a/m;

    .line 268
    new-instance v4, Lcom/swedbank/mobile/app/cards/a/h;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/cards/a/h;-><init>(Lkotlin/e/a/m;)V

    check-cast v4, Lio/reactivex/c/c;

    invoke-virtual {v1, v2, v4}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 269
    invoke-virtual {v1, v2, v3}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v1

    const-string v2, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    invoke-static {v0, v1}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
