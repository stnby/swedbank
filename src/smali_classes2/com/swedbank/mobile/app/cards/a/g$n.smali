.class final Lcom/swedbank/mobile/app/cards/a/g$n;
.super Ljava/lang/Object;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$n;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/o;
    .locals 3
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/a/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$n;->a:Lcom/swedbank/mobile/app/cards/a/g;

    .line 106
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$n;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/business/cards/details/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/details/h;->j()Lio/reactivex/j;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lio/reactivex/j;->b()Lio/reactivex/o;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/swedbank/mobile/app/cards/a/g$n$1;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/cards/a/g$n$1;-><init>(Lcom/swedbank/mobile/app/cards/a/g$n;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 112
    new-instance v1, Lcom/swedbank/mobile/app/cards/a/s$a$m;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/cards/a/s$a$m;-><init>(Z)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->e(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 113
    new-instance v1, Lcom/swedbank/mobile/app/cards/a/s$a$m;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/cards/a/s$a$m;-><init>(Z)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "interactor.changeInterne\u2026eLoading(loading = true))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 262
    new-instance v1, Lcom/swedbank/mobile/app/cards/a/g$w;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/cards/a/g$w;-><init>(Lcom/swedbank/mobile/app/cards/a/g;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "onErrorResumeNext { e: T\u2026.toFatalError()))\n      }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/g$n;->a(Lkotlin/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
