.class public final Lcom/swedbank/mobile/app/cards/a/c;
.super Ljava/lang/Object;
.source "CardDetailsBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/swedbank/mobile/business/cards/details/k;

.field private final c:Lcom/swedbank/mobile/a/e/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/e/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/e/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/c;->c:Lcom/swedbank/mobile/a/e/a/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/details/k;)Lcom/swedbank/mobile/app/cards/a/c;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/details/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/c;->b:Lcom/swedbank/mobile/business/cards/details/k;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/cards/a/c;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/c;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/c;->c:Lcom/swedbank/mobile/a/e/a/a$a;

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/a/c;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/a/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/e/a/a$a;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/a/c;->b:Lcom/swedbank/mobile/business/cards/details/k;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/e/a/a$a;->b(Lcom/swedbank/mobile/business/cards/details/k;)Lcom/swedbank/mobile/a/e/a/a$a;

    move-result-object v0

    .line 33
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/a/a$a;->a()Lcom/swedbank/mobile/a/e/a/a;

    move-result-object v0

    .line 34
    invoke-interface {v0}, Lcom/swedbank/mobile/a/e/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display card details without listener"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 27
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display card details without card id"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
