.class final Lcom/swedbank/mobile/app/cards/a/g$g$1;
.super Ljava/lang/Object;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/g$g;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/g$g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/g$g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$g$1;->a:Lcom/swedbank/mobile/app/cards/a/g$g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/business/util/p;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/util/p;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/a/s$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    sget-object v0, Lcom/swedbank/mobile/business/util/p$b;->a:Lcom/swedbank/mobile/business/util/p$b;

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$g$1;->a:Lcom/swedbank/mobile/app/cards/a/g$g;

    iget-object p1, p1, Lcom/swedbank/mobile/app/cards/a/g$g;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/a/g;->b(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object p1

    invoke-interface {p1, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/util/p$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$g$1;->a:Lcom/swedbank/mobile/app/cards/a/g$g;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/a/g$g;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->c(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v2

    .line 43
    new-instance v3, Lcom/swedbank/mobile/app/cards/a/s$a$b;

    check-cast p1, Lcom/swedbank/mobile/business/util/p$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/p$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/swedbank/mobile/app/cards/a/s$a$b;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    .line 44
    sget-object v4, Lcom/swedbank/mobile/app/cards/a/s$a$h;->a:Lcom/swedbank/mobile/app/cards/a/s$a$h;

    const-wide/16 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 42
    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$g$1;->a:Lcom/swedbank/mobile/app/cards/a/g$g;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/a/g$g;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->b(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/core/ui/x;->c(Z)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/business/util/p;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/g$g$1;->a(Lcom/swedbank/mobile/business/util/p;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
