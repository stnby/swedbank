.class final Lcom/swedbank/mobile/app/cards/a/g$g;
.super Ljava/lang/Object;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$g;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lkotlin/s;)Lio/reactivex/o;
    .locals 4
    .param p1    # Lkotlin/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/s;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/a/s$a;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$g;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/business/cards/details/h;

    move-result-object p1

    invoke-interface {p1}, Lcom/swedbank/mobile/business/cards/details/h;->e()Lio/reactivex/o;

    move-result-object p1

    .line 39
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/g$g$1;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/a/g$g$1;-><init>(Lcom/swedbank/mobile/app/cards/a/g$g;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$g;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->b(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/x;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/swedbank/mobile/core/ui/x$a;->a(Lcom/swedbank/mobile/core/ui/x;ZILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->f(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object p1

    .line 49
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/g$g$2;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/a/g$g$2;-><init>(Lcom/swedbank/mobile/app/cards/a/g$g;)V

    check-cast v0, Lio/reactivex/c/h;

    invoke-virtual {p1, v0}, Lio/reactivex/o;->i(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lkotlin/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/g$g;->a(Lkotlin/s;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
