.class public final Lcom/swedbank/mobile/app/cards/a/e;
.super Ljava/lang/Object;
.source "CardDetails.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/cards/a;Ljava/util/List;)Lcom/swedbank/mobile/app/cards/a/b;
    .locals 27
    .param p0    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/a;",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/cards/f;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/a/b;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "$this$toDetailsViewModel"

    move-object/from16 v2, p0

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "limits"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v3

    .line 61
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->d()Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->h()Ljava/lang/String;

    move-result-object v5

    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v1

    sget-object v6, Lcom/swedbank/mobile/app/cards/e;->a:[I

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v1

    aget v1, v6, v1

    const/4 v7, 0x1

    packed-switch v1, :pswitch_data_0

    .line 115
    sget-object v1, Lcom/swedbank/mobile/app/cards/f;->a:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 114
    :pswitch_0
    sget-object v1, Lcom/swedbank/mobile/app/cards/f;->e:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 113
    :pswitch_1
    sget-object v1, Lcom/swedbank/mobile/app/cards/f;->d:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 108
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v1

    .line 109
    instance-of v8, v1, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v8, :cond_0

    .line 110
    check-cast v1, Lcom/swedbank/mobile/business/cards/s$a;

    .line 107
    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/s$a;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 109
    :goto_0
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v1, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    sget-object v1, Lcom/swedbank/mobile/app/cards/f;->c:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 112
    :cond_1
    sget-object v1, Lcom/swedbank/mobile/app/cards/f;->b:Lcom/swedbank/mobile/app/cards/f;

    .line 64
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v8

    .line 65
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->i()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v9

    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->g()Lcom/swedbank/mobile/business/cards/c;

    move-result-object v10

    .line 67
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->j()Z

    move-result v11

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->k()Z

    move-result v12

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->l()Z

    move-result v13

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->n()Z

    move-result v14

    .line 71
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/e;->a(Lcom/swedbank/mobile/business/cards/a;)Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v15

    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->m()Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->DIGITIZATION_IN_PROGRESS:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    const/16 v16, 0x0

    if-eq v6, v7, :cond_4

    .line 126
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v6

    .line 127
    instance-of v7, v6, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v7, :cond_2

    .line 128
    check-cast v6, Lcom/swedbank/mobile/business/cards/s$a;

    .line 129
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/app/cards/a/f;->a:[I

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/t;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_1

    const/4 v6, 0x0

    goto :goto_2

    :pswitch_3
    const/4 v6, 0x1

    .line 134
    :goto_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_3

    :cond_2
    const/4 v6, 0x0

    :goto_3
    const/4 v7, 0x1

    .line 127
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v6, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_3
    const/16 v17, 0x0

    goto :goto_5

    :cond_4
    :goto_4
    const/16 v17, 0x1

    .line 136
    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v2

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->m()Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/business/cards/DigitizationProgression;->DIGITIZATION_IN_PROGRESS:Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    if-eq v6, v7, :cond_6

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v6

    .line 151
    instance-of v7, v6, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v7, :cond_5

    .line 152
    check-cast v6, Lcom/swedbank/mobile/business/cards/s$a;

    .line 153
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/app/cards/a/f;->a:[I

    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/t;->ordinal()I

    move-result v6

    aget v6, v7, v6

    packed-switch v6, :pswitch_data_2

    const/4 v6, 0x0

    goto :goto_6

    :pswitch_4
    const/4 v6, 0x1

    .line 158
    :goto_6
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v20, v15

    goto :goto_7

    :cond_5
    move-object/from16 v20, v15

    const/4 v6, 0x0

    :goto_7
    const/4 v7, 0x1

    .line 151
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    invoke-static {v6, v15}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    goto :goto_8

    :cond_6
    move-object/from16 v20, v15

    const/4 v7, 0x1

    :goto_8
    const/16 v16, 0x1

    :cond_7
    if-nez v16, :cond_8

    .line 139
    instance-of v6, v2, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v6, :cond_8

    check-cast v2, Lcom/swedbank/mobile/business/cards/s$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v6

    sget-object v7, Lcom/swedbank/mobile/business/cards/t;->g:Lcom/swedbank/mobile/business/cards/t;

    if-eq v6, v7, :cond_8

    .line 160
    new-instance v6, Lcom/swedbank/mobile/app/cards/a/w$a;

    .line 161
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/s$a;->c()Z

    move-result v7

    .line 162
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/s$a;->e()Ljava/lang/String;

    move-result-object v15

    .line 163
    invoke-virtual {v2}, Lcom/swedbank/mobile/business/cards/s$a;->d()Lcom/swedbank/mobile/business/cards/x;

    move-result-object v2

    .line 160
    invoke-direct {v6, v7, v15, v2}, Lcom/swedbank/mobile/app/cards/a/w$a;-><init>(ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/x;)V

    check-cast v6, Lcom/swedbank/mobile/app/cards/a/w;

    move-object/from16 v16, v6

    goto :goto_9

    .line 164
    :cond_8
    sget-object v2, Lcom/swedbank/mobile/app/cards/a/w$b;->a:Lcom/swedbank/mobile/app/cards/a/w$b;

    check-cast v2, Lcom/swedbank/mobile/app/cards/a/w;

    move-object/from16 v16, v2

    .line 74
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/business/cards/a;->q()Lcom/swedbank/mobile/business/cards/r;

    move-result-object v19

    .line 167
    check-cast v0, Ljava/lang/Iterable;

    .line 168
    new-instance v2, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v0, v6}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 169
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 170
    check-cast v6, Lcom/swedbank/mobile/business/cards/f;

    .line 171
    new-instance v7, Lcom/swedbank/mobile/app/cards/a/t;

    .line 172
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/f;->a()Z

    move-result v22

    .line 173
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/f;->b()Ljava/math/BigDecimal;

    move-result-object v23

    .line 174
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/f;->c()Ljava/lang/String;

    move-result-object v24

    .line 175
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/f;->d()Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v25

    .line 176
    invoke-virtual {v6}, Lcom/swedbank/mobile/business/cards/f;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v26

    move-object/from16 v21, v7

    .line 171
    invoke-direct/range {v21 .. v26}, Lcom/swedbank/mobile/app/cards/a/t;-><init>(ZLjava/math/BigDecimal;Ljava/lang/String;Lcom/swedbank/mobile/business/cards/CardLimitService;Lcom/swedbank/mobile/business/cards/CardLimitValidity;)V

    .line 177
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 178
    :cond_9
    move-object/from16 v18, v2

    check-cast v18, Ljava/util/List;

    .line 59
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/b;

    move-object v2, v0

    move-object v6, v1

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move v10, v11

    move v11, v12

    move v12, v13

    move v13, v14

    move-object/from16 v14, v20

    move/from16 v15, v17

    move-object/from16 v17, v19

    invoke-direct/range {v2 .. v18}, Lcom/swedbank/mobile/app/cards/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/g;Lcom/swedbank/mobile/business/cards/c;ZZZZLcom/swedbank/mobile/business/cards/CardClass;ZLcom/swedbank/mobile/app/cards/a/w;Lcom/swedbank/mobile/business/cards/r;Ljava/util/List;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
