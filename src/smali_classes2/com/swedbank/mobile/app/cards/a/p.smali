.class public final synthetic Lcom/swedbank/mobile/app/cards/a/p;
.super Ljava/lang/Object;


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardState;->values()[Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/app/cards/a/p;->a:[I

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardState;->BLOCKED:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardState;->NOT_ISSUED:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->a:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardState;->UNKNOWN:Lcom/swedbank/mobile/business/cards/CardState;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    invoke-static {}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->values()[Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->DAY:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->WEEK:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    sget-object v1, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->MONTH:Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    invoke-virtual {v1}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
