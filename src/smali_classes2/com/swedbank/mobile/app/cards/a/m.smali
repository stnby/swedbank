.class public final Lcom/swedbank/mobile/app/cards/a/m;
.super Lcom/swedbank/mobile/architect/a/b/a/a;
.source "CardDetailsTransition.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/a/m;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/m;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/a/m;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/a/m;->a:Lcom/swedbank/mobile/app/cards/a/m;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Z)Landroidx/l/n;
    .locals 9
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 23
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_transition_target:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(R.st\u2026g_card_transition_target)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget v1, Lcom/swedbank/mobile/app/cards/n$h;->tag_card_list_transition_target:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "resources.getString(R.st\u2026d_list_transition_target)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->tag_cards_background_target:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "resources.getString(R.st\u2026_cards_background_target)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget v3, Lcom/swedbank/mobile/app/cards/n$f;->anim_default_dur:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v3, v3

    .line 28
    sget v5, Lcom/swedbank/mobile/app/cards/n$f;->anim_short_dur:I

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    int-to-long v5, p1

    const/4 p1, 0x0

    const/4 v7, 0x0

    if-eqz p4, :cond_0

    .line 31
    new-instance p2, Landroidx/l/r;

    invoke-direct {p2}, Landroidx/l/r;-><init>()V

    .line 32
    new-instance p3, Lcom/swedbank/mobile/core/ui/a/b;

    const/4 p4, 0x3

    invoke-direct {p3, v7, v7, p4, p1}, Lcom/swedbank/mobile/core/ui/a/b;-><init>(ZZILkotlin/e/b/g;)V

    .line 33
    invoke-virtual {p3, v3, v4}, Lcom/swedbank/mobile/core/ui/a/b;->setDuration(J)Landroidx/l/n;

    move-result-object p1

    .line 34
    invoke-virtual {p1, v0}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p1

    .line 32
    invoke-virtual {p2, p1}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 35
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/j;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/j;-><init>()V

    .line 36
    invoke-virtual {p2, v3, v4}, Lcom/swedbank/mobile/core/ui/a/j;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 37
    invoke-virtual {p2, v2}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 35
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 38
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/f;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/f;-><init>()V

    .line 39
    invoke-virtual {p2, v3, v4}, Lcom/swedbank/mobile/core/ui/a/f;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 40
    invoke-virtual {p2, v1}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 41
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->navigation_bar:I

    invoke-virtual {p2, p3}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    move-result-object p2

    .line 38
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 42
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/c;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/c;-><init>()V

    .line 43
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_details_root_view:I

    invoke-virtual {p2, p3}, Lcom/swedbank/mobile/core/ui/a/c;->addTarget(I)Landroidx/l/n;

    move-result-object p2

    .line 44
    invoke-virtual {p2, v5, v6}, Landroidx/l/n;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 42
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    const-string p2, "TransitionSet()\n        \u2026tDuration(shortDuration))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/l/n;

    return-object p1

    :cond_0
    if-eqz p3, :cond_1

    if-eqz p2, :cond_1

    .line 65
    sget p4, Lcom/swedbank/mobile/app/cards/n$e;->cards_list:I

    invoke-virtual {p2, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;

    if-eqz p4, :cond_1

    .line 67
    sget v8, Lcom/swedbank/mobile/app/cards/n$e;->card_view:I

    invoke-virtual {p3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/swedbank/mobile/app/cards/CardLayout;

    if-eqz p3, :cond_1

    .line 68
    invoke-virtual {p3}, Lcom/swedbank/mobile/app/cards/CardLayout;->b()Ljava/lang/String;

    move-result-object p3

    if-eqz p3, :cond_1

    .line 69
    invoke-virtual {p4, p3}, Lcom/swedbank/mobile/app/cards/list/CardsListRecyclerView;->a(Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_2

    .line 72
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->cards_list_full_loading:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_2

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 48
    :cond_2
    new-instance p2, Landroidx/l/r;

    invoke-direct {p2}, Landroidx/l/r;-><init>()V

    .line 49
    invoke-virtual {p2, v3, v4}, Landroidx/l/r;->a(J)Landroidx/l/r;

    move-result-object p2

    .line 50
    new-instance p3, Lcom/swedbank/mobile/core/ui/a/b;

    const/4 p4, 0x1

    invoke-direct {p3, v7, p4, p4, p1}, Lcom/swedbank/mobile/core/ui/a/b;-><init>(ZZILkotlin/e/b/g;)V

    .line 51
    invoke-virtual {p3, v0}, Lcom/swedbank/mobile/core/ui/a/b;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p1

    .line 50
    invoke-virtual {p2, p1}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 52
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/j;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/j;-><init>()V

    .line 53
    invoke-virtual {p2, v2}, Lcom/swedbank/mobile/core/ui/a/j;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 52
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 54
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/f;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/f;-><init>()V

    .line 55
    invoke-virtual {p2, v3, v4}, Lcom/swedbank/mobile/core/ui/a/f;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 56
    invoke-virtual {p2, v1}, Landroidx/l/n;->addTarget(Ljava/lang/String;)Landroidx/l/n;

    move-result-object p2

    .line 57
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->navigation_bar:I

    invoke-virtual {p2, p3}, Landroidx/l/n;->addTarget(I)Landroidx/l/n;

    move-result-object p2

    .line 54
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    .line 58
    new-instance p2, Lcom/swedbank/mobile/core/ui/a/c;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/a/c;-><init>()V

    .line 59
    sget p3, Lcom/swedbank/mobile/app/cards/n$e;->card_details_root_view:I

    invoke-virtual {p2, p3}, Lcom/swedbank/mobile/core/ui/a/c;->addTarget(I)Landroidx/l/n;

    move-result-object p2

    .line 60
    invoke-virtual {p2, v5, v6}, Landroidx/l/n;->setDuration(J)Landroidx/l/n;

    move-result-object p2

    .line 58
    invoke-virtual {p1, p2}, Landroidx/l/r;->a(Landroidx/l/n;)Landroidx/l/r;

    move-result-object p1

    const-string p2, "TransitionSet()\n        \u2026tDuration(shortDuration))"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/l/n;

    return-object p1
.end method
