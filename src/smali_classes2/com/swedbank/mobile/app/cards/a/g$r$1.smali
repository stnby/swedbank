.class final Lcom/swedbank/mobile/app/cards/a/g$r$1;
.super Ljava/lang/Object;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/g$r;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/g$r;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/g$r;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$r$1;->a:Lcom/swedbank/mobile/app/cards/a/g$r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lio/reactivex/o;
    .locals 10
    .param p1    # Ljava/lang/Boolean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/o<",
            "+",
            "Lcom/swedbank/mobile/app/cards/a/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "success"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 127
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/swedbank/mobile/app/cards/a/s$a$o;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/cards/a/s$a$o;-><init>(Z)V

    invoke-static {p1}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.just(PartialS\u2026Loading(loading = false))"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$r$1;->a:Lcom/swedbank/mobile/app/cards/a/g$r;

    iget-object p1, p1, Lcom/swedbank/mobile/app/cards/a/g$r;->a:Lcom/swedbank/mobile/app/cards/a/g;

    new-instance v0, Lcom/swedbank/mobile/app/cards/a/s$a$n;

    .line 129
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/a/g$r$1;->a:Lcom/swedbank/mobile/app/cards/a/g$r;

    iget-object v1, v1, Lcom/swedbank/mobile/app/cards/a/g$r;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/a/g;->d(Lcom/swedbank/mobile/app/cards/a/g;)Landroid/app/Application;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/cards/n$h;->card_details_wallet_error_deleting_card:I

    invoke-virtual {v1, v2}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026llet_error_deleting_card)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-direct {v0, v1}, Lcom/swedbank/mobile/app/cards/a/s$a$n;-><init>(Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/app/cards/a/s$a;

    .line 262
    invoke-static {p1}, Lcom/swedbank/mobile/app/cards/a/g;->c(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v3

    .line 264
    sget-object v5, Lcom/swedbank/mobile/app/cards/a/s$a$j;->a:Lcom/swedbank/mobile/app/cards/a/s$a$j;

    const-wide/16 v6, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    .line 262
    invoke-static/range {v3 .. v9}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/g$r$1;->a(Ljava/lang/Boolean;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
