.class final Lcom/swedbank/mobile/app/cards/a/g$v$1;
.super Ljava/lang/Object;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/g$v;->a(Lkotlin/s;)Lio/reactivex/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;",
        "Lio/reactivex/s<",
        "+TR;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/g$v;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/g$v;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$v$1;->a:Lcom/swedbank/mobile/app/cards/a/g$v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/details/c;)Lio/reactivex/o;
    .locals 9
    .param p1    # Lcom/swedbank/mobile/business/cards/details/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/details/c;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/a/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/c$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$v$1;->a:Lcom/swedbank/mobile/app/cards/a/g$v;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/a/g$v;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->c(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v1

    .line 78
    new-instance v2, Lcom/swedbank/mobile/app/cards/a/s$a$r;

    .line 79
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/c$c;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/details/c$c;->a()Ljava/lang/String;

    move-result-object p1

    .line 78
    invoke-direct {v2, p1}, Lcom/swedbank/mobile/app/cards/a/s$a$r;-><init>(Ljava/lang/String;)V

    .line 80
    sget-object v3, Lcom/swedbank/mobile/app/cards/a/s$a$i;->a:Lcom/swedbank/mobile/app/cards/a/s$a$i;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    .line 77
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/c$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$v$1;->a:Lcom/swedbank/mobile/app/cards/a/g$v;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/a/g$v;->a:Lcom/swedbank/mobile/app/cards/a/g;

    new-instance v1, Lcom/swedbank/mobile/app/cards/a/s$a$p;

    .line 82
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/c$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/details/c$a;->a()Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/cards/a/s$a$p;-><init>(Ljava/lang/String;)V

    move-object v3, v1

    check-cast v3, Lcom/swedbank/mobile/app/cards/a/s$a;

    .line 262
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->c(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v2

    .line 264
    sget-object v4, Lcom/swedbank/mobile/app/cards/a/s$a$j;->a:Lcom/swedbank/mobile/app/cards/a/s$a$j;

    const-wide/16 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 262
    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    goto :goto_0

    .line 83
    :cond_1
    instance-of v0, p1, Lcom/swedbank/mobile/business/cards/details/c$b;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/s;

    .line 265
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/g$v$1$a;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/cards/a/g$v$1$a;-><init>(Lcom/swedbank/mobile/business/util/s;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    .line 83
    invoke-static {v0}, Lio/reactivex/o;->a(Ljava/util/concurrent/Callable;)Lio/reactivex/o;

    move-result-object p1

    const-string v0, "Observable.error(result.asErrorSupplier())"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/business/cards/details/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/g$v$1;->a(Lcom/swedbank/mobile/business/cards/details/c;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
