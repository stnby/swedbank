.class public final Lcom/swedbank/mobile/app/cards/a/k;
.super Lcom/swedbank/mobile/architect/a/h;
.source "CardDetailsRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/j/d;
.implements Lcom/swedbank/mobile/business/cards/details/m;


# instance fields
.field private final e:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final f:Lcom/swedbank/mobile/data/device/a;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final g:Lcom/swedbank/mobile/app/f/a;

.field private final h:Lcom/swedbank/mobile/app/cards/f/a/b;

.field private final i:Lcom/swedbank/mobile/app/cards/d/b;

.field private final j:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private final k:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

.field private final l:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private final m:Lcom/swedbank/mobile/business/cards/order/b;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/app/cards/f/a/b;Lcom/swedbank/mobile/app/cards/d/b;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/business/cards/order/b;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/app/cards/f/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/app/cards/d/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_card_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
        .annotation runtime Ljavax/inject/Named;
            value = "for_card_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "card_details_contactless_info_dialog_listener"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/swedbank/mobile/business/cards/order/b;
        .annotation runtime Ljavax/inject/Named;
            value = "for_card_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p10    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_card_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p11    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_card_details"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p12    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/app/cards/f/a/b;",
            "Lcom/swedbank/mobile/app/cards/d/b;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/business/cards/order/b;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogBuilder"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingBuilder"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderCardBuilder"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogListener"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "walletOnboardingListener"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactlessInfoDialogListener"

    invoke-static {p8, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderCardListener"

    invoke-static {p9, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p10, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p11, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p12, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p10, p12, p11}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/k;->e:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/k;->f:Lcom/swedbank/mobile/data/device/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/a/k;->g:Lcom/swedbank/mobile/app/f/a;

    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/a/k;->h:Lcom/swedbank/mobile/app/cards/f/a/b;

    iput-object p5, p0, Lcom/swedbank/mobile/app/cards/a/k;->i:Lcom/swedbank/mobile/app/cards/d/b;

    iput-object p6, p0, Lcom/swedbank/mobile/app/cards/a/k;->j:Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p7, p0, Lcom/swedbank/mobile/app/cards/a/k;->k:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    iput-object p8, p0, Lcom/swedbank/mobile/app/cards/a/k;->l:Lcom/swedbank/mobile/business/general/confirmation/c;

    iput-object p9, p0, Lcom/swedbank/mobile/app/cards/a/k;->m:Lcom/swedbank/mobile/business/cards/order/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/k;->g:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/k;->j:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/app/cards/f/a/b;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/k;->h:Lcom/swedbank/mobile/app/cards/f/a/b;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/k;->k:Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/app/cards/d/b;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/k;->i:Lcom/swedbank/mobile/app/cards/d/b;

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/business/cards/order/b;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/k;->m:Lcom/swedbank/mobile/business/cards/order/b;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 45
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/k$e;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/a/k$e;-><init>(Lcom/swedbank/mobile/app/cards/a/k;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/a/k;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/k$g;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/cards/a/k$g;-><init>(Lcom/swedbank/mobile/app/cards/a/k;Ljava/lang/String;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/a/k;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 102
    sget-object v0, Lcom/swedbank/mobile/app/cards/a/k$a;->a:Lcom/swedbank/mobile/app/cards/a/k$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "url"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p0, p1}, Lcom/swedbank/mobile/app/j/d$a;->a(Lcom/swedbank/mobile/app/j/d;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 1

    .line 104
    sget-object v0, Lcom/swedbank/mobile/app/cards/a/k$c;->a:Lcom/swedbank/mobile/app/cards/a/k$c;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public d()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/k;->g:Lcom/swedbank/mobile/app/f/a;

    .line 75
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/a/k;->l:Lcom/swedbank/mobile/business/general/confirmation/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 76
    sget-object v1, Lcom/swedbank/mobile/app/cards/a/v;->a:Lcom/swedbank/mobile/app/cards/a/v;

    check-cast v1, Lcom/swedbank/mobile/app/f/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    const/4 v1, 0x0

    .line 77
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Z)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 106
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public e()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/k;->g:Lcom/swedbank/mobile/app/f/a;

    .line 82
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/a/k;->l:Lcom/swedbank/mobile/business/general/confirmation/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 83
    sget-object v1, Lcom/swedbank/mobile/app/cards/a/u;->a:Lcom/swedbank/mobile/app/cards/a/u;

    check-cast v1, Lcom/swedbank/mobile/app/f/c;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    const/4 v1, 0x0

    .line 84
    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/f/a;->a(Z)Lcom/swedbank/mobile/app/f/a;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/f/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 107
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public f()V
    .locals 1

    .line 108
    sget-object v0, Lcom/swedbank/mobile/app/cards/a/k$d;->a:Lcom/swedbank/mobile/app/cards/a/k$d;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public g()V
    .locals 1

    .line 90
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/k$f;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/cards/a/k$f;-><init>(Lcom/swedbank/mobile/app/cards/a/k;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/a/k;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public h()V
    .locals 1

    .line 110
    sget-object v0, Lcom/swedbank/mobile/app/cards/a/k$b;->a:Lcom/swedbank/mobile/app/cards/a/k$b;

    check-cast v0, Lkotlin/e/a/b;

    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lkotlin/e/a/b;)V

    return-void
.end method

.method public i()Landroid/app/Application;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/k;->e:Landroid/app/Application;

    return-object v0
.end method

.method public j()Lcom/swedbank/mobile/data/device/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/k;->f:Lcom/swedbank/mobile/data/device/a;

    return-object v0
.end method
