.class public final Lcom/swedbank/mobile/app/cards/a/g$w;
.super Ljava/lang/Object;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/cards/a/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "Ljava/lang/Throwable;",
        "Lio/reactivex/s<",
        "+",
        "Lcom/swedbank/mobile/app/cards/a/s$a;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/g;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/a/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/g$w;->a:Lcom/swedbank/mobile/app/cards/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Lio/reactivex/o;
    .locals 9
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/app/cards/a/s$a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "e"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$w;->a:Lcom/swedbank/mobile/app/cards/a/g;

    new-instance v1, Lcom/swedbank/mobile/app/cards/a/s$a$k;

    .line 176
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    .line 175
    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/cards/a/s$a$k;-><init>(Lcom/swedbank/mobile/app/w/b;)V

    move-object v3, v1

    check-cast v3, Lcom/swedbank/mobile/app/cards/a/s$a;

    .line 262
    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/g;->c(Lcom/swedbank/mobile/app/cards/a/g;)Lcom/swedbank/mobile/core/ui/ad;

    move-result-object v2

    .line 264
    sget-object v4, Lcom/swedbank/mobile/app/cards/a/s$a$j;->a:Lcom/swedbank/mobile/app/cards/a/s$a$j;

    const-wide/16 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 262
    invoke-static/range {v2 .. v8}, Lcom/swedbank/mobile/core/ui/ad;->a(Lcom/swedbank/mobile/core/ui/ad;Ljava/lang/Object;Ljava/lang/Object;JILjava/lang/Object;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/g$w;->a(Ljava/lang/Throwable;)Lio/reactivex/o;

    move-result-object p1

    return-object p1
.end method
