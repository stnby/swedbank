.class public final Lcom/swedbank/mobile/app/cards/a/v;
.super Lcom/swedbank/mobile/app/f/c$b;
.source "ContactlessEnabledDialogInformation.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/a/v;

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final e:Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/swedbank/mobile/app/cards/a/v;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/a/v;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/a/v;->a:Lcom/swedbank/mobile/app/cards/a/v;

    .line 7
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->card_contactless_enabled_information_dialog_title:I

    sput v0, Lcom/swedbank/mobile/app/cards/a/v;->b:I

    .line 8
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->card_contactless_enabled_information_dialog_decription:I

    sput v0, Lcom/swedbank/mobile/app/cards/a/v;->c:I

    .line 9
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->card_contactless_enabled_information_dialog_confirm_btn:I

    sput v0, Lcom/swedbank/mobile/app/cards/a/v;->d:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/swedbank/mobile/app/f/c$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 7
    sget v0, Lcom/swedbank/mobile/app/cards/a/v;->b:I

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 8
    sget v0, Lcom/swedbank/mobile/app/cards/a/v;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .line 9
    sget v0, Lcom/swedbank/mobile/app/cards/a/v;->d:I

    return v0
.end method

.method public d()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 10
    sget-object v0, Lcom/swedbank/mobile/app/cards/a/v;->e:Ljava/lang/Integer;

    return-object v0
.end method
