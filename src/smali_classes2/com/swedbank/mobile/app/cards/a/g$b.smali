.class final synthetic Lcom/swedbank/mobile/app/cards/a/g$b;
.super Lkotlin/e/b/i;
.source "CardDetailsPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/g;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/cards/a/s;",
        "Lcom/swedbank/mobile/app/cards/a/s$a;",
        "Lcom/swedbank/mobile/app/cards/a/s;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/g;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/s$a;)Lcom/swedbank/mobile/app/cards/a/s;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/cards/a/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/cards/a/s$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/g$b;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/cards/a/g;

    .line 169
    invoke-static {v0, p1, p2}, Lcom/swedbank/mobile/app/cards/a/g;->a(Lcom/swedbank/mobile/app/cards/a/g;Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/s$a;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/swedbank/mobile/app/cards/a/s;

    check-cast p2, Lcom/swedbank/mobile/app/cards/a/s$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/cards/a/g$b;->a(Lcom/swedbank/mobile/app/cards/a/s;Lcom/swedbank/mobile/app/cards/a/s$a;)Lcom/swedbank/mobile/app/cards/a/s;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/cards/a/g;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/cards/details/CardDetailsViewState;Lcom/swedbank/mobile/app/cards/details/CardDetailsViewState$PartialState;)Lcom/swedbank/mobile/app/cards/details/CardDetailsViewState;"

    return-object v0
.end method
