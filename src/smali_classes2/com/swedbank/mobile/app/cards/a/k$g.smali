.class final Lcom/swedbank/mobile/app/cards/a/k$g;
.super Lkotlin/e/b/k;
.source "CardDetailsRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/a/k;->a(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/a/k;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/a/k;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/k$g;->a:Lcom/swedbank/mobile/app/cards/a/k;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/k$g;->b:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    check-cast p1, Ljava/lang/Iterable;

    .line 103
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0

    .line 104
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 102
    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    if-eqz v0, :cond_2

    const/4 p1, 0x1

    :goto_0
    if-nez p1, :cond_3

    .line 66
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/a/k$g;->a:Lcom/swedbank/mobile/app/cards/a/k;

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/k$g;->a:Lcom/swedbank/mobile/app/cards/a/k;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/a/k;->c(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object v0

    .line 63
    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/a/k$g;->b:Ljava/lang/String;

    invoke-static {v2}, Lkotlin/a/h;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v2, v1}, Lcom/swedbank/mobile/app/cards/f/a/b;->a(Ljava/util/List;Z)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/a/k$g;->a:Lcom/swedbank/mobile/app/cards/a/k;

    invoke-static {v1}, Lcom/swedbank/mobile/app/cards/a/k;->d(Lcom/swedbank/mobile/app/cards/a/k;)Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/app/cards/f/a/b;->a(Lcom/swedbank/mobile/business/cards/wallet/onboarding/g;)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 107
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->c(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_3
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/k$g;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
