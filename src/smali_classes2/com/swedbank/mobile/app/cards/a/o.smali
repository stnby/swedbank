.class public final Lcom/swedbank/mobile/app/cards/a/o;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "CardDetailsViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/cards/a/n;
.implements Lcom/swedbank/mobile/core/ui/widget/u;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final A:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/swedbank/mobile/core/ui/widget/t;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lkotlin/f/c;

.field private final k:Lkotlin/f/c;

.field private final l:Lkotlin/f/c;

.field private final m:Lkotlin/f/c;

.field private final n:Lkotlin/f/c;

.field private final o:Lkotlin/f/c;

.field private final p:Lkotlin/f/c;

.field private final q:Lkotlin/f/c;

.field private final r:Lkotlin/f/c;

.field private final s:Lkotlin/f/c;

.field private final t:Lkotlin/f/c;

.field private final u:Lkotlin/f/c;

.field private final v:Lkotlin/f/c;

.field private final w:Lkotlin/f/c;

.field private final x:Lkotlin/f/c;

.field private y:Z

.field private final z:Lcom/swedbank/mobile/business/f/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x16

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbarLayout"

    const-string v4, "getToolbarLayout()Lcom/google/android/material/appbar/AppBarLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "refreshView"

    const-string v4, "getRefreshView()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "scrollView"

    const-string v4, "getScrollView()Landroidx/core/widget/NestedScrollView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contentView"

    const-string v4, "getContentView()Landroid/view/ViewGroup;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardView"

    const-string v4, "getCardView()Lcom/swedbank/mobile/app/cards/CardLayout;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardStateView"

    const-string v4, "getCardStateView()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "contactlessView"

    const-string v4, "getContactlessView()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "internetShoppingView"

    const-string v4, "getInternetShoppingView()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "digitizedCardGroupViews"

    const-string v4, "getDigitizedCardGroupViews()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "mobileContactlessView"

    const-string v4, "getMobileContactlessView()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "digitalCardCopyNumberView"

    const-string v4, "getDigitalCardCopyNumberView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "offlinePaymentsRemainingView"

    const-string v4, "getOfflinePaymentsRemainingView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "defaultContactlessCardView"

    const-string v4, "getDefaultContactlessCardView()Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "orderCardButton"

    const-string v4, "getOrderCardButton()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "deliveryLocationInfoView"

    const-string v4, "getDeliveryLocationInfoView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "expirationInfoView"

    const-string v4, "getExpirationInfoView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "limitsGroupViews"

    const-string v4, "getLimitsGroupViews()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "limitManagementBtn"

    const-string v4, "getLimitManagementBtn()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "limitStorePaymentView"

    const-string v4, "getLimitStorePaymentView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "limitAtmWithdrawalView"

    const-string v4, "getLimitAtmWithdrawalView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/cards/a/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "limitAtmPaymentView"

    const-string v4, "getLimitAtmPaymentView()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lcom/swedbank/mobile/business/f/a;Lio/reactivex/o;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/business/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/f/a;",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "featureRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backClicks"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->z:Lcom/swedbank/mobile/business/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->A:Lio/reactivex/o;

    .line 40
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_toolbar_layout:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->c:Lkotlin/f/c;

    .line 41
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->d:Lkotlin/f/c;

    .line 42
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_refresh:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->e:Lkotlin/f/c;

    .line 43
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_scroll_view:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->f:Lkotlin/f/c;

    .line 44
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_content:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->g:Lkotlin/f/c;

    .line 45
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_view:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->h:Lkotlin/f/c;

    .line 46
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_card_state:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->i:Lkotlin/f/c;

    .line 47
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_contactless:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->j:Lkotlin/f/c;

    .line 48
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_internet_shopping:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->k:Lkotlin/f/c;

    const/4 p1, 0x2

    .line 49
    new-array p2, p1, [I

    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_details_digitized_card_group_header:I

    const/4 v1, 0x0

    aput v0, p2, v1

    sget v0, Lcom/swedbank/mobile/app/cards/n$e;->card_details_digitized_card_group:I

    const/4 v2, 0x1

    aput v0, p2, v2

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;[I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->l:Lkotlin/f/c;

    .line 50
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_mobile_contactless:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->m:Lkotlin/f/c;

    .line 51
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_digital_card_copy_number:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->n:Lkotlin/f/c;

    .line 52
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_offline_payments_remaining:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->o:Lkotlin/f/c;

    .line 53
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_default_contactless_card:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->p:Lkotlin/f/c;

    .line 54
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_order_card_action_btn:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->q:Lkotlin/f/c;

    .line 55
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_delivery_info:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->r:Lkotlin/f/c;

    .line 56
    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_expiration_info:I

    invoke-static {p0, p2}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/a/o;->s:Lkotlin/f/c;

    .line 57
    new-array p1, p1, [I

    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_limits_group_header:I

    aput p2, p1, v1

    sget p2, Lcom/swedbank/mobile/app/cards/n$e;->card_details_limits_group:I

    aput p2, p1, v2

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;[I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->t:Lkotlin/f/c;

    .line 58
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_limit_management_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->u:Lkotlin/f/c;

    .line 59
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_limits_store_payment:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->v:Lkotlin/f/c;

    .line 60
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_limits_atm_withdrawal:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->w:Lkotlin/f/c;

    .line 61
    sget p1, Lcom/swedbank/mobile/app/cards/n$e;->card_details_limits_atm_payment:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->x:Lkotlin/f/c;

    .line 64
    iput-boolean v2, p0, Lcom/swedbank/mobile/app/cards/a/o;->y:Z

    return-void
.end method

.method private final A()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->n:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0xb

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method private final B()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->o:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0xc

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method private final C()Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->p:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0xd

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;

    return-object v0
.end method

.method private final D()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->q:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0xe

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final E()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->r:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0xf

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method private final F()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->s:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x10

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method private final G()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->t:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x11

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final H()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->u:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x12

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final I()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->v:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x13

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method private final J()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->w:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x14

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method private final K()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->x:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x15

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    return-object v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/a/o;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->r()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->v()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/Button;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->D()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->E()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->F()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->w()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->x()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic h(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/business/f/a;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/a/o;->z:Lcom/swedbank/mobile/business/f/a;

    return-object p0
.end method

.method public static final synthetic i(Lcom/swedbank/mobile/app/cards/a/o;)Ljava/util/List;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->y()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic j(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->z()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic k(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->A()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic l(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->B()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic m(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->C()Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic n(Lcom/swedbank/mobile/app/cards/a/o;)Ljava/util/List;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->G()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic o(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->I()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method private final p()Lcom/google/android/material/appbar/AppBarLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/material/appbar/AppBarLayout;

    return-object v0
.end method

.method public static final synthetic p(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->J()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method private final q()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic q(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->K()Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic r(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/TextView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->H()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method private final r()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private final s()Landroidx/core/widget/NestedScrollView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/core/widget/NestedScrollView;

    return-object v0
.end method

.method private final t()Landroid/view/ViewGroup;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private final u()Lcom/swedbank/mobile/app/cards/CardLayout;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/cards/CardLayout;

    return-object v0
.end method

.method private final v()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    return-object v0
.end method

.method private final w()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->j:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    return-object v0
.end method

.method private final x()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->k:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x8

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    return-object v0
.end method

.method private final y()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->l:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0x9

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final z()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->m:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/cards/a/o;->a:[Lkotlin/h/g;

    const/16 v2, 0xa

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 77
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->r()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    invoke-static {v0}, Lcom/b/b/c/a;->a(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 78
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/a/o;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->a()Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 76
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026er.observeActionClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/cards/a/s;)V
    .locals 23
    .param p1    # Lcom/swedbank/mobile/app/cards/a/s;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    const-string v1, "viewState"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->n()Landroid/content/Context;

    move-result-object v1

    .line 99
    iget-boolean v3, v0, Lcom/swedbank/mobile/app/cards/a/o;->y:Z

    if-nez v3, :cond_0

    .line 100
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->t()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-static {}, Lcom/swedbank/mobile/core/ui/b;->c()Landroidx/l/r;

    move-result-object v4

    check-cast v4, Landroidx/l/n;

    invoke-static {v3, v4}, Landroidx/l/p;->a(Landroid/view/ViewGroup;Landroidx/l/n;)V

    .line 103
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->a()Lcom/swedbank/mobile/app/cards/a/b;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v3, :cond_38

    .line 104
    sget-object v7, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->f()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/swedbank/mobile/app/cards/g;->d(Lcom/swedbank/mobile/business/cards/g;)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v8, "context.getString(CardTy\u2026isplayNameFor(card.type))"

    invoke-static {v7, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->q()Landroidx/appcompat/widget/Toolbar;

    move-result-object v8

    sget v9, Lcom/swedbank/mobile/app/cards/n$h;->card_details_title:I

    new-array v10, v5, [Ljava/lang/Object;

    aput-object v7, v10, v6

    invoke-virtual {v1, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v8, v7}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    .line 107
    invoke-direct/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->u()Lcom/swedbank/mobile/app/cards/CardLayout;

    move-result-object v9

    .line 108
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->a()Ljava/lang/String;

    move-result-object v10

    .line 109
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->b()Ljava/lang/String;

    move-result-object v11

    .line 110
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->c()Ljava/lang/String;

    move-result-object v12

    .line 111
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->h()Z

    move-result v13

    .line 112
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->d()Lcom/swedbank/mobile/app/cards/f;

    move-result-object v14

    .line 113
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->f()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v15

    .line 107
    invoke-virtual/range {v9 .. v15}, Lcom/swedbank/mobile/app/cards/CardLayout;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/g;)V

    .line 115
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->b()Z

    move-result v7

    .line 504
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->a(Lcom/swedbank/mobile/app/cards/a/o;)Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 119
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->d()Z

    move-result v7

    .line 508
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->e()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v8

    sget-object v9, Lcom/swedbank/mobile/app/cards/a/p;->a:[I

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v8

    aget v8, v9, v8

    const/16 v9, 0x8

    packed-switch v8, :pswitch_data_0

    goto/16 :goto_3

    .line 622
    :pswitch_0
    move-object v12, v4

    check-cast v12, Ljava/lang/CharSequence;

    .line 627
    sget-object v8, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v8, Lcom/swedbank/mobile/business/cards/r;

    .line 628
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->b(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v8

    .line 629
    move-object v10, v8

    check-cast v10, Landroid/view/View;

    .line 630
    invoke-virtual {v10, v9}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v8

    .line 632
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 635
    invoke-virtual {v8, v6}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setClickable(Z)V

    .line 636
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    .line 637
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->c(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/Button;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 638
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 640
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->d(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v8

    .line 649
    invoke-virtual {v8, v9}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 651
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_3

    .line 579
    :pswitch_1
    sget v8, Lcom/swedbank/mobile/app/cards/n$h;->card_status_not_activated:I

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v12, v8

    check-cast v12, Ljava/lang/CharSequence;

    .line 581
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->o()Lcom/swedbank/mobile/business/cards/r;

    move-result-object v8

    .line 586
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->b(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v15

    .line 587
    move-object v10, v15

    check-cast v10, Landroid/view/View;

    .line 588
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/16 v16, 0x0

    move-object v10, v15

    move-object v4, v15

    move-object/from16 v15, v16

    .line 590
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 593
    invoke-virtual {v4, v6}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setClickable(Z)V

    .line 594
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 595
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->c(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/Button;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 596
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 598
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->d(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v4

    .line 599
    instance-of v10, v8, Lcom/swedbank/mobile/business/cards/r$c;

    if-nez v10, :cond_3

    .line 600
    invoke-virtual {v4, v6}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 602
    instance-of v10, v8, Lcom/swedbank/mobile/business/cards/r$a;

    if-eqz v10, :cond_1

    check-cast v8, Lcom/swedbank/mobile/business/cards/r$a;

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/cards/r$a;->a()Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 603
    :cond_1
    sget-object v10, Lcom/swedbank/mobile/business/cards/r$b;->a:Lcom/swedbank/mobile/business/cards/r$b;

    invoke-static {v8, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getContext()Landroid/content/Context;

    move-result-object v8

    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_delivery_via_post_text:I

    invoke-virtual {v8, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 604
    :cond_2
    sget-object v4, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    invoke-static {v8, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    .line 607
    :cond_3
    invoke-virtual {v4, v9}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 609
    :goto_0
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_3

    .line 546
    :pswitch_2
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->card_status_blocked:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Ljava/lang/CharSequence;

    .line 550
    sget-object v4, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v4, Lcom/swedbank/mobile/business/cards/r;

    .line 551
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->b(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v8

    .line 552
    move-object v10, v8

    check-cast v10, Landroid/view/View;

    .line 553
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    .line 557
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v8

    .line 555
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 558
    invoke-virtual {v8, v5}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setClickable(Z)V

    .line 559
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    .line 560
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->c(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/Button;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 561
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 563
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->d(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v8

    .line 564
    instance-of v10, v4, Lcom/swedbank/mobile/business/cards/r$c;

    if-nez v10, :cond_6

    .line 565
    invoke-virtual {v8, v6}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 567
    instance-of v10, v4, Lcom/swedbank/mobile/business/cards/r$a;

    if-eqz v10, :cond_4

    check-cast v4, Lcom/swedbank/mobile/business/cards/r$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/r$a;->a()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 568
    :cond_4
    sget-object v10, Lcom/swedbank/mobile/business/cards/r$b;->a:Lcom/swedbank/mobile/business/cards/r$b;

    invoke-static {v4, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v8}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_delivery_via_post_text:I

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 569
    :cond_5
    sget-object v8, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    invoke-static {v4, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 572
    :cond_6
    invoke-virtual {v8, v9}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 574
    :goto_1
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_3

    .line 512
    :pswitch_3
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->card_status_active:I

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Ljava/lang/CharSequence;

    .line 517
    sget-object v4, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    check-cast v4, Lcom/swedbank/mobile/business/cards/r;

    .line 518
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->b(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v8

    .line 519
    move-object v10, v8

    check-cast v10, Landroid/view/View;

    .line 520
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    .line 524
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v8

    .line 522
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 525
    invoke-virtual {v8, v5}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setClickable(Z)V

    .line 526
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    .line 527
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->c(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/Button;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 528
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 530
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->d(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v8

    .line 531
    instance-of v10, v4, Lcom/swedbank/mobile/business/cards/r$c;

    if-nez v10, :cond_9

    .line 532
    invoke-virtual {v8, v6}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 534
    instance-of v10, v4, Lcom/swedbank/mobile/business/cards/r$a;

    if-eqz v10, :cond_7

    check-cast v4, Lcom/swedbank/mobile/business/cards/r$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/r$a;->a()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 535
    :cond_7
    sget-object v10, Lcom/swedbank/mobile/business/cards/r$b;->a:Lcom/swedbank/mobile/business/cards/r$b;

    invoke-static {v4, v10}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v8}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_delivery_via_post_text:I

    invoke-virtual {v4, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 536
    :cond_8
    sget-object v8, Lcom/swedbank/mobile/business/cards/r$c;->a:Lcom/swedbank/mobile/business/cards/r$c;

    invoke-static {v4, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_2

    .line 539
    :cond_9
    invoke-virtual {v8, v9}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setVisibility(I)V

    .line 541
    :goto_2
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 653
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->b(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v4

    xor-int/lit8 v8, v7, 0x1

    .line 654
    invoke-virtual {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setEnabled(Z)V

    .line 655
    invoke-virtual {v4, v7}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setLoading(Z)V

    .line 656
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 123
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->e()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v4

    sget-object v7, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    if-ne v4, v7, :cond_a

    const/4 v4, 0x1

    goto :goto_4

    :cond_a
    const/4 v4, 0x0

    .line 124
    :goto_4
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->g()Lcom/swedbank/mobile/business/cards/c;

    move-result-object v7

    .line 125
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->o()Lcom/swedbank/mobile/business/cards/r;

    move-result-object v8

    .line 658
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->e(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v10

    .line 659
    invoke-virtual {v7}, Lcom/swedbank/mobile/business/cards/c;->b()Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v10, v7}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 660
    move-object v7, v10

    check-cast v7, Landroid/view/View;

    if-eqz v4, :cond_b

    const/4 v4, 0x0

    goto :goto_5

    :cond_b
    const/16 v4, 0x8

    .line 661
    :goto_5
    invoke-virtual {v7, v4}, Landroid/view/View;->setVisibility(I)V

    .line 664
    instance-of v4, v8, Lcom/swedbank/mobile/business/cards/r$a;

    if-eqz v4, :cond_c

    invoke-virtual {v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 665
    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->card_details_expiration_delivery_info_to_branch:I

    new-array v11, v5, [Ljava/lang/Object;

    .line 666
    check-cast v8, Lcom/swedbank/mobile/business/cards/r$a;

    invoke-virtual {v8}, Lcom/swedbank/mobile/business/cards/r$a;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v11, v6

    .line 664
    invoke-virtual {v4, v7, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    .line 668
    :cond_c
    sget-object v4, Lcom/swedbank/mobile/business/cards/r$b;->a:Lcom/swedbank/mobile/business/cards/r$b;

    invoke-static {v8, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->card_details_expiration_delivery_info_by_post:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    .line 669
    :cond_d
    invoke-virtual {v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->card_details_expiration_delivery_info_not_renewed:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_6
    const-string v7, "when (deliveryLocation) \u2026info_not_renewed)\n      }"

    .line 663
    invoke-static {v4, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 671
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v10, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setDescription(Ljava/lang/CharSequence;)V

    .line 672
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 129
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->f()Z

    move-result v4

    .line 681
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->f(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v7

    .line 682
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->i()Z

    move-result v8

    .line 683
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->e()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v10

    sget-object v11, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    if-ne v10, v11, :cond_e

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->h()Z

    move-result v10

    if-eqz v10, :cond_e

    const/4 v10, 0x1

    goto :goto_7

    :cond_e
    const/4 v10, 0x0

    :goto_7
    if-eqz v10, :cond_11

    if-eqz v8, :cond_f

    .line 693
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_feature_enabled_description:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.card_\u2026ture_enabled_description)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_8

    :cond_f
    if-eqz v4, :cond_10

    const-string v10, ""

    goto :goto_8

    .line 695
    :cond_10
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_feature_disabled_description:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.card_\u2026ure_disabled_description)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 696
    :goto_8
    move-object v12, v10

    check-cast v12, Ljava/lang/CharSequence;

    .line 702
    move-object v10, v7

    check-cast v10, Landroid/view/View;

    .line 703
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    .line 707
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v7

    .line 705
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 708
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_9

    :cond_11
    const/4 v8, 0x0

    .line 710
    move-object v12, v8

    check-cast v12, Ljava/lang/CharSequence;

    .line 713
    move-object v8, v7

    check-cast v8, Landroid/view/View;

    .line 714
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    .line 718
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v7

    .line 716
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 719
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    :goto_9
    xor-int/lit8 v8, v4, 0x1

    .line 722
    invoke-virtual {v7, v8}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setEnabled(Z)V

    .line 723
    invoke-virtual {v7, v4}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setLoading(Z)V

    .line 724
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 726
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 134
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->g()Z

    move-result v4

    .line 734
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->g(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v7

    .line 735
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->j()Z

    move-result v8

    .line 737
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->e()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v10

    sget-object v11, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    if-ne v10, v11, :cond_12

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->h(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/business/f/a;

    move-result-object v10

    const-string v11, "feature_card_internet_shopping"

    invoke-interface {v10, v11}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_12

    const/4 v10, 0x1

    goto :goto_a

    :cond_12
    const/4 v10, 0x0

    :goto_a
    if-eqz v10, :cond_15

    if-eqz v8, :cond_13

    .line 747
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_feature_enabled_description:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.card_\u2026ture_enabled_description)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_b

    :cond_13
    if-eqz v4, :cond_14

    const-string v10, ""

    goto :goto_b

    .line 749
    :cond_14
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_feature_disabled_description:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.card_\u2026ure_disabled_description)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 750
    :goto_b
    move-object v12, v10

    check-cast v12, Ljava/lang/CharSequence;

    .line 756
    move-object v10, v7

    check-cast v10, Landroid/view/View;

    .line 757
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    .line 761
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v7

    .line 759
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 762
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_c

    :cond_15
    const/4 v8, 0x0

    .line 764
    move-object v12, v8

    check-cast v12, Ljava/lang/CharSequence;

    .line 767
    move-object v8, v7

    check-cast v8, Landroid/view/View;

    .line 768
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    const/4 v11, 0x0

    .line 772
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    move-object v10, v7

    .line 770
    invoke-static/range {v10 .. v15}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    .line 773
    sget-object v8, Lkotlin/s;->a:Lkotlin/s;

    :goto_c
    xor-int/lit8 v8, v4, 0x1

    .line 776
    invoke-virtual {v7, v8}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setEnabled(Z)V

    .line 777
    invoke-virtual {v7, v4}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setLoading(Z)V

    .line 778
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 780
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 139
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->j()Z

    move-result v4

    .line 140
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->h()Z

    move-result v7

    .line 141
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->i()Z

    move-result v8

    .line 785
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->e()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v10

    sget-object v11, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    if-ne v10, v11, :cond_16

    if-eqz v4, :cond_16

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->i()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->k()Z

    move-result v4

    if-eqz v4, :cond_16

    const/4 v4, 0x1

    goto :goto_d

    :cond_16
    const/4 v4, 0x0

    .line 786
    :goto_d
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->i(Lcom/swedbank/mobile/app/cards/a/o;)Ljava/util/List;

    move-result-object v10

    if-eqz v4, :cond_17

    const/4 v11, 0x0

    goto :goto_e

    :cond_17
    const/16 v11, 0x8

    .line 787
    :goto_e
    check-cast v10, Ljava/lang/Iterable;

    .line 788
    new-instance v12, Ljava/util/ArrayList;

    const/16 v13, 0xa

    invoke-static {v10, v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v14

    invoke-direct {v12, v14}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v12, Ljava/util/Collection;

    .line 789
    invoke-interface {v10}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_f
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_18

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    .line 790
    check-cast v14, Landroid/view/View;

    .line 787
    invoke-virtual {v14, v11}, Landroid/view/View;->setVisibility(I)V

    sget-object v14, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v12, v14}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 791
    :cond_18
    check-cast v12, Ljava/util/List;

    if-eqz v4, :cond_22

    if-nez v7, :cond_1a

    .line 795
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->m()Z

    move-result v4

    if-eqz v4, :cond_19

    goto :goto_10

    :cond_19
    const/4 v4, 0x0

    goto :goto_11

    :cond_1a
    :goto_10
    const/4 v4, 0x1

    .line 797
    :goto_11
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->j(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v7

    .line 798
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->n()Lcom/swedbank/mobile/app/cards/a/w;

    move-result-object v10

    instance-of v10, v10, Lcom/swedbank/mobile/app/cards/a/w$a;

    const/16 v18, 0x0

    .line 800
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    if-eqz v10, :cond_1b

    .line 810
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_feature_enabled_description:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.card_\u2026ture_enabled_description)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_12

    :cond_1b
    if-eqz v4, :cond_1c

    const-string v10, ""

    goto :goto_12

    .line 812
    :cond_1c
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_feature_disabled_description:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "getString(R.string.card_\u2026ure_disabled_description)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 813
    :goto_12
    move-object/from16 v19, v10

    check-cast v19, Ljava/lang/CharSequence;

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v17, v7

    .line 799
    invoke-static/range {v17 .. v22}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Boolean;ILjava/lang/Object;)V

    xor-int/lit8 v10, v4, 0x1

    .line 814
    invoke-virtual {v7, v10}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setEnabled(Z)V

    .line 815
    invoke-virtual {v7, v4}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->setLoading(Z)V

    .line 816
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 820
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->n()Lcom/swedbank/mobile/app/cards/a/w;

    move-result-object v4

    .line 822
    instance-of v7, v4, Lcom/swedbank/mobile/app/cards/a/w$a;

    if-eqz v7, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->k(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v7

    .line 825
    check-cast v4, Lcom/swedbank/mobile/app/cards/a/w$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/a/w$a;->b()Ljava/lang/String;

    move-result-object v4

    .line 826
    move-object v10, v7

    check-cast v10, Landroid/view/View;

    .line 827
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    .line 829
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v7, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_13

    .line 831
    :cond_1d
    sget-object v7, Lcom/swedbank/mobile/app/cards/a/w$b;->a:Lcom/swedbank/mobile/app/cards/a/w$b;

    invoke-static {v4, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->k(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v4

    const/4 v7, 0x0

    .line 833
    move-object v10, v7

    check-cast v10, Ljava/lang/String;

    .line 834
    move-object v7, v4

    check-cast v7, Landroid/view/View;

    .line 835
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 837
    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {v4, v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 840
    :cond_1e
    :goto_13
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 844
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->n()Lcom/swedbank/mobile/app/cards/a/w;

    move-result-object v4

    .line 846
    instance-of v7, v4, Lcom/swedbank/mobile/app/cards/a/w$a;

    if-eqz v7, :cond_1f

    .line 847
    check-cast v4, Lcom/swedbank/mobile/app/cards/a/w$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/a/w$a;->c()Lcom/swedbank/mobile/business/cards/x;

    move-result-object v7

    instance-of v7, v7, Lcom/swedbank/mobile/business/cards/x$a;

    if-eqz v7, :cond_1f

    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->l(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v7

    .line 850
    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/a/w$a;->c()Lcom/swedbank/mobile/business/cards/x;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/business/cards/x$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/cards/x$a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 851
    move-object v10, v7

    check-cast v10, Landroid/view/View;

    .line 852
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    .line 854
    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v7, v4}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_14

    .line 856
    :cond_1f
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->l(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v4

    const/4 v7, 0x0

    .line 858
    move-object v10, v7

    check-cast v10, Ljava/lang/String;

    .line 859
    move-object v7, v4

    check-cast v7, Landroid/view/View;

    .line 860
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    .line 862
    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {v4, v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 865
    :goto_14
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 871
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->n()Lcom/swedbank/mobile/app/cards/a/w;

    move-result-object v4

    .line 873
    instance-of v7, v4, Lcom/swedbank/mobile/app/cards/a/w$a;

    if-eqz v7, :cond_21

    .line 877
    check-cast v4, Lcom/swedbank/mobile/app/cards/a/w$a;

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/a/w$a;->a()Z

    move-result v7

    if-ne v7, v5, :cond_20

    .line 878
    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->card_details_default_contactless_card:I

    goto :goto_15

    .line 879
    :cond_20
    sget v7, Lcom/swedbank/mobile/app/cards/n$h;->card_details_set_default_contactless_card:I

    .line 876
    :goto_15
    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    .line 882
    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/a/w$a;->a()Z

    move-result v4

    xor-int/2addr v4, v5

    .line 883
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->m(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;

    move-result-object v10

    .line 884
    move-object v11, v10

    check-cast v11, Landroid/view/View;

    .line 885
    invoke-virtual {v11, v6}, Landroid/view/View;->setVisibility(I)V

    .line 887
    invoke-virtual {v10, v7, v4}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a(Ljava/lang/CharSequence;Z)V

    .line 890
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 892
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->m(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;

    move-result-object v4

    .line 893
    invoke-virtual {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->setLoading(Z)V

    .line 894
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    goto :goto_16

    :cond_21
    const/4 v4, 0x0

    .line 902
    move-object v7, v4

    check-cast v7, Ljava/lang/CharSequence;

    .line 904
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->m(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;

    move-result-object v4

    .line 905
    move-object v8, v4

    check-cast v8, Landroid/view/View;

    .line 906
    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 908
    invoke-virtual {v4, v7, v6}, Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;->a(Ljava/lang/CharSequence;Z)V

    .line 911
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 914
    :goto_16
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 916
    :cond_22
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 145
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->e()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v4

    sget-object v7, Lcom/swedbank/mobile/business/cards/CardState;->ACTIVE:Lcom/swedbank/mobile/business/cards/CardState;

    if-ne v4, v7, :cond_23

    const/4 v4, 0x1

    goto :goto_17

    :cond_23
    const/4 v4, 0x0

    .line 147
    :goto_17
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->l()Lcom/swedbank/mobile/business/cards/CardClass;

    move-result-object v7

    sget-object v8, Lcom/swedbank/mobile/business/cards/CardClass;->DEBIT:Lcom/swedbank/mobile/business/cards/CardClass;

    if-ne v7, v8, :cond_24

    iget-object v7, v0, Lcom/swedbank/mobile/app/cards/a/o;->z:Lcom/swedbank/mobile/business/f/a;

    const-string v8, "feature_card_limit_management"

    invoke-interface {v7, v8}, Lcom/swedbank/mobile/business/f/a;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_24

    const/4 v7, 0x1

    goto :goto_18

    :cond_24
    const/4 v7, 0x0

    .line 148
    :goto_18
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/a/b;->p()Ljava/util/List;

    move-result-object v3

    if-eqz v4, :cond_36

    .line 918
    move-object v4, v3

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/2addr v4, v5

    if-eqz v4, :cond_36

    .line 919
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->n(Lcom/swedbank/mobile/app/cards/a/o;)Ljava/util/List;

    move-result-object v4

    .line 920
    check-cast v4, Ljava/lang/Iterable;

    .line 921
    new-instance v8, Ljava/util/ArrayList;

    invoke-static {v4, v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v10

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v8, Ljava/util/Collection;

    .line 922
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_19
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_25

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 923
    check-cast v10, Landroid/view/View;

    .line 920
    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    sget-object v10, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v8, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 924
    :cond_25
    check-cast v8, Ljava/util/List;

    .line 925
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->o(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v4

    .line 926
    check-cast v3, Ljava/lang/Iterable;

    .line 927
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_26
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_28

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    move-object v11, v10

    check-cast v11, Lcom/swedbank/mobile/app/cards/a/t;

    .line 926
    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->d()Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v11

    sget-object v12, Lcom/swedbank/mobile/business/cards/CardLimitService;->PURCHASES:Lcom/swedbank/mobile/business/cards/CardLimitService;

    if-ne v11, v12, :cond_27

    const/4 v11, 0x1

    goto :goto_1a

    :cond_27
    const/4 v11, 0x0

    :goto_1a
    if-eqz v11, :cond_26

    goto :goto_1b

    :cond_28
    const/4 v10, 0x0

    .line 928
    :goto_1b
    check-cast v10, Lcom/swedbank/mobile/app/cards/a/t;

    const/4 v8, 0x2

    if-nez v10, :cond_29

    const/4 v11, 0x0

    .line 933
    move-object v10, v11

    check-cast v10, Ljava/lang/String;

    .line 934
    move-object v11, v4

    check-cast v11, Landroid/view/View;

    .line 935
    invoke-virtual {v11, v9}, Landroid/view/View;->setVisibility(I)V

    .line 937
    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {v4, v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_1e

    .line 947
    :cond_29
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->a()Z

    move-result v11

    if-eqz v11, :cond_2a

    .line 951
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v11

    sget-object v12, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    invoke-virtual {v11}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->ordinal()I

    move-result v11

    aget v11, v12, v11

    packed-switch v11, :pswitch_data_1

    .line 955
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not renderable card limit validity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 954
    :pswitch_4
    sget v11, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_monthly:I

    goto :goto_1c

    .line 953
    :pswitch_5
    sget v11, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_weekly:I

    goto :goto_1c

    .line 952
    :pswitch_6
    sget v11, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_daily:I

    .line 956
    :goto_1c
    new-array v12, v8, [Ljava/lang/Object;

    .line 958
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->b()Ljava/math/BigDecimal;

    move-result-object v13

    const/4 v14, 0x0

    .line 959
    invoke-static {v13, v6, v6, v8, v14}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v6

    .line 947
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->c()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v12, v5

    invoke-virtual {v1, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "context.getString(getLim\u2026imitSum(), limitCurrency)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1d

    .line 960
    :cond_2a
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_disabled:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "context.getString(R.stri\u2026_details_limits_disabled)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 962
    :goto_1d
    move-object v11, v4

    check-cast v11, Landroid/view/View;

    .line 963
    invoke-virtual {v11, v6}, Landroid/view/View;->setVisibility(I)V

    .line 965
    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {v4, v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 968
    :goto_1e
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 969
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->p(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v4

    .line 971
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    move-object v12, v11

    check-cast v12, Lcom/swedbank/mobile/app/cards/a/t;

    .line 970
    invoke-virtual {v12}, Lcom/swedbank/mobile/app/cards/a/t;->d()Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v12

    sget-object v13, Lcom/swedbank/mobile/business/cards/CardLimitService;->CASH_OUT:Lcom/swedbank/mobile/business/cards/CardLimitService;

    if-ne v12, v13, :cond_2c

    const/4 v12, 0x1

    goto :goto_1f

    :cond_2c
    const/4 v12, 0x0

    :goto_1f
    if-eqz v12, :cond_2b

    goto :goto_20

    :cond_2d
    const/4 v11, 0x0

    .line 972
    :goto_20
    check-cast v11, Lcom/swedbank/mobile/app/cards/a/t;

    if-nez v11, :cond_2e

    const/4 v10, 0x0

    .line 977
    move-object v11, v10

    check-cast v11, Ljava/lang/String;

    .line 978
    move-object v10, v4

    check-cast v10, Landroid/view/View;

    .line 979
    invoke-virtual {v10, v9}, Landroid/view/View;->setVisibility(I)V

    .line 981
    check-cast v11, Ljava/lang/CharSequence;

    invoke-virtual {v4, v11}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_23

    .line 991
    :cond_2e
    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->a()Z

    move-result v10

    if-eqz v10, :cond_2f

    .line 995
    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v10

    sget-object v12, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    invoke-virtual {v10}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->ordinal()I

    move-result v10

    aget v10, v12, v10

    packed-switch v10, :pswitch_data_2

    .line 999
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not renderable card limit validity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 998
    :pswitch_7
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_monthly:I

    goto :goto_21

    .line 997
    :pswitch_8
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_weekly:I

    goto :goto_21

    .line 996
    :pswitch_9
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_daily:I

    .line 1000
    :goto_21
    new-array v12, v8, [Ljava/lang/Object;

    .line 1002
    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->b()Ljava/math/BigDecimal;

    move-result-object v13

    const/4 v14, 0x0

    .line 1003
    invoke-static {v13, v6, v6, v8, v14}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v6

    .line 991
    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->c()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v12, v5

    invoke-virtual {v1, v10, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "context.getString(getLim\u2026imitSum(), limitCurrency)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_22

    .line 1004
    :cond_2f
    sget v10, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_disabled:I

    invoke-virtual {v1, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "context.getString(R.stri\u2026_details_limits_disabled)"

    invoke-static {v10, v11}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1006
    :goto_22
    move-object v11, v4

    check-cast v11, Landroid/view/View;

    .line 1007
    invoke-virtual {v11, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1009
    check-cast v10, Ljava/lang/CharSequence;

    invoke-virtual {v4, v10}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 1012
    :goto_23
    sget-object v4, Lkotlin/s;->a:Lkotlin/s;

    .line 1013
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->q(Lcom/swedbank/mobile/app/cards/a/o;)Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    move-result-object v4

    .line 1015
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_30
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    move-object v11, v10

    check-cast v11, Lcom/swedbank/mobile/app/cards/a/t;

    .line 1014
    invoke-virtual {v11}, Lcom/swedbank/mobile/app/cards/a/t;->d()Lcom/swedbank/mobile/business/cards/CardLimitService;

    move-result-object v11

    sget-object v12, Lcom/swedbank/mobile/business/cards/CardLimitService;->ATM:Lcom/swedbank/mobile/business/cards/CardLimitService;

    if-ne v11, v12, :cond_31

    const/4 v11, 0x1

    goto :goto_24

    :cond_31
    const/4 v11, 0x0

    :goto_24
    if-eqz v11, :cond_30

    goto :goto_25

    :cond_32
    const/4 v10, 0x0

    .line 1016
    :goto_25
    check-cast v10, Lcom/swedbank/mobile/app/cards/a/t;

    if-nez v10, :cond_33

    const/4 v3, 0x0

    .line 1030
    move-object v8, v3

    check-cast v8, Ljava/lang/String;

    .line 1031
    move-object v3, v4

    check-cast v3, Landroid/view/View;

    .line 1032
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 1034
    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v4, v8}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_28

    .line 1044
    :cond_33
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->a()Z

    move-result v3

    if-eqz v3, :cond_34

    .line 1048
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v3

    sget-object v11, Lcom/swedbank/mobile/app/cards/a/p;->b:[I

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/cards/CardLimitValidity;->ordinal()I

    move-result v3

    aget v3, v11, v3

    packed-switch v3, :pswitch_data_3

    .line 1052
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not renderable card limit validity: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->e()Lcom/swedbank/mobile/business/cards/CardLimitValidity;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 1051
    :pswitch_a
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_monthly:I

    goto :goto_26

    .line 1050
    :pswitch_b
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_weekly:I

    goto :goto_26

    .line 1049
    :pswitch_c
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_daily:I

    .line 1053
    :goto_26
    new-array v11, v8, [Ljava/lang/Object;

    .line 1055
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->b()Ljava/math/BigDecimal;

    move-result-object v12

    const/4 v13, 0x0

    .line 1056
    invoke-static {v12, v6, v6, v8, v13}, Lcom/swedbank/mobile/core/ui/r;->a(Ljava/math/BigDecimal;ZZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v11, v6

    .line 1044
    invoke-virtual {v10}, Lcom/swedbank/mobile/app/cards/a/t;->c()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v11, v5

    invoke-virtual {v1, v3, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "context.getString(getLim\u2026imitSum(), limitCurrency)"

    invoke-static {v3, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_27

    .line 1057
    :cond_34
    sget v3, Lcom/swedbank/mobile/app/cards/n$h;->card_details_limits_disabled:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v8, "context.getString(R.stri\u2026_details_limits_disabled)"

    invoke-static {v3, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1059
    :goto_27
    move-object v8, v4

    check-cast v8, Landroid/view/View;

    .line 1060
    invoke-virtual {v8, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1062
    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 1065
    :goto_28
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 1066
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->r(Lcom/swedbank/mobile/app/cards/a/o;)Landroid/widget/TextView;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v7, :cond_35

    const/4 v9, 0x0

    .line 1067
    :cond_35
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2a

    .line 1069
    :cond_36
    invoke-static/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->n(Lcom/swedbank/mobile/app/cards/a/o;)Ljava/util/List;

    move-result-object v3

    .line 1070
    check-cast v3, Ljava/lang/Iterable;

    .line 1071
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {v3, v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 1072
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_29
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_37

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 1073
    check-cast v7, Landroid/view/View;

    .line 1070
    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    sget-object v7, Lkotlin/s;->a:Lkotlin/s;

    invoke-interface {v4, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_29

    .line 1074
    :cond_37
    check-cast v4, Ljava/util/List;

    .line 150
    :goto_2a
    sget-object v3, Lkotlin/s;->a:Lkotlin/s;

    .line 153
    :cond_38
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_39

    .line 154
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->k()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 155
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v5, v4}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 153
    invoke-virtual {v0, v1, v3}, Lcom/swedbank/mobile/app/cards/a/o;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto/16 :goto_2c

    .line 156
    :cond_39
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->l()Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    if-eqz v3, :cond_3a

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->l()Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "context.getString(viewSt\u2026ror.userDisplayedMessage)"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    .line 158
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v5, v4}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 156
    invoke-virtual {v0, v1, v3}, Lcom/swedbank/mobile/app/cards/a/o;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto/16 :goto_2c

    .line 159
    :cond_3a
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->c()Lcom/swedbank/mobile/business/util/e;

    move-result-object v3

    if-eqz v3, :cond_3e

    .line 160
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->c()Lcom/swedbank/mobile/business/util/e;

    move-result-object v3

    .line 162
    sget v4, Lcom/swedbank/mobile/app/cards/n$h;->card_details_general_loading_error:I

    .line 1079
    instance-of v7, v3, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v7, :cond_3c

    check-cast v3, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 1081
    move-object v7, v3

    check-cast v7, Ljava/util/Collection;

    invoke-interface {v7}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    xor-int/2addr v5, v7

    if-eqz v5, :cond_3b

    move-object v7, v3

    check-cast v7, Ljava/lang/Iterable;

    const-string v3, "\n"

    move-object v8, v3

    check-cast v8, Ljava/lang/CharSequence;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3e

    const/4 v15, 0x0

    invoke-static/range {v7 .. v15}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2b

    .line 1082
    :cond_3b
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2b

    .line 1084
    :cond_3c
    instance-of v4, v3, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v4, :cond_3d

    check-cast v3, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Throwable;

    .line 1085
    invoke-static {v3}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2b
    const-string v4, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 1086
    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/CharSequence;

    .line 163
    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    sget v5, Lcom/swedbank/mobile/app/cards/n$h;->card_details_retry_card_query:I

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v4, v1}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;)V

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 159
    invoke-virtual {v0, v3, v4}, Lcom/swedbank/mobile/app/cards/a/o;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_2c

    .line 1085
    :cond_3d
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 164
    :cond_3e
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->e()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3f

    .line 165
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->e()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 166
    new-instance v3, Lcom/swedbank/mobile/core/ui/widget/s$c$c;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v5, v4}, Lcom/swedbank/mobile/core/ui/widget/s$c$c;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast v3, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 164
    invoke-virtual {v0, v1, v3}, Lcom/swedbank/mobile/app/cards/a/o;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_2c

    .line 167
    :cond_3f
    invoke-virtual/range {p0 .. p0}, Lcom/swedbank/mobile/app/cards/a/o;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    .line 170
    :goto_2c
    invoke-virtual/range {p1 .. p1}, Lcom/swedbank/mobile/app/cards/a/s;->a()Lcom/swedbank/mobile/app/cards/a/b;

    move-result-object v1

    if-eqz v1, :cond_40

    .line 171
    iput-boolean v6, v0, Lcom/swedbank/mobile/app/cards/a/o;->y:Z

    :cond_40
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
    .end packed-switch
.end method

.method public a(Lcom/swedbank/mobile/core/ui/widget/t;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/t;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/a/o;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    return-void
.end method

.method public a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/ui/widget/s$c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u$a;->a(Lcom/swedbank/mobile/core/ui/widget/u;Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 36
    check-cast p1, Lcom/swedbank/mobile/app/cards/a/s;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/a/o;->a(Lcom/swedbank/mobile/app/cards/a/s;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->A:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->q()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 82
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->v()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 497
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public d()Lcom/swedbank/mobile/core/ui/widget/t;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/a/o;->b:Lcom/swedbank/mobile/core/ui/widget/t;

    if-nez v0, :cond_0

    const-string v1, "snackbarRenderer"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected e()V
    .locals 5

    .line 67
    invoke-super {p0}, Lcom/swedbank/mobile/architect/a/b/a;->e()V

    .line 494
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/t;

    invoke-virtual {p0}, Lcom/swedbank/mobile/architect/a/b/a;->o()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/t;-><init>(Landroid/view/View;)V

    .line 495
    new-instance v1, Lcom/swedbank/mobile/app/cards/a/o$a;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/cards/a/o$a;-><init>(Lcom/swedbank/mobile/core/ui/widget/t;)V

    check-cast v1, Lkotlin/e/a/a;

    new-instance v2, Lcom/swedbank/mobile/app/cards/a/q;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/cards/a/q;-><init>(Lkotlin/e/a/a;)V

    check-cast v2, Lio/reactivex/c/a;

    invoke-static {v2}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v1

    const-string v2, "Disposables.fromAction(renderer::dismissSnackbar)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    .line 496
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/cards/a/o;->a(Lcom/swedbank/mobile/core/ui/widget/t;)V

    .line 69
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->p()Lcom/google/android/material/appbar/AppBarLayout;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->s()Landroidx/core/widget/NestedScrollView;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroid/view/View;Landroidx/core/widget/NestedScrollView;)V

    .line 70
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->r()Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 71
    invoke-virtual {v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->getProgressViewEndOffset()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float v1, v1, v2

    float-to-int v1, v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->a(ZI)V

    const/4 v1, 0x2

    .line 72
    new-array v1, v1, [I

    sget v3, Lcom/swedbank/mobile/app/cards/n$b;->brand_orange:I

    const/4 v4, 0x0

    aput v3, v1, v4

    sget v3, Lcom/swedbank/mobile/app/cards/n$b;->brand_turqoise:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    return-void
.end method

.method public f()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 84
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->w()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 498
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public g()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 86
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->x()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 499
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public h()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 88
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->z()Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 500
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public i()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 90
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->C()Lcom/swedbank/mobile/core/ui/widget/EnableSettingView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 501
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public j()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 92
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->D()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 502
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public k()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 94
    invoke-direct {p0}, Lcom/swedbank/mobile/app/cards/a/o;->H()Landroid/widget/TextView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 503
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method
