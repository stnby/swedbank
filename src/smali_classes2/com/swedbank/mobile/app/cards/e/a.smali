.class public final Lcom/swedbank/mobile/app/cards/e/a;
.super Lcom/swedbank/mobile/app/f/c$b;
.source "OutdatedShortcutHelpDialogInformation.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/e/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/swedbank/mobile/app/cards/e/a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/e/a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/e/a;->a:Lcom/swedbank/mobile/app/cards/e/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/swedbank/mobile/app/f/c$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 8
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->card_shortcut_outdated_help_title:I

    return v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 10
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->card_shortcut_outdated_help_description:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .line 12
    sget v0, Lcom/swedbank/mobile/app/cards/n$h;->card_shortcut_outdated_help_ack_btn:I

    return v0
.end method

.method public d()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
