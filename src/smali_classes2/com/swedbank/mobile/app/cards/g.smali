.class public final Lcom/swedbank/mobile/app/cards/g;
.super Ljava/lang/Object;
.source "CardTypeRenderer.kt"


# static fields
.field public static final a:Lcom/swedbank/mobile/app/cards/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/swedbank/mobile/app/cards/g;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/cards/g;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/business/cards/g;)I
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    sget-object v0, Lcom/swedbank/mobile/app/cards/h;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/g;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 47
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_gold_business:I

    goto/16 :goto_0

    .line 46
    :pswitch_1
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_gold_business:I

    goto/16 :goto_0

    .line 45
    :pswitch_2
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_business:I

    goto/16 :goto_0

    .line 44
    :pswitch_3
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_business:I

    goto/16 :goto_0

    .line 43
    :pswitch_4
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_business:I

    goto/16 :goto_0

    .line 42
    :pswitch_5
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_business:I

    goto/16 :goto_0

    .line 41
    :pswitch_6
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_business:I

    goto/16 :goto_0

    .line 40
    :pswitch_7
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_fixed:I

    goto/16 :goto_0

    .line 39
    :pswitch_8
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_fixed:I

    goto/16 :goto_0

    .line 38
    :pswitch_9
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_gold:I

    goto/16 :goto_0

    .line 37
    :pswitch_a
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_gold:I

    goto :goto_0

    .line 36
    :pswitch_b
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_revolving:I

    goto :goto_0

    .line 35
    :pswitch_c
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_revolving:I

    goto :goto_0

    .line 34
    :pswitch_d
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_platinum:I

    goto :goto_0

    .line 33
    :pswitch_e
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_platinum:I

    goto :goto_0

    .line 32
    :pswitch_f
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_gold:I

    goto :goto_0

    .line 31
    :pswitch_10
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_gold:I

    goto :goto_0

    .line 30
    :pswitch_11
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_platinum:I

    goto :goto_0

    .line 29
    :pswitch_12
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_platinum:I

    goto :goto_0

    .line 28
    :pswitch_13
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_platinum:I

    goto :goto_0

    .line 27
    :pswitch_14
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_revolving:I

    goto :goto_0

    .line 26
    :pswitch_15
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->credit_revolving:I

    goto :goto_0

    .line 25
    :pswitch_16
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_private:I

    goto :goto_0

    .line 24
    :pswitch_17
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_private:I

    goto :goto_0

    .line 23
    :pswitch_18
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_senior:I

    goto :goto_0

    .line 22
    :pswitch_19
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->premium_debit:I

    goto :goto_0

    .line 21
    :pswitch_1a
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_plus:I

    goto :goto_0

    .line 20
    :pswitch_1b
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_basic:I

    goto :goto_0

    .line 19
    :pswitch_1c
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_youth_web:I

    goto :goto_0

    .line 18
    :pswitch_1d
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_youth_web:I

    goto :goto_0

    .line 17
    :pswitch_1e
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_basic:I

    goto :goto_0

    .line 16
    :pswitch_1f
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_youth:I

    goto :goto_0

    .line 15
    :pswitch_20
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_basic:I

    goto :goto_0

    .line 14
    :pswitch_21
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_youth:I

    goto :goto_0

    .line 13
    :pswitch_22
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_youth:I

    goto :goto_0

    .line 12
    :pswitch_23
    sget p1, Lcom/swedbank/mobile/app/cards/n$d;->debit_basic:I

    :goto_0
    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_23
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Lcom/swedbank/mobile/business/cards/g;)I
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/swedbank/mobile/app/cards/h;->b:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/g;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 54
    sget p1, Lcom/swedbank/mobile/app/cards/n$b;->white:I

    goto :goto_0

    .line 53
    :pswitch_0
    sget p1, Lcom/swedbank/mobile/app/cards/n$b;->brand_brown_text_icon_54:I

    goto :goto_0

    .line 52
    :pswitch_1
    sget p1, Lcom/swedbank/mobile/app/cards/n$b;->premium_debit_foreground:I

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lcom/swedbank/mobile/business/cards/g;)Z
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/swedbank/mobile/app/cards/h;->c:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/g;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x1

    goto :goto_0

    :pswitch_0
    const/4 p1, 0x0

    :goto_0
    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final d(Lcom/swedbank/mobile/business/cards/g;)I
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "type"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/swedbank/mobile/app/cards/h;->d:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/cards/g;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 108
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_822_lt:I

    goto/16 :goto_0

    .line 107
    :pswitch_1
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_822_lt:I

    goto/16 :goto_0

    .line 106
    :pswitch_2
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_821_lt:I

    goto/16 :goto_0

    .line 105
    :pswitch_3
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_821_lt:I

    goto/16 :goto_0

    .line 104
    :pswitch_4
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_109_lt:I

    goto/16 :goto_0

    .line 103
    :pswitch_5
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_109_lt:I

    goto/16 :goto_0

    .line 102
    :pswitch_6
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_109_lt:I

    goto/16 :goto_0

    .line 101
    :pswitch_7
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_820_lt:I

    goto/16 :goto_0

    .line 100
    :pswitch_8
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_820_lt:I

    goto/16 :goto_0

    .line 99
    :pswitch_9
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_818_lt:I

    goto/16 :goto_0

    .line 98
    :pswitch_a
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_818_lt:I

    goto/16 :goto_0

    .line 97
    :pswitch_b
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_817_lt:I

    goto :goto_0

    .line 96
    :pswitch_c
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_817_lt:I

    goto :goto_0

    .line 95
    :pswitch_d
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_816_lt:I

    goto :goto_0

    .line 94
    :pswitch_e
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_815_lt:I

    goto :goto_0

    .line 93
    :pswitch_f
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_806_lt:I

    goto :goto_0

    .line 92
    :pswitch_10
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_806_lt:I

    goto :goto_0

    .line 91
    :pswitch_11
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_823_lt:I

    goto :goto_0

    .line 90
    :pswitch_12
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_823_lt:I

    goto :goto_0

    .line 89
    :pswitch_13
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_802_lt:I

    goto :goto_0

    .line 88
    :pswitch_14
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_53_lt:I

    goto :goto_0

    .line 87
    :pswitch_15
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_52_lt:I

    goto :goto_0

    .line 86
    :pswitch_16
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_110_lt:I

    goto :goto_0

    .line 85
    :pswitch_17
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_110_lt:I

    goto :goto_0

    .line 84
    :pswitch_18
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_108_lt:I

    goto :goto_0

    .line 83
    :pswitch_19
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_901_lt:I

    goto :goto_0

    .line 82
    :pswitch_1a
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_106_lt:I

    goto :goto_0

    .line 81
    :pswitch_1b
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_111_lt:I

    goto :goto_0

    .line 80
    :pswitch_1c
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_105_lt:I

    goto :goto_0

    .line 79
    :pswitch_1d
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_107_lt:I

    goto :goto_0

    .line 78
    :pswitch_1e
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_107_lt:I

    goto :goto_0

    .line 77
    :pswitch_1f
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_100_lt:I

    goto :goto_0

    .line 76
    :pswitch_20
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_59_lt:I

    goto :goto_0

    .line 75
    :pswitch_21
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_55_lt:I

    goto :goto_0

    .line 74
    :pswitch_22
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_54_lt:I

    goto :goto_0

    .line 73
    :pswitch_23
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_50_lt:I

    goto :goto_0

    .line 72
    :pswitch_24
    sget p1, Lcom/swedbank/mobile/app/cards/n$h;->card_unknown:I

    :goto_0
    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
