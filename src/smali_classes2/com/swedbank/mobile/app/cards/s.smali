.class public final Lcom/swedbank/mobile/app/cards/s;
.super Lcom/swedbank/mobile/app/a/d;
.source "WalletCardSelectedForPaymentTracker.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/app/a/d<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/cards/wallet/ad;

.field private final b:Lcom/swedbank/mobile/business/cards/u;

.field private final c:Lcom/swedbank/mobile/architect/business/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/core/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/cards/wallet/ad;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/b;
        .annotation runtime Ljavax/inject/Named;
            value = "trackCardSelectedForPaymentShortcutUsageUseCase"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            "Lcom/swedbank/mobile/business/cards/u;",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;",
            "Lcom/swedbank/mobile/core/a/c;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "walletRepository"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localCardsRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trackCardSelectedForPaymentShortcutUsage"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p4}, Lcom/swedbank/mobile/app/a/d;-><init>(Lcom/swedbank/mobile/core/a/c;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/s;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/s;->b:Lcom/swedbank/mobile/business/cards/u;

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/s;->c:Lcom/swedbank/mobile/architect/business/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/cards/s;)Lcom/swedbank/mobile/architect/business/b;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/cards/s;->c:Lcom/swedbank/mobile/architect/business/b;

    return-object p0
.end method


# virtual methods
.method protected a(Ljava/lang/String;)Lio/reactivex/w;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/w<",
            "Lcom/swedbank/mobile/core/a/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/s;->a:Lcom/swedbank/mobile/business/cards/wallet/ad;

    .line 25
    invoke-interface {v0}, Lcom/swedbank/mobile/business/cards/wallet/ad;->l()Lio/reactivex/w;

    move-result-object v0

    check-cast v0, Lio/reactivex/aa;

    .line 26
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/s;->b:Lcom/swedbank/mobile/business/cards/u;

    .line 27
    invoke-interface {v1, p1}, Lcom/swedbank/mobile/business/cards/u;->g(Ljava/lang/String;)Lio/reactivex/j;

    move-result-object v1

    .line 28
    sget-object v2, Lcom/swedbank/mobile/app/cards/s$b;->a:Lcom/swedbank/mobile/app/cards/s$b;

    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/j;->d(Lio/reactivex/c/h;)Lio/reactivex/j;

    move-result-object v1

    .line 29
    sget-object v2, Lcom/swedbank/mobile/business/util/a;->a:Lcom/swedbank/mobile/business/util/a;

    invoke-virtual {v1, v2}, Lio/reactivex/j;->d(Ljava/lang/Object;)Lio/reactivex/w;

    move-result-object v1

    const-string v2, "localCardsRepository\n   \u2026        .toSingle(Absent)"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/aa;

    .line 38
    new-instance v2, Lcom/swedbank/mobile/app/cards/s$a;

    invoke-direct {v2, p0, p1}, Lcom/swedbank/mobile/app/cards/s$a;-><init>(Lcom/swedbank/mobile/app/cards/s;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/c/c;

    invoke-static {v0, v1, v2}, Lio/reactivex/w;->a(Lio/reactivex/aa;Lio/reactivex/aa;Lio/reactivex/c/c;)Lio/reactivex/w;

    move-result-object p1

    const-string v0, "Single.zip(s1, s2, BiFun\u2026-> zipper.invoke(t, u) })"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic b(Ljava/lang/Object;)Lio/reactivex/w;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/s;->a(Ljava/lang/String;)Lio/reactivex/w;

    move-result-object p1

    return-object p1
.end method
