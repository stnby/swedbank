.class public final Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "CardLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:F

.field final synthetic f:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/CardLoadingView$1;FFFFILandroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFFFI",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 39
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iput p2, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->b:F

    iput p3, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->c:F

    iput p4, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->d:F

    iput p5, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    iput p6, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->f:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1e

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p7

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 11
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "canvas"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPaint"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iget-object v0, v0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/CardLoadingView;->getPaddingTop()I

    move-result v0

    sub-int v0, p2, v0

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iget-object v1, v1, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    invoke-virtual {v1}, Lcom/swedbank/mobile/app/cards/CardLoadingView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const v1, 0x3f27ef9e    # 0.656f

    mul-float v1, v1, v0

    .line 45
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iget-object v3, v3, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/cards/CardLoadingView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iget-object v4, v4, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    invoke-virtual {v4}, Lcom/swedbank/mobile/app/cards/CardLoadingView;->getPaddingTop()I

    move-result v4

    int-to-float v4, v4

    int-to-float p1, p1

    iget-object v5, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iget-object v5, v5, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    invoke-virtual {v5}, Lcom/swedbank/mobile/app/cards/CardLoadingView;->getPaddingRight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr p1, v5

    invoke-direct {v2, v3, v4, p1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 46
    invoke-virtual {p3, v2, p4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 48
    iget p1, v2, Landroid/graphics/RectF;->left:F

    iget v3, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->b:F

    add-float/2addr p1, v3

    .line 49
    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->b:F

    sub-float/2addr v3, v4

    .line 50
    iget v4, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->c:F

    const v5, 0x3f19999a    # 0.6f

    mul-float v4, v4, v5

    .line 51
    iget v5, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->d:F

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float v7, v3, p1

    const/high16 v8, 0x3f000000    # 0.5f

    mul-float v8, v8, v7

    .line 55
    iget v9, v2, Landroid/graphics/RectF;->bottom:F

    iget v10, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v10

    iget v10, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->c:F

    sub-float/2addr v0, v10

    sub-float/2addr v0, v4

    sub-float/2addr v0, v5

    div-float/2addr v0, v6

    add-float/2addr v9, v0

    add-float/2addr v8, p1

    .line 56
    iget v0, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->c:F

    add-float/2addr v0, v9

    invoke-virtual {v2, p1, v9, v8, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 57
    iget v0, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    iget v8, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    invoke-virtual {p3, v2, v0, v8, p4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    const v0, 0x3e8f5c29    # 0.28f

    mul-float v7, v7, v0

    .line 61
    iget v0, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v5

    add-float/2addr v7, p1

    add-float/2addr v4, v0

    .line 62
    invoke-virtual {v2, p1, v0, v7, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 63
    iget p1, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    iget v0, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    invoke-virtual {p3, v2, p1, v0, p4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    int-to-float p1, p2

    sub-float/2addr p1, v1

    .line 66
    iget-object p2, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView$1;

    iget-object p2, p2, Lcom/swedbank/mobile/app/cards/CardLoadingView$1;->a:Lcom/swedbank/mobile/app/cards/CardLoadingView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/cards/CardLoadingView;->getPaddingBottom()I

    move-result p2

    int-to-float p2, p2

    sub-float/2addr p1, p2

    iget p2, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->f:I

    int-to-float p2, p2

    mul-float p2, p2, v6

    sub-float/2addr p1, p2

    .line 67
    iget p2, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->f:I

    int-to-float p2, p2

    add-float/2addr v1, p2

    sub-float p2, v3, p1

    add-float/2addr p1, v1

    .line 68
    invoke-virtual {v2, p2, v1, v3, p1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 69
    iget p1, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    iget p2, p0, Lcom/swedbank/mobile/app/cards/CardLoadingView$1$1;->e:F

    invoke-virtual {p3, v2, p1, p2, p4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method
