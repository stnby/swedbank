.class public final Lcom/swedbank/mobile/app/cards/d;
.super Ljava/lang/Object;
.source "CardOverview.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/business/cards/a;)Lcom/swedbank/mobile/app/cards/c;
    .locals 15
    .param p0    # Lcom/swedbank/mobile/business/cards/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toOverviewViewModel"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->a()Ljava/lang/String;

    move-result-object v2

    .line 46
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->d()Ljava/lang/String;

    move-result-object v3

    .line 47
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->h()Ljava/lang/String;

    move-result-object v4

    .line 90
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v0

    sget-object v1, Lcom/swedbank/mobile/app/cards/e;->a:[I

    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/CardState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v5, 0x0

    packed-switch v0, :pswitch_data_0

    .line 100
    sget-object v0, Lcom/swedbank/mobile/app/cards/f;->a:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 99
    :pswitch_0
    sget-object v0, Lcom/swedbank/mobile/app/cards/f;->e:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 98
    :pswitch_1
    sget-object v0, Lcom/swedbank/mobile/app/cards/f;->d:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 93
    :pswitch_2
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v0

    .line 94
    instance-of v6, v0, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v6, :cond_0

    .line 95
    check-cast v0, Lcom/swedbank/mobile/business/cards/s$a;

    .line 92
    invoke-virtual {v0}, Lcom/swedbank/mobile/business/cards/s$a;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v5

    .line 94
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v0, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    sget-object v0, Lcom/swedbank/mobile/app/cards/f;->c:Lcom/swedbank/mobile/app/cards/f;

    goto :goto_1

    .line 97
    :cond_1
    sget-object v0, Lcom/swedbank/mobile/app/cards/f;->b:Lcom/swedbank/mobile/app/cards/f;

    .line 49
    :goto_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->f()Lcom/swedbank/mobile/business/cards/CardState;

    move-result-object v6

    .line 50
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->i()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v7

    .line 51
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->j()Z

    move-result v8

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->r()Z

    move-result v9

    .line 53
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->m()Lcom/swedbank/mobile/business/cards/DigitizationProgression;

    move-result-object v10

    .line 54
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->t()Z

    move-result v11

    .line 55
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->o()Ljava/lang/String;

    move-result-object v12

    .line 102
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object v13

    .line 103
    instance-of v14, v13, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v14, :cond_2

    .line 104
    check-cast v13, Lcom/swedbank/mobile/business/cards/s$a;

    .line 56
    invoke-virtual {v13}, Lcom/swedbank/mobile/business/cards/s$a;->b()Lcom/swedbank/mobile/business/cards/t;

    move-result-object v13

    goto :goto_2

    :cond_2
    move-object v13, v5

    :goto_2
    if-eqz v13, :cond_3

    goto :goto_3

    .line 57
    :cond_3
    sget-object v13, Lcom/swedbank/mobile/business/cards/t;->h:Lcom/swedbank/mobile/business/cards/t;

    .line 106
    :goto_3
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/a;->p()Lcom/swedbank/mobile/business/cards/s;

    move-result-object p0

    .line 107
    instance-of v14, p0, Lcom/swedbank/mobile/business/cards/s$a;

    if-eqz v14, :cond_6

    .line 108
    check-cast p0, Lcom/swedbank/mobile/business/cards/s$a;

    .line 58
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/s$a;->d()Lcom/swedbank/mobile/business/cards/x;

    move-result-object p0

    .line 110
    sget-object v5, Lcom/swedbank/mobile/business/cards/x$b;->a:Lcom/swedbank/mobile/business/cards/x$b;

    invoke-static {p0, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance p0, Lcom/swedbank/mobile/app/cards/o;

    const/4 v1, 0x0

    invoke-direct {p0, v1, v1, v1}, Lcom/swedbank/mobile/app/cards/o;-><init>(IZZ)V

    goto :goto_4

    .line 114
    :cond_4
    instance-of v5, p0, Lcom/swedbank/mobile/business/cards/x$a;

    if-eqz v5, :cond_5

    new-instance v5, Lcom/swedbank/mobile/app/cards/o;

    .line 115
    check-cast p0, Lcom/swedbank/mobile/business/cards/x$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/x$a;->b()I

    move-result v14

    .line 116
    invoke-virtual {p0}, Lcom/swedbank/mobile/business/cards/x$a;->a()Z

    move-result p0

    .line 114
    invoke-direct {v5, v14, p0, v1}, Lcom/swedbank/mobile/app/cards/o;-><init>(IZZ)V

    move-object p0, v5

    :goto_4
    move-object v14, p0

    goto :goto_5

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_6
    move-object v14, v5

    .line 44
    :goto_5
    new-instance p0, Lcom/swedbank/mobile/app/cards/c;

    move-object v1, p0

    move-object v5, v0

    invoke-direct/range {v1 .. v14}, Lcom/swedbank/mobile/app/cards/c;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/cards/f;Lcom/swedbank/mobile/business/cards/CardState;Lcom/swedbank/mobile/business/cards/g;ZZLcom/swedbank/mobile/business/cards/DigitizationProgression;ZLjava/lang/String;Lcom/swedbank/mobile/business/cards/t;Lcom/swedbank/mobile/app/cards/o;)V

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
