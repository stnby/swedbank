.class public final Lcom/swedbank/mobile/app/cards/l$a;
.super Lkotlin/e/b/k;
.source "CardsRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/cards/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/l;

.field final synthetic b:Z

.field final synthetic c:Lio/reactivex/x;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/cards/l;ZLio/reactivex/x;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/l$a;->a:Lcom/swedbank/mobile/app/cards/l;

    iput-boolean p2, p0, Lcom/swedbank/mobile/app/cards/l$a;->b:Z

    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/l$a;->c:Lio/reactivex/x;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 3
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$withInspectingChildren"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/cards/l$a;->b:Z

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/l$a;->a:Lcom/swedbank/mobile/app/cards/l;

    sget-object v1, Lcom/swedbank/mobile/app/cards/l$a$1;->a:Lcom/swedbank/mobile/app/cards/l$a$1;

    check-cast v1, Lkotlin/e/a/b;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/app/cards/l;->b(Lcom/swedbank/mobile/app/cards/l;Lkotlin/e/a/b;)V

    .line 51
    :cond_0
    check-cast p1, Ljava/lang/Iterable;

    .line 86
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/architect/a/h;

    .line 51
    instance-of v1, v1, Lcom/swedbank/mobile/business/cards/list/k;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 88
    :goto_0
    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    if-nez v0, :cond_4

    .line 55
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/l$a;->a:Lcom/swedbank/mobile/app/cards/l;

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/l$a;->a:Lcom/swedbank/mobile/app/cards/l;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/l;->a(Lcom/swedbank/mobile/app/cards/l;)Lcom/swedbank/mobile/app/cards/list/b;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/list/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/l$a;->c:Lio/reactivex/x;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    .line 89
    :cond_3
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    goto :goto_1

    .line 57
    :cond_4
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/l$a;->c:Lio/reactivex/x;

    if-eqz p1, :cond_5

    invoke-virtual {v0}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v0

    invoke-interface {p1, v0}, Lio/reactivex/x;->a(Ljava/lang/Object;)V

    :cond_5
    :goto_1
    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/l$a;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
