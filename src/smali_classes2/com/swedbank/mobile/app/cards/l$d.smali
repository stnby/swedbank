.class final Lcom/swedbank/mobile/app/cards/l$d;
.super Lkotlin/e/b/k;
.source "CardsRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/cards/l;->a(Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Collection<",
        "+",
        "Lcom/swedbank/mobile/architect/a/h;",
        ">;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/cards/l;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/cards/l;Ljava/util/List;Z)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/l$d;->a:Lcom/swedbank/mobile/app/cards/l;

    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/l$d;->b:Ljava/util/List;

    iput-boolean p3, p0, Lcom/swedbank/mobile/app/cards/l$d;->c:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 5
    .param p1    # Ljava/util/Collection;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    check-cast p1, Ljava/lang/Iterable;

    .line 91
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 92
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/architect/a/h;

    .line 90
    instance-of v4, v4, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;

    if-eqz v4, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_7

    .line 100
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    .line 101
    :cond_3
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/architect/a/h;

    .line 99
    instance-of v0, v0, Lcom/swedbank/mobile/business/cards/wallet/onboarding/j;

    if-eqz v0, :cond_4

    const/4 v3, 0x1

    :cond_5
    :goto_1
    if-nez v3, :cond_6

    .line 72
    iget-object p1, p0, Lcom/swedbank/mobile/app/cards/l$d;->a:Lcom/swedbank/mobile/app/cards/l;

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/app/cards/l$d;->a:Lcom/swedbank/mobile/app/cards/l;

    invoke-static {v0}, Lcom/swedbank/mobile/app/cards/l;->b(Lcom/swedbank/mobile/app/cards/l;)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/l$d;->b:Ljava/util/List;

    .line 71
    iget-boolean v2, p0, Lcom/swedbank/mobile/app/cards/l$d;->c:Z

    .line 69
    invoke-virtual {v0, v1, v2}, Lcom/swedbank/mobile/app/cards/f/a/b;->a(Ljava/util/List;Z)Lcom/swedbank/mobile/app/cards/f/a/b;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/cards/f/a/b;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    .line 104
    invoke-static {p1, v0}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    :cond_6
    return-void

    .line 86
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "There was supposed to be no children of type "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v0, Lcom/swedbank/mobile/business/cards/wallet/preconditions/e;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 95
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/cards/l$d;->a(Ljava/util/Collection;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
