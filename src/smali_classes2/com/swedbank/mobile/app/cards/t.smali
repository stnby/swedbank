.class public final Lcom/swedbank/mobile/app/cards/t;
.super Ljava/lang/Object;
.source "WalletCardSelectedForPaymentTracker_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/cards/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/a/c;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/swedbank/mobile/app/cards/t;->a:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/swedbank/mobile/app/cards/t;->b:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/swedbank/mobile/app/cards/t;->c:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/swedbank/mobile/app/cards/t;->d:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/cards/t;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/wallet/ad;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/cards/u;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/b<",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/core/a/c;",
            ">;)",
            "Lcom/swedbank/mobile/app/cards/t;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/swedbank/mobile/app/cards/t;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/cards/t;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/cards/s;
    .locals 5

    .line 34
    new-instance v0, Lcom/swedbank/mobile/app/cards/s;

    iget-object v1, p0, Lcom/swedbank/mobile/app/cards/t;->a:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/business/cards/wallet/ad;

    iget-object v2, p0, Lcom/swedbank/mobile/app/cards/t;->b:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/business/cards/u;

    iget-object v3, p0, Lcom/swedbank/mobile/app/cards/t;->c:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/architect/business/b;

    iget-object v4, p0, Lcom/swedbank/mobile/app/cards/t;->d:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/core/a/c;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/cards/s;-><init>(Lcom/swedbank/mobile/business/cards/wallet/ad;Lcom/swedbank/mobile/business/cards/u;Lcom/swedbank/mobile/architect/business/b;Lcom/swedbank/mobile/core/a/c;)V

    return-object v0
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/cards/t;->a()Lcom/swedbank/mobile/app/cards/s;

    move-result-object v0

    return-object v0
.end method
