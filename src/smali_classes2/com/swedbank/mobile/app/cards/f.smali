.class public final enum Lcom/swedbank/mobile/app/cards/f;
.super Ljava/lang/Enum;
.source "CardOverview.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/app/cards/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/app/cards/f;

.field public static final enum b:Lcom/swedbank/mobile/app/cards/f;

.field public static final enum c:Lcom/swedbank/mobile/app/cards/f;

.field public static final enum d:Lcom/swedbank/mobile/app/cards/f;

.field public static final enum e:Lcom/swedbank/mobile/app/cards/f;

.field private static final synthetic f:[Lcom/swedbank/mobile/app/cards/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/swedbank/mobile/app/cards/f;

    new-instance v1, Lcom/swedbank/mobile/app/cards/f;

    const-string v2, "UNKNOWN"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/cards/f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/cards/f;->a:Lcom/swedbank/mobile/app/cards/f;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/cards/f;

    const-string v2, "ACTIVE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/cards/f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/cards/f;->b:Lcom/swedbank/mobile/app/cards/f;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/cards/f;

    const-string v2, "DEFAULT_FOR_MOBILE_CONTACTLESS"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/cards/f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/cards/f;->c:Lcom/swedbank/mobile/app/cards/f;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/cards/f;

    const-string v2, "BLOCKED"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/cards/f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/cards/f;->d:Lcom/swedbank/mobile/app/cards/f;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/cards/f;

    const-string v2, "NOT_ACTIVATED"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/cards/f;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/cards/f;->e:Lcom/swedbank/mobile/app/cards/f;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/app/cards/f;->f:[Lcom/swedbank/mobile/app/cards/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/app/cards/f;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/cards/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/app/cards/f;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/app/cards/f;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/app/cards/f;->f:[Lcom/swedbank/mobile/app/cards/f;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/app/cards/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/app/cards/f;

    return-object v0
.end method
