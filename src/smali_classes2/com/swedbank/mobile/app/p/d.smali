.class public final Lcom/swedbank/mobile/app/p/d;
.super Landroidx/recyclerview/widget/RecyclerView$x;
.source "PreferencesListAdapter.kt"


# instance fields
.field private final a:Lio/reactivex/b/b;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$x;-><init>(Landroid/view/View;)V

    .line 101
    new-instance p1, Lio/reactivex/b/b;

    invoke-direct {p1}, Lio/reactivex/b/b;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/d;->a:Lio/reactivex/b/b;

    return-void
.end method

.method private final b(Ljava/lang/Object;Landroid/view/View;Lkotlin/e/a/m;)Ljava/lang/Object;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Landroid/view/View;",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/view/View;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 113
    instance-of v0, p1, Lcom/swedbank/mobile/app/p/b$b;

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    move-object v0, p2

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;

    .line 114
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 115
    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/app/p/b$b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$b;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setTitle(Ljava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$b;->c()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setTitleColor(Ljava/lang/Integer;)V

    .line 117
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$b;->d()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_0

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setContent(Ljava/lang/CharSequence;)V

    .line 118
    :cond_0
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$b;->e()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_1

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/InformationSettingView;->setDescription(Ljava/lang/CharSequence;)V

    .line 119
    :cond_1
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, p2, v0}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/d;->a:Lio/reactivex/b/b;

    new-instance v1, Lcom/swedbank/mobile/app/p/d$a;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/swedbank/mobile/app/p/d$a;-><init>(Lcom/swedbank/mobile/app/p/d;Ljava/lang/Object;Lkotlin/e/a/m;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto/16 :goto_4

    .line 113
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.core.ui.widget.InformationSettingView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 122
    :cond_3
    instance-of v0, p1, Lcom/swedbank/mobile/app/p/b$c;

    if-eqz v0, :cond_6

    if-eqz p2, :cond_5

    move-object v0, p2

    check-cast v0, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;

    .line 123
    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 124
    move-object v2, p1

    check-cast v2, Lcom/swedbank/mobile/app/p/b$c;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$c;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->a(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$c;->c()Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_4

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;->b(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v1, p0, Lcom/swedbank/mobile/app/p/d;->a:Lio/reactivex/b/b;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$c;->d()Lio/reactivex/o;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 129
    new-instance v6, Lcom/swedbank/mobile/app/p/d$c;

    invoke-direct {v6, v0}, Lcom/swedbank/mobile/app/p/d$c;-><init>(Lcom/swedbank/mobile/core/ui/widget/OnOffSettingView;)V

    check-cast v6, Lkotlin/e/a/b;

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/swedbank/mobile/business/util/u;->a(Lio/reactivex/o;Lkotlin/e/a/b;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)Lio/reactivex/b/c;

    move-result-object v0

    .line 127
    invoke-virtual {v1, v0}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    .line 130
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/b$c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, p2, v0}, Lkotlin/e/a/m;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/d;->a:Lio/reactivex/b/b;

    new-instance v1, Lcom/swedbank/mobile/app/p/d$b;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/swedbank/mobile/app/p/d$b;-><init>(Lcom/swedbank/mobile/app/p/d;Ljava/lang/Object;Lkotlin/e/a/m;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/b/b;->a(Lio/reactivex/b/c;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    goto/16 :goto_4

    .line 122
    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.swedbank.mobile.core.ui.widget.OnOffSettingView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 133
    :cond_6
    instance-of v0, p1, Lcom/swedbank/mobile/app/p/b$d;

    if-eqz v0, :cond_8

    const p3, 0x7f0a0164

    .line 163
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_7

    check-cast p2, Landroid/widget/TextView;

    .line 134
    invoke-virtual {p2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    check-cast p1, Lcom/swedbank/mobile/app/p/b$d;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/p/b$d;->a()I

    move-result p1

    invoke-virtual {p3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    :cond_7
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    goto/16 :goto_4

    .line 136
    :cond_8
    instance-of v0, p1, Lcom/swedbank/mobile/app/p/b$a;

    if-eqz v0, :cond_f

    const v0, 0x7f0a004f

    .line 165
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_d

    check-cast v0, Landroid/view/ViewGroup;

    .line 138
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 140
    move-object v3, p1

    check-cast v3, Lcom/swedbank/mobile/app/p/b$a;

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/p/b$a;->a()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 166
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/swedbank/mobile/app/p/b;

    .line 142
    invoke-static {v4}, Lcom/swedbank/mobile/app/p/h;->a(Lcom/swedbank/mobile/app/p/b;)I

    move-result v5

    if-eq v5, v2, :cond_c

    const/4 v6, 0x5

    if-eq v5, v6, :cond_b

    const/16 v6, 0xa

    if-eq v5, v6, :cond_a

    const/16 v6, 0xf

    if-ne v5, v6, :cond_9

    const v5, 0x7f0d0032

    goto :goto_2

    .line 172
    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "No view holder for viewType="

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_a
    const v5, 0x7f0d0040

    goto :goto_2

    :cond_b
    const v5, 0x7f0d0041

    goto :goto_2

    :cond_c
    const v5, 0x7f0d003d

    .line 177
    :goto_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 176
    invoke-virtual {v6, v5, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    const-string v6, "LayoutInflater\n      .fr\u2026youtResId, parent, false)"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 145
    invoke-direct {p0, v4, v5, p3}, Lcom/swedbank/mobile/app/p/d;->b(Ljava/lang/Object;Landroid/view/View;Lkotlin/e/a/m;)Ljava/lang/Object;

    goto :goto_1

    :cond_d
    const p3, 0x7f0a0050

    .line 152
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string p3, "itemView\n          .find\u2026reference_disabled_group)"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/swedbank/mobile/app/p/b$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/p/b$a;->b()Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_e

    goto :goto_3

    :cond_e
    const/16 v1, 0x8

    .line 180
    :goto_3
    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    :goto_4
    return-object p1

    .line 155
    :cond_f
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No rendering defined for "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/d;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/d;->a:Lio/reactivex/b/b;

    invoke-virtual {v0}, Lio/reactivex/b/b;->c()V

    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;Lkotlin/e/a/m;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lkotlin/e/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Landroid/view/View;",
            "Lkotlin/e/a/m<",
            "-",
            "Landroid/view/View;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemView"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setClickListener"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/d;->a()V

    .line 105
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/p/d;->b(Ljava/lang/Object;Landroid/view/View;Lkotlin/e/a/m;)Ljava/lang/Object;

    return-void
.end method
