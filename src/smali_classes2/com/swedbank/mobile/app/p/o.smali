.class public final Lcom/swedbank/mobile/app/p/o;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "PreferencesViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/p/n;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lcom/swedbank/mobile/app/p/g;

.field private final e:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/p/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/p/o;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "preferencesList"

    const-string v4, "getPreferencesList()Landroidx/recyclerview/widget/RecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/p/o;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/o;->e:Lio/reactivex/o;

    const p1, 0x7f0a02dd

    .line 27
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/o;->b:Lkotlin/f/c;

    const p1, 0x7f0a0260

    .line 28
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/o;->c:Lkotlin/f/c;

    .line 30
    new-instance p1, Lcom/swedbank/mobile/app/p/g;

    invoke-direct {p1}, Lcom/swedbank/mobile/app/p/g;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/o;->d:Lcom/swedbank/mobile/app/p/g;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/p/o;)Landroid/content/Context;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/o;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/p/o;)Lcom/swedbank/mobile/app/p/g;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/o;->d:Lcom/swedbank/mobile/app/p/g;

    return-object p0
.end method

.method private final d()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/p/o;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/p/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method private final f()Landroidx/recyclerview/widget/RecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/p/o;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/p/o;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/o;->e:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/o;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/p/q;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/p/q;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/p/q;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v1, p0, Lcom/swedbank/mobile/app/p/o;->d:Lcom/swedbank/mobile/app/p/g;

    .line 62
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/p/q;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    .line 60
    invoke-virtual {v1, v0, p1}, Lcom/swedbank/mobile/app/p/g;->a(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/app/p/q;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/p/o;->a(Lcom/swedbank/mobile/app/p/q;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/o;->d:Lcom/swedbank/mobile/app/p/g;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/p/g;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 54
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/o;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 83
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    .line 55
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/o;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v3, v2}, Lcom/b/b/d/d;->a(Landroid/view/View;Lkotlin/e/a/a;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    .line 53
    invoke-static {v1, v0}, Lcom/swedbank/mobile/app/p/s;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    .line 56
    new-instance v1, Lcom/swedbank/mobile/app/p/o$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/p/o$a;-><init>(Lcom/swedbank/mobile/app/p/o;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "observeSecretSequenceEnt\u2026st.LENGTH_SHORT).show() }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method protected e()V
    .locals 10

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/o;->d()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/o;->f()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 34
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/o;->f()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    .line 67
    invoke-static {p0}, Lcom/swedbank/mobile/app/p/o;->a(Lcom/swedbank/mobile/app/p/o;)Landroid/content/Context;

    move-result-object v2

    .line 68
    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 69
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 70
    new-instance v8, Lcom/swedbank/mobile/core/ui/widget/f;

    new-instance v1, Lcom/swedbank/mobile/core/ui/widget/g$c;

    const/4 v9, 0x1

    new-array v3, v9, [I

    const/4 v4, 0x0

    const/16 v5, 0xa

    aput v5, v3, v4

    invoke-direct {v1, v3}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v5, v1

    check-cast v5, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v3, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast v8, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v0, v8}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 73
    invoke-virtual {v0, v9}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 74
    invoke-static {p0}, Lcom/swedbank/mobile/app/p/o;->b(Lcom/swedbank/mobile/app/p/o;)Lcom/swedbank/mobile/app/p/g;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 75
    new-instance v1, Lcom/swedbank/mobile/app/p/o$b;

    invoke-direct {v1, p0, v0}, Lcom/swedbank/mobile/app/p/o$b;-><init>(Lcom/swedbank/mobile/app/p/o;Landroidx/recyclerview/widget/RecyclerView;)V

    check-cast v1, Lio/reactivex/c/a;

    invoke-static {v1}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.fromAction {\u2026easeResources(list)\n    }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/architect/a/b/a;->b(Lio/reactivex/b/c;)V

    return-void
.end method
