.class final Lcom/swedbank/mobile/app/p/i$g;
.super Ljava/lang/Object;
.source "PreferencesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/p/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/p/i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/p/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/i$g;->a:Lcom/swedbank/mobile/app/p/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lkotlin/k;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/p/i$g;->a(Lkotlin/k;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lkotlin/k;)Ljava/util/List;
    .locals 12
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "Ljava/lang/Boolean;",
            "+",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/p/b;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    const-string v1, "preferences"

    .line 64
    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 65
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 113
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 114
    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/app/p/c;

    .line 65
    invoke-virtual {v3}, Lcom/swedbank/mobile/app/p/c;->c()Lcom/swedbank/mobile/app/p/a;

    move-result-object v3

    .line 116
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 115
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 119
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 123
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 133
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 134
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/swedbank/mobile/app/p/a;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 67
    sget-object v4, Lcom/swedbank/mobile/app/p/j;->a:[I

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/p/a;->ordinal()I

    move-result v5

    aget v4, v4, v5

    const v5, 0x7f1101e6

    const v6, 0x7f1101e5

    const v7, 0x7f1101e4

    const v8, 0x7f1101e7

    const/4 v9, 0x1

    if-eq v4, v9, :cond_2

    .line 75
    new-instance v4, Ljava/util/ArrayList;

    const/4 v9, 0x4

    invoke-direct {v4, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 76
    iget-object v9, p0, Lcom/swedbank/mobile/app/p/i$g;->a:Lcom/swedbank/mobile/app/p/i;

    .line 146
    sget-object v9, Lcom/swedbank/mobile/app/p/j;->b:[I

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/p/a;->ordinal()I

    move-result v3

    aget v3, v9, v3

    packed-switch v3, :pswitch_data_0

    .line 150
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const v5, 0x7f1101e5

    goto :goto_2

    :pswitch_1
    const v5, 0x7f1101e4

    goto :goto_2

    :pswitch_2
    const v5, 0x7f1101e7

    .line 76
    :goto_2
    :pswitch_3
    new-instance v3, Lcom/swedbank/mobile/app/p/b$d;

    invoke-direct {v3, v5}, Lcom/swedbank/mobile/app/p/b$d;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v3, p0, Lcom/swedbank/mobile/app/p/i$g;->a:Lcom/swedbank/mobile/app/p/i;

    .line 152
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v2

    .line 156
    new-instance v3, Lcom/swedbank/mobile/app/p/i$i;

    invoke-direct {v3}, Lcom/swedbank/mobile/app/p/i$i;-><init>()V

    check-cast v3, Ljava/util/Comparator;

    invoke-static {v2, v3}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object v2

    .line 154
    sget-object v3, Lcom/swedbank/mobile/app/p/i$n;->a:Lcom/swedbank/mobile/app/p/i$n;

    check-cast v3, Lkotlin/e/a/b;

    invoke-static {v2, v3}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v2

    .line 79
    move-object v3, v4

    check-cast v3, Ljava/util/Collection;

    invoke-static {v2, v3}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Collection;)Ljava/util/Collection;

    .line 75
    check-cast v4, Ljava/util/List;

    goto :goto_4

    :cond_2
    const/4 v4, 0x2

    .line 68
    new-array v4, v4, [Lcom/swedbank/mobile/app/p/b;

    const/4 v10, 0x0

    .line 69
    iget-object v11, p0, Lcom/swedbank/mobile/app/p/i$g;->a:Lcom/swedbank/mobile/app/p/i;

    .line 135
    sget-object v11, Lcom/swedbank/mobile/app/p/j;->b:[I

    invoke-virtual {v3}, Lcom/swedbank/mobile/app/p/a;->ordinal()I

    move-result v3

    aget v3, v11, v3

    packed-switch v3, :pswitch_data_1

    .line 139
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_4
    const v5, 0x7f1101e5

    goto :goto_3

    :pswitch_5
    const v5, 0x7f1101e4

    goto :goto_3

    :pswitch_6
    const v5, 0x7f1101e7

    .line 69
    :goto_3
    :pswitch_7
    new-instance v3, Lcom/swedbank/mobile/app/p/b$d;

    invoke-direct {v3, v5}, Lcom/swedbank/mobile/app/p/b$d;-><init>(I)V

    check-cast v3, Lcom/swedbank/mobile/app/p/b;

    aput-object v3, v4, v10

    .line 71
    iget-object v3, p0, Lcom/swedbank/mobile/app/p/i$g;->a:Lcom/swedbank/mobile/app/p/i;

    .line 141
    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/a/h;->h(Ljava/lang/Iterable;)Lkotlin/i/e;

    move-result-object v2

    .line 145
    new-instance v3, Lcom/swedbank/mobile/app/p/i$h;

    invoke-direct {v3}, Lcom/swedbank/mobile/app/p/i$h;-><init>()V

    check-cast v3, Ljava/util/Comparator;

    invoke-static {v2, v3}, Lkotlin/i/f;->a(Lkotlin/i/e;Ljava/util/Comparator;)Lkotlin/i/e;

    move-result-object v2

    .line 143
    sget-object v3, Lcom/swedbank/mobile/app/p/i$n;->a:Lcom/swedbank/mobile/app/p/i$n;

    check-cast v3, Lkotlin/e/a/b;

    invoke-static {v2, v3}, Lkotlin/i/f;->c(Lkotlin/i/e;Lkotlin/e/a/b;)Lkotlin/i/e;

    move-result-object v2

    .line 73
    invoke-static {v2}, Lkotlin/i/f;->b(Lkotlin/i/e;)Ljava/util/List;

    move-result-object v2

    .line 70
    new-instance v3, Lcom/swedbank/mobile/app/p/b$a;

    invoke-direct {v3, v2, v0}, Lcom/swedbank/mobile/app/p/b$a;-><init>(Ljava/util/List;Z)V

    check-cast v3, Lcom/swedbank/mobile/app/p/b;

    aput-object v3, v4, v9

    .line 68
    invoke-static {v4}, Lkotlin/a/h;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 81
    :goto_4
    check-cast v4, Ljava/lang/Iterable;

    .line 157
    invoke-static {p1, v4}, Lkotlin/a/h;->a(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto/16 :goto_1

    .line 159
    :cond_3
    check-cast p1, Ljava/util/List;

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_7
    .end packed-switch
.end method
