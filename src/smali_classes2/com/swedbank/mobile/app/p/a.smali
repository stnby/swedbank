.class public final enum Lcom/swedbank/mobile/app/p/a;
.super Ljava/lang/Enum;
.source "PreferencePlugins.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/swedbank/mobile/app/p/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/swedbank/mobile/app/p/a;

.field public static final enum b:Lcom/swedbank/mobile/app/p/a;

.field public static final enum c:Lcom/swedbank/mobile/app/p/a;

.field public static final enum d:Lcom/swedbank/mobile/app/p/a;

.field private static final synthetic e:[Lcom/swedbank/mobile/app/p/a;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/swedbank/mobile/app/p/a;

    new-instance v1, Lcom/swedbank/mobile/app/p/a;

    const-string v2, "SECURITY"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/p/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/p/a;->a:Lcom/swedbank/mobile/app/p/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/p/a;

    const-string v2, "APP_SERVICES"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/p/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/p/a;->b:Lcom/swedbank/mobile/app/p/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/p/a;

    const-string v2, "INTERFACE"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/p/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/p/a;->c:Lcom/swedbank/mobile/app/p/a;

    aput-object v1, v0, v3

    new-instance v1, Lcom/swedbank/mobile/app/p/a;

    const-string v2, "OTHER"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/swedbank/mobile/app/p/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/swedbank/mobile/app/p/a;->d:Lcom/swedbank/mobile/app/p/a;

    aput-object v1, v0, v3

    sput-object v0, Lcom/swedbank/mobile/app/p/a;->e:[Lcom/swedbank/mobile/app/p/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/swedbank/mobile/app/p/a;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/p/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/swedbank/mobile/app/p/a;

    return-object p0
.end method

.method public static values()[Lcom/swedbank/mobile/app/p/a;
    .locals 1

    sget-object v0, Lcom/swedbank/mobile/app/p/a;->e:[Lcom/swedbank/mobile/app/p/a;

    invoke-virtual {v0}, [Lcom/swedbank/mobile/app/p/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/swedbank/mobile/app/p/a;

    return-object v0
.end method
