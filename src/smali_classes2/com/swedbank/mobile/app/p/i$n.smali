.class public final synthetic Lcom/swedbank/mobile/app/p/i$n;
.super Lkotlin/e/b/i;
.source "PreferencesPresenter.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/p/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1019
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/app/p/c;",
        "Lcom/swedbank/mobile/app/p/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/p/i$n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/p/i$n;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/p/i$n;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/p/i$n;->a:Lcom/swedbank/mobile/app/p/i$n;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/p/c;)Lcom/swedbank/mobile/app/p/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/p/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {p1}, Lcom/swedbank/mobile/app/p/r;->a(Lcom/swedbank/mobile/app/p/c;)Lcom/swedbank/mobile/app/p/b;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 2

    const-class v0, Lcom/swedbank/mobile/app/p/r;

    const-string v1, "app_ltRelease"

    invoke-static {v0, v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/h/c;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/app/p/c;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/p/i$n;->a(Lcom/swedbank/mobile/app/p/c;)Lcom/swedbank/mobile/app/p/b;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "toPreferenceItem"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "toPreferenceItem(Lcom/swedbank/mobile/app/preferences/PreferencePlugin;)Lcom/swedbank/mobile/app/preferences/PreferenceItem;"

    return-object v0
.end method
