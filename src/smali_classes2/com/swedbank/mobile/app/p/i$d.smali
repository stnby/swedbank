.class final Lcom/swedbank/mobile/app/p/i$d;
.super Ljava/lang/Object;
.source "PreferencesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/p/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/g<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/p/i;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/p/i;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/i$d;->a:Lcom/swedbank/mobile/app/p/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/i$d;->a:Lcom/swedbank/mobile/app/p/i;

    invoke-static {v0}, Lcom/swedbank/mobile/app/p/i;->b(Lcom/swedbank/mobile/app/p/i;)Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 113
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/swedbank/mobile/app/p/c;

    .line 34
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/p/c;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    iget-object p1, p0, Lcom/swedbank/mobile/app/p/i$d;->a:Lcom/swedbank/mobile/app/p/i;

    invoke-static {p1}, Lcom/swedbank/mobile/app/p/i;->a(Lcom/swedbank/mobile/app/p/i;)Lcom/swedbank/mobile/business/preferences/b;

    move-result-object p1

    .line 37
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/p/c;->f()Lkotlin/h/b;

    move-result-object v0

    .line 38
    invoke-virtual {v1}, Lcom/swedbank/mobile/app/p/c;->g()Lkotlin/e/a/b;

    move-result-object v1

    .line 36
    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/business/preferences/b;->a(Lkotlin/h/b;Lkotlin/e/a/b;)V

    return-void

    .line 114
    :cond_1
    new-instance p1, Ljava/util/NoSuchElementException;

    const-string v0, "Collection contains no element matching the predicate."

    invoke-direct {p1, v0}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public synthetic b(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/p/i$d;->a(Ljava/lang/String;)V

    return-void
.end method
