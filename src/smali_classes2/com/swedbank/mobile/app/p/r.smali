.class public final Lcom/swedbank/mobile/app/p/r;
.super Ljava/lang/Object;
.source "PrefrenceItems.kt"


# direct methods
.method public static final a(Lcom/swedbank/mobile/app/p/c;)Lcom/swedbank/mobile/app/p/b;
    .locals 7
    .param p0    # Lcom/swedbank/mobile/app/p/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toPreferenceItem"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    instance-of v0, p0, Lcom/swedbank/mobile/app/p/c$a;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swedbank/mobile/app/p/b$b;

    .line 36
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c;->k()Ljava/lang/String;

    move-result-object v2

    .line 37
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c;->d()I

    move-result v3

    .line 38
    check-cast p0, Lcom/swedbank/mobile/app/p/c$a;

    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c$a;->e()Ljava/lang/Integer;

    move-result-object v4

    .line 39
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c$a;->h()Ljava/lang/Integer;

    move-result-object v5

    .line 40
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c$a;->j()Ljava/lang/Integer;

    move-result-object v6

    move-object v1, v0

    .line 35
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/app/p/b$b;-><init>(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    check-cast v0, Lcom/swedbank/mobile/app/p/b;

    goto :goto_0

    .line 42
    :cond_0
    instance-of v0, p0, Lcom/swedbank/mobile/app/p/c$b;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/app/p/b$c;

    .line 43
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c;->k()Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c;->d()I

    move-result v2

    .line 45
    check-cast p0, Lcom/swedbank/mobile/app/p/c$b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c$b;->e()Ljava/lang/Integer;

    move-result-object v3

    .line 46
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/c$b;->h()Lio/reactivex/o;

    move-result-object p0

    .line 42
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/swedbank/mobile/app/p/b$c;-><init>(Ljava/lang/String;ILjava/lang/Integer;Lio/reactivex/o;)V

    check-cast v0, Lcom/swedbank/mobile/app/p/b;

    :goto_0
    return-object v0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
