.class public final Lcom/swedbank/mobile/app/p/g;
.super Landroidx/recyclerview/widget/RecyclerView$a;
.source "PreferencesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/swedbank/mobile/app/p/d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/p/b;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroidx/recyclerview/widget/f$b;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 28
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    .line 29
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<PreferenceId>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/p/g;->a:Lcom/b/c/c;

    .line 31
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/p/g;)Lcom/b/c/c;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/g;->a:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/p/d;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x5

    if-eq p2, v0, :cond_2

    const/16 v0, 0xa

    if-eq p2, v0, :cond_1

    const/16 v0, 0xf

    if-ne p2, v0, :cond_0

    const p2, 0x7f0d0032

    goto :goto_0

    .line 168
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No view holder for viewType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    const p2, 0x7f0d0040

    goto :goto_0

    :cond_2
    const p2, 0x7f0d0041

    goto :goto_0

    :cond_3
    const p2, 0x7f0d003d

    .line 173
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    .line 172
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string p2, "LayoutInflater\n      .fr\u2026youtResId, parent, false)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance p2, Lcom/swedbank/mobile/app/p/d;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/app/p/d;-><init>(Landroid/view/View;)V

    return-object p2
.end method

.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/g;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 4
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "list"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    .line 71
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$x;

    move-result-object v2

    instance-of v3, v2, Lcom/swedbank/mobile/app/p/d;

    if-nez v3, :cond_0

    const/4 v2, 0x0

    :cond_0
    check-cast v2, Lcom/swedbank/mobile/app/p/d;

    if-eqz v2, :cond_1

    .line 72
    invoke-virtual {v2}, Lcom/swedbank/mobile/app/p/d;->a()V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/p/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/p/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    move-object v0, p1

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$x;

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$a;->onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$x;)V

    .line 66
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/p/d;->a()V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/p/d;I)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/p/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/app/p/b;

    .line 56
    iget-object v0, p1, Lcom/swedbank/mobile/app/p/d;->itemView:Landroid/view/View;

    const-string v1, "holder.itemView"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance v1, Lcom/swedbank/mobile/app/p/g$a;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/p/g$a;-><init>(Lcom/swedbank/mobile/app/p/g;)V

    check-cast v1, Lkotlin/e/a/m;

    .line 54
    invoke-virtual {p1, p2, v0, v1}, Lcom/swedbank/mobile/app/p/d;->a(Ljava/lang/Object;Landroid/view/View;Lkotlin/e/a/m;)V

    return-void
.end method

.method public final a(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/recyclerview/widget/f$b;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/p/b;",
            ">;",
            "Landroidx/recyclerview/widget/f$b;",
            ")V"
        }
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    .line 36
    iput-object p1, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    .line 37
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 39
    :cond_0
    iget-object p1, p0, Lcom/swedbank/mobile/app/p/g;->c:Landroidx/recyclerview/widget/f$b;

    if-eq p1, p2, :cond_2

    .line 40
    move-object p1, p0

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p2, p1}, Landroidx/recyclerview/widget/f$b;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_1

    .line 38
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/p/g;->notifyDataSetChanged()V

    .line 42
    :cond_2
    :goto_1
    iput-object p2, p0, Lcom/swedbank/mobile/app/p/g;->c:Landroidx/recyclerview/widget/f$b;

    return-void
.end method

.method public getItemCount()I
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/p/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/p/b;

    invoke-static {p1}, Lcom/swedbank/mobile/app/p/h;->a(Lcom/swedbank/mobile/app/p/b;)I

    move-result p1

    return p1
.end method

.method public synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$x;I)V
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/app/p/d;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/p/g;->a(Lcom/swedbank/mobile/app/p/d;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$x;
    .locals 0

    .line 28
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/p/g;->a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/p/d;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$x;

    return-object p1
.end method

.method public synthetic onViewRecycled(Landroidx/recyclerview/widget/RecyclerView$x;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/app/p/d;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/p/g;->a(Lcom/swedbank/mobile/app/p/d;)V

    return-void
.end method
