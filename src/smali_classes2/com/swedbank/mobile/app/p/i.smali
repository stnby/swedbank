.class public final Lcom/swedbank/mobile/app/p/i;
.super Lcom/swedbank/mobile/architect/a/d;
.source "PreferencesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/p/n;",
        "Lcom/swedbank/mobile/app/p/q;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/preferences/b;


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/swedbank/mobile/business/preferences/b;)V
    .locals 1
    .param p1    # Ljava/util/Set;
        .annotation runtime Ljavax/inject/Named;
            value = "for_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/preferences/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/app/p/c;",
            ">;",
            "Lcom/swedbank/mobile/business/preferences/b;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "preferences"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/i;->a:Ljava/util/Set;

    iput-object p2, p0, Lcom/swedbank/mobile/app/p/i;->b:Lcom/swedbank/mobile/business/preferences/b;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/p/i;)Lcom/swedbank/mobile/business/preferences/b;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/i;->b:Lcom/swedbank/mobile/business/preferences/b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/p/i;)Ljava/util/Set;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/i;->a:Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 26
    sget-object v0, Lcom/swedbank/mobile/app/p/i$a;->a:Lcom/swedbank/mobile/app/p/i$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/p/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/swedbank/mobile/app/p/i$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/p/i$b;-><init>(Lcom/swedbank/mobile/app/p/i;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/p/i$c;->a:Lcom/swedbank/mobile/app/p/i$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/p/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 32
    new-instance v2, Lcom/swedbank/mobile/app/p/i$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/p/i$d;-><init>(Lcom/swedbank/mobile/app/p/i;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 42
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 44
    sget-object v2, Lcom/swedbank/mobile/app/p/i$l;->a:Lcom/swedbank/mobile/app/p/i$l;

    check-cast v2, Lkotlin/e/a/b;

    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/p/i;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v2

    .line 45
    new-instance v3, Lcom/swedbank/mobile/app/p/i$m;

    invoke-direct {v3, p0}, Lcom/swedbank/mobile/app/p/i$m;-><init>(Lcom/swedbank/mobile/app/p/i;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v2

    .line 46
    invoke-virtual {v2}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v2

    .line 47
    invoke-virtual {v2}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v2

    .line 49
    sget-object v3, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 50
    iget-object v4, p0, Lcom/swedbank/mobile/app/p/i;->b:Lcom/swedbank/mobile/business/preferences/b;

    invoke-interface {v4}, Lcom/swedbank/mobile/business/preferences/b;->a()Lio/reactivex/o;

    move-result-object v4

    .line 52
    iget-object v5, p0, Lcom/swedbank/mobile/app/p/i;->a:Ljava/util/Set;

    check-cast v5, Ljava/lang/Iterable;

    .line 113
    new-instance v6, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v5, v7}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 114
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 115
    check-cast v7, Lcom/swedbank/mobile/app/p/c;

    .line 54
    invoke-virtual {v7}, Lcom/swedbank/mobile/app/p/c;->i()Lio/reactivex/o;

    move-result-object v8

    .line 55
    new-instance v9, Lcom/swedbank/mobile/app/p/i$e;

    invoke-direct {v9, v7}, Lcom/swedbank/mobile/app/p/i$e;-><init>(Lcom/swedbank/mobile/app/p/c;)V

    check-cast v9, Lio/reactivex/c/h;

    invoke-virtual {v8, v9}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_0
    check-cast v6, Ljava/util/List;

    check-cast v6, Ljava/lang/Iterable;

    .line 52
    invoke-static {v6}, Lio/reactivex/o;->b(Ljava/lang/Iterable;)Lio/reactivex/o;

    move-result-object v5

    .line 57
    iget-object v6, p0, Lcom/swedbank/mobile/app/p/i;->a:Ljava/util/Set;

    sget-object v7, Lcom/swedbank/mobile/app/p/i$f;->a:Lcom/swedbank/mobile/app/p/i$f;

    check-cast v7, Lio/reactivex/c/c;

    invoke-virtual {v5, v6, v7}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v5

    const-string v6, "Observable\n            .\u2026          }\n            }"

    invoke-static {v5, v6}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v3, v4, v5}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v3

    .line 63
    new-instance v4, Lcom/swedbank/mobile/app/p/i$g;

    invoke-direct {v4, p0}, Lcom/swedbank/mobile/app/p/i$g;-><init>(Lcom/swedbank/mobile/app/p/i;)V

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    const-string v4, "Observables.combineLates\u2026              }\n        }"

    invoke-static {v3, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    sget-object v4, Lcom/swedbank/mobile/app/p/i$j;->a:Lcom/swedbank/mobile/app/p/i$j;

    check-cast v4, Lkotlin/e/a/m;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {v3, v6, v4, v5, v6}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v3

    .line 85
    sget-object v4, Lcom/swedbank/mobile/app/p/i$k;->a:Lcom/swedbank/mobile/app/p/i$k;

    check-cast v4, Lio/reactivex/c/h;

    invoke-virtual {v3, v4}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v3

    const/4 v4, 0x4

    .line 92
    new-array v4, v4, [Lio/reactivex/s;

    const/4 v6, 0x0

    .line 93
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v4, v6

    .line 94
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v4, v5

    const/4 v0, 0x2

    .line 95
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v4, v0

    const/4 v0, 0x3

    .line 96
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v4, v0

    .line 92
    invoke-static {v4}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026ecretSequenceEntryStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
