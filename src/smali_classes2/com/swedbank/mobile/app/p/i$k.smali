.class final Lcom/swedbank/mobile/app/p/i$k;
.super Ljava/lang/Object;
.source "PreferencesPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/p/i;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/p/i$k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/p/i$k;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/p/i$k;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/p/i$k;->a:Lcom/swedbank/mobile/app/p/i$k;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/p/q;
    .locals 2
    .param p1    # Lcom/swedbank/mobile/core/ui/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/j<",
            "+",
            "Lcom/swedbank/mobile/app/p/b;",
            ">;)",
            "Lcom/swedbank/mobile/app/p/q;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance v0, Lcom/swedbank/mobile/app/p/q;

    .line 87
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->a()Ljava/util/List;

    move-result-object v1

    .line 88
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/j;->b()Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    .line 86
    invoke-direct {v0, v1, p1}, Lcom/swedbank/mobile/app/p/q;-><init>(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/swedbank/mobile/core/ui/j;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/p/i$k;->a(Lcom/swedbank/mobile/core/ui/j;)Lcom/swedbank/mobile/app/p/q;

    move-result-object p1

    return-object p1
.end method
