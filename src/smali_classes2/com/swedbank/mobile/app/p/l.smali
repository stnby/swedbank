.class public final Lcom/swedbank/mobile/app/p/l;
.super Lcom/swedbank/mobile/architect/a/h;
.source "PreferencesRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/preferences/e;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/l/a;

.field private final f:Lcom/swedbank/mobile/business/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/l/a;Lcom/swedbank/mobile/business/preferences/a;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/l/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/preferences/a;
        .annotation runtime Ljavax/inject/Named;
            value = "for_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_preferences"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/l/a;",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "changeLanguageBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferenceListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p3, p5, p4}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/l;->e:Lcom/swedbank/mobile/app/l/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/p/l;->f:Lcom/swedbank/mobile/business/preferences/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/p/l;)Lcom/swedbank/mobile/app/l/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/l;->e:Lcom/swedbank/mobile/app/l/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/p/l;)Lcom/swedbank/mobile/business/preferences/a;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/l;->f:Lcom/swedbank/mobile/business/preferences/a;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 29
    new-instance v0, Lcom/swedbank/mobile/app/p/l$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/p/l$a;-><init>(Lcom/swedbank/mobile/app/p/l;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/p/l;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lkotlin/h/b;)V
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/swedbank/mobile/architect/business/e;",
            ">(",
            "Lkotlin/h/b<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "nodeClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/swedbank/mobile/app/p/l$b;

    invoke-static {p1}, Lkotlin/e/a;->a(Lkotlin/h/b;)Ljava/lang/Class;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/p/l$b;-><init>(Ljava/lang/Class;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/p/l;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public a(Lkotlin/h/b;Lkotlin/e/a/b;)V
    .locals 1
    .param p1    # Lkotlin/h/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/e/a/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/h/b<",
            "*>;",
            "Lkotlin/e/a/b<",
            "-",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "+",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;)V"
        }
    .end annotation

    const-string v0, "preferenceNodeClass"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferenceBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/swedbank/mobile/app/p/l$c;

    invoke-direct {v0, p0, p1, p2}, Lcom/swedbank/mobile/app/p/l$c;-><init>(Lcom/swedbank/mobile/app/p/l;Lkotlin/h/b;Lkotlin/e/a/b;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/p/l;->b(Lkotlin/e/a/b;)V

    return-void
.end method
