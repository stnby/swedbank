.class public final Lcom/swedbank/mobile/app/p/a/e;
.super Lcom/swedbank/mobile/architect/a/h;
.source "ClearDataRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/preferences/cleardata/d;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/f/a;

.field private final f:Lcom/swedbank/mobile/business/general/confirmation/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/f/a;Lcom/swedbank/mobile/business/general/confirmation/c;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/f/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_clear_data_preference"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_clear_data_preference"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/f/a;",
            "Lcom/swedbank/mobile/business/general/confirmation/c;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "confirmationDialogBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "confirmationDialogListener"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    .line 23
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/a/e;->e:Lcom/swedbank/mobile/app/f/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/p/a/e;->f:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/p/a/e;)Lcom/swedbank/mobile/app/f/a;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/a/e;->e:Lcom/swedbank/mobile/app/f/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/p/a/e;)Lcom/swedbank/mobile/business/general/confirmation/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/a/e;->f:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 1

    .line 24
    new-instance v0, Lcom/swedbank/mobile/app/p/a/e$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/p/a/e$a;-><init>(Lcom/swedbank/mobile/app/p/a/e;)V

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/p/a/e;->b(Lkotlin/e/a/b;)V

    return-void
.end method
