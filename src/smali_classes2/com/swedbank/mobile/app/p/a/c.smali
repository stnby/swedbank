.class public final Lcom/swedbank/mobile/app/p/a/c;
.super Lcom/swedbank/mobile/app/p/c$a;
.source "ClearDataPreferencePlugin.kt"


# instance fields
.field private final a:Lcom/swedbank/mobile/app/p/a/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/p/a/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/p/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "nodeBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/swedbank/mobile/app/p/c$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/p/a/c;->a:Lcom/swedbank/mobile/app/p/a/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/p/a/c;)Lcom/swedbank/mobile/app/p/a/a;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/swedbank/mobile/app/p/a/c;->a:Lcom/swedbank/mobile/app/p/a/a;

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method public c()Lcom/swedbank/mobile/app/p/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 21
    sget-object v0, Lcom/swedbank/mobile/app/p/a;->a:Lcom/swedbank/mobile/app/p/a;

    return-object v0
.end method

.method public d()I
    .locals 1

    const v0, 0x7f1100e7

    return v0
.end method

.method public e()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const v0, 0x7f060070

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public f()Lkotlin/h/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/h/b<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 25
    const-class v0, Lcom/swedbank/mobile/business/preferences/cleardata/d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public g()Lkotlin/e/a/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/b<",
            "Lcom/swedbank/mobile/business/preferences/a;",
            "Lcom/swedbank/mobile/architect/a/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 27
    new-instance v0, Lcom/swedbank/mobile/app/p/a/c$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/p/a/c$a;-><init>(Lcom/swedbank/mobile/app/p/a/c;)V

    check-cast v0, Lkotlin/e/a/b;

    return-object v0
.end method

.method public h()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public j()Ljava/lang/Integer;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const v0, 0x7f1100e6

    .line 33
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
