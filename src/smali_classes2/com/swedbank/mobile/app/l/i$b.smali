.class public final Lcom/swedbank/mobile/app/l/i$b;
.super Landroid/widget/ArrayAdapter;
.source "ChangeLanguageViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/s;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/l/i;->a(Ljava/util/List;Lcom/swedbank/mobile/business/c/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Lcom/swedbank/mobile/business/c/g;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/s;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/l/i;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/swedbank/mobile/business/c/g;


# direct methods
.method constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/swedbank/mobile/app/l/i;Ljava/util/List;Lcom/swedbank/mobile/business/c/g;)V
    .locals 0

    iput-object p4, p0, Lcom/swedbank/mobile/app/l/i$b;->a:Lcom/swedbank/mobile/app/l/i;

    iput-object p5, p0, Lcom/swedbank/mobile/app/l/i$b;->b:Ljava/util/List;

    iput-object p6, p0, Lcom/swedbank/mobile/app/l/i$b;->c:Lcom/swedbank/mobile/business/c/g;

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v1, p0, Lcom/swedbank/mobile/app/l/i$b;->a:Lcom/swedbank/mobile/app/l/i;

    iget-object v2, p0, Lcom/swedbank/mobile/app/l/i$b;->b:Ljava/util/List;

    iget-object v4, p0, Lcom/swedbank/mobile/app/l/i$b;->c:Lcom/swedbank/mobile/business/c/g;

    move v3, p1

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v1 .. v6}, Lcom/swedbank/mobile/app/l/i;->a(Lcom/swedbank/mobile/app/l/i;Ljava/util/List;ILcom/swedbank/mobile/business/c/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/l/i$b;->a:Lcom/swedbank/mobile/app/l/i;

    iget-object v1, p0, Lcom/swedbank/mobile/app/l/i$b;->b:Ljava/util/List;

    .line 143
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/business/c/g;

    .line 144
    invoke-static {v0, p1}, Lcom/swedbank/mobile/app/l/i;->a(Lcom/swedbank/mobile/app/l/i;Lcom/swedbank/mobile/business/c/g;)I

    move-result p1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    .line 146
    instance-of p3, p2, Landroid/widget/ImageView;

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, p2

    :goto_0
    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 149
    :cond_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p3, "parent.context"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f0d0050

    .line 153
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 156
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const-string v2, "LayoutInflater.from(this)"

    invoke-static {p2, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2, p3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    if-eqz p2, :cond_3

    check-cast p2, Landroid/widget/ImageView;

    .line 151
    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    check-cast p2, Landroid/view/View;

    :cond_2
    :goto_1
    return-object p2

    .line 155
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p1    # Landroid/widget/AdapterView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const-string p4, "parent"

    invoke-static {p1, p4}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "view"

    invoke-static {p2, p1}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object p1, p0, Lcom/swedbank/mobile/app/l/i$b;->a:Lcom/swedbank/mobile/app/l/i;

    invoke-static {p1}, Lcom/swedbank/mobile/app/l/i;->a(Lcom/swedbank/mobile/app/l/i;)Lcom/b/c/c;

    move-result-object p1

    iget-object p2, p0, Lcom/swedbank/mobile/app/l/i$b;->b:Ljava/util/List;

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/b/c/c;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1
    .param p1    # Landroid/widget/AdapterView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/s$a;->a(Lcom/swedbank/mobile/core/ui/s;Landroid/widget/AdapterView;)V

    return-void
.end method
