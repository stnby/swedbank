.class public final Lcom/swedbank/mobile/app/l/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "ChangeLanguagePresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/l/h;",
        "Lcom/swedbank/mobile/app/l/l;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/language/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/language/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/language/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/l/c;->a:Lcom/swedbank/mobile/business/language/a;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 4

    .line 14
    sget-object v0, Lio/reactivex/i/d;->a:Lio/reactivex/i/d;

    .line 15
    iget-object v1, p0, Lcom/swedbank/mobile/app/l/c;->a:Lcom/swedbank/mobile/business/language/a;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/language/a;->a()Lio/reactivex/w;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v1

    const-string v2, "interactor.getAllAvailab\u2026anguages().toObservable()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v2, p0, Lcom/swedbank/mobile/app/l/c;->a:Lcom/swedbank/mobile/business/language/a;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/language/a;->b()Lio/reactivex/o;

    move-result-object v2

    .line 14
    invoke-virtual {v0, v1, v2}, Lio/reactivex/i/d;->a(Lio/reactivex/o;Lio/reactivex/o;)Lio/reactivex/o;

    move-result-object v0

    .line 17
    sget-object v1, Lcom/swedbank/mobile/app/l/c$c;->a:Lcom/swedbank/mobile/app/l/c$c;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 23
    sget-object v1, Lcom/swedbank/mobile/app/l/c$a;->a:Lcom/swedbank/mobile/app/l/c$a;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/l/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    new-instance v2, Lcom/swedbank/mobile/app/l/c$b;

    iget-object v3, p0, Lcom/swedbank/mobile/app/l/c;->a:Lcom/swedbank/mobile/business/language/a;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/l/c$b;-><init>(Lcom/swedbank/mobile/business/language/a;)V

    check-cast v2, Lkotlin/e/a/b;

    new-instance v3, Lcom/swedbank/mobile/app/l/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/l/d;-><init>(Lkotlin/e/a/b;)V

    check-cast v3, Lio/reactivex/c/g;

    invoke-virtual {v1, v3}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 26
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 29
    check-cast v0, Lio/reactivex/s;

    .line 30
    check-cast v1, Lio/reactivex/s;

    .line 28
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026    languageChangeStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
