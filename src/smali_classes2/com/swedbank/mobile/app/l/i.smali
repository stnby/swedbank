.class public final Lcom/swedbank/mobile/app/l/i;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ChangeLanguageViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/l/h;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 29
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Language>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/l/i;->a:Lcom/b/c/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/l/i;Lcom/swedbank/mobile/business/c/g;)I
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/l/i;->a(Lcom/swedbank/mobile/business/c/g;)I

    move-result p0

    return p0
.end method

.method private final a(Lcom/swedbank/mobile/business/c/g;)I
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 123
    sget-object v0, Lcom/swedbank/mobile/app/l/j;->a:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/g;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 129
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const p1, 0x7f0800b5

    goto :goto_0

    :pswitch_1
    const p1, 0x7f0800b6

    goto :goto_0

    :pswitch_2
    const p1, 0x7f0800b7

    goto :goto_0

    :pswitch_3
    const p1, 0x7f0800b4

    goto :goto_0

    :pswitch_4
    const p1, 0x7f0800b8

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/l/i;Ljava/util/List;ILcom/swedbank/mobile/business/c/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/swedbank/mobile/app/l/i;->a(Ljava/util/List;ILcom/swedbank/mobile/business/c/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final a(Ljava/util/List;ILcom/swedbank/mobile/business/c/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;I",
            "Lcom/swedbank/mobile/business/c/g;",
            "Landroid/view/View;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .line 86
    new-instance v0, Lcom/swedbank/mobile/app/l/i$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/swedbank/mobile/app/l/i$a;-><init>(Lcom/swedbank/mobile/app/l/i;Ljava/util/List;ILcom/swedbank/mobile/business/c/g;)V

    const/4 p1, 0x0

    if-eqz p4, :cond_2

    .line 100
    instance-of p2, p4, Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p4

    :goto_0
    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_1

    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/l/i$a;->a(Landroid/view/ViewGroup;)V

    :cond_1
    return-object p4

    .line 103
    :cond_2
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p3, "parent.context"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const p3, 0x7f0d003e

    .line 160
    check-cast p1, Landroid/view/ViewGroup;

    const/4 p4, 0x0

    .line 163
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const-string p5, "LayoutInflater.from(this)"

    invoke-static {p2, p5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    invoke-virtual {p2, p3, p1, p4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Landroid/view/ViewGroup;

    .line 105
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/l/i$a;->a(Landroid/view/ViewGroup;)V

    check-cast p1, Landroid/view/View;

    return-object p1

    .line 162
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/l/i;)Lcom/b/c/c;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/swedbank/mobile/app/l/i;->a:Lcom/b/c/c;

    return-object p0
.end method

.method private final a(Ljava/util/List;Lcom/swedbank/mobile/business/c/g;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;",
            "Lcom/swedbank/mobile/business/c/g;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/l/i;->o()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a02dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    const-string v1, "toolbar"

    .line 43
    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v1, 0x7f0a015c

    .line 143
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/AppCompatSpinner;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 145
    :cond_0
    invoke-virtual {v0}, Landroidx/appcompat/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0d003f

    const/4 v3, 0x0

    .line 146
    check-cast v3, Landroid/view/ViewGroup;

    const/4 v4, 0x0

    .line 149
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const-string v5, "LayoutInflater.from(this)"

    invoke-static {v1, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v1, Landroidx/appcompat/widget/AppCompatSpinner;

    .line 150
    new-instance v2, Landroidx/appcompat/widget/Toolbar$b;

    const/4 v3, -0x2

    const/4 v4, -0x1

    const v5, 0x800015

    invoke-direct {v2, v3, v4, v5}, Landroidx/appcompat/widget/Toolbar$b;-><init>(III)V

    check-cast v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/AppCompatSpinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    move-object v2, v1

    check-cast v2, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 44
    :goto_0
    new-instance v0, Lcom/swedbank/mobile/app/l/i$b;

    invoke-virtual {p0}, Lcom/swedbank/mobile/app/l/i;->n()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    move-object v2, v0

    move-object v5, p1

    move-object v6, p0

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/l/i$b;-><init>(Landroid/content/Context;ILjava/util/List;Lcom/swedbank/mobile/app/l/i;Ljava/util/List;Lcom/swedbank/mobile/business/c/g;)V

    .line 56
    move-object v2, v0

    check-cast v2, Landroid/widget/SpinnerAdapter;

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/AppCompatSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 57
    check-cast v0, Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/AppCompatSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 58
    invoke-interface {p1, p2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {v1, p1}, Landroidx/appcompat/widget/AppCompatSpinner;->setSelection(I)V

    return-void

    .line 148
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type androidx.appcompat.widget.AppCompatSpinner"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/l/i;Lcom/swedbank/mobile/business/c/g;)I
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/swedbank/mobile/app/l/i;->b(Lcom/swedbank/mobile/business/c/g;)I

    move-result p0

    return p0
.end method

.method private final b(Lcom/swedbank/mobile/business/c/g;)I
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/c/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 134
    sget-object v0, Lcom/swedbank/mobile/app/l/j;->b:[I

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/c/g;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 140
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    const p1, 0x7f11013e

    goto :goto_0

    :pswitch_1
    const p1, 0x7f11013f

    goto :goto_0

    :pswitch_2
    const p1, 0x7f110140

    goto :goto_0

    :pswitch_3
    const p1, 0x7f11013d

    goto :goto_0

    :pswitch_4
    const p1, 0x7f110141

    :goto_0
    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/c/g;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/l/i;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/app/l/l;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/l/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/l/l;->a()Ljava/util/List;

    move-result-object v0

    .line 35
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/l/l;->b()Lcom/swedbank/mobile/business/c/g;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 36
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 37
    invoke-direct {p0, v0, p1}, Lcom/swedbank/mobile/app/l/i;->a(Ljava/util/List;Lcom/swedbank/mobile/business/c/g;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/swedbank/mobile/app/l/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/l/i;->a(Lcom/swedbank/mobile/app/l/l;)V

    return-void
.end method
