.class final Lcom/swedbank/mobile/app/l/i$a;
.super Lkotlin/e/b/k;
.source "ChangeLanguageViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/l/i;->a(Ljava/util/List;ILcom/swedbank/mobile/business/c/g;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/view/ViewGroup;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/l/i;

.field final synthetic b:Ljava/util/List;

.field final synthetic c:I

.field final synthetic d:Lcom/swedbank/mobile/business/c/g;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/l/i;Ljava/util/List;ILcom/swedbank/mobile/business/c/g;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/l/i$a;->a:Lcom/swedbank/mobile/app/l/i;

    iput-object p2, p0, Lcom/swedbank/mobile/app/l/i$a;->b:Ljava/util/List;

    iput p3, p0, Lcom/swedbank/mobile/app/l/i$a;->c:I

    iput-object p4, p0, Lcom/swedbank/mobile/app/l/i$a;->d:Lcom/swedbank/mobile/business/c/g;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)V
    .locals 7
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$renderView"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/swedbank/mobile/app/l/i$a;->b:Ljava/util/List;

    iget v1, p0, Lcom/swedbank/mobile/app/l/i$a;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/business/c/g;

    .line 88
    iget-object v1, p0, Lcom/swedbank/mobile/app/l/i$a;->d:Lcom/swedbank/mobile/business/c/g;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const v4, 0x7f0a0153

    .line 89
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 90
    check-cast v4, Landroid/widget/TextView;

    .line 91
    invoke-virtual {v4}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/swedbank/mobile/app/l/i$a;->a:Lcom/swedbank/mobile/app/l/i;

    invoke-static {v6, v0}, Lcom/swedbank/mobile/app/l/i;->b(Lcom/swedbank/mobile/app/l/i;Lcom/swedbank/mobile/business/c/g;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setSelected(Z)V

    const v4, 0x7f0a0151

    .line 94
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "findViewById<View>(R.id.item_language_checked)"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    xor-int/2addr v1, v3

    if-eqz v1, :cond_1

    const/4 v2, 0x4

    .line 143
    :cond_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0152

    .line 95
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    .line 96
    iget-object v1, p0, Lcom/swedbank/mobile/app/l/i$a;->a:Lcom/swedbank/mobile/app/l/i;

    invoke-static {v1, v0}, Lcom/swedbank/mobile/app/l/i;->a(Lcom/swedbank/mobile/app/l/i;Lcom/swedbank/mobile/business/c/g;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/l/i$a;->a(Landroid/view/ViewGroup;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
