.class public Lcom/swedbank/mobile/app/SwedbankApplication;
.super Landroid/app/Application;
.source "SwedbankApplication.kt"

# interfaces
.implements Lcom/swedbank/mobile/a/ae/c;
.implements Lcom/swedbank/mobile/a/c/a/b;
.implements Lcom/swedbank/mobile/core/a/d;
.implements Lcom/swedbank/mobile/data/device/i;
.implements Lcom/swedbank/mobile/data/g;
.implements Lcom/swedbank/mobile/data/q;
.implements Lcom/swedbank/mobile/data/transfer/payment/c;


# instance fields
.field private a:Lcom/swedbank/mobile/a/b/n;

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference<",
            "Lcom/swedbank/mobile/a/c/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 40
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/SwedbankApplication;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->b:Ljava/util/concurrent/atomic/AtomicReference;

    return-object p0
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/a/b/n;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public a(Lcom/swedbank/mobile/a/c/a/a;)Lio/reactivex/b/c;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/c/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "authComp"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 76
    new-instance v0, Lcom/swedbank/mobile/app/SwedbankApplication$a;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/SwedbankApplication$a;-><init>(Lcom/swedbank/mobile/app/SwedbankApplication;Lcom/swedbank/mobile/a/c/a/a;)V

    check-cast v0, Lio/reactivex/c/a;

    invoke-static {v0}, Lio/reactivex/b/d;->a(Lio/reactivex/c/a;)Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "Disposables.fromAction {\u2026Set(authComp, null)\n    }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a(Lcom/swedbank/mobile/data/push/PushMessageListenerService;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/push/PushMessageListenerService;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "service"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/a/b/n;->a(Lcom/swedbank/mobile/data/push/PushMessageListenerService;)V

    return-void
.end method

.method public a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, p1}, Lcom/swedbank/mobile/a/b/n;->a(Lcom/swedbank/mobile/data/wallet/PaymentTokenReplenishWorker;)V

    return-void
.end method

.method public b()Lcom/swedbank/mobile/data/transfer/payment/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/data/transfer/payment/a;

    return-object v0
.end method

.method public c()Lcom/swedbank/mobile/core/a/c;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->c()Lcom/swedbank/mobile/core/a/c;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/swedbank/mobile/data/device/h;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->d()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public e()Lcom/swedbank/mobile/architect/business/g;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/swedbank/mobile/architect/business/g<",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/widget/root/g;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->e()Lcom/swedbank/mobile/architect/business/g;

    move-result-object v0

    return-object v0
.end method

.method public f()Lcom/swedbank/mobile/app/widget/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->f()Lcom/swedbank/mobile/app/widget/h;

    move-result-object v0

    return-object v0
.end method

.method public g()Lcom/swedbank/mobile/business/widget/configuration/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 91
    iget-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    if-nez v0, :cond_0

    const-string v1, "appComponent"

    invoke-static {v1}, Lkotlin/e/b/j;->b(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->g()Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .line 44
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 46
    invoke-static {}, Lcom/swedbank/mobile/a/b/s;->h()Lcom/swedbank/mobile/a/b/n$a;

    move-result-object v0

    .line 47
    move-object v1, p0

    check-cast v1, Landroid/app/Application;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/b/n$a;->a(Landroid/app/Application;)Lcom/swedbank/mobile/a/b/n$a;

    move-result-object v0

    .line 48
    new-instance v2, Lcom/swedbank/mobile/data/i;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/data/i;-><init>(Landroid/app/Application;)V

    invoke-interface {v0, v2}, Lcom/swedbank/mobile/a/b/n$a;->a(Lcom/swedbank/mobile/data/i;)Lcom/swedbank/mobile/a/b/n$a;

    move-result-object v0

    .line 49
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n$a;->a()Lcom/swedbank/mobile/a/b/n;

    move-result-object v0

    .line 112
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->b()Lcom/swedbank/mobile/architect/business/a;

    move-result-object v1

    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->a()Lcom/swedbank/mobile/architect/a/c;

    move-result-object v2

    invoke-interface {v2}, Lcom/swedbank/mobile/architect/a/c;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/architect/business/a;->a(Lcom/swedbank/mobile/architect/a/h;)V

    .line 113
    invoke-interface {v0}, Lcom/swedbank/mobile/a/b/n;->c()Lcom/swedbank/mobile/core/a/c;

    move-result-object v1

    invoke-interface {v1}, Lcom/swedbank/mobile/core/a/c;->a()Lio/reactivex/b/c;

    .line 114
    iput-object v0, p0, Lcom/swedbank/mobile/app/SwedbankApplication;->a:Lcom/swedbank/mobile/a/b/n;

    return-void
.end method
