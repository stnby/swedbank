.class public final Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "AccountSelectionRecyclerView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/core/ui/widget/k;

.field private b:Z

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/b/c/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/b<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-instance p2, Ljava/util/LinkedHashSet;

    invoke-direct {p2}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p2, Ljava/util/Set;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->c:Ljava/util/Set;

    .line 34
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->d:Ljava/util/List;

    .line 36
    invoke-static {}, Lcom/b/c/b;->a()Lcom/b/c/b;

    move-result-object p2

    const-string p3, "BehaviorRelay.create<Set<AccountId>>()"

    invoke-static {p2, p3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->e:Lcom/b/c/b;

    const/4 p2, 0x1

    .line 40
    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->setHasFixedSize(Z)V

    .line 41
    new-instance p3, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p3, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 42
    new-instance p3, Lcom/swedbank/mobile/core/ui/widget/f;

    .line 43
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/g$c;

    new-array v1, p2, [I

    const/4 v7, 0x0

    aput p2, v1, v7

    invoke-direct {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/g$c;-><init>([I)V

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v0, p3

    move-object v1, p1

    .line 42
    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p3}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    const/4 p1, 0x2

    .line 45
    new-array p3, p1, [Lkotlin/k;

    .line 46
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 138
    sget v1, Lcom/swedbank/mobile/app/widget/b$e;->item_widget_account_selection_separator:I

    .line 139
    sget-object v2, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;

    check-cast v2, Lkotlin/e/a/m;

    .line 137
    invoke-static {v1, v2}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v1

    .line 46
    invoke-static {v0, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    aput-object v0, p3, v7

    .line 47
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 141
    sget v0, Lcom/swedbank/mobile/app/widget/b$e;->item_widget_account_selection_account:I

    .line 142
    new-instance v1, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;-><init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)V

    check-cast v1, Lkotlin/e/a/m;

    .line 140
    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/widget/l;->a(ILkotlin/e/a/m;)Lkotlin/e/a/b;

    move-result-object v0

    .line 47
    invoke-static {p1, v0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p1

    aput-object p1, p3, p2

    .line 45
    invoke-static {p3}, Lcom/swedbank/mobile/core/ui/widget/l;->a([Lkotlin/k;)Landroid/util/SparseArray;

    move-result-object p1

    .line 44
    new-instance p3, Lcom/swedbank/mobile/core/ui/widget/k;

    const/4 v0, 0x0

    invoke-direct {p3, v0, p1, p2, v0}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object p3, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a:Lcom/swedbank/mobile/core/ui/widget/k;

    .line 48
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a:Lcom/swedbank/mobile/core/ui/widget/k;

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 27
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 28
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->c:Ljava/util/Set;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Lcom/b/c/b;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->e:Lcom/b/c/b;

    return-object p0
.end method


# virtual methods
.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->e:Lcom/b/c/b;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Lkotlin/k;)V
    .locals 6
    .param p1    # Lkotlin/k;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/k<",
            "+",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            ">;+",
            "Landroidx/recyclerview/widget/f$b;",
            ">;)V"
        }
    .end annotation

    const-string v0, "listItems"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p1}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/f$b;

    .line 53
    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    .line 127
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 128
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 129
    check-cast v3, Lcom/swedbank/mobile/app/widget/view/accountselection/a;

    .line 55
    instance-of v4, v3, Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;

    if-eqz v4, :cond_0

    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/i;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    goto :goto_1

    .line 56
    :cond_0
    instance-of v4, v3, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;

    if-eqz v4, :cond_1

    new-instance v4, Lcom/swedbank/mobile/core/ui/widget/i;

    const/4 v5, 0x2

    invoke-direct {v4, v3, v5}, Lcom/swedbank/mobile/core/ui/widget/i;-><init>(Ljava/lang/Object;I)V

    .line 57
    :goto_1
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 130
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 59
    iput-object v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->d:Ljava/util/List;

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a:Lcom/swedbank/mobile/core/ui/widget/k;

    invoke-virtual {v0, v2, p1}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Ljava/util/List;Landroidx/recyclerview/widget/f$b;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 109
    instance-of v0, p1, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;

    if-nez v0, :cond_0

    .line 110
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 113
    :cond_0
    check-cast p1, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b:Z

    .line 114
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;->d()Ljava/util/Set;

    move-result-object v0

    .line 134
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 135
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Lcom/b/c/b;

    move-result-object v0

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 115
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;->c()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lkotlin/k;)V

    .line 116
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;->a()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 102
    new-instance v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;

    .line 103
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 104
    iget-boolean v2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b:Z

    .line 105
    iget-object v3, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->d:Ljava/util/List;

    .line 106
    iget-object v4, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->c:Ljava/util/Set;

    .line 102
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$a;-><init>(Landroid/os/Parcelable;ZLjava/util/List;Ljava/util/Set;)V

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public final setInitialSelectedAccounts(Ljava/util/Set;)V
    .locals 1
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "accountIds"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-boolean v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 66
    iput-boolean v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b:Z

    .line 131
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v0

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 132
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Lcom/b/c/b;

    move-result-object p1

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 68
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a:Lcom/swedbank/mobile/core/ui/widget/k;

    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/k;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
