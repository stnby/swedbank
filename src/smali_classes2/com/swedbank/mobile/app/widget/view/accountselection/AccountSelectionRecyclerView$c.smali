.class public final Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;
.super Lkotlin/e/b/k;
.source "AccountSelectionRecyclerView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;",
        ">;",
        "Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$c;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;",
            ">;",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;->b()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
