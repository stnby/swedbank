.class public final Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;
.super Lcom/swedbank/mobile/core/ui/h;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;Landroid/widget/CheckBox;Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->b:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->c:Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;

    .line 113
    invoke-direct {p0}, Lcom/swedbank/mobile/core/ui/h;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "v"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->b:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    const/4 v0, 0x1

    xor-int/2addr p1, v0

    .line 302
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;

    iget-object v1, v1, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->c:Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;

    invoke-virtual {v2}, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;->b()Ljava/lang/String;

    move-result-object v2

    if-ne p1, v0, :cond_0

    .line 304
    invoke-static {v1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 305
    :cond_0
    invoke-static {v1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 307
    :goto_0
    invoke-static {v1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->b(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Lcom/b/c/b;

    move-result-object v0

    invoke-static {v1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/b/c/b;->b(Ljava/lang/Object;)V

    .line 309
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method
