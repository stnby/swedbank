.class public final Lcom/swedbank/mobile/app/widget/view/accountselection/b;
.super Ljava/lang/Object;
.source "AccountSelectionListItem.kt"


# static fields
.field private static final a:Lkotlin/e/a/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/e/a/m<",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    sget-object v0, Lcom/swedbank/mobile/app/widget/view/accountselection/b$a;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/b$a;

    check-cast v0, Lkotlin/e/a/m;

    sput-object v0, Lcom/swedbank/mobile/app/widget/view/accountselection/b;->a:Lkotlin/e/a/m;

    return-void
.end method

.method public static final a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .param p0    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/business/a/c;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toSortedAccountSelectionListItems"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x3ff8000000000000L    # 1.5

    mul-double v1, v1, v3

    double-to-int v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 23
    check-cast p0, Ljava/lang/Iterable;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/e/a/b;

    sget-object v2, Lcom/swedbank/mobile/app/widget/view/accountselection/c;->a:Lkotlin/h/i;

    check-cast v2, Lkotlin/e/a/b;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/swedbank/mobile/app/widget/view/accountselection/d;->a:Lkotlin/h/i;

    check-cast v2, Lkotlin/e/a/b;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v1}, Lkotlin/b/a;->a([Lkotlin/e/a/b;)Ljava/util/Comparator;

    move-result-object v1

    invoke-static {p0, v1}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 38
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 39
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 40
    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/business/a/c;

    .line 24
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/c;->e()Ljava/lang/String;

    move-result-object v3

    .line 42
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 41
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 45
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 49
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 52
    :cond_1
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 26
    new-instance v3, Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/widget/view/accountselection/a$b;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    check-cast v1, Ljava/lang/Iterable;

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    .line 53
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 54
    check-cast v3, Lcom/swedbank/mobile/business/a/c;

    .line 28
    new-instance v4, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;

    .line 29
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/c;->a()Ljava/lang/String;

    move-result-object v5

    .line 30
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/a/c;->h()Ljava/lang/String;

    move-result-object v3

    .line 28
    invoke-direct {v4, v5, v3}, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method public static final a()Lkotlin/e/a/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/e/a/m<",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 35
    sget-object v0, Lcom/swedbank/mobile/app/widget/view/accountselection/b;->a:Lkotlin/e/a/m;

    return-object v0
.end method
