.class final Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;
.super Lkotlin/e/b/k;
.source "AccountSelectionLoadingView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Landroid/content/res/Resources;",
        "Landroid/util/DisplayMetrics;",
        "Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView;

.field final synthetic b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;->b:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;
    .locals 25
    .param p1    # Landroid/content/res/Resources;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/DisplayMetrics;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v3, p2

    move-object/from16 v1, p0

    const-string v2, "resources"

    invoke-static {v8, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "metrics"

    invoke-static {v3, v2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v2, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;->b:Landroid/content/Context;

    sget v4, Lcom/swedbank/mobile/app/widget/b$a;->background_color:I

    .line 137
    invoke-static {v2, v4}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v17

    move/from16 v15, v17

    .line 33
    new-instance v2, Landroid/graphics/Paint;

    move-object v7, v2

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    new-instance v2, Landroid/graphics/Paint;

    move-object v5, v2

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/16 v4, 0x28

    invoke-static {v4}, Lcom/swedbank/mobile/core/ui/ag;->a(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    sget v4, Lcom/swedbank/mobile/app/widget/b$b;->list_item_separator_horizontal_padding:I

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v9, v4

    move v6, v9

    .line 37
    sget v4, Lcom/swedbank/mobile/app/widget/b$b;->widget_account_selection_content_padding_horizontal:I

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v11, v4

    .line 38
    sget v4, Lcom/swedbank/mobile/app/widget/b$b;->widget_account_selection_content_padding_vertical:I

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    .line 39
    sget v10, Lcom/swedbank/mobile/app/widget/b$b;->widget_account_selection_separator_padding_top:I

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    int-to-float v10, v10

    .line 40
    sget v12, Lcom/swedbank/mobile/app/widget/b$b;->widget_account_selection_separator_padding_bottom:I

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    int-to-float v13, v12

    .line 41
    sget v12, Lcom/swedbank/mobile/app/widget/b$b;->widget_account_selection_checkbox_spacing:I

    invoke-virtual {v8, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    int-to-float v14, v12

    const/16 v12, 0x8

    int-to-float v12, v12

    const/4 v0, 0x1

    .line 138
    invoke-static {v0, v12, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v12

    const/16 v0, 0x10

    int-to-float v0, v0

    move-object/from16 v19, v2

    const/4 v2, 0x2

    .line 139
    invoke-static {v2, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    move/from16 v16, v13

    move v13, v0

    int-to-float v8, v2

    .line 140
    invoke-static {v2, v8, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v20

    add-float v4, v4, v20

    mul-float v4, v4, v8

    add-float/2addr v4, v0

    const/16 v0, 0x12

    int-to-float v0, v0

    const/4 v2, 0x1

    .line 141
    invoke-static {v2, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    move v2, v10

    move v10, v0

    const/16 v0, 0xe

    int-to-float v0, v0

    move-object/from16 v22, v1

    const/4 v1, 0x2

    .line 142
    invoke-static {v1, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v21

    move/from16 v23, v4

    move v4, v9

    move/from16 v9, v21

    .line 143
    invoke-static {v1, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    add-float/2addr v0, v2

    add-float v0, v0, v16

    mul-float v8, v8, v20

    add-float v2, v0, v8

    move-object/from16 v0, v19

    const/4 v1, 0x1

    int-to-float v8, v1

    .line 144
    invoke-static {v1, v8, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    .line 52
    new-instance v1, Lcom/swedbank/mobile/core/ui/m;

    move/from16 v24, v2

    move-object/from16 v2, p1

    move-object v8, v1

    invoke-direct {v1, v2, v4, v0}, Lcom/swedbank/mobile/core/ui/m;-><init>(Landroid/content/res/Resources;FLandroid/graphics/Paint;)V

    .line 57
    new-instance v18, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;

    move-object/from16 v1, p0

    move-object/from16 v0, v18

    iget-object v2, v1, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView;

    move-object/from16 v16, v2

    check-cast v16, Landroid/view/View;

    move-object/from16 v1, v22

    move/from16 v4, v23

    move/from16 v2, v24

    invoke-direct/range {v0 .. v17}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;-><init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;FFFLandroid/graphics/Paint;FLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFFILandroid/view/View;I)V

    return-object v18
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Landroid/content/res/Resources;

    check-cast p2, Landroid/util/DisplayMetrics;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;

    move-result-object p1

    return-object p1
.end method
