.class public final Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;
.super Lcom/swedbank/mobile/core/ui/af;
.source "AccountSelectionLoadingView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;->a(Landroid/content/res/Resources;Landroid/util/DisplayMetrics;)Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;

.field final synthetic b:F

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Landroid/graphics/Paint;

.field final synthetic f:F

.field final synthetic g:Landroid/graphics/Paint;

.field final synthetic h:Lcom/swedbank/mobile/core/ui/m;

.field final synthetic i:F

.field final synthetic j:F

.field final synthetic k:F

.field final synthetic l:F

.field final synthetic m:F

.field final synthetic n:F

.field final synthetic o:I


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;FFFLandroid/graphics/Paint;FLandroid/graphics/Paint;Lcom/swedbank/mobile/core/ui/m;FFFFFFILandroid/view/View;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFF",
            "Landroid/graphics/Paint;",
            "F",
            "Landroid/graphics/Paint;",
            "Lcom/swedbank/mobile/core/ui/m;",
            "FFFFFFI",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    .line 57
    iput-object v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1;

    move v1, p2

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->b:F

    move v1, p3

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->c:F

    move v1, p4

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->d:F

    move-object v1, p5

    iput-object v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->e:Landroid/graphics/Paint;

    move v1, p6

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->f:F

    move-object v1, p7

    iput-object v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->g:Landroid/graphics/Paint;

    move-object v1, p8

    iput-object v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->h:Lcom/swedbank/mobile/core/ui/m;

    move v1, p9

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->i:F

    move/from16 v1, p10

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->j:F

    move/from16 v1, p11

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->k:F

    move/from16 v1, p12

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->l:F

    move/from16 v1, p13

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->m:F

    move/from16 v1, p14

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->n:F

    move/from16 v1, p15

    iput v1, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->o:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x1c

    const/4 v5, 0x0

    move-object p1, p0

    move-object/from16 p2, p16

    move/from16 p3, p17

    move p4, v1

    move p5, v2

    move p6, v3

    move p7, v4

    move-object p8, v5

    invoke-direct/range {p1 .. p8}, Lcom/swedbank/mobile/core/ui/af;-><init>(Landroid/view/View;IIIZILkotlin/e/b/g;)V

    return-void
.end method


# virtual methods
.method protected a(IILandroid/graphics/Canvas;)V
    .locals 18
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, p3

    const-string v3, "canvas"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-float v3, v1

    .line 60
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    const/4 v9, 0x2

    if-ge v7, v9, :cond_2

    .line 68
    iget v9, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->b:F

    .line 70
    iget v10, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->c:F

    .line 71
    iget v11, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->d:F

    .line 74
    iget-object v12, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->e:Landroid/graphics/Paint;

    .line 75
    iget v13, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->f:F

    .line 76
    iget-object v14, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->g:Landroid/graphics/Paint;

    .line 77
    iget-object v15, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->h:Lcom/swedbank/mobile/core/ui/m;

    add-float/2addr v9, v8

    .line 136
    invoke-virtual {v15}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v16

    add-float v6, v9, v16

    add-float v0, v6, v11

    .line 137
    invoke-virtual {v4, v5, v6, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 138
    invoke-virtual {v2, v4, v14}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    const/4 v0, 0x0

    :goto_1
    const/4 v6, 0x1

    if-ge v0, v6, :cond_0

    .line 141
    iget v6, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v6, v10

    move/from16 v17, v7

    add-float v7, v6, v11

    .line 142
    invoke-virtual {v4, v5, v6, v3, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 143
    invoke-virtual {v2, v4, v14}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    move/from16 v7, v17

    goto :goto_1

    :cond_0
    move/from16 v17, v7

    float-to-int v0, v9

    const/4 v7, 0x0

    .line 150
    invoke-virtual {v15, v7, v0, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->a(IIILandroid/graphics/Canvas;)V

    .line 157
    invoke-virtual {v15}, Lcom/swedbank/mobile/core/ui/m;->a()F

    move-result v0

    add-float/2addr v9, v0

    iput v9, v4, Landroid/graphics/RectF;->bottom:F

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v6, :cond_1

    .line 159
    iget v7, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v7, v11

    sub-float v9, v3, v13

    add-float v14, v7, v10

    .line 160
    invoke-virtual {v4, v13, v7, v9, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 161
    invoke-virtual {v2, v4, v12}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 165
    :cond_1
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v11

    float-to-int v6, v0

    const/4 v7, 0x0

    .line 166
    invoke-virtual {v15, v7, v6, v1, v2}, Lcom/swedbank/mobile/core/ui/m;->b(IIILandroid/graphics/Canvas;)V

    .line 172
    invoke-virtual {v15}, Lcom/swedbank/mobile/core/ui/m;->b()F

    move-result v6

    add-float/2addr v0, v6

    add-float/2addr v8, v0

    add-int/lit8 v0, v17, 0x1

    move v7, v0

    move-object/from16 v0, p0

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected a(IILandroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 16
    .param p3    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/Paint;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    const-string v3, "canvas"

    invoke-static {v1, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "itemPaint"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    const/4 v5, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_0
    const/4 v7, 0x2

    if-ge v5, v7, :cond_1

    .line 90
    iget v8, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->b:F

    .line 91
    iget v9, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->i:F

    .line 92
    iget v10, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->f:F

    sub-float/2addr v8, v9

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v8, v11

    add-float/2addr v8, v6

    move/from16 v11, p1

    int-to-float v12, v11

    const v13, 0x3df5c28f    # 0.12f

    mul-float v13, v13, v12

    add-float/2addr v13, v10

    add-float/2addr v9, v8

    .line 174
    invoke-virtual {v3, v10, v8, v13, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 175
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 97
    iget v8, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->b:F

    add-float/2addr v8, v6

    move v9, v8

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v7, :cond_0

    .line 100
    iget v10, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->d:F

    iget v13, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->j:F

    sub-float/2addr v10, v13

    int-to-float v13, v7

    div-float/2addr v10, v13

    add-float/2addr v10, v9

    .line 101
    iget v14, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->k:F

    iget v15, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->l:F

    add-float/2addr v14, v15

    .line 102
    iget v15, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->k:F

    iget v4, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->l:F

    add-float/2addr v15, v4

    iget v4, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->j:F

    add-float/2addr v15, v4

    iget v4, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->j:F

    add-float/2addr v4, v10

    .line 101
    invoke-virtual {v3, v14, v10, v15, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 103
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 106
    iget v4, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->d:F

    iget v10, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->m:F

    sub-float/2addr v4, v10

    div-float/2addr v4, v13

    add-float/2addr v4, v9

    .line 107
    iget v10, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->k:F

    iget v14, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->j:F

    add-float/2addr v10, v14

    iget v14, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->l:F

    mul-float v14, v14, v13

    add-float/2addr v10, v14

    iget v13, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->n:F

    add-float/2addr v10, v13

    const v13, 0x3ea3d70a    # 0.32f

    mul-float v13, v13, v12

    add-float/2addr v13, v10

    .line 108
    iget v14, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->m:F

    add-float/2addr v14, v4

    invoke-virtual {v3, v10, v4, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    .line 109
    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 111
    iget v4, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->d:F

    iget v10, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->c:F

    add-float/2addr v4, v10

    add-float/2addr v9, v4

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 114
    :cond_0
    iget v4, v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionLoadingView$1$1;->c:F

    sub-float/2addr v9, v4

    add-float/2addr v6, v9

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
