.class public final Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;
.super Lkotlin/e/b/k;
.source "AccountSelectionRecyclerView.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;",
        ">;",
        "Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;",
            ">;",
            "Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    sget v0, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_selection_account_name:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;->c()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    sget v0, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_selection_account_checkbox:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 81
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;->a:Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    invoke-static {v1}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 82
    invoke-virtual {p1}, Lcom/swedbank/mobile/core/ui/widget/j;->a()Landroid/view/View;

    move-result-object p1

    .line 127
    new-instance v1, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;

    invoke-direct {v1, p0, v0, p2}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b$a;-><init>(Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView$b;Landroid/widget/CheckBox;Lcom/swedbank/mobile/app/widget/view/accountselection/a$a;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
