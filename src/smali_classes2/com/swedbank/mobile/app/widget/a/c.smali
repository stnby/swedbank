.class public final Lcom/swedbank/mobile/app/widget/a/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WidgetConfigurationPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/widget/a/j;",
        "Lcom/swedbank/mobile/app/widget/a/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/configuration/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/configuration/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/configuration/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/c;->a:Lcom/swedbank/mobile/business/widget/configuration/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/a/c;)Lcom/swedbank/mobile/business/widget/configuration/c;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/a/c;->a:Lcom/swedbank/mobile/business/widget/configuration/c;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 10

    .line 25
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 26
    sget-object v1, Lcom/swedbank/mobile/app/widget/a/c$j;->a:Lcom/swedbank/mobile/app/widget/a/c$j;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/widget/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 24
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/swedbank/mobile/app/widget/a/c$k;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/widget/a/c$k;-><init>(Lcom/swedbank/mobile/app/widget/a/c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/a/c;->a:Lcom/swedbank/mobile/business/widget/configuration/c;

    invoke-interface {v1}, Lcom/swedbank/mobile/business/widget/configuration/c;->c()Lio/reactivex/w;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v1

    .line 41
    sget-object v2, Lcom/swedbank/mobile/app/widget/a/c$i;->a:Lcom/swedbank/mobile/app/widget/a/c$i;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/app/widget/a/f;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/widget/a/f;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/a/c;->a:Lcom/swedbank/mobile/business/widget/configuration/c;

    invoke-interface {v2}, Lcom/swedbank/mobile/business/widget/configuration/c;->e()Lio/reactivex/o;

    move-result-object v2

    .line 44
    sget-object v3, Lcom/swedbank/mobile/app/widget/a/c$b;->a:Lcom/swedbank/mobile/app/widget/a/c$b;

    check-cast v3, Lkotlin/e/a/b;

    if-eqz v3, :cond_1

    new-instance v4, Lcom/swedbank/mobile/app/widget/a/f;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/widget/a/f;-><init>(Lkotlin/e/a/b;)V

    move-object v3, v4

    :cond_1
    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, "interactor.observeAllAcc\u2026ccountSelectionListItems)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {}, Lcom/swedbank/mobile/app/widget/view/accountselection/b;->a()Lkotlin/e/a/m;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v5, v3, v4, v5}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    .line 46
    sget-object v3, Lcom/swedbank/mobile/app/widget/a/c$c;->a:Lcom/swedbank/mobile/app/widget/a/c$c;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 48
    sget-object v3, Lcom/swedbank/mobile/app/widget/a/c$d;->a:Lcom/swedbank/mobile/app/widget/a/c$d;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/widget/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 49
    new-instance v5, Lcom/swedbank/mobile/app/widget/a/c$e;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/widget/a/c$e;-><init>(Lcom/swedbank/mobile/app/widget/a/c;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v3, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 50
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 51
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 53
    sget-object v5, Lcom/swedbank/mobile/app/widget/a/c$l;->a:Lcom/swedbank/mobile/app/widget/a/c$l;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/widget/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 56
    sget-object v6, Lcom/swedbank/mobile/app/widget/a/c$h;->a:Lcom/swedbank/mobile/app/widget/a/c$h;

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v6

    .line 59
    invoke-static {}, Lkotlin/a/ac;->a()Ljava/util/Set;

    move-result-object v7

    invoke-virtual {v5, v7}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v5

    .line 60
    sget-object v7, Lcom/swedbank/mobile/app/widget/a/c$f;->a:Lcom/swedbank/mobile/app/widget/a/c$f;

    check-cast v7, Lkotlin/e/a/b;

    invoke-virtual {p0, v7}, Lcom/swedbank/mobile/app/widget/a/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v7

    check-cast v7, Lio/reactivex/s;

    invoke-virtual {v5, v7}, Lio/reactivex/o;->e(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v5

    .line 61
    new-instance v7, Lcom/swedbank/mobile/app/widget/a/c$g;

    iget-object v8, p0, Lcom/swedbank/mobile/app/widget/a/c;->a:Lcom/swedbank/mobile/business/widget/configuration/c;

    invoke-direct {v7, v8}, Lcom/swedbank/mobile/app/widget/a/c$g;-><init>(Lcom/swedbank/mobile/business/widget/configuration/c;)V

    check-cast v7, Lkotlin/e/a/b;

    new-instance v8, Lcom/swedbank/mobile/app/widget/a/e;

    invoke-direct {v8, v7}, Lcom/swedbank/mobile/app/widget/a/e;-><init>(Lkotlin/e/a/b;)V

    check-cast v8, Lio/reactivex/c/g;

    invoke-virtual {v5, v8}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v5

    .line 62
    invoke-virtual {v5}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v5

    .line 63
    invoke-virtual {v5}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v5

    const/4 v7, 0x6

    .line 65
    new-array v7, v7, [Lio/reactivex/s;

    const/4 v8, 0x0

    .line 66
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v7, v8

    .line 67
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v7, v4

    const/4 v0, 0x2

    .line 68
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v7, v0

    const/4 v0, 0x3

    .line 69
    check-cast v6, Lio/reactivex/s;

    aput-object v6, v7, v0

    const/4 v0, 0x4

    .line 70
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v7, v0

    const/4 v0, 0x5

    .line 71
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v7, v0

    .line 65
    invoke-static {v7}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026,\n        completeStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v1, Lcom/swedbank/mobile/app/widget/a/m;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lcom/swedbank/mobile/app/widget/a/m;-><init>(Lcom/swedbank/mobile/business/util/e;ZZLkotlin/k;Ljava/util/Set;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/widget/a/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/widget/a/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/widget/a/c$a;-><init>(Lcom/swedbank/mobile/app/widget/a/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 92
    new-instance v3, Lcom/swedbank/mobile/app/widget/a/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/widget/a/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 93
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
