.class public final Lcom/swedbank/mobile/app/widget/a/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "WidgetConfigurationViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/widget/a/j;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lkotlin/f/c;

.field private final c:Lkotlin/f/c;

.field private final d:Lkotlin/f/c;

.field private final e:Lkotlin/f/c;

.field private final f:Lkotlin/f/c;

.field private final g:Lkotlin/f/c;

.field private final h:Lkotlin/f/c;

.field private final i:Lkotlin/f/c;

.field private final j:Lio/reactivex/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x8

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "toolbar"

    const-string v4, "getToolbar()Landroidx/appcompat/widget/Toolbar;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenLoadingView"

    const-string v4, "getFullScreenLoadingView()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "fullScreenErrorViews"

    const-string v4, "getFullScreenErrorViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "accountSelectionViews"

    const-string v4, "getAccountSelectionViews()Landroid/view/View;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorTitleView"

    const-string v4, "getErrorTitleView()Landroid/widget/TextView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "errorRetryBtn"

    const-string v4, "getErrorRetryBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "accountSelectionList"

    const-string v4, "getAccountSelectionList()Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/a/k;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "completeBtn"

    const-string v4, "getCompleteBtn()Landroid/widget/Button;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/o;)V
    .locals 1
    .param p1    # Lio/reactivex/o;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "backClicks"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->j:Lio/reactivex/o;

    .line 29
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->toolbar:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->b:Lkotlin/f/c;

    .line 30
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_loading_view:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->c:Lkotlin/f/c;

    .line 31
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_error_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->d:Lkotlin/f/c;

    .line 32
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_account_selection_group:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->e:Lkotlin/f/c;

    .line 33
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_error_title:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->f:Lkotlin/f/c;

    .line 34
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_error_retry_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->g:Lkotlin/f/c;

    .line 35
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_account_selection:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->h:Lkotlin/f/c;

    .line 36
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_conf_complete_btn:I

    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/am;->a(Lcom/swedbank/mobile/architect/a/b/a;I)Lkotlin/f/c;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/k;->i:Lkotlin/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/widget/TextView;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->j()Landroid/widget/TextView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/content/Context;
    .locals 0

    .line 26
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/widget/a/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->h()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->g()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/widget/Button;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->q()Landroid/widget/Button;

    move-result-object p0

    return-object p0
.end method

.method private final f()Landroidx/appcompat/widget/Toolbar;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->b:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    return-object v0
.end method

.method public static final synthetic f(Lcom/swedbank/mobile/app/widget/a/k;)Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->p()Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    move-result-object p0

    return-object p0
.end method

.method private final g()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->c:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static final synthetic g(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->i()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private final h()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->d:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final i()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->e:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final j()Landroid/widget/TextView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->f:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private final k()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->g:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method

.method private final p()Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->h:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    return-object v0
.end method

.method private final q()Landroid/widget/Button;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->i:Lkotlin/f/c;

    sget-object v1, Lcom/swedbank/mobile/app/widget/a/k;->a:[Lkotlin/h/g;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/c;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 38
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->q()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 98
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public a(Lcom/swedbank/mobile/app/widget/a/m;)V
    .locals 14
    .param p1    # Lcom/swedbank/mobile/app/widget/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->g(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->c(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->d(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->b()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 104
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->d(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 53
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    .line 106
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->a(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/widget/TextView;

    move-result-object v0

    .line 107
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->b(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/content/Context;

    move-result-object v2

    .line 108
    sget v3, Lcom/swedbank/mobile/app/widget/b$g;->widget_conf_general_loading_error:I

    .line 111
    instance-of v4, p1, Lcom/swedbank/mobile/business/util/e$b;

    if-eqz v4, :cond_2

    check-cast p1, Lcom/swedbank/mobile/business/util/e$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$b;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 113
    move-object v4, p1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_1

    move-object v5, p1

    check-cast v5, Ljava/lang/Iterable;

    const-string p1, "\n"

    move-object v6, p1

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 116
    :cond_2
    instance-of v3, p1, Lcom/swedbank/mobile/business/util/e$a;

    if-eqz v3, :cond_3

    check-cast p1, Lcom/swedbank/mobile/business/util/e$a;

    invoke-virtual {p1}, Lcom/swedbank/mobile/business/util/e$a;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    .line 117
    invoke-static {p1}, Lcom/swedbank/mobile/app/w/c;->a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p1

    invoke-virtual {v2, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "context.getString(it.toF\u2026r().userDisplayedMessage)"

    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    const-string v2, "fold(\n    ifLeft = { con\u2026al_error)\n      }\n    }\n)"

    .line 118
    invoke-static {p1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 119
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->c(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 117
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 54
    :cond_4
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->d()Lkotlin/k;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/k;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 128
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->a(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/widget/TextView;

    move-result-object p1

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->b(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/swedbank/mobile/app/widget/b$g;->widget_conf_no_accounts_message:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->c(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 56
    :cond_5
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->e()Ljava/util/Set;

    move-result-object v0

    .line 57
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->d()Lkotlin/k;

    move-result-object v2

    .line 58
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/a/m;->c()Z

    move-result p1

    .line 137
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->e(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/widget/Button;->setEnabled(Z)V

    if-eqz v0, :cond_6

    .line 138
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->f(Lcom/swedbank/mobile/app/widget/a/k;)Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->setInitialSelectedAccounts(Ljava/util/Set;)V

    .line 139
    :cond_6
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->f(Lcom/swedbank/mobile/app/widget/a/k;)Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    move-result-object p1

    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a(Lkotlin/k;)V

    .line 140
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/a/k;->g(Lcom/swedbank/mobile/app/widget/a/k;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/swedbank/mobile/app/widget/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/a/k;->a(Lcom/swedbank/mobile/app/widget/a/m;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/k;->j:Lio/reactivex/o;

    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v1

    invoke-static {v1}, Lcom/b/b/a/a;->b(Landroidx/appcompat/widget/Toolbar;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->d(Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "backClicks.mergeWith(toolbar.navigationClicks())"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 40
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->k()Landroid/widget/Button;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 99
    new-instance v1, Lcom/swedbank/mobile/core/ui/an;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/core/ui/an;-><init>(Landroid/view/View;)V

    check-cast v1, Lio/reactivex/o;

    return-object v1
.end method

.method public d()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->p()Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method protected e()V
    .locals 2

    .line 44
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->p()Lcom/swedbank/mobile/app/widget/view/accountselection/AccountSelectionRecyclerView;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0, v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Landroidx/appcompat/widget/Toolbar;Landroidx/recyclerview/widget/RecyclerView;)V

    .line 45
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/a/k;->f()Landroidx/appcompat/widget/Toolbar;

    move-result-object v0

    sget v1, Lcom/swedbank/mobile/app/widget/b$g;->widget_conf_toolbar_title:I

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(I)V

    return-void
.end method
