.class public final Lcom/swedbank/mobile/app/widget/a/h;
.super Lcom/swedbank/mobile/architect/a/h;
.source "WidgetConfigurationRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/widget/configuration/g;


# instance fields
.field private final e:Lcom/swedbank/mobile/data/device/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/data/device/a;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/f;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/data/device/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_widget_configuration"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/architect/a/b/f;
        .annotation runtime Ljavax/inject/Named;
            value = "for_widget_configuration"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/data/device/a;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/f;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activityManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewDetails"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0, p2, p4, p3}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/a/h;->e:Lcom/swedbank/mobile/data/device/a;

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    .line 27
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/a/h;->e:Lcom/swedbank/mobile/data/device/a;

    new-instance v1, Lcom/swedbank/mobile/app/widget/a/h$a;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/app/widget/a/h$a;-><init>(Z)V

    check-cast v1, Lkotlin/e/a/b;

    const/4 p1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, p1, v1, v2, p1}, Lcom/swedbank/mobile/data/device/a$a;->a(Lcom/swedbank/mobile/data/device/a;Lkotlin/e/a/a;Lkotlin/e/a/b;ILjava/lang/Object;)V

    return-void
.end method
