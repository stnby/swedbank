.class final synthetic Lcom/swedbank/mobile/app/widget/a/c$i;
.super Lkotlin/e/b/i;
.source "WidgetConfigurationPresenter.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/b<",
        "Ljava/util/Set<",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Lcom/swedbank/mobile/app/widget/a/m$a$d;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/widget/a/c$i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/widget/a/c$i;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/widget/a/c$i;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/widget/a/c$i;->a:Lcom/swedbank/mobile/app/widget/a/c$i;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/e/b/i;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;)Lcom/swedbank/mobile/app/widget/a/m$a$d;
    .locals 1
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/app/widget/a/m$a$d;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/swedbank/mobile/app/widget/a/m$a$d;

    .line 41
    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/widget/a/m$a$d;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/widget/a/m$a$d;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/a/c$i;->a(Ljava/util/Set;)Lcom/swedbank/mobile/app/widget/a/m$a$d;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "<init>(Ljava/util/Set;)V"

    return-object v0
.end method
