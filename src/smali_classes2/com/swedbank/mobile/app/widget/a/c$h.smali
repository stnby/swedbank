.class final Lcom/swedbank/mobile/app/widget/a/c$h;
.super Ljava/lang/Object;
.source "WidgetConfigurationPresenter.kt"

# interfaces
.implements Lio/reactivex/c/h;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/a/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/c/h<",
        "TT;TR;>;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/widget/a/c$h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/widget/a/c$h;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/widget/a/c$h;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/widget/a/c$h;->a:Lcom/swedbank/mobile/app/widget/a/c$h;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;)Lcom/swedbank/mobile/app/widget/a/m$a$c;
    .locals 1
    .param p1    # Ljava/util/Set;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/swedbank/mobile/app/widget/a/m$a$c;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    new-instance v0, Lcom/swedbank/mobile/app/widget/a/m$a$c;

    invoke-direct {v0, p1}, Lcom/swedbank/mobile/app/widget/a/m$a$c;-><init>(Z)V

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/a/c$h;->a(Ljava/util/Set;)Lcom/swedbank/mobile/app/widget/a/m$a$c;

    move-result-object p1

    return-object p1
.end method
