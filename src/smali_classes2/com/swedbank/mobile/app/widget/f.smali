.class public final Lcom/swedbank/mobile/app/widget/f;
.super Ljava/lang/Object;
.source "WidgetService.kt"


# instance fields
.field private final a:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/business/cards/g;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/cards/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "digitizedCardId"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/f;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/f;->b:Lcom/swedbank/mobile/business/cards/g;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/swedbank/mobile/business/cards/g;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/f;->b:Lcom/swedbank/mobile/business/cards/g;

    return-object v0
.end method
