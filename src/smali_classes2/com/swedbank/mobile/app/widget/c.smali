.class public final Lcom/swedbank/mobile/app/widget/c;
.super Ljava/lang/Object;
.source "WidgetIntentHandler.kt"

# interfaces
.implements Lcom/swedbank/mobile/data/device/h;


# instance fields
.field private final a:Lcom/swedbank/mobile/architect/business/a/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/swedbank/mobile/core/a/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/architect/business/a/c;Lcom/swedbank/mobile/core/a/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/architect/business/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/core/a/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/architect/business/a/c<",
            "Lcom/swedbank/mobile/business/root/c;",
            ">;",
            "Lcom/swedbank/mobile/core/a/c;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flowManager"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adobeAnalyticsManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/c;->a:Lcom/swedbank/mobile/architect/business/a/c;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/c;->b:Lcom/swedbank/mobile/core/a/c;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Z
    .locals 5
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "intent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto/16 :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0x11d33edc

    const/4 v4, 0x1

    if-eq v2, v3, :cond_5

    const v3, 0x40a164bd

    if-eq v2, v3, :cond_3

    const v3, 0x6088c873

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "extra_account_shown_status"

    .line 32
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c;->a:Lcom/swedbank/mobile/architect/business/a/c;

    new-instance v1, Lcom/swedbank/mobile/business/widget/update/b;

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/widget/update/b;-><init>(Z)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    if-eqz p1, :cond_2

    .line 35
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/c;->b:Lcom/swedbank/mobile/core/a/c;

    sget v0, Lcom/swedbank/mobile/app/widget/b$d;->widget_update_balance:I

    sget-object v1, Lcom/swedbank/mobile/core/a/f;->a:Lcom/swedbank/mobile/core/a/f;

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/core/a/c;->a(ILcom/swedbank/mobile/core/a/f;)V

    :cond_2
    return v4

    :cond_3
    const-string v2, "com.swedbank.mobile.widget.LIST_ITEM_CLICK"

    .line 26
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "extra_digitized_card_id"

    .line 40
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 41
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c;->a:Lcom/swedbank/mobile/architect/business/a/c;

    new-instance v1, Lcom/swedbank/mobile/business/cards/wallet/payment/e;

    invoke-static {p1}, Lcom/swedbank/mobile/business/util/f;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/business/util/e;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/swedbank/mobile/business/cards/wallet/payment/e;-><init>(Lcom/swedbank/mobile/business/util/e;)V

    check-cast v1, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    .line 42
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/c;->b:Lcom/swedbank/mobile/core/a/c;

    sget v0, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_contactless:I

    sget-object v1, Lcom/swedbank/mobile/core/a/f;->a:Lcom/swedbank/mobile/core/a/f;

    invoke-interface {p1, v0, v1}, Lcom/swedbank/mobile/core/a/c;->a(ILcom/swedbank/mobile/core/a/f;)V

    :cond_4
    return v4

    :cond_5
    const-string v2, "android.appwidget.action.APPWIDGET_CONFIGURE"

    .line 26
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 28
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/c;->a:Lcom/swedbank/mobile/architect/business/a/c;

    sget-object v0, Lcom/swedbank/mobile/business/widget/b;->a:Lcom/swedbank/mobile/business/widget/b;

    check-cast v0, Lcom/swedbank/mobile/architect/business/a/b;

    invoke-interface {p1, v0}, Lcom/swedbank/mobile/architect/business/a/c;->a(Lcom/swedbank/mobile/architect/business/a/b;)V

    return v4

    :cond_6
    :goto_0
    const-string v0, "extra_is_app_launch_click"

    .line 47
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 48
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/c;->b:Lcom/swedbank/mobile/core/a/c;

    sget v0, Lcom/swedbank/mobile/app/widget/b$d;->widget_top_icon:I

    sget-object v2, Lcom/swedbank/mobile/core/a/f;->a:Lcom/swedbank/mobile/core/a/f;

    invoke-interface {p1, v0, v2}, Lcom/swedbank/mobile/core/a/c;->a(ILcom/swedbank/mobile/core/a/f;)V

    :cond_7
    return v1
.end method
