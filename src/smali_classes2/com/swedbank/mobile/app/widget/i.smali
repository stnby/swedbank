.class public final Lcom/swedbank/mobile/app/widget/i;
.super Ljava/lang/Object;
.source "WidgetRouter.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/widget/h;


# instance fields
.field private final a:Landroid/appwidget/AppWidgetManager;

.field private final b:Ljava/util/Random;

.field private final c:Landroid/app/Application;

.field private final d:Lcom/swedbank/mobile/business/e/i;

.field private final e:Lcom/swedbank/mobile/business/widget/configuration/h;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/swedbank/mobile/business/e/i;Lcom/swedbank/mobile/business/widget/configuration/h;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/business/e/i;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/business/widget/configuration/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceRepository"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "widgetRepository"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/i;->d:Lcom/swedbank/mobile/business/e/i;

    iput-object p3, p0, Lcom/swedbank/mobile/app/widget/i;->e:Lcom/swedbank/mobile/business/widget/configuration/h;

    .line 36
    iget-object p1, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    check-cast p1, Landroid/content/Context;

    const-string p2, "appwidget"

    .line 296
    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/appwidget/AppWidgetManager;

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/i;->a:Landroid/appwidget/AppWidgetManager;

    .line 37
    new-instance p1, Ljava/util/Random;

    invoke-direct {p1}, Ljava/util/Random;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/i;->b:Ljava/util/Random;

    return-void

    .line 296
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.appwidget.AppWidgetManager"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/i;->b:Ljava/util/Random;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/e/i;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/i;->d:Lcom/swedbank/mobile/business/e/i;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/widget/configuration/h;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/i;->e:Lcom/swedbank/mobile/business/widget/configuration/h;

    return-object p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/i;->a:Landroid/appwidget/AppWidgetManager;

    return-object p0
.end method


# virtual methods
.method public a()V
    .locals 8

    const/4 v0, 0x0

    .line 141
    check-cast v0, [I

    .line 143
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 144
    new-instance v1, Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/swedbank/mobile/app/widget/AppWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 40
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/swedbank/mobile/app/widget/b$e;->view_widget_disabled:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 41
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_top_bar:I

    .line 147
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 148
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    .line 149
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->c(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v5

    .line 152
    invoke-interface {v5}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extra_is_app_launch_click"

    const/4 v7, 0x1

    .line 151
    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x8000000

    .line 146
    invoke-static {v3, v4, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 155
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 160
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 161
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    .line 162
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->c(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v4

    invoke-interface {v4}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.appwidget.action.APPWIDGET_CONFIGURE"

    .line 163
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const v5, 0x10008000

    .line 164
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 159
    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 167
    sget v3, Lcom/swedbank/mobile/app/widget/b$d;->widget_setup_action_btn:I

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 169
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method public a(Z[I)V
    .locals 9
    .param p2    # [I
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    if-eqz p2, :cond_0

    goto :goto_0

    .line 176
    :cond_0
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object p2

    .line 177
    new-instance v0, Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, Lcom/swedbank/mobile/app/widget/AppWidgetProvider;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object p2

    :goto_0
    if-eqz p2, :cond_1

    move-object v0, p2

    goto :goto_1

    .line 180
    :cond_1
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 181
    new-instance v1, Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/swedbank/mobile/app/widget/AppWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 47
    :goto_1
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/swedbank/mobile/app/widget/b$e;->view_widget:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 48
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_top_icon:I

    .line 184
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 185
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    .line 186
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->c(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v5

    .line 189
    invoke-interface {v5}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extra_is_app_launch_click"

    const/4 v7, 0x1

    .line 188
    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x8000000

    .line 183
    invoke-static {v3, v4, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 192
    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 197
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 198
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    .line 199
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->c(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v4

    invoke-interface {v4}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "android.appwidget.action.APPWIDGET_CONFIGURE"

    .line 200
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const v5, 0x10008000

    .line 201
    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 196
    invoke-static {v2, v3, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 204
    sget v3, Lcom/swedbank/mobile/app/widget/b$d;->widget_setup_action_btn:I

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    .line 52
    sget v4, Lcom/swedbank/mobile/app/widget/b$d;->widget_update_balance:I

    invoke-virtual {v1, v4, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 53
    sget v4, Lcom/swedbank/mobile/app/widget/b$d;->widget_toggle_balance:I

    sget v8, Lcom/swedbank/mobile/app/widget/b$c;->ic_visibility_off:I

    invoke-virtual {v1, v4, v8}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_2

    .line 55
    :cond_2
    sget v4, Lcom/swedbank/mobile/app/widget/b$d;->widget_update_balance:I

    invoke-virtual {v1, v4, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 56
    sget v4, Lcom/swedbank/mobile/app/widget/b$d;->widget_toggle_balance:I

    sget v8, Lcom/swedbank/mobile/app/widget/b$c;->ic_visibility:I

    invoke-virtual {v1, v4, v8}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 58
    :goto_2
    sget v4, Lcom/swedbank/mobile/app/widget/b$d;->widget_toggle_balance:I

    invoke-virtual {v1, v4, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 59
    sget v3, Lcom/swedbank/mobile/app/widget/b$d;->widget_loading_view:I

    invoke-virtual {v1, v3, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 61
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_toggle_balance:I

    xor-int/2addr p1, v7

    .line 211
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    .line 213
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    .line 215
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->d(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v8

    invoke-interface {v8, p1}, Lcom/swedbank/mobile/business/widget/configuration/h;->b(Z)Landroid/content/Intent;

    move-result-object p1

    const/high16 v8, 0x10000000

    .line 212
    invoke-static {v4, v3, p1, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    .line 210
    invoke-virtual {v1, v2, p1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 62
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_update_balance:I

    .line 218
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 220
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 222
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->d(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v4

    invoke-interface {v4, v7}, Lcom/swedbank/mobile/business/widget/configuration/h;->b(Z)Landroid/content/Intent;

    move-result-object v4

    .line 219
    invoke-static {v3, v2, v4, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 217
    invoke-virtual {v1, p1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 64
    new-instance p1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/swedbank/mobile/app/widget/WidgetService;

    invoke-direct {p1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_accounts:I

    invoke-virtual {v1, v2, p1}, Landroid/widget/RemoteViews;->setRemoteAdapter(ILandroid/content/Intent;)V

    .line 66
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_accounts:I

    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_no_accounts:I

    invoke-virtual {v1, p1, v2}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    .line 225
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    .line 226
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->b(Lcom/swedbank/mobile/app/widget/i;)Ljava/util/Random;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 227
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->c(Lcom/swedbank/mobile/app/widget/i;)Lcom/swedbank/mobile/business/e/i;

    move-result-object v3

    invoke-interface {v3}, Lcom/swedbank/mobile/business/e/i;->e()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.swedbank.mobile.widget.LIST_ITEM_CLICK"

    .line 228
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 224
    invoke-static {p1, v2, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p1

    .line 232
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_accounts:I

    invoke-virtual {v1, v2, p1}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 234
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 240
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object p1

    sget v0, Lcom/swedbank/mobile/app/widget/b$d;->widget_accounts:I

    invoke-virtual {p1, p2, v0}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    return-void
.end method

.method public b()V
    .locals 4

    const/4 v0, 0x0

    .line 242
    check-cast v0, [I

    .line 244
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 245
    new-instance v1, Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->a(Lcom/swedbank/mobile/app/widget/i;)Landroid/app/Application;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/swedbank/mobile/app/widget/AppWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 72
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/i;->c:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/swedbank/mobile/app/widget/b$e;->view_widget:I

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 73
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_loading_view:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 74
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_update_balance:I

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 75
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_toggle_balance:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 247
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/i;->e(Lcom/swedbank/mobile/app/widget/i;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    return-void
.end method
