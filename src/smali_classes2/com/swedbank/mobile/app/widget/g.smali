.class public final Lcom/swedbank/mobile/app/widget/g;
.super Ljava/lang/Object;
.source "WidgetService.kt"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/widget/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lkotlin/d;

.field private final d:Lkotlin/d;

.field private final e:Lkotlin/d;

.field private final f:Lkotlin/d;

.field private final g:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x4

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/g;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "resources"

    const-string v4, "getResources()Landroid/content/res/Resources;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/g;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardWidth"

    const-string v4, "getCardWidth()I"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/g;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardHeight"

    const-string v4, "getCardHeight()I"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lkotlin/e/b/t;

    const-class v2, Lcom/swedbank/mobile/app/widget/g;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "cardCornerRadius"

    const-string v4, "getCardCornerRadius()F"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/t;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/s;)Lkotlin/h/i;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/widget/g;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/g;->g:Landroid/content/Context;

    .line 36
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/g;->b:Ljava/util/List;

    .line 38
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/widget/g$d;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/widget/g$d;-><init>(Lcom/swedbank/mobile/app/widget/g;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/g;->c:Lkotlin/d;

    .line 39
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/widget/g$c;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/widget/g$c;-><init>(Lcom/swedbank/mobile/app/widget/g;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/g;->d:Lkotlin/d;

    .line 40
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/widget/g$b;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/widget/g$b;-><init>(Lcom/swedbank/mobile/app/widget/g;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/g;->e:Lkotlin/d;

    .line 41
    sget-object p1, Lkotlin/i;->c:Lkotlin/i;

    new-instance v0, Lcom/swedbank/mobile/app/widget/g$a;

    invoke-direct {v0, p0}, Lcom/swedbank/mobile/app/widget/g$a;-><init>(Lcom/swedbank/mobile/app/widget/g;)V

    check-cast v0, Lkotlin/e/a/a;

    invoke-static {p1, v0}, Lkotlin/e;->a(Lkotlin/i;Lkotlin/e/a/a;)Lkotlin/d;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/g;->f:Lkotlin/d;

    return-void
.end method

.method private final a()Landroid/content/res/Resources;
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->c:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/widget/g;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    return-object v0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/g;)Landroid/content/res/Resources;
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/g;->a()Landroid/content/res/Resources;

    move-result-object p0

    return-object p0
.end method

.method private final b()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->d:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/widget/g;->a:[Lkotlin/h/g;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/widget/g;)I
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/g;->b()I

    move-result p0

    return p0
.end method

.method private final c()I
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->e:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/widget/g;->a:[Lkotlin/h/g;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/widget/g;)I
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/g;->c()I

    move-result p0

    return p0
.end method

.method private final d()F
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->f:Lkotlin/d;

    sget-object v1, Lcom/swedbank/mobile/app/widget/g;->a:[Lkotlin/h/g;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/d;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/widget/g;)F
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/swedbank/mobile/app/widget/g;->d()F

    move-result p0

    return p0
.end method

.method public static final synthetic e(Lcom/swedbank/mobile/app/widget/g;)Landroid/content/Context;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/g;->g:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 11
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/widget/e;

    .line 51
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/g;->g:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/swedbank/mobile/app/widget/b$e;->item_widget_account:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 52
    sget v1, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_title:I

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/e;->a()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 53
    sget v1, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_subtitle:I

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/e;->b()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 54
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/g;->g:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/e;->c()Lcom/swedbank/mobile/app/widget/f;

    move-result-object p1

    if-nez p1, :cond_0

    .line 156
    sget p1, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_contactless:I

    const/16 v1, 0x8

    invoke-virtual {v0, p1, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 158
    :cond_0
    sget v2, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_contactless:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 159
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/f;->b()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v2

    .line 167
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/g;->a(Lcom/swedbank/mobile/app/widget/g;)Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "resources"

    invoke-static {v4, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    sget-object v5, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {v5, v2}, Lcom/swedbank/mobile/app/cards/g;->a(Lcom/swedbank/mobile/business/cards/g;)I

    move-result v5

    .line 169
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/g;->b(Lcom/swedbank/mobile/app/widget/g;)I

    move-result v6

    .line 170
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/g;->c(Lcom/swedbank/mobile/app/widget/g;)I

    move-result v7

    const/4 v8, 0x0

    const/16 v9, 0x10

    const/4 v10, 0x0

    .line 161
    invoke-static/range {v4 .. v10}, Lcom/swedbank/mobile/core/ui/e;->a(Landroid/content/res/Resources;IIILcom/swedbank/mobile/core/ui/w;ILjava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 172
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v4}, Landroidx/core/graphics/drawable/d;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroidx/core/graphics/drawable/b;

    move-result-object v1

    const-string v4, "RoundedBitmapDrawableFac\u2026xt.resources, cardBitmap)"

    invoke-static {v1, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-static {p0}, Lcom/swedbank/mobile/app/widget/g;->d(Lcom/swedbank/mobile/app/widget/g;)F

    move-result v4

    invoke-virtual {v1, v4}, Landroidx/core/graphics/drawable/b;->a(F)V

    .line 174
    invoke-virtual {v1}, Landroidx/core/graphics/drawable/b;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroidx/core/graphics/drawable/b;->getIntrinsicHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 175
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 176
    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v6

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v7

    invoke-virtual {v1, v3, v3, v6, v7}, Landroidx/core/graphics/drawable/b;->setBounds(IIII)V

    .line 177
    invoke-virtual {v1, v5}, Landroidx/core/graphics/drawable/b;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_2

    .line 180
    sget v1, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_card_image:I

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 182
    :cond_2
    sget v1, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_contactless_image:I

    .line 184
    sget-object v3, Lcom/swedbank/mobile/app/cards/g;->a:Lcom/swedbank/mobile/app/cards/g;

    invoke-virtual {v3, v2}, Lcom/swedbank/mobile/app/cards/g;->c(Lcom/swedbank/mobile/business/cards/g;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Lcom/swedbank/mobile/app/widget/b$c;->ic_contactless_light:I

    goto :goto_1

    .line 185
    :cond_3
    sget v2, Lcom/swedbank/mobile/app/widget/b$c;->ic_contactless_dark:I

    .line 182
    :goto_1
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 187
    sget v1, Lcom/swedbank/mobile/app/widget/b$d;->widget_account_contactless:I

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "extra_digitized_card_id"

    .line 188
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/f;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 187
    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    :goto_2
    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 0

    return-void
.end method

.method public onDataSetChanged()V
    .locals 8

    .line 66
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/swedbank/mobile/a/ae/d;->c(Landroid/content/Context;)Lcom/swedbank/mobile/business/widget/configuration/h;

    move-result-object v0

    .line 67
    sget-object v1, Lio/reactivex/i/e;->a:Lio/reactivex/i/e;

    .line 68
    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/configuration/h;->d()Lio/reactivex/w;

    move-result-object v2

    check-cast v2, Lio/reactivex/aa;

    .line 69
    invoke-interface {v0}, Lcom/swedbank/mobile/business/widget/configuration/h;->e()Lio/reactivex/o;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object v0

    const-string v3, "observeIsBalanceShown().firstOrError()"

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/aa;

    .line 67
    invoke-virtual {v1, v2, v0}, Lio/reactivex/i/e;->a(Lio/reactivex/aa;Lio/reactivex/aa;)Lio/reactivex/w;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lio/reactivex/w;->b()Ljava/lang/Object;

    move-result-object v0

    .line 71
    check-cast v0, Lkotlin/k;

    invoke-virtual {v0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    const-string v2, "enrolledAccounts"

    .line 72
    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 192
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 193
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 194
    check-cast v3, Lcom/swedbank/mobile/business/widget/configuration/a;

    .line 75
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/widget/configuration/a;->a()Lcom/swedbank/mobile/business/a/a;

    move-result-object v4

    const-string v5, "isBalanceShown"

    .line 76
    invoke-static {v0, v5}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 196
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/a/a;->e()Lcom/swedbank/mobile/business/a/d;

    move-result-object v4

    .line 200
    invoke-virtual {v4}, Lcom/swedbank/mobile/business/a/d;->a()Lcom/swedbank/mobile/business/c;

    move-result-object v4

    .line 199
    invoke-static {v4}, Lcom/swedbank/mobile/app/a;->a(Lcom/swedbank/mobile/business/c;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 204
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*** "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/swedbank/mobile/business/a/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 78
    :goto_1
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/widget/configuration/a;->a()Lcom/swedbank/mobile/business/a/a;

    move-result-object v5

    .line 208
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/a/a;->c()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v5}, Lcom/swedbank/mobile/business/a/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/swedbank/mobile/app/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 79
    :goto_2
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/widget/configuration/a;->b()Lcom/swedbank/mobile/business/widget/configuration/b;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 210
    new-instance v5, Lcom/swedbank/mobile/app/widget/f;

    .line 211
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/widget/configuration/b;->a()Ljava/lang/String;

    move-result-object v7

    .line 212
    invoke-virtual {v3}, Lcom/swedbank/mobile/business/widget/configuration/b;->b()Lcom/swedbank/mobile/business/cards/g;

    move-result-object v3

    .line 210
    invoke-direct {v5, v7, v3}, Lcom/swedbank/mobile/app/widget/f;-><init>(Ljava/lang/String;Lcom/swedbank/mobile/business/cards/g;)V

    goto :goto_3

    :cond_2
    const/4 v5, 0x0

    .line 73
    :goto_3
    new-instance v3, Lcom/swedbank/mobile/app/widget/e;

    invoke-direct {v3, v4, v6, v5}, Lcom/swedbank/mobile/app/widget/e;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/swedbank/mobile/app/widget/f;)V

    .line 80
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_3
    check-cast v2, Ljava/util/List;

    .line 71
    iput-object v2, p0, Lcom/swedbank/mobile/app/widget/g;->b:Ljava/util/List;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    .line 59
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/swedbank/mobile/app/widget/g;->b:Ljava/util/List;

    return-void
.end method
