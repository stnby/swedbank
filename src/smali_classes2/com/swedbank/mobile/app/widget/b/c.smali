.class public final Lcom/swedbank/mobile/app/widget/b/c;
.super Lcom/swedbank/mobile/architect/a/d;
.source "WidgetPreferencesPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/widget/b/i;",
        "Lcom/swedbank/mobile/app/widget/b/l;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/widget/preferences/a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/widget/preferences/a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/widget/preferences/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/b/c;->a:Lcom/swedbank/mobile/business/widget/preferences/a;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/b/c;)Lcom/swedbank/mobile/business/widget/preferences/a;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/b/c;->a:Lcom/swedbank/mobile/business/widget/preferences/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .line 24
    sget-object v0, Lkotlin/s;->a:Lkotlin/s;

    invoke-static {v0}, Lio/reactivex/o;->d(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    check-cast v0, Lio/reactivex/s;

    .line 25
    sget-object v1, Lcom/swedbank/mobile/app/widget/b/c$g;->a:Lcom/swedbank/mobile/app/widget/b/c$g;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/widget/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    check-cast v1, Lio/reactivex/s;

    .line 23
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 26
    new-instance v1, Lcom/swedbank/mobile/app/widget/b/c$h;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/widget/b/c$h;-><init>(Lcom/swedbank/mobile/app/widget/b/c;)V

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->m(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/b/c;->a:Lcom/swedbank/mobile/business/widget/preferences/a;

    .line 40
    invoke-interface {v1}, Lcom/swedbank/mobile/business/widget/preferences/a;->c()Lio/reactivex/w;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lio/reactivex/w;->f()Lio/reactivex/o;

    move-result-object v1

    .line 42
    sget-object v2, Lcom/swedbank/mobile/app/widget/b/c$f;->a:Lcom/swedbank/mobile/app/widget/b/c$f;

    check-cast v2, Lkotlin/e/a/b;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/swedbank/mobile/app/widget/b/e;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/widget/b/e;-><init>(Lkotlin/e/a/b;)V

    move-object v2, v3

    :cond_0
    check-cast v2, Lio/reactivex/c/h;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v1

    .line 44
    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/b/c;->a:Lcom/swedbank/mobile/business/widget/preferences/a;

    .line 45
    invoke-interface {v2}, Lcom/swedbank/mobile/business/widget/preferences/a;->d()Lio/reactivex/o;

    move-result-object v2

    .line 46
    sget-object v3, Lcom/swedbank/mobile/app/widget/b/c$b;->a:Lcom/swedbank/mobile/app/widget/b/c$b;

    check-cast v3, Lkotlin/e/a/b;

    if-eqz v3, :cond_1

    new-instance v4, Lcom/swedbank/mobile/app/widget/b/e;

    invoke-direct {v4, v3}, Lcom/swedbank/mobile/app/widget/b/e;-><init>(Lkotlin/e/a/b;)V

    move-object v3, v4

    :cond_1
    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    const-string v3, "interactor\n        .obse\u2026ccountSelectionListItems)"

    invoke-static {v2, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {}, Lcom/swedbank/mobile/app/widget/view/accountselection/b;->a()Lkotlin/e/a/m;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v2, v5, v3, v4, v5}, Lcom/swedbank/mobile/core/ui/k;->a(Lio/reactivex/o;Lkotlin/e/a/m;Lkotlin/e/a/m;ILjava/lang/Object;)Lio/reactivex/o;

    move-result-object v2

    .line 48
    sget-object v3, Lcom/swedbank/mobile/app/widget/b/c$c;->a:Lcom/swedbank/mobile/app/widget/b/c$c;

    check-cast v3, Lio/reactivex/c/h;

    invoke-virtual {v2, v3}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v2

    .line 50
    sget-object v3, Lcom/swedbank/mobile/app/widget/b/c$d;->a:Lcom/swedbank/mobile/app/widget/b/c$d;

    check-cast v3, Lkotlin/e/a/b;

    invoke-virtual {p0, v3}, Lcom/swedbank/mobile/app/widget/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v3

    .line 51
    new-instance v5, Lcom/swedbank/mobile/app/widget/b/c$e;

    invoke-direct {v5, p0}, Lcom/swedbank/mobile/app/widget/b/c$e;-><init>(Lcom/swedbank/mobile/app/widget/b/c;)V

    check-cast v5, Lio/reactivex/c/g;

    invoke-virtual {v3, v5}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v3

    .line 52
    invoke-virtual {v3}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v3

    .line 53
    invoke-virtual {v3}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v3

    .line 55
    sget-object v5, Lcom/swedbank/mobile/app/widget/b/c$i;->a:Lcom/swedbank/mobile/app/widget/b/c$i;

    check-cast v5, Lkotlin/e/a/b;

    invoke-virtual {p0, v5}, Lcom/swedbank/mobile/app/widget/b/c;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v5

    .line 56
    new-instance v6, Lcom/swedbank/mobile/app/widget/b/c$j;

    invoke-direct {v6, p0}, Lcom/swedbank/mobile/app/widget/b/c$j;-><init>(Lcom/swedbank/mobile/app/widget/b/c;)V

    check-cast v6, Lio/reactivex/c/h;

    invoke-virtual {v5, v6}, Lio/reactivex/o;->b(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v5

    const/4 v6, 0x5

    .line 62
    new-array v6, v6, [Lio/reactivex/s;

    const/4 v7, 0x0

    .line 63
    check-cast v0, Lio/reactivex/s;

    aput-object v0, v6, v7

    .line 64
    check-cast v2, Lio/reactivex/s;

    aput-object v2, v6, v4

    const/4 v0, 0x2

    .line 65
    check-cast v1, Lio/reactivex/s;

    aput-object v1, v6, v0

    const/4 v0, 0x3

    .line 66
    check-cast v5, Lio/reactivex/s;

    aput-object v5, v6, v0

    const/4 v0, 0x4

    .line 67
    check-cast v3, Lio/reactivex/s;

    aput-object v3, v6, v0

    .line 62
    invoke-static {v6}, Lio/reactivex/o;->b([Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.mergeArray(\n \u2026,\n        completeStream)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    new-instance v1, Lcom/swedbank/mobile/app/widget/b/l;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v8}, Lcom/swedbank/mobile/app/widget/b/l;-><init>(Lcom/swedbank/mobile/business/util/e;ZLkotlin/k;Ljava/util/Set;ILkotlin/e/b/g;)V

    new-instance v2, Lcom/swedbank/mobile/app/widget/b/c$a;

    move-object v3, p0

    check-cast v3, Lcom/swedbank/mobile/app/widget/b/c;

    invoke-direct {v2, v3}, Lcom/swedbank/mobile/app/widget/b/c$a;-><init>(Lcom/swedbank/mobile/app/widget/b/c;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 88
    new-instance v3, Lcom/swedbank/mobile/app/widget/b/d;

    invoke-direct {v3, v2}, Lcom/swedbank/mobile/app/widget/b/d;-><init>(Lkotlin/e/a/m;)V

    check-cast v3, Lio/reactivex/c/c;

    invoke-virtual {v0, v1, v3}, Lio/reactivex/o;->a(Ljava/lang/Object;Lio/reactivex/c/c;)Lio/reactivex/o;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 89
    invoke-virtual {v0, v1, v2}, Lio/reactivex/o;->c(J)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "scan(skippedInitialViewS\u2026Reducer)\n        .skip(1)"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
