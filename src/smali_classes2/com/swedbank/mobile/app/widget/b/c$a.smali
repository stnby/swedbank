.class final synthetic Lcom/swedbank/mobile/app/widget/b/c$a;
.super Lkotlin/e/b/i;
.source "WidgetPreferencesPresenter.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/b/c;->a()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/i;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/widget/b/l;",
        "Lcom/swedbank/mobile/app/widget/b/l$a;",
        "Lcom/swedbank/mobile/app/widget/b/l;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/widget/b/c;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lkotlin/e/b/i;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/widget/b/l;Lcom/swedbank/mobile/app/widget/b/l$a;)Lcom/swedbank/mobile/app/widget/b/l;
    .locals 8
    .param p1    # Lcom/swedbank/mobile/app/widget/b/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/widget/b/l$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "p2"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/b/c$a;->b:Ljava/lang/Object;

    check-cast v0, Lcom/swedbank/mobile/app/widget/b/c;

    .line 89
    instance-of v0, p2, Lcom/swedbank/mobile/app/widget/b/l$a$b;

    if-eqz v0, :cond_0

    .line 90
    check-cast p2, Lcom/swedbank/mobile/app/widget/b/l$a$b;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/b/l$a$b;->a()Z

    move-result v2

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p1

    .line 89
    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/widget/b/l;->a(Lcom/swedbank/mobile/app/widget/b/l;Lcom/swedbank/mobile/business/util/e;ZLkotlin/k;Ljava/util/Set;ILjava/lang/Object;)Lcom/swedbank/mobile/app/widget/b/l;

    move-result-object p1

    goto :goto_0

    .line 92
    :cond_0
    instance-of v0, p2, Lcom/swedbank/mobile/app/widget/b/l$a$a;

    if-eqz v0, :cond_1

    .line 93
    check-cast p2, Lcom/swedbank/mobile/app/widget/b/l$a$a;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/b/l$a$a;->a()Lcom/swedbank/mobile/business/util/e;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p1

    .line 92
    invoke-static/range {v0 .. v6}, Lcom/swedbank/mobile/app/widget/b/l;->a(Lcom/swedbank/mobile/app/widget/b/l;Lcom/swedbank/mobile/business/util/e;ZLkotlin/k;Ljava/util/Set;ILjava/lang/Object;)Lcom/swedbank/mobile/app/widget/b/l;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_1
    instance-of v0, p2, Lcom/swedbank/mobile/app/widget/b/l$a$d;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 96
    check-cast p2, Lcom/swedbank/mobile/app/widget/b/l$a$d;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/b/l$a$d;->a()Lkotlin/k;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0xb

    const/4 v7, 0x0

    move-object v1, p1

    .line 95
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/widget/b/l;->a(Lcom/swedbank/mobile/app/widget/b/l;Lcom/swedbank/mobile/business/util/e;ZLkotlin/k;Ljava/util/Set;ILjava/lang/Object;)Lcom/swedbank/mobile/app/widget/b/l;

    move-result-object p1

    goto :goto_0

    .line 97
    :cond_2
    instance-of v0, p2, Lcom/swedbank/mobile/app/widget/b/l$a$c;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 98
    check-cast p2, Lcom/swedbank/mobile/app/widget/b/l$a$c;

    invoke-virtual {p2}, Lcom/swedbank/mobile/app/widget/b/l$a$c;->a()Ljava/util/Set;

    move-result-object v5

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v1, p1

    .line 97
    invoke-static/range {v1 .. v7}, Lcom/swedbank/mobile/app/widget/b/l;->a(Lcom/swedbank/mobile/app/widget/b/l;Lcom/swedbank/mobile/business/util/e;ZLkotlin/k;Ljava/util/Set;ILjava/lang/Object;)Lcom/swedbank/mobile/app/widget/b/l;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/swedbank/mobile/app/widget/b/l;

    check-cast p2, Lcom/swedbank/mobile/app/widget/b/l$a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/widget/b/c$a;->a(Lcom/swedbank/mobile/app/widget/b/l;Lcom/swedbank/mobile/app/widget/b/l$a;)Lcom/swedbank/mobile/app/widget/b/l;

    move-result-object p1

    return-object p1
.end method

.method public final a()Lkotlin/h/c;
    .locals 1

    const-class v0, Lcom/swedbank/mobile/app/widget/b/c;

    invoke-static {v0}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "viewStateReduce(Lcom/swedbank/mobile/app/widget/preferences/WidgetPreferencesViewState;Lcom/swedbank/mobile/app/widget/preferences/WidgetPreferencesViewState$PartialState;)Lcom/swedbank/mobile/app/widget/preferences/WidgetPreferencesViewState;"

    return-object v0
.end method
