.class public final Lcom/swedbank/mobile/app/widget/AppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "AppWidgetProvider.kt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/appwidget/AppWidgetManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appWidgetManager"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "appWidgetIds"

    invoke-static {p3, p2}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-static {p1}, Lcom/swedbank/mobile/a/ae/d;->a(Landroid/content/Context;)Lcom/swedbank/mobile/architect/business/g;

    move-result-object p2

    invoke-interface {p2}, Lcom/swedbank/mobile/architect/business/g;->f_()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lio/reactivex/o;

    .line 18
    invoke-virtual {p2}, Lio/reactivex/o;->j()Lio/reactivex/w;

    move-result-object p2

    .line 19
    invoke-virtual {p2}, Lio/reactivex/w;->b()Ljava/lang/Object;

    move-result-object p2

    .line 20
    check-cast p2, Lcom/swedbank/mobile/business/widget/root/g;

    .line 21
    invoke-static {p1}, Lcom/swedbank/mobile/a/ae/d;->b(Landroid/content/Context;)Lcom/swedbank/mobile/app/widget/h;

    move-result-object p1

    .line 22
    sget-object v0, Lcom/swedbank/mobile/app/widget/a;->a:[I

    invoke-virtual {p2}, Lcom/swedbank/mobile/business/widget/root/g;->ordinal()I

    move-result p2

    aget p2, v0, p2

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 p2, 0x1

    .line 28
    invoke-interface {p1, p2, p3}, Lcom/swedbank/mobile/app/widget/h;->a(Z[I)V

    goto :goto_0

    :pswitch_1
    const/4 p2, 0x0

    .line 25
    invoke-interface {p1, p2, p3}, Lcom/swedbank/mobile/app/widget/h;->a(Z[I)V

    goto :goto_0

    .line 24
    :pswitch_2
    invoke-interface {p1}, Lcom/swedbank/mobile/app/widget/h;->a()V

    goto :goto_0

    .line 23
    :pswitch_3
    invoke-interface {p1}, Lcom/swedbank/mobile/app/widget/h;->b()V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
