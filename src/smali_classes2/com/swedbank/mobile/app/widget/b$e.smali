.class public final Lcom/swedbank/mobile/app/widget/b$e;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/widget/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final activity_main:I = 0x7f0d001c

.field public static final browser_actions_context_menu_page:I = 0x7f0d001d

.field public static final browser_actions_context_menu_row:I = 0x7f0d001e

.field public static final design_bottom_navigation_item:I = 0x7f0d0020

.field public static final design_bottom_sheet_dialog:I = 0x7f0d0021

.field public static final design_layout_snackbar:I = 0x7f0d0022

.field public static final design_layout_snackbar_include:I = 0x7f0d0023

.field public static final design_layout_tab_icon:I = 0x7f0d0024

.field public static final design_layout_tab_text:I = 0x7f0d0025

.field public static final design_menu_item_action_area:I = 0x7f0d0026

.field public static final design_navigation_item:I = 0x7f0d0027

.field public static final design_navigation_item_header:I = 0x7f0d0028

.field public static final design_navigation_item_separator:I = 0x7f0d0029

.field public static final design_navigation_item_subheader:I = 0x7f0d002a

.field public static final design_navigation_menu:I = 0x7f0d002b

.field public static final design_navigation_menu_item:I = 0x7f0d002c

.field public static final design_text_input_password_icon:I = 0x7f0d002d

.field public static final fingerprint_dialog_container:I = 0x7f0d002e

.field public static final fingerprint_dialog_content:I = 0x7f0d002f

.field public static final item_bottom_sheet_action:I = 0x7f0d0034

.field public static final item_card:I = 0x7f0d0035

.field public static final item_contact_method:I = 0x7f0d0036

.field public static final item_list_separator:I = 0x7f0d0040

.field public static final item_widget_account:I = 0x7f0d005c

.field public static final item_widget_account_selection_account:I = 0x7f0d005d

.field public static final item_widget_account_selection_separator:I = 0x7f0d005e

.field public static final layout_widget_setup:I = 0x7f0d005f

.field public static final mtrl_layout_snackbar:I = 0x7f0d0060

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d0061

.field public static final notification_action:I = 0x7f0d0062

.field public static final notification_action_tombstone:I = 0x7f0d0063

.field public static final notification_media_action:I = 0x7f0d0064

.field public static final notification_media_cancel_action:I = 0x7f0d0065

.field public static final notification_template_big_media:I = 0x7f0d0066

.field public static final notification_template_big_media_custom:I = 0x7f0d0067

.field public static final notification_template_big_media_narrow:I = 0x7f0d0068

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0d0069

.field public static final notification_template_custom_big:I = 0x7f0d006a

.field public static final notification_template_icon_group:I = 0x7f0d006b

.field public static final notification_template_lines_media:I = 0x7f0d006c

.field public static final notification_template_media:I = 0x7f0d006d

.field public static final notification_template_media_custom:I = 0x7f0d006e

.field public static final notification_template_part_chronometer:I = 0x7f0d006f

.field public static final notification_template_part_time:I = 0x7f0d0070

.field public static final select_dialog_item_material:I = 0x7f0d0071

.field public static final select_dialog_multichoice_material:I = 0x7f0d0072

.field public static final select_dialog_singlechoice_material:I = 0x7f0d0073

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d0074

.field public static final toolbar:I = 0x7f0d0075

.field public static final view_bottom_sheet_dialog:I = 0x7f0d007c

.field public static final view_card_details:I = 0x7f0d007d

.field public static final view_cards_list:I = 0x7f0d007e

.field public static final view_challenge:I = 0x7f0d007f

.field public static final view_contact:I = 0x7f0d0080

.field public static final view_navigation:I = 0x7f0d008c

.field public static final view_not_authenticated:I = 0x7f0d008e

.field public static final view_onboarding:I = 0x7f0d008f

.field public static final view_onboarding_error_handler:I = 0x7f0d0090

.field public static final view_onboarding_loading:I = 0x7f0d0092

.field public static final view_recurring_login:I = 0x7f0d00a4

.field public static final view_retry:I = 0x7f0d00a5

.field public static final view_wallet_onboarding_contactless:I = 0x7f0d00ad

.field public static final view_wallet_onboarding_default_pay_app:I = 0x7f0d00ae

.field public static final view_wallet_onboarding_digitization:I = 0x7f0d00af

.field public static final view_wallet_onboarding_lock_screen:I = 0x7f0d00b0

.field public static final view_wallet_onboarding_nfc:I = 0x7f0d00b1

.field public static final view_wallet_onboarding_registration:I = 0x7f0d00b2

.field public static final view_wallet_onboarding_welcome:I = 0x7f0d00b3

.field public static final view_wallet_payment_result:I = 0x7f0d00b4

.field public static final view_wallet_payment_tap:I = 0x7f0d00b5

.field public static final view_widget:I = 0x7f0d00b6

.field public static final view_widget_configuration:I = 0x7f0d00b7

.field public static final view_widget_disabled:I = 0x7f0d00b8

.field public static final view_widget_preferences:I = 0x7f0d00b9

.field public static final wallet_payment_card_view:I = 0x7f0d00ba

.field public static final widget_card:I = 0x7f0d00bb

.field public static final widget_enable_setting:I = 0x7f0d00bc

.field public static final widget_information_setting:I = 0x7f0d00be

.field public static final widget_navigation_item:I = 0x7f0d00bf

.field public static final widget_on_off_setting:I = 0x7f0d00c0

.field public static final widget_slide_to_confirm:I = 0x7f0d00c1

.field public static final widget_snackbar:I = 0x7f0d00c2
