.class final Lcom/swedbank/mobile/app/widget/c/c$b$1$a;
.super Lkotlin/e/b/k;
.source "WidgetRootRouterImpl.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/widget/c/c$b$1;->a(Ljava/util/Collection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Lcom/swedbank/mobile/a/c/e/b;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/widget/c/c$b$1;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/widget/c/c$b$1;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a:Lcom/swedbank/mobile/app/widget/c/c$b$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/a/c/e/b;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/a/c/e/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "sessionModule"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a:Lcom/swedbank/mobile/app/widget/c/c$b$1;

    iget-object v0, v0, Lcom/swedbank/mobile/app/widget/c/c$b$1;->a:Lcom/swedbank/mobile/app/widget/c/c$b;

    iget-object v0, v0, Lcom/swedbank/mobile/app/widget/c/c$b;->a:Lcom/swedbank/mobile/app/widget/c/c;

    .line 56
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a:Lcom/swedbank/mobile/app/widget/c/c$b$1;

    iget-object v1, v1, Lcom/swedbank/mobile/app/widget/c/c$b$1;->a:Lcom/swedbank/mobile/app/widget/c/c$b;

    iget-object v1, v1, Lcom/swedbank/mobile/app/widget/c/c$b;->a:Lcom/swedbank/mobile/app/widget/c/c;

    invoke-static {v1}, Lcom/swedbank/mobile/app/widget/c/c;->c(Lcom/swedbank/mobile/app/widget/c/c;)Lcom/swedbank/mobile/app/widget/d/a;

    move-result-object v1

    .line 57
    iget-object v2, p0, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a:Lcom/swedbank/mobile/app/widget/c/c$b$1;

    iget-object v2, v2, Lcom/swedbank/mobile/app/widget/c/c$b$1;->a:Lcom/swedbank/mobile/app/widget/c/c$b;

    iget-object v2, v2, Lcom/swedbank/mobile/app/widget/c/c$b;->a:Lcom/swedbank/mobile/app/widget/c/c;

    invoke-static {v2}, Lcom/swedbank/mobile/app/widget/c/c;->d(Lcom/swedbank/mobile/app/widget/c/c;)Lcom/swedbank/mobile/business/widget/update/e;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/swedbank/mobile/app/widget/d/a;->a(Lcom/swedbank/mobile/business/widget/update/e;)Lcom/swedbank/mobile/app/widget/d/a;

    move-result-object v1

    .line 58
    invoke-virtual {v1, p1}, Lcom/swedbank/mobile/app/widget/d/a;->a(Lcom/swedbank/mobile/a/c/e/b;)Lcom/swedbank/mobile/app/widget/d/a;

    move-result-object p1

    .line 59
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a:Lcom/swedbank/mobile/app/widget/c/c$b$1;

    iget-object v1, v1, Lcom/swedbank/mobile/app/widget/c/c$b$1;->a:Lcom/swedbank/mobile/app/widget/c/c$b;

    iget-boolean v1, v1, Lcom/swedbank/mobile/app/widget/c/c$b;->b:Z

    invoke-virtual {p1, v1}, Lcom/swedbank/mobile/app/widget/d/a;->a(Z)Lcom/swedbank/mobile/app/widget/d/a;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/widget/d/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object p1

    .line 61
    iget-object v1, p0, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a:Lcom/swedbank/mobile/app/widget/c/c$b$1;

    iget-object v1, v1, Lcom/swedbank/mobile/app/widget/c/c$b$1;->b:Lio/reactivex/k;

    invoke-virtual {p1}, Lcom/swedbank/mobile/architect/a/h;->q()Lcom/swedbank/mobile/architect/business/d;

    move-result-object v2

    invoke-interface {v1, v2}, Lio/reactivex/k;->a(Ljava/lang/Object;)V

    .line 82
    invoke-static {v0, p1}, Lcom/swedbank/mobile/architect/a/h;->a(Lcom/swedbank/mobile/architect/a/h;Lcom/swedbank/mobile/architect/a/h;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/swedbank/mobile/a/c/e/b;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/c/c$b$1$a;->a(Lcom/swedbank/mobile/a/c/e/b;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
