.class public final Lcom/swedbank/mobile/app/widget/c/c;
.super Lcom/swedbank/mobile/architect/a/h;
.source "WidgetRootRouterImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/business/widget/root/f;


# instance fields
.field private final e:Lcom/swedbank/mobile/app/b/e/b/a;

.field private final f:Lcom/swedbank/mobile/app/widget/d/a;

.field private final g:Lcom/swedbank/mobile/app/widget/h;

.field private final h:Lcom/swedbank/mobile/business/authentication/session/l;

.field private final i:Lcom/swedbank/mobile/business/widget/update/e;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/app/b/e/b/a;Lcom/swedbank/mobile/app/widget/d/a;Lcom/swedbank/mobile/app/widget/h;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/widget/update/e;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V
    .locals 7
    .param p1    # Lcom/swedbank/mobile/app/b/e/b/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/widget/d/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/swedbank/mobile/app/widget/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/swedbank/mobile/business/authentication/session/l;
        .annotation runtime Ljavax/inject/Named;
            value = "for_widget_root"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/swedbank/mobile/business/widget/update/e;
        .annotation runtime Ljavax/inject/Named;
            value = "for_widget_root"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p6    # Lcom/swedbank/mobile/architect/business/c;
        .annotation runtime Ljavax/inject/Named;
            value = "for_widget_root"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p7    # Lcom/swedbank/mobile/architect/a/b/g;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/app/b/e/b/a;",
            "Lcom/swedbank/mobile/app/widget/d/a;",
            "Lcom/swedbank/mobile/app/widget/h;",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "scopedSessionBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "widgetUpdateBuilder"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "widgetRouter"

    invoke-static {p3, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionListener"

    invoke-static {p4, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "widgetUpdateListener"

    invoke-static {p5, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "interactor"

    invoke-static {p6, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewManager"

    invoke-static {p7, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    .line 32
    invoke-direct/range {v1 .. v6}, Lcom/swedbank/mobile/architect/a/h;-><init>(Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;Lcom/swedbank/mobile/architect/a/b/f;ILkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/c/c;->e:Lcom/swedbank/mobile/app/b/e/b/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/c/c;->f:Lcom/swedbank/mobile/app/widget/d/a;

    iput-object p3, p0, Lcom/swedbank/mobile/app/widget/c/c;->g:Lcom/swedbank/mobile/app/widget/h;

    iput-object p4, p0, Lcom/swedbank/mobile/app/widget/c/c;->h:Lcom/swedbank/mobile/business/authentication/session/l;

    iput-object p5, p0, Lcom/swedbank/mobile/app/widget/c/c;->i:Lcom/swedbank/mobile/business/widget/update/e;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/c/c;)Lcom/swedbank/mobile/app/b/e/b/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/c/c;->e:Lcom/swedbank/mobile/app/b/e/b/a;

    return-object p0
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/widget/c/c;Lkotlin/e/a/b;)V
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/c/c;->b(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/widget/c/c;)Lcom/swedbank/mobile/business/authentication/session/l;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/c/c;->h:Lcom/swedbank/mobile/business/authentication/session/l;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/widget/c/c;Lkotlin/e/a/b;)V
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/widget/c/c;->c(Lkotlin/e/a/b;)V

    return-void
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/widget/c/c;)Lcom/swedbank/mobile/app/widget/d/a;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/c/c;->f:Lcom/swedbank/mobile/app/widget/d/a;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/widget/c/c;)Lcom/swedbank/mobile/business/widget/update/e;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/swedbank/mobile/app/widget/c/c;->i:Lcom/swedbank/mobile/business/widget/update/e;

    return-object p0
.end method


# virtual methods
.method public a(Z)Lio/reactivex/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/j<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 44
    new-instance v0, Lcom/swedbank/mobile/app/widget/c/c$b;

    invoke-direct {v0, p0, p1}, Lcom/swedbank/mobile/app/widget/c/c$b;-><init>(Lcom/swedbank/mobile/app/widget/c/c;Z)V

    check-cast v0, Lio/reactivex/m;

    invoke-static {v0}, Lio/reactivex/j;->a(Lio/reactivex/m;)Lio/reactivex/j;

    move-result-object p1

    const-string v0, "Maybe.create { emitter -\u2026      }\n      }\n    }\n  }"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public a()V
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/widget/c/c;->r()V

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/c;->g:Lcom/swedbank/mobile/app/widget/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/app/widget/h;->a()V

    return-void
.end method

.method public b()V
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/c;->g:Lcom/swedbank/mobile/app/widget/h;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v2, v1, v3, v1}, Lcom/swedbank/mobile/app/widget/h$a;->a(Lcom/swedbank/mobile/app/widget/h;Z[IILjava/lang/Object;)V

    return-void
.end method

.method public c()V
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/c;->g:Lcom/swedbank/mobile/app/widget/h;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v0, v2, v1, v3, v1}, Lcom/swedbank/mobile/app/widget/h$a;->a(Lcom/swedbank/mobile/app/widget/h;Z[IILjava/lang/Object;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/c;->g:Lcom/swedbank/mobile/app/widget/h;

    invoke-interface {v0}, Lcom/swedbank/mobile/app/widget/h;->b()V

    return-void
.end method

.method public e()V
    .locals 1

    .line 77
    sget-object v0, Lcom/swedbank/mobile/app/widget/c/c$a;->a:Lcom/swedbank/mobile/app/widget/c/c$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/widget/c/c;->c(Lkotlin/e/a/b;)V

    return-void
.end method
