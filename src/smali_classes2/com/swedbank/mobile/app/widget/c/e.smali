.class public final Lcom/swedbank/mobile/app/widget/c/e;
.super Ljava/lang/Object;
.source "WidgetRootRouterImpl_Factory.java"

# interfaces
.implements La/a/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "La/a/d<",
        "Lcom/swedbank/mobile/app/widget/c/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/d/a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/h;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final g:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/swedbank/mobile/app/widget/c/e;->a:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/swedbank/mobile/app/widget/c/e;->b:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/swedbank/mobile/app/widget/c/e;->c:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/swedbank/mobile/app/widget/c/e;->d:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/swedbank/mobile/app/widget/c/e;->e:Ljavax/inject/Provider;

    .line 40
    iput-object p6, p0, Lcom/swedbank/mobile/app/widget/c/e;->f:Ljavax/inject/Provider;

    .line 41
    iput-object p7, p0, Lcom/swedbank/mobile/app/widget/c/e;->g:Ljavax/inject/Provider;

    return-void
.end method

.method public static a(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/swedbank/mobile/app/widget/c/e;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/b/e/b/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/d/a;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/app/widget/h;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/authentication/session/l;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/business/widget/update/e;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/business/c<",
            "*>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/swedbank/mobile/architect/a/b/g;",
            ">;)",
            "Lcom/swedbank/mobile/app/widget/c/e;"
        }
    .end annotation

    .line 56
    new-instance v8, Lcom/swedbank/mobile/app/widget/c/e;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/app/widget/c/e;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public a()Lcom/swedbank/mobile/app/widget/c/c;
    .locals 9

    .line 46
    new-instance v8, Lcom/swedbank/mobile/app/widget/c/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->a:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/swedbank/mobile/app/b/e/b/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->b:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/swedbank/mobile/app/widget/d/a;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->c:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/swedbank/mobile/app/widget/h;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->d:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/swedbank/mobile/business/authentication/session/l;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->e:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/swedbank/mobile/business/widget/update/e;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->f:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/swedbank/mobile/architect/business/c;

    iget-object v0, p0, Lcom/swedbank/mobile/app/widget/c/e;->g:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/swedbank/mobile/architect/a/b/g;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/swedbank/mobile/app/widget/c/c;-><init>(Lcom/swedbank/mobile/app/b/e/b/a;Lcom/swedbank/mobile/app/widget/d/a;Lcom/swedbank/mobile/app/widget/h;Lcom/swedbank/mobile/business/authentication/session/l;Lcom/swedbank/mobile/business/widget/update/e;Lcom/swedbank/mobile/architect/business/c;Lcom/swedbank/mobile/architect/a/b/g;)V

    return-object v8
.end method

.method public synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/widget/c/e;->a()Lcom/swedbank/mobile/app/widget/c/c;

    move-result-object v0

    return-object v0
.end method
