.class public final Lcom/swedbank/mobile/app/plugins/list/a;
.super Landroidx/recyclerview/widget/RecyclerView$a;
.source "ListPluginAdapter.kt"

# interfaces
.implements Lcom/swedbank/mobile/core/ui/widget/d;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$a<",
        "Lcom/swedbank/mobile/app/plugins/list/h;",
        ">;",
        "Lcom/swedbank/mobile/core/ui/widget/d;"
    }
.end annotation


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/plugins/list/d;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Landroidx/recyclerview/widget/f$b;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "+",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    const-string v0, "renderersMap"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "persistableData"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$a;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->e:Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/a;->f:Ljava/util/Map;

    .line 27
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string p2, "PublishRelay.create<ListPluginAction>()"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->a:Lcom/b/c/c;

    .line 29
    invoke-static {}, Lkotlin/a/h;->a()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/plugins/list/a;)Ljava/util/Map;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->e:Ljava/util/Map;

    return-object p0
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/plugins/list/h;
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->e:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/f;

    .line 46
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v1, "parent.context"

    invoke-static {p2, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/swedbank/mobile/app/plugins/list/f;->b()I

    move-result v0

    const/4 v1, 0x0

    .line 110
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    const-string v2, "LayoutInflater.from(this)"

    invoke-static {p2, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 47
    new-instance p2, Lcom/swedbank/mobile/app/plugins/list/h;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/app/plugins/list/h;-><init>(Landroid/view/View;)V

    return-object p2

    .line 109
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.View"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 44
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No layout res found for requested view type: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 43
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->a:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/app/plugins/list/b;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/b;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/b;->c()Landroidx/recyclerview/widget/f$b;

    move-result-object p1

    .line 70
    iget-object v1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    .line 71
    iput-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    .line 72
    iget-boolean v2, p0, Lcom/swedbank/mobile/app/plugins/list/a;->c:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    .line 73
    iput-boolean v2, p0, Lcom/swedbank/mobile/app/plugins/list/a;->c:Z

    .line 74
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/a;->notifyDataSetChanged()V

    goto :goto_1

    .line 77
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    if-nez p1, :cond_2

    goto :goto_0

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->d:Landroidx/recyclerview/widget/f$b;

    if-eq v0, p1, :cond_4

    .line 80
    move-object v0, p0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/f$b;->a(Landroidx/recyclerview/widget/RecyclerView$a;)V

    goto :goto_1

    .line 78
    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/a;->notifyDataSetChanged()V

    .line 82
    :cond_4
    :goto_1
    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->d:Landroidx/recyclerview/widget/f$b;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/plugins/list/h;I)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/app/plugins/list/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/swedbank/mobile/app/plugins/list/d;

    .line 111
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Lcom/swedbank/mobile/app/plugins/list/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/h/b;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/f;

    .line 54
    iget-object v1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->f:Ljava/util/Map;

    .line 55
    iget-object v2, p0, Lcom/swedbank/mobile/app/plugins/list/a;->a:Lcom/b/c/c;

    check-cast v2, Lio/reactivex/c/g;

    .line 51
    invoke-interface {v0, p2, p1, v1, v2}, Lcom/swedbank/mobile/app/plugins/list/f;->a(Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/h;Ljava/util/Map;Lio/reactivex/c/g;)V

    return-void

    .line 112
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No renderer found for list plugin item "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 111
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/app/plugins/list/d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 63
    iput-boolean v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->c:Z

    .line 64
    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    .line 65
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/a;->notifyDataSetChanged()V

    return-void
.end method

.method public a(I)Z
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/d;

    .line 106
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Lcom/swedbank/mobile/app/plugins/list/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/h/b;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/f;

    .line 108
    invoke-interface {v0}, Lcom/swedbank/mobile/app/plugins/list/f;->c()Z

    move-result p1

    return p1

    .line 107
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No renderer found for list plugin item "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 106
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/d;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/d;

    .line 103
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Lcom/swedbank/mobile/app/plugins/list/a;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/h/b;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/swedbank/mobile/app/plugins/list/f;

    .line 105
    invoke-interface {v0}, Lcom/swedbank/mobile/app/plugins/list/f;->a()Lkotlin/h/b;

    move-result-object p1

    .line 37
    invoke-interface {p1}, Lkotlin/h/b;->hashCode()I

    move-result p1

    return p1

    .line 104
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No renderer found for list plugin item "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$x;I)V
    .locals 0

    .line 23
    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/h;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Lcom/swedbank/mobile/app/plugins/list/h;I)V

    return-void
.end method

.method public synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$x;
    .locals 0

    .line 23
    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Landroid/view/ViewGroup;I)Lcom/swedbank/mobile/app/plugins/list/h;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$x;

    return-object p1
.end method
