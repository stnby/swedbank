.class final Lcom/swedbank/mobile/app/plugins/list/e$a;
.super Lkotlin/e/b/k;
.source "ListPluginItem.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/plugins/list/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/app/plugins/list/d;",
        "Lcom/swedbank/mobile/app/plugins/list/d;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/swedbank/mobile/app/plugins/list/e$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/swedbank/mobile/app/plugins/list/e$a;

    invoke-direct {v0}, Lcom/swedbank/mobile/app/plugins/list/e$a;-><init>()V

    sput-object v0, Lcom/swedbank/mobile/app/plugins/list/e$a;->a:Lcom/swedbank/mobile/app/plugins/list/e$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/d;

    check-cast p2, Lcom/swedbank/mobile/app/plugins/list/d;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/plugins/list/e$a;->a(Lcom/swedbank/mobile/app/plugins/list/d;Lcom/swedbank/mobile/app/plugins/list/d;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/app/plugins/list/d;Lcom/swedbank/mobile/app/plugins/list/d;)Z
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/plugins/list/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/plugins/list/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "lhs"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-interface {p1}, Lcom/swedbank/mobile/app/plugins/list/d;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Lcom/swedbank/mobile/app/plugins/list/d;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/swedbank/mobile/app/plugins/list/d;->a()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lcom/swedbank/mobile/app/plugins/list/d;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
