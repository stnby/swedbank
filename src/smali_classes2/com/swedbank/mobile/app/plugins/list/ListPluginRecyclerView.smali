.class public final Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "ListPluginRecyclerView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;
    }
.end annotation


# static fields
.field static final synthetic a:[Lkotlin/h/g;


# instance fields
.field private final b:Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;

.field private c:Lcom/swedbank/mobile/app/plugins/list/a;

.field private final d:Lcom/swedbank/mobile/core/ui/l;

.field private e:Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Integer;

.field private final h:Lkotlin/f/d;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/h/g;

    new-instance v1, Lkotlin/e/b/n;

    const-class v2, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;

    invoke-static {v2}, Lkotlin/e/b/v;->a(Ljava/lang/Class;)Lkotlin/h/b;

    move-result-object v2

    const-string v3, "hasMoreToLoad"

    const-string v4, "getHasMoreToLoad()Z"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/e/b/n;-><init>(Lkotlin/h/c;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/e/b/v;->a(Lkotlin/e/b/m;)Lkotlin/h/f;

    move-result-object v1

    check-cast v1, Lkotlin/h/g;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a:[Lkotlin/h/g;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance p2, Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;

    invoke-direct {p2, p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b:Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;

    .line 25
    new-instance p2, Lcom/swedbank/mobile/core/ui/l;

    invoke-direct {p2}, Lcom/swedbank/mobile/core/ui/l;-><init>()V

    iput-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->d:Lcom/swedbank/mobile/core/ui/l;

    .line 28
    new-instance p2, Ljava/util/HashMap;

    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    check-cast p2, Ljava/util/Map;

    iput-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->f:Ljava/util/Map;

    .line 31
    sget-object p2, Lkotlin/f/a;->a:Lkotlin/f/a;

    iget-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->d:Lcom/swedbank/mobile/core/ui/l;

    invoke-virtual {p2}, Lcom/swedbank/mobile/core/ui/l;->a()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    .line 137
    new-instance p3, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$a;

    invoke-direct {p3, p2, p2, p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$a;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)V

    check-cast p3, Lkotlin/f/d;

    .line 139
    iput-object p3, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->h:Lkotlin/f/d;

    .line 36
    iget-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b:Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 37
    iget-object p2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->d:Lcom/swedbank/mobile/core/ui/l;

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$n;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$n;)V

    .line 38
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/f;

    sget-object p3, Lcom/swedbank/mobile/core/ui/widget/g$a;->a:Lcom/swedbank/mobile/core/ui/widget/g$a;

    move-object v4, p3

    check-cast v4, Lcom/swedbank/mobile/core/ui/widget/g;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v0, p2

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/swedbank/mobile/core/ui/widget/f;-><init>(Landroid/content/Context;IZLcom/swedbank/mobile/core/ui/widget/g;ILkotlin/e/b/g;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {p0, p2}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    const/4 p1, 0x0

    .line 40
    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$f;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$f;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 20
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 21
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)Lcom/swedbank/mobile/app/plugins/list/a;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->c:Lcom/swedbank/mobile/app/plugins/list/a;

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)Lcom/swedbank/mobile/core/ui/l;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->d:Lcom/swedbank/mobile/core/ui/l;

    return-object p0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->b:Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;

    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginLayoutManager;->o()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/swedbank/mobile/app/plugins/list/b;)V
    .locals 2
    .param p1    # Lcom/swedbank/mobile/app/plugins/list/b;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)Lcom/swedbank/mobile/app/plugins/list/a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->stopScroll()V

    .line 80
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Lcom/swedbank/mobile/app/plugins/list/b;)V

    .line 81
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    const/4 v1, 0x0

    .line 82
    check-cast v1, Ljava/lang/Integer;

    iput-object v1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->g:Ljava/lang/Integer;

    .line 83
    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->scrollToPosition(I)V

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/b;->a()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->setHasMoreToLoad(Z)V

    return-void

    .line 131
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Adapter not initialized, check your code"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljavax/inject/Provider<",
            "Ljava/util/List<",
            "Lcom/swedbank/mobile/app/plugins/list/f<",
            "*>;>;>;>;)V"
        }
    .end annotation

    const-string v0, "rendererProviders"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->c:Lcom/swedbank/mobile/app/plugins/list/a;

    if-nez v0, :cond_4

    .line 45
    new-instance v0, Landroidx/c/a;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroidx/c/a;-><init>(I)V

    .line 46
    check-cast p1, Ljava/lang/Iterable;

    .line 121
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljavax/inject/Provider;

    .line 47
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "singlePluginRenderers.get()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 122
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/swedbank/mobile/app/plugins/list/f;

    .line 48
    invoke-interface {v2}, Lcom/swedbank/mobile/app/plugins/list/f;->a()Lkotlin/h/b;

    move-result-object v3

    invoke-interface {v3}, Lkotlin/h/b;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroidx/c/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 52
    :cond_1
    iget-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->e:Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;->b()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_2

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    check-cast v1, Ljava/util/Map;

    iput-object v1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->f:Ljava/util/Map;

    .line 53
    :cond_2
    new-instance p1, Lcom/swedbank/mobile/app/plugins/list/a;

    check-cast v0, Ljava/util/Map;

    iget-object v1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->f:Ljava/util/Map;

    invoke-direct {p1, v0, v1}, Lcom/swedbank/mobile/app/plugins/list/a;-><init>(Ljava/util/Map;Ljava/util/Map;)V

    .line 54
    move-object v0, p1

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 55
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->e:Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    .line 56
    check-cast v1, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    iput-object v1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->e:Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    .line 57
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;->c()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/app/plugins/list/a;->a(Ljava/util/List;)V

    .line 53
    :cond_3
    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->c:Lcom/swedbank/mobile/app/plugins/list/a;

    :cond_4
    return-void
.end method

.method public final b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lcom/swedbank/mobile/business/i/a/c;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 125
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)Lcom/swedbank/mobile/app/plugins/list/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/a;->a()Lio/reactivex/o;

    move-result-object v0

    return-object v0

    .line 125
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter not initialized, check your code"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final c()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->d:Lcom/swedbank/mobile/core/ui/l;

    invoke-virtual {v0}, Lcom/swedbank/mobile/core/ui/l;->b()Lio/reactivex/o;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 3

    .line 128
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)Lcom/swedbank/mobile/app/plugins/list/a;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 130
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/a;->getItemCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a()I

    move-result v0

    const/16 v1, 0x19

    const/4 v2, 0x0

    if-le v0, v1, :cond_0

    .line 71
    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->scrollToPosition(I)V

    goto :goto_0

    :cond_0
    if-lez v0, :cond_1

    .line 72
    invoke-virtual {p0, v2}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->smoothScrollToPosition(I)V

    :cond_1
    :goto_0
    return-void

    .line 128
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter not initialized, check your code"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getHasMoreToLoad()Z
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->h:Lkotlin/f/d;

    sget-object v1, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/f/d;->a(Ljava/lang/Object;Lkotlin/h/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 97
    instance-of v0, p1, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    if-nez v0, :cond_0

    .line 98
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 101
    :cond_0
    check-cast p1, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;->a()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 102
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 103
    move-object v1, v0

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->g:Ljava/lang/Integer;

    .line 104
    iput-object p1, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->e:Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    .line 105
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;->d()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->setHasMoreToLoad(Z)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 7
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 90
    invoke-super {p0}, Landroidx/recyclerview/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 91
    iget-object v2, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->f:Ljava/util/Map;

    .line 134
    invoke-static {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a(Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;)Lcom/swedbank/mobile/app/plugins/list/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {v0}, Lcom/swedbank/mobile/app/plugins/list/a;->b()Ljava/util/List;

    move-result-object v3

    .line 93
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->getHasMoreToLoad()Z

    move-result v4

    .line 94
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a()I

    move-result v5

    .line 89
    new-instance v6, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView$b;-><init>(Landroid/os/Parcelable;Ljava/util/Map;Ljava/util/List;ZI)V

    check-cast v6, Landroid/os/Parcelable;

    return-object v6

    .line 134
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter not initialized, check your code"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final setHasMoreToLoad(Z)V
    .locals 3

    iget-object v0, p0, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->h:Lkotlin/f/d;

    sget-object v1, Lcom/swedbank/mobile/app/plugins/list/ListPluginRecyclerView;->a:[Lkotlin/h/g;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p0, v1, p1}, Lkotlin/f/d;->a(Ljava/lang/Object;Lkotlin/h/g;Ljava/lang/Object;)V

    return-void
.end method
