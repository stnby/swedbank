.class public final Lcom/swedbank/mobile/app/f/a;
.super Ljava/lang/Object;
.source "ConfirmationDialogBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/general/confirmation/c;

.field private b:Lcom/swedbank/mobile/app/f/c;

.field private c:Z

.field private d:Ljava/lang/String;

.field private final e:Lcom/swedbank/mobile/a/g/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/g/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/g/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/a;->e:Lcom/swedbank/mobile/a/g/a$a;

    const/4 p1, 0x1

    .line 15
    iput-boolean p1, p0, Lcom/swedbank/mobile/app/f/a;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/app/f/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/f/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "information"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a;

    .line 23
    iput-object p1, v0, Lcom/swedbank/mobile/app/f/a;->b:Lcom/swedbank/mobile/app/f/c;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/app/f/a;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a;

    .line 19
    iput-object p1, v0, Lcom/swedbank/mobile/app/f/a;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/swedbank/mobile/app/f/a;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "tag"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a;

    .line 31
    iput-object p1, v0, Lcom/swedbank/mobile/app/f/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)Lcom/swedbank/mobile/app/f/a;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a;

    .line 27
    iput-boolean p1, v0, Lcom/swedbank/mobile/app/f/a;->c:Z

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 3
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/a;->e:Lcom/swedbank/mobile/a/g/a$a;

    .line 35
    iget-object v1, p0, Lcom/swedbank/mobile/app/f/a;->a:Lcom/swedbank/mobile/business/general/confirmation/c;

    if-eqz v1, :cond_2

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/g/a$a;->b(Lcom/swedbank/mobile/business/general/confirmation/c;)Lcom/swedbank/mobile/a/g/a$a;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/swedbank/mobile/app/f/a;->b:Lcom/swedbank/mobile/app/f/c;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/g/a$a;->b(Lcom/swedbank/mobile/app/f/c;)Lcom/swedbank/mobile/a/g/a$a;

    move-result-object v0

    .line 41
    iget-boolean v1, p0, Lcom/swedbank/mobile/app/f/a;->c:Z

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/g/a$a;->b(Z)Lcom/swedbank/mobile/a/g/a$a;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/swedbank/mobile/app/f/a;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "UUID.randomUUID().toString()"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/g/a$a;->b(Ljava/lang/String;)Lcom/swedbank/mobile/a/g/a$a;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Lcom/swedbank/mobile/a/g/a$a;->a()Lcom/swedbank/mobile/a/g/a;

    move-result-object v0

    .line 44
    invoke-interface {v0}, Lcom/swedbank/mobile/a/g/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 38
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display confirmation dialog without knowing how to render it"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 35
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display confirmation dialog without parent node"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
