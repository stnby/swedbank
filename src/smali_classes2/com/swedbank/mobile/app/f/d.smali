.class public final Lcom/swedbank/mobile/app/f/d;
.super Lcom/swedbank/mobile/architect/a/d;
.source "ConfirmationDialogPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/f/i;",
        "Lcom/swedbank/mobile/app/f/l;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/general/confirmation/a;

.field private final b:Lcom/swedbank/mobile/app/f/c;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/general/confirmation/a;Lcom/swedbank/mobile/app/f/c;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/f/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "information"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/d;->a:Lcom/swedbank/mobile/business/general/confirmation/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/f/d;->b:Lcom/swedbank/mobile/app/f/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/f/d;)Lcom/swedbank/mobile/business/general/confirmation/a;
    .locals 0

    .line 8
    iget-object p0, p0, Lcom/swedbank/mobile/app/f/d;->a:Lcom/swedbank/mobile/business/general/confirmation/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 14
    sget-object v0, Lcom/swedbank/mobile/app/f/d$a;->a:Lcom/swedbank/mobile/app/f/d$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/f/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 15
    new-instance v1, Lcom/swedbank/mobile/app/f/d$b;

    invoke-direct {v1, p0}, Lcom/swedbank/mobile/app/f/d$b;-><init>(Lcom/swedbank/mobile/app/f/d;)V

    check-cast v1, Lio/reactivex/c/g;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 16
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 17
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 19
    sget-object v1, Lcom/swedbank/mobile/app/f/d$c;->a:Lcom/swedbank/mobile/app/f/d$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/f/d;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 20
    new-instance v2, Lcom/swedbank/mobile/app/f/d$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/f/d$d;-><init>(Lcom/swedbank/mobile/app/f/d;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 22
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 25
    check-cast v0, Lio/reactivex/s;

    .line 26
    check-cast v1, Lio/reactivex/s;

    .line 24
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/swedbank/mobile/app/f/l;

    iget-object v2, p0, Lcom/swedbank/mobile/app/f/d;->b:Lcom/swedbank/mobile/app/f/c;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lcom/swedbank/mobile/app/f/l;-><init>(Lcom/swedbank/mobile/app/f/c;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026formation = information))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
