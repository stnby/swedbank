.class final Lcom/swedbank/mobile/app/f/a/k$a;
.super Lkotlin/e/b/k;
.source "BottomSheetDialogViewImpl.kt"

# interfaces
.implements Lkotlin/e/a/m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/f/a/k;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/m<",
        "Lcom/swedbank/mobile/core/ui/widget/j<",
        "Lcom/swedbank/mobile/app/f/a/a;",
        ">;",
        "Lcom/swedbank/mobile/app/f/a/a;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/swedbank/mobile/app/f/a/k;


# direct methods
.method constructor <init>(Lcom/swedbank/mobile/app/f/a/k;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/a/k$a;->a:Lcom/swedbank/mobile/app/f/a/k;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/core/ui/widget/j;

    check-cast p2, Lcom/swedbank/mobile/app/f/a/a;

    invoke-virtual {p0, p1, p2}, Lcom/swedbank/mobile/app/f/a/k$a;->a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/f/a/a;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method

.method public final a(Lcom/swedbank/mobile/core/ui/widget/j;Lcom/swedbank/mobile/app/f/a/a;)V
    .locals 3
    .param p1    # Lcom/swedbank/mobile/core/ui/widget/j;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/f/a/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/swedbank/mobile/core/ui/widget/j<",
            "Lcom/swedbank/mobile/app/f/a/a;",
            ">;",
            "Lcom/swedbank/mobile/app/f/a/a;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "action"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget v0, Lcom/swedbank/mobile/core/a$d;->bottom_sheet_action_title:I

    invoke-virtual {p1, v0}, Lcom/swedbank/mobile/core/ui/widget/j;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 39
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/f/a/a;->b()Lkotlin/e/a/b;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "view.resources"

    invoke-static {v1, v2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lkotlin/e/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/f/a/a;->c()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    const/4 v1, 0x0

    .line 41
    invoke-virtual {p1, v0, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 43
    :cond_0
    check-cast p1, Landroid/view/View;

    .line 109
    new-instance v0, Lcom/swedbank/mobile/app/f/a/k$a$a;

    invoke-direct {v0, p0, p2}, Lcom/swedbank/mobile/app/f/a/k$a$a;-><init>(Lcom/swedbank/mobile/app/f/a/k$a;Lcom/swedbank/mobile/app/f/a/a;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
