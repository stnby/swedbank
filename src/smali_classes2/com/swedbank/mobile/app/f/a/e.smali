.class public final Lcom/swedbank/mobile/app/f/a/e;
.super Lcom/swedbank/mobile/architect/a/d;
.source "BottomSheetDialogPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/swedbank/mobile/architect/a/d<",
        "Lcom/swedbank/mobile/app/f/a/j;",
        "Lcom/swedbank/mobile/app/f/a/m;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/swedbank/mobile/business/general/confirmation/bottom/a;

.field private final b:Lcom/swedbank/mobile/app/f/a/d;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/business/general/confirmation/bottom/a;Lcom/swedbank/mobile/app/f/a/d;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/bottom/a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/f/a/d;
        .annotation runtime Ljavax/inject/Named;
            value = "bottom_sheet_dialog_information"
        .end annotation

        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "interactor"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "information"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/d;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/a/e;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/a;

    iput-object p2, p0, Lcom/swedbank/mobile/app/f/a/e;->b:Lcom/swedbank/mobile/app/f/a/d;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/f/a/e;)Lcom/swedbank/mobile/business/general/confirmation/bottom/a;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/swedbank/mobile/app/f/a/e;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 5

    .line 17
    sget-object v0, Lcom/swedbank/mobile/app/f/a/e$a;->a:Lcom/swedbank/mobile/app/f/a/e$a;

    check-cast v0, Lkotlin/e/a/b;

    invoke-virtual {p0, v0}, Lcom/swedbank/mobile/app/f/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v0

    .line 18
    new-instance v1, Lcom/swedbank/mobile/app/f/a/e$b;

    iget-object v2, p0, Lcom/swedbank/mobile/app/f/a/e;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/a;

    invoke-direct {v1, v2}, Lcom/swedbank/mobile/app/f/a/e$b;-><init>(Lcom/swedbank/mobile/business/general/confirmation/bottom/a;)V

    check-cast v1, Lkotlin/e/a/b;

    new-instance v2, Lcom/swedbank/mobile/app/f/a/f;

    invoke-direct {v2, v1}, Lcom/swedbank/mobile/app/f/a/f;-><init>(Lkotlin/e/a/b;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v0, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v0

    .line 22
    sget-object v1, Lcom/swedbank/mobile/app/f/a/e$c;->a:Lcom/swedbank/mobile/app/f/a/e$c;

    check-cast v1, Lkotlin/e/a/b;

    invoke-virtual {p0, v1}, Lcom/swedbank/mobile/app/f/a/e;->a(Lkotlin/e/a/b;)Lio/reactivex/o;

    move-result-object v1

    .line 23
    new-instance v2, Lcom/swedbank/mobile/app/f/a/e$d;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/f/a/e$d;-><init>(Lcom/swedbank/mobile/app/f/a/e;)V

    check-cast v2, Lio/reactivex/c/g;

    invoke-virtual {v1, v2}, Lio/reactivex/o;->a(Lio/reactivex/c/g;)Lio/reactivex/o;

    move-result-object v1

    .line 24
    invoke-virtual {v1}, Lio/reactivex/o;->k()Lio/reactivex/b;

    move-result-object v1

    .line 25
    invoke-virtual {v1}, Lio/reactivex/b;->g()Lio/reactivex/o;

    move-result-object v1

    .line 28
    check-cast v0, Lio/reactivex/s;

    .line 29
    check-cast v1, Lio/reactivex/s;

    .line 27
    invoke-static {v0, v1}, Lio/reactivex/o;->b(Lio/reactivex/s;Lio/reactivex/s;)Lio/reactivex/o;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/swedbank/mobile/app/f/a/m;

    iget-object v2, p0, Lcom/swedbank/mobile/app/f/a/e;->b:Lcom/swedbank/mobile/app/f/a/d;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lcom/swedbank/mobile/app/f/a/m;-><init>(Lcom/swedbank/mobile/app/f/a/d;Ljava/lang/Throwable;ILkotlin/e/b/g;)V

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Ljava/lang/Object;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "Observable.merge(\n      \u2026formation = information))"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {p0, v0}, Lcom/swedbank/mobile/architect/a/d;->a(Lcom/swedbank/mobile/architect/a/d;Lio/reactivex/o;)V

    return-void
.end method
