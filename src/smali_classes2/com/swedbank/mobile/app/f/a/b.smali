.class public final Lcom/swedbank/mobile/app/f/a/b;
.super Ljava/lang/Object;
.source "BottomSheetDialogBuilder.kt"

# interfaces
.implements Lcom/swedbank/mobile/architect/a/c;


# instance fields
.field private a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

.field private b:Lcom/swedbank/mobile/app/f/a/d;

.field private final c:Lcom/swedbank/mobile/a/g/a/a$a;


# direct methods
.method public constructor <init>(Lcom/swedbank/mobile/a/g/a/a$a;)V
    .locals 1
    .param p1    # Lcom/swedbank/mobile/a/g/a/a$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "componentBuilder"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/a/b;->c:Lcom/swedbank/mobile/a/g/a/a$a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/swedbank/mobile/app/f/a/d;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/app/f/a/d;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "information"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a/b;

    .line 20
    iput-object p1, v0, Lcom/swedbank/mobile/app/f/a/b;->b:Lcom/swedbank/mobile/app/f/a/d;

    return-object v0
.end method

.method public final a(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;)Lcom/swedbank/mobile/app/f/a/b;
    .locals 1
    .param p1    # Lcom/swedbank/mobile/business/general/confirmation/bottom/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a/b;

    .line 16
    iput-object p1, v0, Lcom/swedbank/mobile/app/f/a/b;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    return-object v0
.end method

.method public a()Lcom/swedbank/mobile/architect/a/h;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/a/b;->c:Lcom/swedbank/mobile/a/g/a/a$a;

    .line 24
    iget-object v1, p0, Lcom/swedbank/mobile/app/f/a/b;->a:Lcom/swedbank/mobile/business/general/confirmation/bottom/c;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/g/a/a$a;->b(Lcom/swedbank/mobile/business/general/confirmation/bottom/c;)Lcom/swedbank/mobile/a/g/a/a$a;

    move-result-object v0

    .line 27
    iget-object v1, p0, Lcom/swedbank/mobile/app/f/a/b;->b:Lcom/swedbank/mobile/app/f/a/d;

    if-eqz v1, :cond_0

    invoke-interface {v0, v1}, Lcom/swedbank/mobile/a/g/a/a$a;->b(Lcom/swedbank/mobile/app/f/a/d;)Lcom/swedbank/mobile/a/g/a/a$a;

    move-result-object v0

    .line 30
    invoke-interface {v0}, Lcom/swedbank/mobile/a/g/a/a$a;->a()Lcom/swedbank/mobile/a/g/a/a;

    move-result-object v0

    .line 31
    invoke-interface {v0}, Lcom/swedbank/mobile/a/g/a/a;->a()Lcom/swedbank/mobile/architect/a/h;

    move-result-object v0

    return-object v0

    .line 27
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display bottom sheet dialog without knowing how to render it"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 24
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot display bottom sheet dialog without parent node"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
