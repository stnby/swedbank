.class public final Lcom/swedbank/mobile/app/f/a/k;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "BottomSheetDialogViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/f/a/j;
.implements Lcom/swedbank/mobile/core/ui/i;


# instance fields
.field private a:Landroid/app/Dialog;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private b:Lio/reactivex/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final c:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/swedbank/mobile/core/ui/widget/k;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    .line 31
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object v0

    const-string v1, "Disposables.disposed()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->b:Lio/reactivex/b/c;

    .line 32
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<BottomSheetActionKey>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->c:Lcom/b/c/c;

    .line 33
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object v0

    const-string v1, "PublishRelay.create<Unit>()"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->d:Lcom/b/c/c;

    .line 34
    new-instance v0, Lcom/swedbank/mobile/core/ui/widget/k;

    .line 36
    sget v1, Lcom/swedbank/mobile/core/a$e;->item_bottom_sheet_action:I

    .line 37
    new-instance v2, Lcom/swedbank/mobile/app/f/a/k$a;

    invoke-direct {v2, p0}, Lcom/swedbank/mobile/app/f/a/k$a;-><init>(Lcom/swedbank/mobile/app/f/a/k;)V

    check-cast v2, Lkotlin/e/a/m;

    .line 35
    invoke-static {v1, v2}, Lcom/swedbank/mobile/core/ui/widget/l;->b(ILkotlin/e/a/m;)Landroid/util/SparseArray;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 34
    invoke-direct {v0, v2, v1, v3, v2}, Lcom/swedbank/mobile/core/ui/widget/k;-><init>(Ljava/util/List;Landroid/util/SparseArray;ILkotlin/e/b/g;)V

    iput-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->e:Lcom/swedbank/mobile/core/ui/widget/k;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/f/a/k;)Landroid/content/Context;
    .locals 0

    .line 28
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/f/a/k;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/f/a/k;)Lcom/b/c/c;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/f/a/k;->d:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/f/a/k;)Lcom/swedbank/mobile/core/ui/widget/k;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/f/a/k;->e:Lcom/swedbank/mobile/core/ui/widget/k;

    return-object p0
.end method

.method public static final synthetic d(Lcom/swedbank/mobile/app/f/a/k;)Lcom/b/c/c;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/swedbank/mobile/app/f/a/k;->c:Lcom/b/c/c;

    return-object p0
.end method


# virtual methods
.method public a()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->c:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 30
    iput-object p1, p0, Lcom/swedbank/mobile/app/f/a/k;->a:Landroid/app/Dialog;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/f/a/m;)V
    .locals 11
    .param p1    # Lcom/swedbank/mobile/app/f/a/m;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/f/a/k;->c()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_7

    .line 53
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/a/m;->a()Lcom/swedbank/mobile/app/f/a/d;

    move-result-object p1

    if-eqz p1, :cond_7

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/a/k;

    .line 109
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/k;->a(Lcom/swedbank/mobile/app/f/a/k;)Landroid/content/Context;

    move-result-object v1

    .line 110
    new-instance v2, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-direct {v2, v1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 119
    sget v3, Lcom/swedbank/mobile/core/a$e;->view_bottom_sheet_dialog:I

    const/4 v4, 0x0

    .line 120
    move-object v5, v4

    check-cast v5, Landroid/view/ViewGroup;

    .line 123
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const-string v7, "LayoutInflater.from(this)"

    invoke-static {v6, v7}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    .line 122
    invoke-virtual {v6, v3, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_6

    check-cast v3, Landroid/view/ViewGroup;

    .line 133
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 135
    instance-of v6, p1, Lcom/swedbank/mobile/app/f/a/d$b;

    if-eqz v6, :cond_1

    .line 136
    move-object v6, p1

    check-cast v6, Lcom/swedbank/mobile/app/f/a/d$b;

    invoke-virtual {v6}, Lcom/swedbank/mobile/app/f/a/d$b;->a()I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "resources.getString(information.titleResId)"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Ljava/lang/CharSequence;

    .line 137
    move-object v9, v3

    check-cast v9, Landroid/view/View;

    sget v10, Lcom/swedbank/mobile/core/a$d;->bottom_sheet_title:I

    .line 138
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_0

    check-cast v10, Landroid/widget/TextView;

    .line 139
    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    :cond_0
    invoke-virtual {v6}, Lcom/swedbank/mobile/app/f/a/d$b;->b()Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_3

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    .line 144
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    if-eqz v5, :cond_3

    .line 146
    sget v6, Lcom/swedbank/mobile/core/a$d;->bottom_sheet_subtitle:I

    .line 147
    invoke-virtual {v9, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    check-cast v6, Landroid/widget/TextView;

    .line 148
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 155
    :cond_1
    instance-of v6, p1, Lcom/swedbank/mobile/app/f/a/d$a;

    if-eqz v6, :cond_3

    .line 156
    move-object v6, p1

    check-cast v6, Lcom/swedbank/mobile/app/f/a/d$a;

    const-string v8, "resources"

    invoke-static {v5, v8}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Lcom/swedbank/mobile/app/f/a/d$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v8

    .line 157
    move-object v9, v3

    check-cast v9, Landroid/view/View;

    sget v10, Lcom/swedbank/mobile/core/a$d;->bottom_sheet_title:I

    .line 158
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_2

    check-cast v10, Landroid/widget/TextView;

    .line 159
    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :cond_2
    invoke-virtual {v6, v5}, Lcom/swedbank/mobile/app/f/a/d$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_3

    if-eqz v5, :cond_3

    .line 166
    sget v6, Lcom/swedbank/mobile/core/a$d;->bottom_sheet_subtitle:I

    .line 167
    invoke-virtual {v9, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    check-cast v6, Landroid/widget/TextView;

    .line 168
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    :cond_3
    :goto_0
    check-cast v3, Landroid/view/View;

    sget v5, Lcom/swedbank/mobile/core/a$d;->bottom_sheet_action_list:I

    .line 177
    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_5

    check-cast v5, Landroidx/recyclerview/widget/RecyclerView;

    .line 178
    new-instance v6, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v5}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v6, v8}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v6, Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {v5, v6}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$i;)V

    .line 179
    new-instance v6, Lcom/swedbank/mobile/core/ui/widget/e;

    invoke-virtual {v5}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "context"

    invoke-static {v8, v9}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x2

    invoke-direct {v6, v8, v7, v9, v4}, Lcom/swedbank/mobile/core/ui/widget/e;-><init>(Landroid/content/Context;IILkotlin/e/b/g;)V

    check-cast v6, Landroidx/recyclerview/widget/RecyclerView$h;

    invoke-virtual {v5, v6}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$h;)V

    .line 180
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/k;->c(Lcom/swedbank/mobile/app/f/a/k;)Lcom/swedbank/mobile/core/ui/widget/k;

    move-result-object v6

    check-cast v6, Landroidx/recyclerview/widget/RecyclerView$a;

    invoke-virtual {v5, v6}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$a;)V

    .line 181
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/a/k;->c(Lcom/swedbank/mobile/app/f/a/k;)Lcom/swedbank/mobile/core/ui/widget/k;

    move-result-object v5

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/a/d;->c()Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    .line 182
    new-instance v7, Ljava/util/ArrayList;

    const/16 v8, 0xa

    invoke-static {v6, v8}, Lkotlin/a/h;->a(Ljava/lang/Iterable;I)I

    move-result v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 183
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 181
    invoke-static {v8}, Lcom/swedbank/mobile/core/ui/widget/l;->a(Ljava/lang/Object;)Lcom/swedbank/mobile/core/ui/widget/i;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 185
    :cond_4
    check-cast v7, Ljava/util/List;

    .line 181
    invoke-static {v5, v7, v4, v9, v4}, Lcom/swedbank/mobile/core/ui/widget/k;->a(Lcom/swedbank/mobile/core/ui/widget/k;Ljava/util/List;Landroidx/recyclerview/widget/f$b;ILjava/lang/Object;)V

    .line 189
    :cond_5
    invoke-virtual {v2, v3}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 190
    new-instance v3, Lcom/swedbank/mobile/app/f/a/k$b;

    invoke-direct {v3, v0, v1, p1}, Lcom/swedbank/mobile/app/f/a/k$b;-><init>(Lcom/swedbank/mobile/app/f/a/k;Landroid/content/Context;Lcom/swedbank/mobile/app/f/a/d;)V

    check-cast v3, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v2, v3}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 118
    check-cast v2, Landroid/app/Dialog;

    .line 117
    invoke-virtual {v0, v2}, Lcom/swedbank/mobile/app/f/a/k;->b(Landroid/app/Dialog;)Landroid/app/Dialog;

    goto :goto_2

    .line 122
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_2
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Lcom/swedbank/mobile/app/f/a/k;->b:Lio/reactivex/b/c;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/swedbank/mobile/app/f/a/m;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/f/a/k;->a(Lcom/swedbank/mobile/app/f/a/m;)V

    return-void
.end method

.method public b(Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 1
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/i$a;->a(Lcom/swedbank/mobile/core/ui/i;Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method public b()Lio/reactivex/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->d:Lcom/b/c/c;

    check-cast v0, Lio/reactivex/o;

    return-object v0
.end method

.method public c()Landroid/app/Dialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->a:Landroid/app/Dialog;

    return-object v0
.end method

.method public f()Lio/reactivex/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/a/k;->b:Lio/reactivex/b/c;

    return-object v0
.end method
