.class public final Lcom/swedbank/mobile/app/f/j;
.super Lcom/swedbank/mobile/architect/a/b/a;
.source "ConfirmationDialogViewImpl.kt"

# interfaces
.implements Lcom/swedbank/mobile/app/f/i;
.implements Lcom/swedbank/mobile/core/ui/i;


# instance fields
.field private final a:Lcom/b/c/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/b/c/c<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/app/Dialog;
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation
.end field

.field private c:Lio/reactivex/b/c;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z
        .annotation runtime Ljavax/inject/Named;
            value = "confirmation_dialog_cancelable"
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Lcom/swedbank/mobile/architect/a/b/a;-><init>()V

    iput-boolean p1, p0, Lcom/swedbank/mobile/app/f/j;->d:Z

    .line 22
    invoke-static {}, Lcom/b/c/c;->a()Lcom/b/c/c;

    move-result-object p1

    const-string v0, "PublishRelay.create<Int>()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/j;->a:Lcom/b/c/c;

    .line 24
    invoke-static {}, Lio/reactivex/b/d;->b()Lio/reactivex/b/c;

    move-result-object p1

    const-string v0, "Disposables.disposed()"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/f/j;->c:Lio/reactivex/b/c;

    return-void
.end method

.method public static final synthetic a(Lcom/swedbank/mobile/app/f/j;)Landroid/content/Context;
    .locals 0

    .line 19
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/f/j;->n()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lcom/swedbank/mobile/app/f/j;)Lcom/b/c/c;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/swedbank/mobile/app/f/j;->a:Lcom/b/c/c;

    return-object p0
.end method

.method public static final synthetic c(Lcom/swedbank/mobile/app/f/j;)Z
    .locals 0

    .line 19
    iget-boolean p0, p0, Lcom/swedbank/mobile/app/f/j;->d:Z

    return p0
.end method


# virtual methods
.method public a(Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;
    .locals 1
    .param p1    # Landroidx/appcompat/app/b$a;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$showWithinViewLifecycle"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-static {p0, p1}, Lcom/swedbank/mobile/core/ui/i$a;->a(Lcom/swedbank/mobile/core/ui/i;Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;

    move-result-object p1

    return-object p1
.end method

.method public a()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/j;->a:Lcom/b/c/c;

    .line 27
    sget-object v1, Lcom/swedbank/mobile/app/f/j$a;->a:Lcom/swedbank/mobile/app/f/j$a;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/swedbank/mobile/app/f/j$b;->a:Lcom/swedbank/mobile/app/f/j$b;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "buttonsStream\n      .fil\u2026IVE }\n      .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/app/Dialog;)V
    .locals 0
    .param p1    # Landroid/app/Dialog;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 23
    iput-object p1, p0, Lcom/swedbank/mobile/app/f/j;->b:Landroid/app/Dialog;

    return-void
.end method

.method public a(Lcom/swedbank/mobile/app/f/l;)V
    .locals 6
    .param p1    # Lcom/swedbank/mobile/app/f/l;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "viewState"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/f/j;->c()Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_7

    .line 36
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/l;->a()Lcom/swedbank/mobile/app/f/c;

    move-result-object p1

    if-eqz p1, :cond_7

    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/f/j;

    .line 78
    new-instance v1, Landroidx/appcompat/app/b$a;

    invoke-static {v0}, Lcom/swedbank/mobile/app/f/j;->a(Lcom/swedbank/mobile/app/f/j;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/appcompat/app/b$a;-><init>(Landroid/content/Context;)V

    .line 89
    instance-of v2, p1, Lcom/swedbank/mobile/app/f/c$b;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    check-cast p1, Lcom/swedbank/mobile/app/f/c$b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/c$b;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/appcompat/app/b$a;->a(I)Landroidx/appcompat/app/b$a;

    move-result-object v2

    .line 92
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/c$b;->c()I

    move-result v4

    invoke-virtual {v2, v4, v3}, Landroidx/appcompat/app/b$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/b$a;

    move-result-object v2

    .line 94
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/c$b;->b()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_0

    check-cast v4, Ljava/lang/Number;

    invoke-virtual {v4}, Ljava/lang/Number;->intValue()I

    move-result v4

    invoke-virtual {v2, v4}, Landroidx/appcompat/app/b$a;->b(I)Landroidx/appcompat/app/b$a;

    .line 95
    :cond_0
    invoke-virtual {p1}, Lcom/swedbank/mobile/app/f/c$b;->d()Ljava/lang/Integer;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {v1, p1, v3}, Landroidx/appcompat/app/b$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/b$a;

    :cond_1
    const-string p1, "setTitle(information.tit\u2026it, null) }\n            }"

    .line 91
    invoke-static {v2, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :cond_2
    instance-of v2, p1, Lcom/swedbank/mobile/app/f/c$a;

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Landroidx/appcompat/app/b$a;->a()Landroid/content/Context;

    move-result-object v2

    const-string v4, "context"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 98
    check-cast p1, Lcom/swedbank/mobile/app/f/c$a;

    const-string v4, "resources"

    invoke-static {v2, v4}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/app/f/c$a;->a(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroidx/appcompat/app/b$a;->a(Ljava/lang/CharSequence;)Landroidx/appcompat/app/b$a;

    move-result-object v4

    .line 101
    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/app/f/c$a;->c(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Landroidx/appcompat/app/b$a;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/b$a;

    move-result-object v4

    .line 103
    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/app/f/c$a;->b(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4, v5}, Landroidx/appcompat/app/b$a;->b(Ljava/lang/CharSequence;)Landroidx/appcompat/app/b$a;

    .line 104
    :cond_3
    invoke-virtual {p1, v2}, Lcom/swedbank/mobile/app/f/c$a;->d(Landroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {v1, p1, v3}, Landroidx/appcompat/app/b$a;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/b$a;

    :cond_4
    const-string p1, "context.resources.let { \u2026              }\n        }"

    .line 97
    invoke-static {v4, p1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v4

    .line 86
    :goto_0
    new-instance p1, Lcom/swedbank/mobile/app/f/j$g;

    invoke-direct {p1, v0}, Lcom/swedbank/mobile/app/f/j$g;-><init>(Lcom/swedbank/mobile/app/f/j;)V

    check-cast p1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v2, p1}, Landroidx/appcompat/app/b$a;->a(Landroid/content/DialogInterface$OnCancelListener;)Landroidx/appcompat/app/b$a;

    move-result-object p1

    .line 85
    invoke-static {v0}, Lcom/swedbank/mobile/app/f/j;->c(Lcom/swedbank/mobile/app/f/j;)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroidx/appcompat/app/b$a;->a(Z)Landroidx/appcompat/app/b$a;

    move-result-object p1

    const-string v1, "AlertDialog.Builder(cont\u2026setCancelable(cancelable)"

    invoke-static {p1, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v0, p1}, Lcom/swedbank/mobile/app/f/j;->a(Landroidx/appcompat/app/b$a;)Landroidx/appcompat/app/b;

    move-result-object p1

    const/4 v1, -0x1

    .line 108
    invoke-virtual {p1, v1}, Landroidx/appcompat/app/b;->a(I)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_5

    check-cast v1, Landroid/view/View;

    .line 110
    new-instance v2, Lcom/swedbank/mobile/app/f/j$e;

    invoke-direct {v2, v0}, Lcom/swedbank/mobile/app/f/j$e;-><init>(Lcom/swedbank/mobile/app/f/j;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const/4 v1, -0x2

    .line 114
    invoke-virtual {p1, v1}, Landroidx/appcompat/app/b;->a(I)Landroid/widget/Button;

    move-result-object p1

    if-eqz p1, :cond_7

    check-cast p1, Landroid/view/View;

    .line 116
    new-instance v1, Lcom/swedbank/mobile/app/f/j$f;

    invoke-direct {v1, v0}, Lcom/swedbank/mobile/app/f/j$f;-><init>(Lcom/swedbank/mobile/app/f/j;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 97
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_7
    :goto_1
    return-void
.end method

.method public a(Lio/reactivex/b/c;)V
    .locals 1
    .param p1    # Lio/reactivex/b/c;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/swedbank/mobile/app/f/j;->c:Lio/reactivex/b/c;

    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/swedbank/mobile/app/f/l;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/f/j;->a(Lcom/swedbank/mobile/app/f/l;)V

    return-void
.end method

.method public b()Lio/reactivex/o;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/o<",
            "Lkotlin/s;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/j;->a:Lcom/b/c/c;

    .line 31
    sget-object v1, Lcom/swedbank/mobile/app/f/j$c;->a:Lcom/swedbank/mobile/app/f/j$c;

    check-cast v1, Lio/reactivex/c/k;

    invoke-virtual {v0, v1}, Lcom/b/c/c;->a(Lio/reactivex/c/k;)Lio/reactivex/o;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/swedbank/mobile/app/f/j$d;->a:Lcom/swedbank/mobile/app/f/j$d;

    check-cast v1, Lio/reactivex/c/h;

    invoke-virtual {v0, v1}, Lio/reactivex/o;->h(Lio/reactivex/c/h;)Lio/reactivex/o;

    move-result-object v0

    const-string v1, "buttonsStream\n      .fil\u2026IVE }\n      .map { Unit }"

    invoke-static {v0, v1}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public c()Landroid/app/Dialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/Nullable;
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/j;->b:Landroid/app/Dialog;

    return-object v0
.end method

.method public f()Lio/reactivex/b/c;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/swedbank/mobile/app/f/j;->c:Lio/reactivex/b/c;

    return-object v0
.end method
