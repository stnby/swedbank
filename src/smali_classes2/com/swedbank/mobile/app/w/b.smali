.class public abstract Lcom/swedbank/mobile/app/w/b;
.super Ljava/lang/Object;
.source "FatalError.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/swedbank/mobile/app/w/b$d;,
        Lcom/swedbank/mobile/app/w/b$a;,
        Lcom/swedbank/mobile/app/w/b$b;,
        Lcom/swedbank/mobile/app/w/b$c;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/e/b/g;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/swedbank/mobile/app/w/b;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/Throwable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract b()I
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 58
    move-object v0, p0

    check-cast v0, Lcom/swedbank/mobile/app/w/b;

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    return v1

    .line 59
    :cond_0
    instance-of v0, p1, Lcom/swedbank/mobile/app/w/b;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/w/b;->a()Ljava/lang/Throwable;

    move-result-object v0

    check-cast p1, Lcom/swedbank/mobile/app/w/b;

    invoke-virtual {p1}, Lcom/swedbank/mobile/app/w/b;->a()Ljava/lang/Throwable;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    return v2

    .line 63
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/swedbank/mobile/app/w/b;->a()Ljava/lang/Throwable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->hashCode()I

    move-result v0

    return v0
.end method
