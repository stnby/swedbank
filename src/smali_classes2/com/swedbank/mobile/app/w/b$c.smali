.class public final Lcom/swedbank/mobile/app/w/b$c;
.super Lcom/swedbank/mobile/app/w/b;
.source "FatalError.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/w/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# instance fields
.field private final a:Ljava/lang/Throwable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;II)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cause"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/w/b;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/w/b$c;->a:Ljava/lang/Throwable;

    iput p2, p0, Lcom/swedbank/mobile/app/w/b$c;->b:I

    iput p3, p0, Lcom/swedbank/mobile/app/w/b$c;->c:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Throwable;IIILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 39
    sget p3, Lcom/swedbank/mobile/core/a$f;->error_server_error:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/swedbank/mobile/app/w/b$c;-><init>(Ljava/lang/Throwable;II)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Throwable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/swedbank/mobile/app/w/b$c;->a:Ljava/lang/Throwable;

    return-object v0
.end method

.method public b()I
    .locals 1

    .line 39
    iget v0, p0, Lcom/swedbank/mobile/app/w/b$c;->c:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Lorg/jetbrains/annotations/Nullable;
        .end annotation
    .end param

    .line 42
    instance-of v0, p1, Lcom/swedbank/mobile/app/w/b$c;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 43
    :cond_0
    invoke-super {p0, p1}, Lcom/swedbank/mobile/app/w/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    .line 45
    :cond_1
    iget v0, p0, Lcom/swedbank/mobile/app/w/b$c;->b:I

    check-cast p1, Lcom/swedbank/mobile/app/w/b$c;

    iget p1, p1, Lcom/swedbank/mobile/app/w/b$c;->b:I

    if-eq v0, p1, :cond_2

    return v1

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 2

    .line 51
    invoke-super {p0}, Lcom/swedbank/mobile/app/w/b;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 52
    iget v1, p0, Lcom/swedbank/mobile/app/w/b$c;->b:I

    add-int/2addr v0, v1

    return v0
.end method
