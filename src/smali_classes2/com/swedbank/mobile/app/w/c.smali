.class public final Lcom/swedbank/mobile/app/w/c;
.super Ljava/lang/Object;
.source "FatalError.kt"


# direct methods
.method public static final a(Ljava/lang/Throwable;)Lcom/swedbank/mobile/app/w/b;
    .locals 10
    .param p0    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$toFatalError"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    sget-object v0, Lcom/swedbank/mobile/data/network/NoConnectivityException;->a:Lcom/swedbank/mobile/data/network/NoConnectivityException;

    invoke-static {p0, v0}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/swedbank/mobile/app/w/b$a;

    invoke-direct {v0, p0, v3, v2, v1}, Lcom/swedbank/mobile/app/w/b$a;-><init>(Ljava/lang/Throwable;IILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/app/w/b;

    goto :goto_0

    .line 72
    :cond_0
    instance-of v0, p0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/swedbank/mobile/app/w/b$b;

    invoke-direct {v0, p0, v3, v2, v1}, Lcom/swedbank/mobile/app/w/b$b;-><init>(Ljava/lang/Throwable;IILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/app/w/b;

    goto :goto_0

    .line 73
    :cond_1
    instance-of v0, p0, Lretrofit2/HttpException;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/swedbank/mobile/app/w/b$c;

    move-object v1, p0

    check-cast v1, Lretrofit2/HttpException;

    invoke-virtual {v1}, Lretrofit2/HttpException;->a()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, v0

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/swedbank/mobile/app/w/b$c;-><init>(Ljava/lang/Throwable;IIILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/app/w/b;

    goto :goto_0

    .line 74
    :cond_2
    new-instance v0, Lcom/swedbank/mobile/app/w/b$d;

    invoke-direct {v0, p0, v3, v2, v1}, Lcom/swedbank/mobile/app/w/b$d;-><init>(Ljava/lang/Throwable;IILkotlin/e/b/g;)V

    check-cast v0, Lcom/swedbank/mobile/app/w/b;

    :goto_0
    return-object v0
.end method
