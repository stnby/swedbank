.class final Lcom/swedbank/mobile/app/w/a$b;
.super Lkotlin/e/b/k;
.source "ExternalNavigations.kt"

# interfaces
.implements Lkotlin/e/a/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/swedbank/mobile/app/w/a;->a(Lcom/swedbank/mobile/data/device/a;Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/e/b/k;",
        "Lkotlin/e/a/b<",
        "Landroid/app/Activity;",
        "Lkotlin/s;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/swedbank/mobile/app/w/a$b;->a:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/e/b/k;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    check-cast p1, Landroid/content/Context;

    sget v0, Lcom/swedbank/mobile/core/a$b;->white:I

    .line 139
    invoke-static {p1, v0}, Landroidx/core/a/a;->c(Landroid/content/Context;I)I

    move-result v0

    .line 95
    new-instance v1, Landroidx/browser/a/a$a;

    invoke-direct {v1}, Landroidx/browser/a/a$a;-><init>()V

    .line 96
    sget v2, Lcom/swedbank/mobile/core/a$a;->slide_in_bottom:I

    sget v3, Lcom/swedbank/mobile/core/a$a;->no_anim:I

    invoke-virtual {v1, p1, v2, v3}, Landroidx/browser/a/a$a;->a(Landroid/content/Context;II)Landroidx/browser/a/a$a;

    move-result-object v1

    .line 97
    sget v2, Lcom/swedbank/mobile/core/a$a;->no_anim:I

    sget v3, Lcom/swedbank/mobile/core/a$a;->slide_out_bottom:I

    invoke-virtual {v1, p1, v2, v3}, Landroidx/browser/a/a$a;->b(Landroid/content/Context;II)Landroidx/browser/a/a$a;

    move-result-object v1

    .line 98
    invoke-virtual {v1}, Landroidx/browser/a/a$a;->a()Landroidx/browser/a/a$a;

    move-result-object v1

    .line 99
    invoke-virtual {v1, v0}, Landroidx/browser/a/a$a;->a(I)Landroidx/browser/a/a$a;

    move-result-object v1

    .line 100
    invoke-virtual {v1, v0}, Landroidx/browser/a/a$a;->b(I)Landroidx/browser/a/a$a;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroidx/browser/a/a$a;->b()Landroidx/browser/a/a;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/swedbank/mobile/app/w/a$b;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroidx/browser/a/a;->a(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lcom/swedbank/mobile/app/w/a$b;->a(Landroid/app/Activity;)V

    sget-object p1, Lkotlin/s;->a:Lkotlin/s;

    return-object p1
.end method
