.class public final Lcom/swedbank/mobile/app/w/d;
.super Ljava/lang/Object;
.source "MessagesRendering.kt"


# direct methods
.method public static final a(Ljava/util/List;)Lkotlin/k;
    .locals 14
    .param p0    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/swedbank/mobile/business/authentication/k$d;",
            ">;)",
            "Lkotlin/k<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    const-string v0, "$this$gatherLoginMessages"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    check-cast p0, Ljava/lang/Iterable;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 55
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 56
    move-object v3, v2

    check-cast v3, Lcom/swedbank/mobile/business/authentication/k$d;

    .line 43
    instance-of v3, v3, Lcom/swedbank/mobile/business/authentication/k$d$a;

    if-eqz v3, :cond_0

    .line 57
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_1
    new-instance p0, Lkotlin/k;

    invoke-direct {p0, v0, v1}, Lkotlin/k;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 44
    invoke-virtual {p0}, Lkotlin/k;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p0}, Lkotlin/k;->d()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    .line 45
    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    const-string v0, "\n"

    .line 46
    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/swedbank/mobile/app/w/e;->a:Lkotlin/h/i;

    move-object v7, v0

    check-cast v7, Lkotlin/e/a/b;

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 47
    move-object v1, v0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lez v1, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    const/4 v4, 0x0

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    move-object v0, v4

    .line 48
    :goto_2
    move-object v5, p0

    check-cast v5, Ljava/lang/Iterable;

    const-string p0, "\n"

    .line 49
    move-object v6, p0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object p0, Lcom/swedbank/mobile/app/w/f;->a:Lkotlin/h/i;

    move-object v11, p0

    check-cast v11, Lkotlin/e/a/b;

    const/16 v12, 0x1e

    const/4 v13, 0x0

    invoke-static/range {v5 .. v13}, Lkotlin/a/h;->a(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/e/a/b;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 50
    move-object v1, p0

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_4

    const/4 v2, 0x1

    :cond_4
    if-eqz v2, :cond_5

    goto :goto_3

    :cond_5
    move-object p0, v4

    .line 51
    :goto_3
    invoke-static {v0, p0}, Lkotlin/q;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/k;

    move-result-object p0

    return-object p0
.end method

.method public static final a(Lcom/swedbank/mobile/core/ui/widget/u;Landroid/content/Context;Lcom/swedbank/mobile/app/w/h;)V
    .locals 4
    .param p0    # Lcom/swedbank/mobile/core/ui/widget/u;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/swedbank/mobile/app/w/h;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "$this$renderSnackbarMessages"

    invoke-static {p0, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewState"

    invoke-static {p2, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-interface {p2}, Lcom/swedbank/mobile/app/w/h;->d()Ljava/lang/String;

    move-result-object v0

    .line 26
    invoke-interface {p2}, Lcom/swedbank/mobile/app/w/h;->e()Ljava/lang/String;

    move-result-object v1

    .line 27
    invoke-interface {p2}, Lcom/swedbank/mobile/app/w/h;->f()Lcom/swedbank/mobile/app/w/b;

    move-result-object p2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 30
    invoke-static {v1}, Lcom/swedbank/mobile/core/ui/ap;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 31
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-direct {p2, v3, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast p2, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 29
    invoke-interface {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    .line 33
    invoke-virtual {p2}, Lcom/swedbank/mobile/app/w/b;->b()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "context.getString(fatalError.userDisplayedMessage)"

    invoke-static {p1, p2}, Lkotlin/e/b/j;->a(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    .line 34
    new-instance p2, Lcom/swedbank/mobile/core/ui/widget/s$c$a;

    invoke-direct {p2, v3, v2, v3}, Lcom/swedbank/mobile/core/ui/widget/s$c$a;-><init>(Ljava/lang/CharSequence;ILkotlin/e/b/g;)V

    check-cast p2, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 32
    invoke-interface {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 36
    invoke-static {v0}, Lcom/swedbank/mobile/core/ui/ap;->a(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 37
    sget-object p2, Lcom/swedbank/mobile/core/ui/widget/s$c$d;->a:Lcom/swedbank/mobile/core/ui/widget/s$c$d;

    check-cast p2, Lcom/swedbank/mobile/core/ui/widget/s$c;

    .line 35
    invoke-interface {p0, p1, p2}, Lcom/swedbank/mobile/core/ui/widget/u;->a(Ljava/lang/CharSequence;Lcom/swedbank/mobile/core/ui/widget/s$c;)V

    goto :goto_0

    .line 38
    :cond_2
    invoke-interface {p0}, Lcom/swedbank/mobile/core/ui/widget/u;->d()Lcom/swedbank/mobile/core/ui/widget/t;

    move-result-object p0

    invoke-virtual {p0}, Lcom/swedbank/mobile/core/ui/widget/t;->b()V

    :goto_0
    return-void
.end method
