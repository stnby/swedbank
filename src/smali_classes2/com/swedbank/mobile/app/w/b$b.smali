.class public final Lcom/swedbank/mobile/app/w/b$b;
.super Lcom/swedbank/mobile/app/w/b;
.source "FatalError.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/swedbank/mobile/app/w/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/lang/Throwable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;I)V
    .locals 1
    .param p1    # Ljava/lang/Throwable;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "cause"

    invoke-static {p1, v0}, Lkotlin/e/b/j;->b(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, v0}, Lcom/swedbank/mobile/app/w/b;-><init>(Lkotlin/e/b/g;)V

    iput-object p1, p0, Lcom/swedbank/mobile/app/w/b$b;->a:Ljava/lang/Throwable;

    iput p2, p0, Lcom/swedbank/mobile/app/w/b$b;->b:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Throwable;IILkotlin/e/b/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 30
    sget p2, Lcom/swedbank/mobile/core/a$f;->error_server_maintenance:I

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/swedbank/mobile/app/w/b$b;-><init>(Ljava/lang/Throwable;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Throwable;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/swedbank/mobile/app/w/b$b;->a:Ljava/lang/Throwable;

    return-object v0
.end method

.method public b()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/swedbank/mobile/app/w/b$b;->b:I

    return v0
.end method
